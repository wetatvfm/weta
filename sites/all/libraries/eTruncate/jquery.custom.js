 jQuery(function($){

  // Calendar Event Functions
  $(".calitems").append($('<div class="clear"></div><div class="episodes-bottom  moredates p10"><p></p></div>'));
  $(".calitems").prepend($('<div class="clear"></div><div class="episodes-bottom previous p10"><p></p></div>'));

  // initializes eTruncate to truncate all the elements with the class "more"
  // within the div.caldates
  // it also places the button inside the element with the id
  // "buttonContainer" (created dynamically before) and changes the default
  // button labels
  $(".caldates").eTruncate({
    buttonContainer: ".calitems .moredates p",
    showText: "See Additional Event Dates",
    hideText: "See Fewer Event Dates"
  });

  $(".prevdates").eTruncate({
    buttonContainer: ".calitems .previous p",
    showText: "See Previous Event Dates",
    hideText: "Hide Previous Event Dates",
    elements: ".prevdate"
  });
});
