<?php

/**
 * @file
 * Provides an EntityAPIController for our weta_pbs_media_manager show entities.
 */

/**
 * Extending the EntityAPIController for the pbs_media_managerShow entity.
 */
class PbsMediaManagerShowEntityController extends EntityAPIController {

  /**
   * Build the content.
   *
   * @inheritdoc
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $build = parent::buildContent($entity, $view_mode, $langcode, $content);

    return $build;
  }

  /**
   * Load the entity.
   *
   * @inheritdoc
   */
  public function load($ids = array(), $conditions = array()) {
    $entities = parent::load($ids, $conditions);

    // Unserialize the image field.
    if (is_array($entities)) {
      foreach ($entities as $show) {
        if (!is_array($show->images)) {
          $images = unserialize($show->images);
          $show->images = $images;
        }
      }
    }

    return $entities;
  }

  /**
   * Save the entity.
   *
   * @inheritdoc
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    if (is_array($entity->images)) {
      $entity->images = serialize($entity->images);
    }

    return parent::save($entity, $transaction);
  }

}

/**
 * Project entity class extending the Entity class.
 */
class PbsMediaManagerShowEntity extends Entity {

  /**
   * Change the default URI from default/id to pbs_show/id.
   */
  protected function defaultUri() {
    return array('path' => 'pbs_show/' . $this->identifier());
  }

  /**
   * Get the full detail record for this show.
   */
  public function getFullDetail() {
    return $this->makeRequest();
  }

  /**
   * Uses the Media Manager API to pull a list of seasons for this Show.
   */
  public function getSeasons() {
    return $this->makeRequest('season');
  }

  /**
   * Uses the Media Manager API to pull a list of assets for this Show.
   */
  public function getAssets() {
    return $this->makeRequest('asset');
  }

  /**
   * Uses the Media Manager API to pull a list of specials for this Show.
   */
  public function getSpecials() {
    return $this->makeRequest('special');
  }

  /**
   * Uses the Media Manager API to pull a list of collections for this Show.
   */
  public function getCollections() {
    return $this->makeRequest('collection');
  }

  /**
   * Uses the Media Manager API to pull a list of taxonomies for this Show.
   */
  public function getTaxonomies() {
    return $this->makeRequest('taxonomies');
  }

  /**
   * Uses the Media Manager API to build list of episodes for the Show.
   *
   * @param int $count
   *   The number of episodes to retrieve.
   * @param bool $include_specials
   *   Include specials in the list of returned episodes.
   * @param bool $require_players
   *   Only include results that are available to be viewed.
   * @param bool $include_clips
   *   Include clips if there are not enough episodes retrieved.
   * @param bool $include_previews
   *   Include previews if there aer not enough episodes retrieved.
   *
   * @return array
   *   Returns an array of assets, giving preference to full length episodes.
   */
  public function getLatestEpisodes($count, $include_specials = TRUE, $require_players = TRUE, $include_clips = FALSE, $include_previews = FALSE, $language = NULL) {
    // For performance reasons, limit the number of items queried. But still
    // request enough items to allow for some to be filtered out.  The max
    // allowed by the API is 50.
    $max = 50;
    if ($count * 2 < 50) {
      $max = $count * 2;
    }
    // Set up the default query arguments.
    $queryargs = array(
      'show-id' => $this->pbs_media_manager_show_id,
      'type' => 'full_length',
      'platform-slug' => 'partnerplayer',
      'sort' => '-premiered_on',
      'page-size' => $max,
      'page' => 1,
    );

    $episodes = pbs_media_manager_request('asset', NULL, NULL, NULL, $queryargs);

    // Episode processing only needs to happen if there are results.
    // The 'data' index only exists in empty results.
    // We can't trust the premiered_on sort for episodes that premiered on
    // the same day.
    if (!array_key_exists('data', $episodes)) {
      usort($episodes, function ($a, $b) {
        if (isset($b['attributes']) && isset($a['attributes'])) {
          // If the episodes premiered on the same day...
          if ($b['attributes']['premiered_on'] == $a['attributes']['premiered_on']) {
            // And if there's an episode ordinal for both episodes...
            if (isset($a['attributes']['episode']['attributes']['ordinal']) &&  isset($b['attributes']['episode']['attributes']['ordinal'])) {
              // Sort by the ordinal.
              return $b['attributes']['episode']['attributes']['ordinal'] - $a['attributes']['episode']['attributes']['ordinal'];
            }
          }
          // Otherwise, just sort by premiered_on.
          return strcmp($b['attributes']['premiered_on'], $a['attributes']['premiered_on']);
        }
        else {
          return false;
        }
      });

      // Filter out unwanted items, such as specials or items without players.
      if ($require_players || !($include_specials)) {

        $filtered_episodes = array();

        foreach ($episodes as $episode) {
          $pass = TRUE;
          if ($require_players) {
            if (!($this->checkAvailability($episode))) {
              $pass = FALSE;
            }
          }
          if (!$include_specials) {
            if ($episode['attributes']['episode']['type'] == 'special') {
              $pass = FALSE;
            }
          }
          if ($language) {
            if ($episode['attributes']['language'] != $language) {
              $pass = FALSE;
            }
          }

          // If all conditions are met, add to the results.
          if ($pass) {
            $filtered_episodes[] = $episode;
          }

          // If we've met the number of items requested, we're done and can
          // return the results.
          if (count($filtered_episodes) >= $count) {
            // Add a flag for whether an asset is behind the Passport paywall.
            foreach ($filtered_episodes as &$filtered_episode) {
              $filtered_episode['attributes']['is_passport'] = $this->checkPassport($filtered_episode);
            }
            return $filtered_episodes;
          }
        }

        $episodes = $filtered_episodes;
      }

      // If we've met the number of items requested, we're done and can
      // return the results.
      if (count($episodes) >= $count) {
        // Limit the results to the number requested.
        $episodes = array_slice($episodes, 0, $count);

        // Add a flag for whether an asset is behind the Passport paywall.
        foreach ($episodes as &$episode) {
          $episode['attributes']['is_passport'] = $this->checkPassport($episode);
        }

        return $episodes;
      }
    }
    // If there are no valid results, reset the array to avoid counting errors.
    else {
      $episodes = array();
    }

    // If there aren't enough results and $include_clips and $include_previews
    // are TRUE, we'll need to do a second call to fill out the results.
    if (($include_clips) || ($include_previews)) {
      // How many more items are needed?
      $needed = $count - count($episodes);

      if ($include_clips) {
        $queryargs['type'] = 'clip';
      }
      if ($include_previews) {
        $queryargs['type'] = 'preview';
      }
      if ($include_clips && $include_previews) {
        $queryargs['type'] = 'clip&type=preview';
      }

      $more_episodes = pbs_media_manager_request('asset', NULL, NULL, NULL, $queryargs);

      // Filter out unwanted items, such as those without players.
      if ($require_players) {

        $filtered_moreeps = array();

        foreach ($more_episodes as $episode) {
          // If the asset is available, add to the results.
          if ($this->checkAvailability($episode)) {
            $filtered_moreeps[] = $episode;
          }

          // If we've met the number of items requested, we're done and can
          // return the results.
          if (count($filtered_moreeps) >= $needed) {
            $episodes = array_merge($episodes, $filtered_moreeps);

            // Add a flag for whether an asset is behind the Passport paywall.
            foreach ($episodes as &$thisepisode) {
              $thisepisode['attributes']['is_passport'] = $this->checkPassport($thisepisode);
            }
            return $episodes;
          }
        }
        $more_episodes = $filtered_moreeps;
      }

      $episodes = array_merge($episodes, $more_episodes);

      // Add a flag for whether an asset is behind the Passport paywall.
      foreach ($episodes as &$episode) {
        $episode['attributes']['is_passport'] = $this->checkPassport($episode);
      }
    }

    // Finally, whatever we have is good enough.
    return $episodes;
  }

  /**
   * Makes the actual request of the API.
   *
   * @param string $resource
   *   The primary resource to lookup. Defaults to the show itself.
   *
   * @return bool|object
   *   Returns the response object or False.
   */
  private function makeRequest($resource = 'show') {
    module_load_include('module', 'pbs_media_manager');

    if ($resource == 'show') {
      return pbs_media_manager_request('show', $this->pbs_media_manager_show_id);
    }

    return pbs_media_manager_request($resource, $this->pbs_media_manager_show_id, 'show');
  }

  /**
   * Check to see if an asset is available to be viewed.
   *
   * @param array $asset
   *   The asset array as returned from the API.
   *
   * @return bool
   *   Returns TRUE if available.
   */
  private function checkAvailability(array $asset) {
    if (!isset($asset['attributes']['availabilities'])) {
      return FALSE;
    }
    else {
      // Get the current date/time.
      $now = time();

      // Convert the station members availabilities to timestamps.
      $station_members_start = strtotime($asset['attributes']['availabilities']['station_members']['start']);
      $station_members_end = strtotime($asset['attributes']['availabilities']['station_members']['end']);

      // If now is within the station members availability window, then the
      // asset is available.
      $return = FALSE;
      if ($now >= $station_members_start && ($now <= $station_members_end ||
          empty($station_members_end))) {
        $return = TRUE;
      }
      return $return;
    }
  }

  /**
   * Check to see if an asset is behind the Passport paywall.
   *
   * @param array $asset
   *   The asset array as returned from the API.
   *
   * @return bool
   *   Returns TRUE if behind the Passport paywall.
   */
  private function checkPassport(array $asset) {
    if (!isset($asset['attributes']['availabilities'])) {
      return FALSE;
    }
    else {
      // Get the current date/time.
      $now = time();

      // Convert the member availabilities to timestamps.
      $public_start = strtotime($asset['attributes']['availabilities']['public']['start']);
      $public_end = strtotime($asset['attributes']['availabilities']['public']['end']);
      $all_members_start = strtotime($asset['attributes']['availabilities']['all_members']['start']);
      $all_members_end = strtotime($asset['attributes']['availabilities']['all_members']['end']);
      $station_members_start = strtotime($asset['attributes']['availabilities']['station_members']['start']);
      $station_members_end = strtotime($asset['attributes']['availabilities']['station_members']['end']);

      // Start broad and narrow it down.
      $return = FALSE;
      // Check to see if we're within the public window.
      if ($now >= $public_start && $now <= $public_end) {
        // This asset is available to all, so is not in Passport.
        $return = FALSE;
      }
      // Check to see if we're within the all members window.
      elseif ($now >= $all_members_start && $now <= $all_members_end) {
        // This asset is available to all members, so is in Passport.
        $return = TRUE;
      }
      // Check to see if we're within the station members window.
      elseif ($now >= $station_members_start && $now <= $station_members_end) {
        $return = TRUE;
      }
      return $return;
    }
  }

}
