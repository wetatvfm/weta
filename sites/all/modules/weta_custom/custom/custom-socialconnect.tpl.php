<div class="list_box red_gray_list connect_social mtop_40">
  <h2>Connect with WETA</h2>
  <div class="insides">
    <ul class="sm_icons">
      <li class="span_6 col"><a href="http://www.facebook.com/wetatvfm"><img src="<?php print base_path() . drupal_get_path('theme', $GLOBALS['theme']);?>/templates/images/Facebook.jpg" alt="Facebook" /></a></li>
      <li class="span_6 col"><a href="http://www.youtube.com/wetatv26"><img src="<?php print base_path() . drupal_get_path('theme', $GLOBALS['theme']);?>/templates/images/you_tube.jpg" alt="YouTube" /></a></li>
    </ul>
      <ul class="sm_icons">
      <li class="span_6 col"><a href="http://twitter.com/wetatvfm"><img src="<?php print base_path() . drupal_get_path('theme', $GLOBALS['theme']);?>/templates/images/twitter.jpg" alt="Twitter" /></a></li>
      <li class="span_6 col"><a href="http://pinterest.com/wetatvfm/"><img src="<?php print base_path() . drupal_get_path('theme', $GLOBALS['theme']);?>/templates/images/pinterest.jpg" alt="Pinterest" /></a></li>
    </ul>
    <div class="clearfix"></div>
  </div>
</div>