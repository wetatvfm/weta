<?php
/**
 * @file
 * carousel.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function carousel_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|carousel|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'carousel';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_slides',
      ),
    ),
    'fields' => array(
      'field_slides' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 0,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|carousel|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|carousel|homepage_carousel';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'carousel';
  $ds_layout->view_mode = 'homepage_carousel';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_slides',
      ),
    ),
    'fields' => array(
      'field_slides' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => TRUE,
    'layout_link_attribute' => FALSE,
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|carousel|homepage_carousel'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|carousel|landing_carousel';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'carousel';
  $ds_layout->view_mode = 'landing_carousel';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_slides',
      ),
    ),
    'fields' => array(
      'field_slides' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => FALSE,
  );
  $export['node|carousel|landing_carousel'] = $ds_layout;

  return $export;
}
