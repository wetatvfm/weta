<?php
/**
 * @file
 * carousel.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function carousel_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_display_date'.
  $field_bases['field_display_date'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_display_date',
    'field_permissions' => array(
      'type' => 1,
    ),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'date',
    'settings' => array(
      'cache_count' => 1,
      'cache_enabled' => 1,
      'granularity' => array(
        'day' => 'day',
        'hour' => 0,
        'minute' => 0,
        'month' => 'month',
        'second' => 0,
        'year' => 'year',
      ),
      'repeat' => 0,
      'timezone_db' => '',
      'todate' => '',
      'tz_handling' => 'none',
    ),
    'translatable' => 0,
    'type' => 'datetime',
  );

  // Exported field_base: 'field_slides'.
  $field_bases['field_slides'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_slides',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'nid' => array(
        0 => 'nid',
      ),
    ),
    'locked' => 0,
    'module' => 'node_reference',
    'settings' => array(
      'referenceable_types' => array(
        'audio_file' => 0,
        'basic_page' => 0,
        'blog_post' => 0,
        'board_meetings' => 0,
        'bracket_match' => 0,
        'bracket_team' => 0,
        'calendar_event' => 0,
        'carousel' => 0,
        'cove_video' => 0,
        'feature_block' => 'feature_block',
        'featured_links' => 0,
        'flyout_links' => 0,
        'fm_cd_pick' => 0,
        'fm_channels' => 0,
        'fm_classical_conversations' => 0,
        'fm_generic_feature' => 0,
        'fm_opera_at_eight' => 0,
        'fm_opera_at_eight_upload' => 0,
        'fm_opera_encore' => 0,
        'fm_opera_encore_upload' => 0,
        'fm_opera_house' => 0,
        'fm_playlist' => 0,
        'fm_playlist_upload' => 0,
        'fm_shifts' => 0,
        'fm_theme_events' => 0,
        'fm_themes' => 0,
        'giveaway' => 0,
        'homepage_dates' => 0,
        'house_ad' => 0,
        'internship' => 0,
        'job_opening' => 0,
        'lightbox' => 0,
        'map_points' => 0,
        'preroll' => 0,
        'press_contact' => 0,
        'press_image' => 0,
        'press_page' => 0,
        'press_program' => 0,
        'press_release' => 0,
        'quiz_personality' => 0,
        'quiz_results' => 0,
        'quiz_trivia' => 0,
        'slideshow' => 0,
        'station_relations_page' => 0,
        'station_relations_subpage' => 0,
        'status_update' => 0,
        'timeline_item' => 0,
        'tv_channels' => 0,
        'tv_series' => 0,
        'tv_series_custom' => 0,
        'venue' => 0,
        'video_upload' => 0,
        'volunteer_sponsor' => 0,
        'volunteer_spotlight' => 0,
        'wanted_poster' => 0,
        'webform' => 0,
        'webform_display' => 0,
        'weta_player_ad_hack' => 0,
        'youtube_promos' => 0,
      ),
      'view' => array(
        'args' => array(
          0 => 'feature_block',
        ),
        'display_name' => 'references_1',
        'view_name' => 'carousel_references',
      ),
    ),
    'translatable' => 0,
    'type' => 'node_reference',
  );

  return $field_bases;
}
