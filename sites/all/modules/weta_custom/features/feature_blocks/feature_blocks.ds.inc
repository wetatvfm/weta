<?php
/**
 * @file
 * feature_blocks.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function feature_blocks_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|feature_block|carousel';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'feature_block';
  $ds_fieldsetting->view_mode = 'carousel';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h2',
        'class' => 'gray',
      ),
    ),
  );
  $export['node|feature_block|carousel'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|feature_block|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'feature_block';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|feature_block|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|feature_block|flyout_feature';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'feature_block';
  $ds_fieldsetting->view_mode = 'flyout_feature';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|feature_block|flyout_feature'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|feature_block|highlight_20_80';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'feature_block';
  $ds_fieldsetting->view_mode = 'highlight_20_80';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h5',
        'class' => '',
      ),
    ),
  );
  $export['node|feature_block|highlight_20_80'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|feature_block|highlight_30_60';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'feature_block';
  $ds_fieldsetting->view_mode = 'highlight_30_60';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|feature_block|highlight_30_60'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|feature_block|highlight_50_50';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'feature_block';
  $ds_fieldsetting->view_mode = 'highlight_50_50';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h3',
        'class' => '',
      ),
    ),
  );
  $export['node|feature_block|highlight_50_50'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|feature_block|home_50_50';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'feature_block';
  $ds_fieldsetting->view_mode = 'home_50_50';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h3',
        'class' => '',
      ),
    ),
  );
  $export['node|feature_block|home_50_50'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|feature_block|image_top_with_description';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'feature_block';
  $ds_fieldsetting->view_mode = 'image_top_with_description';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h3',
        'class' => '',
      ),
    ),
  );
  $export['node|feature_block|image_top_with_description'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|feature_block|image_top_with_short_description';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'feature_block';
  $ds_fieldsetting->view_mode = 'image_top_with_short_description';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h3',
        'class' => '',
      ),
    ),
  );
  $export['node|feature_block|image_top_with_short_description'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|feature_block|mini_feature';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'feature_block';
  $ds_fieldsetting->view_mode = 'mini_feature';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h3',
        'class' => '',
      ),
    ),
  );
  $export['node|feature_block|mini_feature'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|feature_block|productions_feature';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'feature_block';
  $ds_fieldsetting->view_mode = 'productions_feature';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h3',
        'class' => '',
      ),
    ),
  );
  $export['node|feature_block|productions_feature'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|feature_block|quicktab_content';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'feature_block';
  $ds_fieldsetting->view_mode = 'quicktab_content';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h3',
        'class' => '',
      ),
    ),
  );
  $export['node|feature_block|quicktab_content'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|feature_block|support_quad_box';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'feature_block';
  $ds_fieldsetting->view_mode = 'support_quad_box';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h2',
        'class' => 'blue',
      ),
    ),
  );
  $export['node|feature_block|support_quad_box'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|feature_block|text_box';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'feature_block';
  $ds_fieldsetting->view_mode = 'text_box';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|feature_block|text_box'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|feature_block|text_box_h3';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'feature_block';
  $ds_fieldsetting->view_mode = 'text_box_h3';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h3',
        'class' => '',
      ),
    ),
  );
  $export['node|feature_block|text_box_h3'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|feature_block|text_box_with_header';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'feature_block';
  $ds_fieldsetting->view_mode = 'text_box_with_header';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|feature_block|text_box_with_header'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|feature_block|wide_feature';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'feature_block';
  $ds_fieldsetting->view_mode = 'wide_feature';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h3',
        'class' => '',
      ),
    ),
  );
  $export['node|feature_block|wide_feature'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function feature_blocks_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|feature_block|basic_square';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'feature_block';
  $ds_layout->view_mode = 'basic_square';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_feature_image',
        1 => 'field_linked_title',
        2 => 'group_text_wrapper',
        3 => 'field_subhead',
        4 => 'field_primary_link',
      ),
    ),
    'fields' => array(
      'field_feature_image' => 'ds_content',
      'field_linked_title' => 'ds_content',
      'group_text_wrapper' => 'ds_content',
      'field_subhead' => 'ds_content',
      'field_primary_link' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|feature_block|basic_square'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|feature_block|basic_square_blue';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'feature_block';
  $ds_layout->view_mode = 'basic_square_blue';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_feature_image',
        1 => 'group_text_wrapper',
        2 => 'field_primary_link',
        3 => 'field_linked_title',
        4 => 'field_subhead',
      ),
    ),
    'fields' => array(
      'field_feature_image' => 'ds_content',
      'group_text_wrapper' => 'ds_content',
      'field_primary_link' => 'ds_content',
      'field_linked_title' => 'ds_content',
      'field_subhead' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 0,
  );
  $export['node|feature_block|basic_square_blue'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|feature_block|basic_square_kids';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'feature_block';
  $ds_layout->view_mode = 'basic_square_kids';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_linked_title',
        1 => 'field_subhead',
        2 => 'field_primary_link',
      ),
    ),
    'fields' => array(
      'field_linked_title' => 'ds_content',
      'field_subhead' => 'ds_content',
      'field_primary_link' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|feature_block|basic_square_kids'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|feature_block|carousel';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'feature_block';
  $ds_layout->view_mode = 'carousel';
  $ds_layout->layout = 'slideshow';
  $ds_layout->settings = array(
    'regions' => array(
      'image' => array(
        0 => 'field_feature_image',
        1 => 'field_feature_category',
      ),
      'caption' => array(
        2 => 'field_subhead',
        3 => 'title',
        4 => 'field_description',
        5 => 'field_primary_link',
        6 => 'field_links',
      ),
    ),
    'fields' => array(
      'field_feature_image' => 'image',
      'field_feature_category' => 'image',
      'field_subhead' => 'caption',
      'title' => 'caption',
      'field_description' => 'caption',
      'field_primary_link' => 'caption',
      'field_links' => 'caption',
    ),
    'classes' => array(),
    'wrappers' => array(
      'image' => 'div',
      'caption' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|feature_block|carousel'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|feature_block|flyout_feature';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'feature_block';
  $ds_layout->view_mode = 'flyout_feature';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_feature_image',
        1 => 'title',
        2 => 'field_short_description',
        3 => 'field_primary_link',
      ),
    ),
    'fields' => array(
      'field_feature_image' => 'ds_content',
      'title' => 'ds_content',
      'field_short_description' => 'ds_content',
      'field_primary_link' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|feature_block|flyout_feature'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|feature_block|green_square';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'feature_block';
  $ds_layout->view_mode = 'green_square';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_feature_image',
        1 => 'group_text_wrapper',
        2 => 'field_linked_title',
        3 => 'field_subhead',
        4 => 'field_primary_link',
      ),
    ),
    'fields' => array(
      'field_feature_image' => 'ds_content',
      'group_text_wrapper' => 'ds_content',
      'field_linked_title' => 'ds_content',
      'field_subhead' => 'ds_content',
      'field_primary_link' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|feature_block|green_square'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|feature_block|highlight_20_80';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'feature_block';
  $ds_layout->view_mode = 'highlight_20_80';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_feature_image',
        1 => 'title',
        2 => 'field_short_description',
        3 => 'field_primary_link',
        4 => 'group_text_wrapper',
      ),
    ),
    'fields' => array(
      'field_feature_image' => 'ds_content',
      'title' => 'ds_content',
      'field_short_description' => 'ds_content',
      'field_primary_link' => 'ds_content',
      'group_text_wrapper' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|feature_block|highlight_20_80'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|feature_block|highlight_30_60';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'feature_block';
  $ds_layout->view_mode = 'highlight_30_60';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_feature_image',
        1 => 'title',
        2 => 'field_description',
        3 => 'field_links',
      ),
    ),
    'fields' => array(
      'field_feature_image' => 'ds_content',
      'title' => 'ds_content',
      'field_description' => 'ds_content',
      'field_links' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|feature_block|highlight_30_60'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|feature_block|highlight_50_50';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'feature_block';
  $ds_layout->view_mode = 'highlight_50_50';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_feature_image',
        1 => 'title',
        2 => 'group_text_wrapper',
        3 => 'field_description',
        4 => 'field_primary_link',
      ),
    ),
    'fields' => array(
      'field_feature_image' => 'ds_content',
      'title' => 'ds_content',
      'group_text_wrapper' => 'ds_content',
      'field_description' => 'ds_content',
      'field_primary_link' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|feature_block|highlight_50_50'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|feature_block|home_50_50';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'feature_block';
  $ds_layout->view_mode = 'home_50_50';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_feature_image',
        1 => 'title',
        2 => 'field_short_description',
        3 => 'field_primary_link',
        4 => 'group_text_wrapper',
      ),
    ),
    'fields' => array(
      'field_feature_image' => 'ds_content',
      'title' => 'ds_content',
      'field_short_description' => 'ds_content',
      'field_primary_link' => 'ds_content',
      'group_text_wrapper' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|feature_block|home_50_50'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|feature_block|image_top_with_description';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'feature_block';
  $ds_layout->view_mode = 'image_top_with_description';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_feature_image',
        1 => 'title',
        2 => 'field_description',
        3 => 'field_primary_link',
      ),
    ),
    'fields' => array(
      'field_feature_image' => 'ds_content',
      'title' => 'ds_content',
      'field_description' => 'ds_content',
      'field_primary_link' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|feature_block|image_top_with_description'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|feature_block|image_top_with_short_description';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'feature_block';
  $ds_layout->view_mode = 'image_top_with_short_description';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'field_feature_image',
        2 => 'field_short_description',
        3 => 'field_primary_link',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'field_feature_image' => 'ds_content',
      'field_short_description' => 'ds_content',
      'field_primary_link' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|feature_block|image_top_with_short_description'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|feature_block|landing_carousel';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'feature_block';
  $ds_layout->view_mode = 'landing_carousel';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_feature_image',
        1 => 'field_subhead',
        2 => 'field_description',
        3 => 'field_links',
      ),
    ),
    'fields' => array(
      'field_feature_image' => 'ds_content',
      'field_subhead' => 'ds_content',
      'field_description' => 'ds_content',
      'field_links' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|feature_block|landing_carousel'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|feature_block|mini_feature';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'feature_block';
  $ds_layout->view_mode = 'mini_feature';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_feature_image',
        1 => 'field_description_with_link',
        2 => 'title',
        3 => 'field_short_description',
        4 => 'field_primary_link',
      ),
    ),
    'fields' => array(
      'field_feature_image' => 'ds_content',
      'field_description_with_link' => 'ds_content',
      'title' => 'ds_content',
      'field_short_description' => 'ds_content',
      'field_primary_link' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|feature_block|mini_feature'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|feature_block|productions_feature';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'feature_block';
  $ds_layout->view_mode = 'productions_feature';
  $ds_layout->layout = 'productions_feature';
  $ds_layout->settings = array(
    'regions' => array(
      'image' => array(
        0 => 'field_feature_image',
      ),
      'text' => array(
        1 => 'title',
        2 => 'field_short_description',
        3 => 'field_primary_link',
        4 => 'field_links',
      ),
    ),
    'fields' => array(
      'field_feature_image' => 'image',
      'title' => 'text',
      'field_short_description' => 'text',
      'field_primary_link' => 'text',
      'field_links' => 'text',
    ),
    'classes' => array(),
    'wrappers' => array(
      'image' => 'div',
      'text' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|feature_block|productions_feature'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|feature_block|quicktab_content';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'feature_block';
  $ds_layout->view_mode = 'quicktab_content';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_feature_image',
        1 => 'title',
        2 => 'field_short_description',
        3 => 'field_primary_link',
      ),
    ),
    'fields' => array(
      'field_feature_image' => 'ds_content',
      'title' => 'ds_content',
      'field_short_description' => 'ds_content',
      'field_primary_link' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 0,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|feature_block|quicktab_content'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|feature_block|support_quad_box';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'feature_block';
  $ds_layout->view_mode = 'support_quad_box';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'field_description',
        2 => 'field_primary_link',
        3 => 'field_links',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'field_description' => 'ds_content',
      'field_primary_link' => 'ds_content',
      'field_links' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|feature_block|support_quad_box'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|feature_block|text_box';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'feature_block';
  $ds_layout->view_mode = 'text_box';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'field_description',
        2 => 'field_primary_link',
        3 => 'field_links',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'field_description' => 'ds_content',
      'field_primary_link' => 'ds_content',
      'field_links' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|feature_block|text_box'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|feature_block|text_box_h3';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'feature_block';
  $ds_layout->view_mode = 'text_box_h3';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'field_description',
        2 => 'field_primary_link',
        3 => 'field_links',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'field_description' => 'ds_content',
      'field_primary_link' => 'ds_content',
      'field_links' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|feature_block|text_box_h3'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|feature_block|text_box_with_header';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'feature_block';
  $ds_layout->view_mode = 'text_box_with_header';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'field_description',
        2 => 'field_links',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'field_description' => 'ds_content',
      'field_links' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|feature_block|text_box_with_header'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|feature_block|wide_feature';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'feature_block';
  $ds_layout->view_mode = 'wide_feature';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'group_image',
        1 => 'field_feature_image',
        2 => 'title',
        3 => 'field_description',
        4 => 'field_primary_link',
        5 => 'field_links',
        6 => 'group_text_wrapper',
      ),
    ),
    'fields' => array(
      'group_image' => 'ds_content',
      'field_feature_image' => 'ds_content',
      'title' => 'ds_content',
      'field_description' => 'ds_content',
      'field_primary_link' => 'ds_content',
      'field_links' => 'ds_content',
      'group_text_wrapper' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|feature_block|wide_feature'] = $ds_layout;

  return $export;
}

/**
 * Implements hook_ds_view_modes_info().
 */
function feature_blocks_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'basic_square';
  $ds_view_mode->label = 'Basic Square (Red)';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['basic_square'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'basic_square_blue';
  $ds_view_mode->label = 'Basic Square (Blue)';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['basic_square_blue'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'basic_square_kids';
  $ds_view_mode->label = 'Basic Square (Green)';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['basic_square_kids'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'big_30_60';
  $ds_view_mode->label = 'Big 30/60';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['big_30_60'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'carousel';
  $ds_view_mode->label = 'Carousel';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['carousel'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'flyout_feature';
  $ds_view_mode->label = 'Flyout Feature';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['flyout_feature'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'full_width_promo';
  $ds_view_mode->label = 'Full Width Promo';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['full_width_promo'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'green_square';
  $ds_view_mode->label = 'Basic Square (Green) 2';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['green_square'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'highlight_20_80';
  $ds_view_mode->label = 'Highlight 20/80';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['highlight_20_80'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'highlight_30_60';
  $ds_view_mode->label = 'Highlight 30/60';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['highlight_30_60'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'highlight_50_50';
  $ds_view_mode->label = 'Highlight 50/50';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['highlight_50_50'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'home_50_50';
  $ds_view_mode->label = 'Home 50/50';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['home_50_50'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'homepage_carousel';
  $ds_view_mode->label = 'Hero Carousel';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['homepage_carousel'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'image_only';
  $ds_view_mode->label = 'Image Only';
  $ds_view_mode->entities = array(
    'taxonomy_term' => 'taxonomy_term',
  );
  $export['image_only'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'image_top_with_description';
  $ds_view_mode->label = 'Image Top with Description';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['image_top_with_description'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'image_top_with_short_description';
  $ds_view_mode->label = 'Image Top with Short Description';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['image_top_with_short_description'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'landing_carousel';
  $ds_view_mode->label = 'Landing Carousel';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['landing_carousel'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'mini_feature';
  $ds_view_mode->label = 'Mini Feature';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['mini_feature'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'quicktab_content';
  $ds_view_mode->label = 'Quicktab Content';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['quicktab_content'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'support_quad_box';
  $ds_view_mode->label = 'Blue Top Box';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['support_quad_box'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'text_box';
  $ds_view_mode->label = 'Text Box (h2)';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['text_box'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'text_box_h3';
  $ds_view_mode->label = 'Text Box (h3)';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['text_box_h3'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'text_box_with_header';
  $ds_view_mode->label = 'Text Box with Header';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['text_box_with_header'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'wide_feature';
  $ds_view_mode->label = 'Wide Feature';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['wide_feature'] = $ds_view_mode;

  return $export;
}
