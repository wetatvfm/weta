<?php
/**
 * @file
 * feature_blocks.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function feature_blocks_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function feature_blocks_image_default_styles() {
  $styles = array();

  // Exported image style: 16x9_feature.
  $styles['16x9_feature'] = array(
    'label' => '16x9 Feature',
    'effects' => array(
      57 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 640,
          'height' => 360,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => '16x9_feature',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: calendar_basic.
  $styles['calendar_basic'] = array(
    'label' => 'Calendar Square',
    'effects' => array(
      25 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 125,
          'height' => 125,
          'upscale' => 1,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'calendar_basic',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: calendar_highlight.
  $styles['calendar_highlight'] = array(
    'label' => 'Calendar Highlight',
    'effects' => array(
      26 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 319,
          'height' => 159,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'calendar_highlight',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: cropped_banner.
  $styles['cropped_banner'] = array(
    'label' => 'Cropped TV Banner',
    'effects' => array(
      27 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 551,
          'height' => 254,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'cropped_banner',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: feature_square.
  $styles['feature_square'] = array(
    'label' => 'Feature Block Default',
    'effects' => array(
      60 => array(
        'name' => 'manualcrop_crop',
        'data' => array(
          'width' => 330,
          'height' => 185,
          'keepproportions' => 1,
          'reuse_crop_style' => '',
          'style_name' => 'feature_square',
        ),
        'weight' => 0,
      ),
      61 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 330,
          'height' => 185,
        ),
        'weight' => 3,
      ),
    ),
  );

  // Exported image style: flyout_highlight.
  $styles['flyout_highlight'] = array(
    'label' => 'Flyout',
    'effects' => array(
      28 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 225,
          'height' => 129,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'flyout_highlight',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: hero_slide.
  $styles['hero_slide'] = array(
    'label' => 'Hero Slide',
    'effects' => array(
      59 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 1440,
          'height' => 560,
          'upscale' => 1,
          'respectminimum' => 0,
          'onlyscaleifcrop' => 0,
          'style_name' => 'hero_slide',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: highlight_20-80.
  $styles['highlight_20-80'] = array(
    'label' => 'Highlight 20/80 (120x97)',
    'effects' => array(
      30 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 400,
          'height' => 324,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'highlight_20-80',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: merlin.
  $styles['merlin'] = array(
    'label' => '16x9 for PBS (640x360)',
    'effects' => array(
      32 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 640,
          'height' => 360,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'merlin',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: mini_feature.
  $styles['mini_feature'] = array(
    'label' => 'Mini Feature',
    'effects' => array(
      33 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 120,
          'height' => 97,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'mini_feature',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: productions_highlight.
  $styles['productions_highlight'] = array(
    'label' => 'Productions Highlight (319x159)',
    'effects' => array(
      35 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 319,
          'height' => 159,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'productions_highlight',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: sidebar_strip.
  $styles['sidebar_strip'] = array(
    'label' => 'Sidebar Strip',
    'effects' => array(
      56 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 400,
          'height' => 150,
          'upscale' => 1,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'sidebar_strip',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: skinny_strip.
  $styles['skinny_strip'] = array(
    'label' => 'Skinny Strip',
    'effects' => array(
      63 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 900,
          'height' => 253,
          'upscale' => 1,
          'respectminimum' => 0,
          'onlyscaleifcrop' => 0,
          'style_name' => 'skinny_strip',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: squarish.
  $styles['squarish'] = array(
    'label' => 'Bottom Boxes (205x189)',
    'effects' => array(
      36 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 205,
          'height' => 189,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'squarish',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: wide_feature.
  $styles['wide_feature'] = array(
    'label' => 'Wide Feature (410x240)',
    'effects' => array(
      39 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 410,
          'height' => 240,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'wide_feature',
        ),
        'weight' => 0,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function feature_blocks_node_info() {
  $items = array(
    'feature_block' => array(
      'name' => t('Feature Block'),
      'base' => 'node_content',
      'description' => t('Feature blocks for display around the site'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
