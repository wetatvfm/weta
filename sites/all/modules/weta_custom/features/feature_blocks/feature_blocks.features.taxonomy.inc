<?php
/**
 * @file
 * feature_blocks.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function feature_blocks_taxonomy_default_vocabularies() {
  return array(
    'feature_block_eligibility' => array(
      'name' => 'Feature Block Filter',
      'machine_name' => 'feature_block_eligibility',
      'description' => 'Buckets to filter feature blocks for easy selection',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'feature_block_filter' => array(
      'name' => 'Site Sections',
      'machine_name' => 'feature_block_filter',
      'description' => 'Filter for feature blocks to make searching easier',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
