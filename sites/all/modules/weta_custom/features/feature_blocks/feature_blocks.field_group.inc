<?php
/**
 * @file
 * feature_blocks.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function feature_blocks_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_descriptions|node|feature_block|form';
  $field_group->group_name = 'group_descriptions';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feature_block';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Descriptions',
    'weight' => '3',
    'children' => array(
      0 => 'field_description',
      1 => 'field_short_description',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Descriptions',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => ' group-descriptions field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_descriptions|node|feature_block|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_image|node|feature_block|form';
  $field_group->group_name = 'group_image';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feature_block';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Image',
    'weight' => '1',
    'children' => array(
      0 => 'field_feature_image',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Image',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => ' group-image field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_image|node|feature_block|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_image|node|feature_block|wide_feature';
  $field_group->group_name = 'group_image';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feature_block';
  $field_group->mode = 'wide_feature';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Image',
    'weight' => '0',
    'children' => array(
      0 => 'field_feature_image',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Image',
      'instance_settings' => array(
        'classes' => 'span_3 col p10 group-image field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'id' => 'node_feature_block_wide_feature_group_image',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_image|node|feature_block|wide_feature'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_links|node|feature_block|form';
  $field_group->group_name = 'group_links';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feature_block';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Links',
    'weight' => '4',
    'children' => array(
      0 => 'field_links',
      1 => 'field_primary_link',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Links',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => ' group-links field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_links|node|feature_block|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_placement|node|feature_block|form';
  $field_group->group_name = 'group_placement';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feature_block';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Placement',
    'weight' => '5',
    'children' => array(
      0 => 'field_display_eligibility',
      1 => 'field_feature_category',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Placement',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => ' group-placement field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_placement|node|feature_block|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_text_wrapper|node|feature_block|basic_square';
  $field_group->group_name = 'group_text_wrapper';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feature_block';
  $field_group->mode = 'basic_square';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Text Wrapper',
    'weight' => '1',
    'children' => array(
      0 => 'field_subhead',
      1 => 'field_linked_title',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Text Wrapper',
      'instance_settings' => array(
        'classes' => 'text_wrapper group-text-wrapper field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
        'id' => 'node_feature_block_basic_square_group_text_wrapper',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_text_wrapper|node|feature_block|basic_square'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_text_wrapper|node|feature_block|basic_square_blue';
  $field_group->group_name = 'group_text_wrapper';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feature_block';
  $field_group->mode = 'basic_square_blue';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Text Wrapper',
    'weight' => '1',
    'children' => array(
      0 => 'field_subhead',
      1 => 'field_linked_title',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Text Wrapper',
      'instance_settings' => array(
        'classes' => 'text_wrapper group-text-wrapper field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'id' => 'node_feature_block_basic_square_blue_group_text_wrapper',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_text_wrapper|node|feature_block|basic_square_blue'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_text_wrapper|node|feature_block|basic_square_green';
  $field_group->group_name = 'group_text_wrapper';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feature_block';
  $field_group->mode = 'basic_square_green';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Text Wrapper',
    'weight' => '0',
    'children' => array(
      0 => 'field_subhead',
      1 => 'field_linked_title',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Text Wrapper',
      'instance_settings' => array(
        'classes' => 'text_wrapper group-text-wrapper field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'id' => 'node_feature_block_basic_square_green_group_text_wrapper',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_text_wrapper|node|feature_block|basic_square_green'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_text_wrapper|node|feature_block|green_square';
  $field_group->group_name = 'group_text_wrapper';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feature_block';
  $field_group->mode = 'green_square';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Text Wrapper',
    'weight' => '1',
    'children' => array(
      0 => 'field_subhead',
      1 => 'field_linked_title',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Text Wrapper',
      'instance_settings' => array(
        'classes' => 'text_wrapper group-text-wrapper field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'id' => 'node_feature_block_green_square_group_text_wrapper',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_text_wrapper|node|feature_block|green_square'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_text_wrapper|node|feature_block|highlight_20_80';
  $field_group->group_name = 'group_text_wrapper';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feature_block';
  $field_group->mode = 'highlight_20_80';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Text Wrapper',
    'weight' => '17',
    'children' => array(
      0 => 'field_short_description',
      1 => 'field_primary_link',
      2 => 'title',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Text Wrapper',
      'instance_settings' => array(
        'classes' => 'span_9 col',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'id' => 'node_feature_block_highlight_20_80_group_text_wrapper',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_text_wrapper|node|feature_block|highlight_20_80'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_text_wrapper|node|feature_block|highlight_50_50';
  $field_group->group_name = 'group_text_wrapper';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feature_block';
  $field_group->mode = 'highlight_50_50';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Text Wrapper',
    'weight' => '1',
    'children' => array(
      0 => 'field_description',
      1 => 'field_primary_link',
      2 => 'title',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Text Wrapper',
      'instance_settings' => array(
        'classes' => 'text_wrapper group-text-wrapper field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'id' => 'node_feature_block_highlight_50_50_group_text_wrapper',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_text_wrapper|node|feature_block|highlight_50_50'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_text_wrapper|node|feature_block|home_50_50';
  $field_group->group_name = 'group_text_wrapper';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feature_block';
  $field_group->mode = 'home_50_50';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Text Wrapper',
    'weight' => '16',
    'children' => array(
      0 => 'field_short_description',
      1 => 'field_primary_link',
      2 => 'title',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Text Wrapper',
      'instance_settings' => array(
        'classes' => 'text_wrapper group-text-wrapper field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'id' => 'node_feature_block_home_50_50_group_text_wrapper',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_text_wrapper|node|feature_block|home_50_50'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_text_wrapper|node|feature_block|wide_feature';
  $field_group->group_name = 'group_text_wrapper';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feature_block';
  $field_group->mode = 'wide_feature';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Text Wrapper',
    'weight' => '18',
    'children' => array(
      0 => 'field_description',
      1 => 'field_links',
      2 => 'field_primary_link',
      3 => 'title',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Text Wrapper',
      'instance_settings' => array(
        'classes' => 'span_9 col p10 group-text-wrapper field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'id' => 'node_feature_block_wide_feature_group_text_wrapper',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_text_wrapper|node|feature_block|wide_feature'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_titles|node|feature_block|form';
  $field_group->group_name = 'group_titles';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feature_block';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Titles',
    'weight' => '0',
    'children' => array(
      0 => 'field_subhead',
      1 => 'field_linked_title',
      2 => 'title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Titles',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => ' group-titles field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_titles|node|feature_block|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Descriptions');
  t('Image');
  t('Links');
  t('Placement');
  t('Text Wrapper');
  t('Titles');

  return $field_groups;
}
