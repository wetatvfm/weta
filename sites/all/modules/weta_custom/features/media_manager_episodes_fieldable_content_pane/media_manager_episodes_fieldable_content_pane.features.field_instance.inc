<?php
/**
 * @file
 * media_manager_episodes_fieldable_content_pane.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function media_manager_episodes_fieldable_content_pane_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'fieldable_panels_pane-pbs_media_manager_episodes-field_media_manager_show'.
  $field_instances['fieldable_panels_pane-pbs_media_manager_episodes-field_media_manager_show'] = array(
    'bundle' => 'pbs_media_manager_episodes',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'weta_pbs_media_manager',
        'settings' => array(
          'number_of_episodes' => 7,
        ),
        'type' => 'weta_pbs_media_manager_show_episodes_formatter',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'fieldable_panels_pane',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_media_manager_show',
    'label' => 'Media Manager Show',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'pbs_media_manager_show',
      'settings' => array(),
      'type' => 'pbs_media_manager_show_id',
      'weight' => -3,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Media Manager Show');

  return $field_instances;
}
