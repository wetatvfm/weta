<?php
/**
 * @file
 * media_manager_episodes_fieldable_content_pane.fieldable_panels_pane_type.inc
 */

/**
 * Implements hook_default_fieldable_panels_pane_type().
 */
function media_manager_episodes_fieldable_content_pane_default_fieldable_panels_pane_type() {
  $export = array();

  $fieldable_panels_pane_type = new stdClass();
  $fieldable_panels_pane_type->disabled = FALSE; /* Edit this to true to make a default fieldable_panels_pane_type disabled initially */
  $fieldable_panels_pane_type->api_version = 1;
  $fieldable_panels_pane_type->name = 'pbs_media_manager_episodes';
  $fieldable_panels_pane_type->title = 'PBS Media Manager Episodes';
  $fieldable_panels_pane_type->description = 'Add episodes of a PBS Media Manager Show.';
  $export['pbs_media_manager_episodes'] = $fieldable_panels_pane_type;

  return $export;
}
