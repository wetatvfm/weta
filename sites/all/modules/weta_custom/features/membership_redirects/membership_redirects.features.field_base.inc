<?php
/**
 * @file
 * membership_redirects.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function membership_redirects_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_redirect_destination'.
  $field_bases['field_redirect_destination'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_redirect_destination',
    'field_permissions' => array(
      'type' => 1,
    ),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'link',
    'settings' => array(
      'attributes' => array(
        'class' => '',
        'rel' => '',
        'target' => 'default',
      ),
      'display' => array(
        'url_cutoff' => 80,
      ),
      'enable_tokens' => 1,
      'title' => 'optional',
      'title_maxlength' => 128,
      'title_value' => '',
      'url' => 0,
    ),
    'translatable' => 0,
    'type' => 'link_field',
  );

  // Exported field_base: 'field_redirect_notes'.
  $field_bases['field_redirect_notes'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_redirect_notes',
    'field_permissions' => array(
      'type' => 1,
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'text_long',
  );

  // Exported field_base: 'field_redirect_source'.
  $field_bases['field_redirect_source'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_redirect_source',
    'field_permissions' => array(
      'type' => 1,
    ),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'annual' => 'annual',
        'annualfund' => 'annualfund',
        'challenge' => 'challenge',
        'classicalapp' => 'classicalapp',
        'donate' => 'donate',
        'donatefm' => 'donatefm',
        'donatefocm' => 'donatefocm',
        'donatelc' => 'donatelc',
        'donatenow' => 'donatenow',
        'donatetv' => 'donatetv',
        'donateweb' => 'donateweb',
        'getpassport' => 'getpassport',
        'give' => 'give',
        'givedoor2door' => 'givedoor2door',
        'givelightbox' => 'givelightbox',
        'givenow' => 'givenow',
        'givepassport' => 'givepassport',
        'in-memory-of-gwen' => 'in-memory-of-gwen',
        'join' => 'join',
        'journalismfund' => 'journalismfund',
        'leaderboard' => 'leaderboard',
        'mag' => 'mag',
        'monthly' => 'monthly',
        'npr' => 'npr',
        'payment' => 'payment',
        'pbs-donate' => 'pbs-donate',
        'pbs-join' => 'pbs-join',
        'pbs-monthly' => 'pbs-monthly',
        'pbs-renew' => 'pbs-renew',
        'programfund' => 'programfund',
        'renewtoday' => 'renewtoday',
        'renew' => 'renew',
        'sustainer' => 'sustainer',
        'thankyougift' => 'thankyougift',
        'ukshop' => 'ukshop',
        'yearend' => 'yearend',
        'youdecide' => 'youdecide',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  return $field_bases;
}
