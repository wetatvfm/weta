<?php
/**
 * @file
 * membership_redirects.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function membership_redirects_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'node-membership_redirects-field_redirect_destination'.
  $field_instances['node-membership_redirects-field_redirect_destination'] = array(
    'bundle' => 'membership_redirects',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Where the redirect should go (e.g. a link to Engaging Networks or Pledgecart).  Query strings are allowed.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'link',
        'settings' => array(),
        'type' => 'link_default',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_redirect_destination',
    'label' => 'Destination',
    'required' => 1,
    'settings' => array(
      'absolute_url' => 1,
      'attributes' => array(
        'class' => '',
        'configurable_class' => 0,
        'configurable_title' => 0,
        'rel' => '',
        'target' => 'default',
        'title' => '',
      ),
      'display' => array(
        'url_cutoff' => '',
      ),
      'enable_tokens' => 0,
      'rel_remove' => 'default',
      'title' => 'none',
      'title_label_use_field_label' => 0,
      'title_maxlength' => 128,
      'title_value' => '',
      'url' => 0,
      'user_register_form' => FALSE,
      'validate_url' => 1,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'link',
      'settings' => array(),
      'type' => 'link_field',
      'weight' => 10,
    ),
  );

  // Exported field_instance: 'node-membership_redirects-field_redirect_notes'.
  $field_instances['node-membership_redirects-field_redirect_notes'] = array(
    'bundle' => 'membership_redirects',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Any notes to help you remember the purpose of this redirect',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 4,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_redirect_notes',
    'label' => 'Notes',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 11,
    ),
  );

  // Exported field_instance: 'node-membership_redirects-field_redirect_source'.
  $field_instances['node-membership_redirects-field_redirect_source'] = array(
    'bundle' => 'membership_redirects',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The source url (e.g., if you are creating a redirect from weta.org/donate to pledgecart, you would make sure that "donate" is selected.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_redirect_source',
    'label' => 'Source',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 9,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Any notes to help you remember the purpose of this redirect');
  t('Destination');
  t('Notes');
  t('Source');
  t('The source url (e.g., if you are creating a redirect from weta.org/donate to pledgecart, you would make sure that "donate" is selected.');
  t('Where the redirect should go (e.g. a link to Engaging Networks or Pledgecart).  Query strings are allowed.');

  return $field_instances;
}
