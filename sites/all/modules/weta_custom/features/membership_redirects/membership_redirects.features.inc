<?php
/**
 * @file
 * membership_redirects.features.inc
 */

/**
 * Implements hook_node_info().
 */
function membership_redirects_node_info() {
  $items = array(
    'membership_redirects' => array(
      'name' => t('Membership Redirects'),
      'base' => 'node_content',
      'description' => t('Redirects managed by the membership department'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
