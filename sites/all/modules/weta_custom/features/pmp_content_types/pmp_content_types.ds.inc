<?php
/**
 * @file
 * pmp_content_types.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function pmp_content_types_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|pmp_story|generic';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'pmp_story';
  $ds_layout->view_mode = 'generic';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'field_pmp_published',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'field_pmp_published' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|pmp_story|generic'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|pmp_story|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'pmp_story';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'two_col_3_9';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_pmp_image_reference',
      ),
      'right' => array(
        1 => 'title',
        2 => 'field_pmp_published',
        3 => 'field_pmp_teaser',
        4 => 'node_link',
      ),
    ),
    'fields' => array(
      'field_pmp_image_reference' => 'left',
      'title' => 'right',
      'field_pmp_published' => 'right',
      'field_pmp_teaser' => 'right',
      'node_link' => 'right',
    ),
    'limit' => array(
      'field_pmp_image_reference' => '1',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|pmp_story|teaser'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|pmp_video|generic';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'pmp_video';
  $ds_layout->view_mode = 'generic';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'field_pmp_published',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'field_pmp_published' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|pmp_video|generic'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|pmp_video|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'pmp_video';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'two_col_3_9';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_pmp_image_reference',
      ),
      'right' => array(
        1 => 'title',
        2 => 'field_pmp_published',
        3 => 'field_pmp_description',
        4 => 'node_link',
      ),
    ),
    'fields' => array(
      'field_pmp_image_reference' => 'left',
      'title' => 'right',
      'field_pmp_published' => 'right',
      'field_pmp_description' => 'right',
      'node_link' => 'right',
    ),
    'limit' => array(
      'field_pmp_image_reference' => '1',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|pmp_video|teaser'] = $ds_layout;

  return $export;
}

/**
 * Implements hook_ds_view_modes_info().
 */
function pmp_content_types_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'generic';
  $ds_view_mode->label = 'Generic';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['generic'] = $ds_view_mode;

  return $export;
}
