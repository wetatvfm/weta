<?php
/**
 * @file
 * pmp_content_types.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pmp_content_types_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "file_entity" && $api == "file_default_displays") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function pmp_content_types_node_info() {
  $items = array(
    'pmp_story' => array(
      'name' => t('PMP Story'),
      'base' => 'node_content',
      'description' => t('Stories imported from the PMP'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'pmp_video' => array(
      'name' => t('PMP Video'),
      'base' => 'node_content',
      'description' => t('Video imported from the PMP'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
