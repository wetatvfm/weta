<?php
/**
 * @file
 * pmp_content_types.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function pmp_content_types_taxonomy_default_vocabularies() {
  return array(
    'pmp_tags' => array(
      'name' => 'PMP Tags',
      'machine_name' => 'pmp_tags',
      'description' => 'Tags from the PMP',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
