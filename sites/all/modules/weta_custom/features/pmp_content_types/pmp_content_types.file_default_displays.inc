<?php
/**
 * @file
 * pmp_content_types.file_default_displays.inc
 */

/**
 * Implements hook_file_default_displays().
 */
function pmp_content_types_file_default_displays() {
  $export = array();

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'audio__default__file_field_file_default';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = array();
  $export['audio__default__file_field_file_default'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__highlight__file_field_file_default';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = '';
  $export['image__highlight__file_field_file_default'] = $file_display;

  return $export;
}
