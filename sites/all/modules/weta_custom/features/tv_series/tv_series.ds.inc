<?php
/**
 * @file
 * tv_series.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function tv_series_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|tv_series|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'tv_series';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_series_id',
        1 => 'field_description',
        2 => 'field_nola_root',
        3 => 'field_category',
        4 => 'field_movie_type',
        5 => 'field_program_id',
        6 => 'field_last_update_from_feed',
        7 => 'field_title_id',
        8 => 'field_cove_program_id_manual',
        9 => 'field_weta_production',
        10 => 'field_adjusted_title',
        11 => 'field_sort_label',
        12 => 'field_header_image',
        13 => 'field_schedule_summary',
        14 => 'field_edited_description',
        15 => 'field_related_series',
      ),
    ),
    'fields' => array(
      'field_series_id' => 'ds_content',
      'field_description' => 'ds_content',
      'field_nola_root' => 'ds_content',
      'field_category' => 'ds_content',
      'field_movie_type' => 'ds_content',
      'field_program_id' => 'ds_content',
      'field_last_update_from_feed' => 'ds_content',
      'field_title_id' => 'ds_content',
      'field_cove_program_id_manual' => 'ds_content',
      'field_weta_production' => 'ds_content',
      'field_adjusted_title' => 'ds_content',
      'field_sort_label' => 'ds_content',
      'field_header_image' => 'ds_content',
      'field_schedule_summary' => 'ds_content',
      'field_edited_description' => 'ds_content',
      'field_related_series' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 0,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|tv_series|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|tv_series|highlight_20_80';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'tv_series';
  $ds_layout->view_mode = 'highlight_20_80';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_header_image',
        1 => 'group_text_wrapper',
        2 => 'title',
        3 => 'field_description',
        4 => 'node_link',
      ),
    ),
    'fields' => array(
      'field_header_image' => 'ds_content',
      'group_text_wrapper' => 'ds_content',
      'title' => 'ds_content',
      'field_description' => 'ds_content',
      'node_link' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|tv_series|highlight_20_80'] = $ds_layout;

  return $export;
}
