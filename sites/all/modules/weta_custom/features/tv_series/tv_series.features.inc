<?php
/**
 * @file
 * tv_series.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function tv_series_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "panelizer" && $api == "panelizer") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function tv_series_node_info() {
  $items = array(
    'tv_series' => array(
      'name' => t('TV Series'),
      'base' => 'node_content',
      'description' => t('Individual TV series'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
