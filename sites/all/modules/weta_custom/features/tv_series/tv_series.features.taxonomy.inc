<?php
/**
 * @file
 * tv_series.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function tv_series_taxonomy_default_vocabularies() {
  return array(
    'tv_series_categories' => array(
      'name' => 'TV Series Categories',
      'machine_name' => 'tv_series_categories',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
