<?php
/**
 * @file
 * tv_series.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function tv_series_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create field_header_image'.
  $permissions['create field_header_image'] = array(
    'name' => 'create field_header_image',
    'roles' => array(
      'Administrator' => 'Administrator',
      'Digital Media' => 'Digital Media',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_header_image'.
  $permissions['edit field_header_image'] = array(
    'name' => 'edit field_header_image',
    'roles' => array(
      'Administrator' => 'Administrator',
      'Digital Media' => 'Digital Media',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_header_image'.
  $permissions['edit own field_header_image'] = array(
    'name' => 'edit own field_header_image',
    'roles' => array(
      'Administrator' => 'Administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_header_image'.
  $permissions['view field_header_image'] = array(
    'name' => 'view field_header_image',
    'roles' => array(
      'Administrator' => 'Administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_header_image'.
  $permissions['view own field_header_image'] = array(
    'name' => 'view own field_header_image',
    'roles' => array(
      'Administrator' => 'Administrator',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
