<?php
/**
 * @file
 * tv_series.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function tv_series_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_editable_fields|node|tv_series|form';
  $field_group->group_name = 'group_editable_fields';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'tv_series';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Editable Fields',
    'weight' => '1',
    'children' => array(
      0 => 'field_description',
      1 => 'field_weta_production',
      2 => 'field_header_image',
      3 => 'field_schedule_summary',
      4 => 'field_edited_description',
      5 => 'field_related_series',
      6 => 'field_links',
      7 => 'field_kids_image',
      8 => 'field_downloads',
      9 => 'field_tv_extra_text',
      10 => 'field_media_manager_show',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => ' group-editable-fields field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_editable_fields|node|tv_series|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_imported_fields|node|tv_series|form';
  $field_group->group_name = 'group_imported_fields';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'tv_series';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Imported Fields',
    'weight' => '4',
    'children' => array(
      0 => 'field_series_id',
      1 => 'field_nola_root',
      2 => 'field_category',
      3 => 'field_movie_type',
      4 => 'field_program_id',
      5 => 'field_last_update_from_feed',
      6 => 'field_title_id',
      7 => 'field_cove_program_id_manual',
      8 => 'field_adjusted_title',
      9 => 'field_sort_label',
      10 => 'field_show_id',
      11 => 'field_airs_on_uk',
      12 => 'field_airs_on_kids',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Imported Fields',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => ' group-imported-fields field-group-fieldset',
        'description' => 'These fields are populated by the TV Series Import and should not be edited.',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_imported_fields|node|tv_series|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_text_wrapper|node|tv_series|highlight_20_80';
  $field_group->group_name = 'group_text_wrapper';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'tv_series';
  $field_group->mode = 'highlight_20_80';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Text Wrapper',
    'weight' => '1',
    'children' => array(
      0 => 'field_description',
      1 => 'title',
      2 => 'node_link',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Text Wrapper',
      'instance_settings' => array(
        'id' => 'node_feature_block_highlight_20_80_group_text_wrapper',
        'classes' => 'span_9 col',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_text_wrapper|node|tv_series|highlight_20_80'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Editable Fields');
  t('Imported Fields');
  t('Text Wrapper');

  return $field_groups;
}
