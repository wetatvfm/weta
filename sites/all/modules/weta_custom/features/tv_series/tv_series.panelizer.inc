<?php
/**
 * @file
 * tv_series.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function tv_series_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'tv_series';
  $panelizer->access = '';
  $panelizer->view_mode = 'page_manager';
  $panelizer->name = 'node:tv_series:default';
  $panelizer->css_id = '';
  $panelizer->css_class = '';
  $panelizer->css = '';
  $panelizer->no_blocks = FALSE;
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $display = new panels_display();
  $display->layout = 'tvseries';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => array(),
      'center' => NULL,
      'top' => array(),
      'left' => array(),
      'right_top' => array(),
      'right_bottom' => array(),
      'banner' => NULL,
      'bottom' => NULL,
    ),
    'style' => 'stylizer',
    'top' => array(
      'style' => 'stylizer',
    ),
    'left' => array(
      'style' => 'stylizer',
    ),
    'right_top' => array(
      'style' => 'stylizer',
    ),
    'right_bottom' => array(
      'style' => 'stylizer',
    ),
  );
  $display->cache = array(
    'method' => 0,
  );
  $display->title = '';
  $display->uuid = '56980b00-8452-4cc4-a1e0-2d11206225da';
  $display->storage_type = 'panelizer_default';
  $display->storage_id = 'node:tv_series:default';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-f1bec97b-fadc-4f38-bf2e-f3f576273d44';
  $pane->panel = 'banner';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_header_image';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'image',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'image_style' => 'cropped_banner',
      'image_link' => '',
    ),
    'context' => 'panelizer',
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array(
    'method' => 'simple',
    'settings' => array(
      'lifetime' => '900',
      'granularity' => 'context',
    ),
  );
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'f1bec97b-fadc-4f38-bf2e-f3f576273d44';
  $display->content['new-f1bec97b-fadc-4f38-bf2e-f3f576273d44'] = $pane;
  $display->panels['banner'][0] = 'new-f1bec97b-fadc-4f38-bf2e-f3f576273d44';
  $pane = new stdClass();
  $pane->pid = 'new-1d134619-6480-43a4-97d9-e8ec980696a2';
  $pane->panel = 'bottom';
  $pane->type = 'block';
  $pane->subtype = 'weta_pbs_media_manager-episodes';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '1d134619-6480-43a4-97d9-e8ec980696a2';
  $display->content['new-1d134619-6480-43a4-97d9-e8ec980696a2'] = $pane;
  $display->panels['bottom'][0] = 'new-1d134619-6480-43a4-97d9-e8ec980696a2';
  $pane = new stdClass();
  $pane->pid = 'new-51017934-2afb-4ae6-9410-94873bd506df';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'quicktabs-episodes_by_series';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array(
    'method' => 'lazy',
    'settings' => array(
      'load_strategy' => 'page-loaded',
      'show_spinner' => 1,
      'load_text' => '',
    ),
  );
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => 'episodes',
    'css_class' => '',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '51017934-2afb-4ae6-9410-94873bd506df';
  $display->content['new-51017934-2afb-4ae6-9410-94873bd506df'] = $pane;
  $display->panels['left'][0] = 'new-51017934-2afb-4ae6-9410-94873bd506df';
  $pane = new stdClass();
  $pane->pid = 'new-79638a99-a010-44af-a72c-f22956d95565';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'weta_coveapi-episodes';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array(
    'method' => 'lazy',
    'settings' => array(
      'load_strategy' => 'page-loaded',
      'show_spinner' => 1,
      'load_text' => '',
    ),
  );
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '79638a99-a010-44af-a72c-f22956d95565';
  $display->content['new-79638a99-a010-44af-a72c-f22956d95565'] = $pane;
  $display->panels['left'][1] = 'new-79638a99-a010-44af-a72c-f22956d95565';
  $pane = new stdClass();
  $pane->pid = 'new-d689b876-c5be-4a0c-aa4c-a81f6334e609';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'weta_coveapi-clips';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array(
    'method' => 'lazy',
    'settings' => array(
      'load_strategy' => 'page-loaded',
      'show_spinner' => 1,
      'load_text' => '',
    ),
  );
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = 'd689b876-c5be-4a0c-aa4c-a81f6334e609';
  $display->content['new-d689b876-c5be-4a0c-aa4c-a81f6334e609'] = $pane;
  $display->panels['left'][2] = 'new-d689b876-c5be-4a0c-aa4c-a81f6334e609';
  $pane = new stdClass();
  $pane->pid = 'new-a356879f-2dfd-4f7e-a09b-76004cce5baa';
  $pane->panel = 'left';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_related_series';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'node_reference_default',
    'delta_limit' => '0',
    'delta_offset' => '0',
    'delta_reversed' => 0,
    'formatter_settings' => array(),
    'context' => 'panelizer',
    'override_title' => 1,
    'override_title_text' => 'You May Also Like...',
  );
  $pane->cache = array(
    'method' => 'simple',
    'settings' => array(
      'lifetime' => '900',
      'granularity' => 'context',
    ),
  );
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => 'related',
    'css_class' => '',
  );
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = 'a356879f-2dfd-4f7e-a09b-76004cce5baa';
  $display->content['new-a356879f-2dfd-4f7e-a09b-76004cce5baa'] = $pane;
  $display->panels['left'][3] = 'new-a356879f-2dfd-4f7e-a09b-76004cce5baa';
  $pane = new stdClass();
  $pane->pid = 'new-1f5aa30b-66a5-41ef-a657-91b3112c2a42';
  $pane->panel = 'right_top';
  $pane->type = 'block';
  $pane->subtype = 'sharethis-sharethis_block';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array(
    'method' => 'simple',
    'settings' => array(
      'lifetime' => '900',
      'granularity' => 'context',
    ),
  );
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '1f5aa30b-66a5-41ef-a657-91b3112c2a42';
  $display->content['new-1f5aa30b-66a5-41ef-a657-91b3112c2a42'] = $pane;
  $display->panels['right_top'][0] = 'new-1f5aa30b-66a5-41ef-a657-91b3112c2a42';
  $pane = new stdClass();
  $pane->pid = 'new-1664e32e-a887-443c-98f7-70ff02144645';
  $pane->panel = 'right_top';
  $pane->type = 'block';
  $pane->subtype = 'weta_coveapi-previews';
  $pane->shown = FALSE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array(
    'method' => 'lazy',
    'settings' => array(
      'load_strategy' => 'page-loaded',
      'show_spinner' => 1,
      'load_text' => '',
    ),
  );
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '1664e32e-a887-443c-98f7-70ff02144645';
  $display->content['new-1664e32e-a887-443c-98f7-70ff02144645'] = $pane;
  $display->panels['right_top'][1] = 'new-1664e32e-a887-443c-98f7-70ff02144645';
  $pane = new stdClass();
  $pane->pid = 'new-2457b682-d987-4783-b344-d331375e7f2b';
  $pane->panel = 'right_top';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_links';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'link_default',
    'delta_limit' => '0',
    'delta_offset' => '0',
    'delta_reversed' => 0,
    'formatter_settings' => array(),
    'context' => 'panelizer',
    'override_title' => 1,
    'override_title_text' => 'Links of Interest',
  );
  $pane->cache = array(
    'method' => 'simple',
    'settings' => array(
      'lifetime' => '900',
      'granularity' => 'context',
    ),
  );
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'sidenav ml20',
  );
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '2457b682-d987-4783-b344-d331375e7f2b';
  $display->content['new-2457b682-d987-4783-b344-d331375e7f2b'] = $pane;
  $display->panels['right_top'][2] = 'new-2457b682-d987-4783-b344-d331375e7f2b';
  $pane = new stdClass();
  $pane->pid = 'new-0549f1a5-6159-4072-beb1-58ba693e42de';
  $pane->panel = 'right_top';
  $pane->type = 'block';
  $pane->subtype = 'custom-social_connect';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array(
    'method' => 'simple',
    'settings' => array(
      'lifetime' => '900',
      'granularity' => 'context',
    ),
  );
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = '0549f1a5-6159-4072-beb1-58ba693e42de';
  $display->content['new-0549f1a5-6159-4072-beb1-58ba693e42de'] = $pane;
  $display->panels['right_top'][3] = 'new-0549f1a5-6159-4072-beb1-58ba693e42de';
  $pane = new stdClass();
  $pane->pid = 'new-9f52c2e4-6939-43e7-9adb-05222a354f79';
  $pane->panel = 'right_top';
  $pane->type = 'block';
  $pane->subtype = 'block-15';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Sign Up for WETA Highlights',
  );
  $pane->cache = array(
    'method' => 'simple',
    'settings' => array(
      'lifetime' => '900',
      'granularity' => 'context',
    ),
  );
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 4;
  $pane->locks = array();
  $pane->uuid = '9f52c2e4-6939-43e7-9adb-05222a354f79';
  $display->content['new-9f52c2e4-6939-43e7-9adb-05222a354f79'] = $pane;
  $display->panels['right_top'][4] = 'new-9f52c2e4-6939-43e7-9adb-05222a354f79';
  $pane = new stdClass();
  $pane->pid = 'new-7011e8a8-a92b-4192-91f0-9107f53f4ec5';
  $pane->panel = 'top';
  $pane->type = 'node_title';
  $pane->subtype = 'node_title';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'link' => 0,
    'context' => 'panelizer',
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array(
    'method' => 'simple',
    'settings' => array(
      'lifetime' => '900',
      'granularity' => 'context',
    ),
  );
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '7011e8a8-a92b-4192-91f0-9107f53f4ec5';
  $display->content['new-7011e8a8-a92b-4192-91f0-9107f53f4ec5'] = $pane;
  $display->panels['top'][0] = 'new-7011e8a8-a92b-4192-91f0-9107f53f4ec5';
  $pane = new stdClass();
  $pane->pid = 'new-82d99b4c-9feb-4023-ab9f-f3531a981c8f';
  $pane->panel = 'top';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_schedule_summary';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'text_default',
    'delta_limit' => '0',
    'delta_offset' => '0',
    'delta_reversed' => 0,
    'formatter_settings' => array(),
    'context' => 'panelizer',
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array(
    'method' => 'simple',
    'settings' => array(
      'lifetime' => '900',
      'granularity' => 'context',
    ),
  );
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '82d99b4c-9feb-4023-ab9f-f3531a981c8f';
  $display->content['new-82d99b4c-9feb-4023-ab9f-f3531a981c8f'] = $pane;
  $display->panels['top'][1] = 'new-82d99b4c-9feb-4023-ab9f-f3531a981c8f';
  $pane = new stdClass();
  $pane->pid = 'new-a4b0c111-1a79-487e-8c26-5ae5fcc54fb7';
  $pane->panel = 'top';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_edited_description';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'text_default',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => 'panelizer',
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array(
    'method' => 'simple',
    'settings' => array(
      'lifetime' => '900',
      'granularity' => 'context',
    ),
  );
  $pane->style = array(
    'style' => 'stylizer',
    'settings' => array(),
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = 'a4b0c111-1a79-487e-8c26-5ae5fcc54fb7';
  $display->content['new-a4b0c111-1a79-487e-8c26-5ae5fcc54fb7'] = $pane;
  $display->panels['top'][2] = 'new-a4b0c111-1a79-487e-8c26-5ae5fcc54fb7';
  $pane = new stdClass();
  $pane->pid = 'new-b52c614b-f05c-49cd-8d3a-1b236433d5df';
  $pane->panel = 'top';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_description';
  $pane->shown = TRUE;
  $pane->access = array(
    'logic' => 'and',
    'plugins' => array(
      0 => array(
        'name' => 'php',
        'settings' => array(
          'description' => 'Do not show if Edited Description exists',
          'php' => 'if (empty($contexts[\'panelizer\']->data->field_edited_description)) {
  return TRUE;
}',
        ),
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'text_default',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => 'panelizer',
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array(
    'method' => 'simple',
    'settings' => array(
      'lifetime' => '900',
      'granularity' => 'context',
    ),
  );
  $pane->style = array(
    'style' => 'stylizer',
    'settings' => array(),
  );
  $pane->css = array(
    'css_id' => 'related',
    'css_class' => '',
  );
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = 'b52c614b-f05c-49cd-8d3a-1b236433d5df';
  $display->content['new-b52c614b-f05c-49cd-8d3a-1b236433d5df'] = $pane;
  $display->panels['top'][3] = 'new-b52c614b-f05c-49cd-8d3a-1b236433d5df';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:tv_series:default'] = $panelizer;

  return $export;
}
