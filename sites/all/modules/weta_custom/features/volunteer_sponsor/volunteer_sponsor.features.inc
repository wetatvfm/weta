<?php
/**
 * @file
 * volunteer_sponsor.features.inc
 */

/**
 * Implements hook_node_info().
 */
function volunteer_sponsor_node_info() {
  $items = array(
    'volunteer_sponsor' => array(
      'name' => t('Volunteer Sponsor'),
      'base' => 'node_content',
      'description' => t('Sponsors for the Volunteer landing page'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
