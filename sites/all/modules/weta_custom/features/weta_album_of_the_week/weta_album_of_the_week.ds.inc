<?php

/**
 * @file
 * weta_album_of_the_week.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function weta_album_of_the_week_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|fm_cd_pick|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'fm_cd_pick';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '20',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h4',
        'class' => '',
      ),
    ),
  );
  $export['node|fm_cd_pick|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|fm_cd_pick|teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'fm_cd_pick';
  $ds_fieldsetting->view_mode = 'teaser';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h4',
        'class' => '',
      ),
    ),
  );
  $export['node|fm_cd_pick|teaser'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function weta_album_of_the_week_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|fm_cd_pick|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'fm_cd_pick';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_start_date',
        1 => 'group_image',
        2 => 'group_text_wrapper',
        3 => 'field_image',
        4 => 'group_description',
        5 => 'body',
        6 => 'title',
        7 => 'field_cd_label',
        8 => 'field_link',
        9 => 'field_primary_link',
      ),
    ),
    'fields' => array(
      'field_start_date' => 'ds_content',
      'group_image' => 'ds_content',
      'group_text_wrapper' => 'ds_content',
      'field_image' => 'ds_content',
      'group_description' => 'ds_content',
      'body' => 'ds_content',
      'title' => 'ds_content',
      'field_cd_label' => 'ds_content',
      'field_link' => 'ds_content',
      'field_primary_link' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|fm_cd_pick|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|fm_cd_pick|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'fm_cd_pick';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'field_cd_label',
        2 => 'group_image',
        3 => 'group_text_wrapper',
        4 => 'field_primary_link',
        5 => 'field_image',
        6 => 'group_description',
        7 => 'body',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'field_cd_label' => 'ds_content',
      'group_image' => 'ds_content',
      'group_text_wrapper' => 'ds_content',
      'field_primary_link' => 'ds_content',
      'field_image' => 'ds_content',
      'group_description' => 'ds_content',
      'body' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|fm_cd_pick|teaser'] = $ds_layout;

  return $export;
}
