<?php

/**
 * @file
 * weta_album_of_the_week.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function weta_album_of_the_week_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function weta_album_of_the_week_node_info() {
  $items = array(
    'fm_cd_pick' => array(
      'name' => t('FM Album of the Week'),
      'base' => 'node_content',
      'description' => t('CD Pick and Vocal CD Pick of the Week'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
