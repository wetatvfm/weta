<?php
/**
 * @file
 * weta_canvasser.features.inc
 */

/**
 * Implements hook_node_info().
 */
function weta_canvasser_node_info() {
  $items = array(
    'canvasser' => array(
      'name' => t('Canvasser'),
      'base' => 'node_content',
      'description' => t('Fundraising Canvasser'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
