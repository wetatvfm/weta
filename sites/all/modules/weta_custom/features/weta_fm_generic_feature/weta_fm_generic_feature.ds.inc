<?php

/**
 * @file
 * weta_fm_generic_feature.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function weta_fm_generic_feature_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|fm_generic_feature|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'fm_generic_feature';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h4',
        'class' => '',
      ),
    ),
  );
  $export['node|fm_generic_feature|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|fm_generic_feature|generic';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'fm_generic_feature';
  $ds_fieldsetting->view_mode = 'generic';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h3',
        'class' => '',
      ),
    ),
  );
  $export['node|fm_generic_feature|generic'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|fm_generic_feature|teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'fm_generic_feature';
  $ds_fieldsetting->view_mode = 'teaser';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h4',
        'class' => '',
      ),
    ),
  );
  $export['node|fm_generic_feature|teaser'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function weta_fm_generic_feature_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|fm_generic_feature|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'fm_generic_feature';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_image',
        1 => 'field_photo_credit',
        2 => 'title',
        3 => 'field_subtitle',
        4 => 'field_broadcast_date',
        5 => 'field_teaser',
        6 => 'field_featured_performances',
        7 => 'field_additional_info',
      ),
    ),
    'fields' => array(
      'field_image' => 'ds_content',
      'field_photo_credit' => 'ds_content',
      'title' => 'ds_content',
      'field_subtitle' => 'ds_content',
      'field_broadcast_date' => 'ds_content',
      'field_teaser' => 'ds_content',
      'field_featured_performances' => 'ds_content',
      'field_additional_info' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|fm_generic_feature|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|fm_generic_feature|generic';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'fm_generic_feature';
  $ds_layout->view_mode = 'generic';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'field_subtitle',
        2 => 'field_broadcast_date',
        3 => 'field_nso_audio_file',
        4 => 'field_image',
        5 => 'field_photo_credit',
        6 => 'field_teaser',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'field_subtitle' => 'ds_content',
      'field_broadcast_date' => 'ds_content',
      'field_nso_audio_file' => 'ds_content',
      'field_image' => 'ds_content',
      'field_photo_credit' => 'ds_content',
      'field_teaser' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|fm_generic_feature|generic'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|fm_generic_feature|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'fm_generic_feature';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'field_subtitle',
        2 => 'field_broadcast_date',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'field_subtitle' => 'ds_content',
      'field_broadcast_date' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|fm_generic_feature|teaser'] = $ds_layout;

  return $export;
}
