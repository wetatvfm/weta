<?php

/**
 * @file
 * weta_fm_generic_feature.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function weta_fm_generic_feature_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function weta_fm_generic_feature_node_info() {
  $items = array(
    'fm_generic_feature' => array(
      'name' => t('FM Generic Feature'),
      'base' => 'node_content',
      'description' => t('For Center Stage at Wolf Trap, Choral Showcase, Front Row Washington, Library of Congress and NSO Showcase.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
