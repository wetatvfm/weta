<?php

/**
 * @file
 * weta_fm_generic_feature.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function weta_fm_generic_feature_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_nso_showcase_audio|node|fm_generic_feature|form';
  $field_group->group_name = 'group_nso_showcase_audio';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'fm_generic_feature';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'NSO Showcase Audio',
    'weight' => '10',
    'children' => array(
      0 => 'field_nso_audio_file',
      1 => 'field_nso_expiration',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'NSO Showcase Audio',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-nso-showcase-audio field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_nso_showcase_audio|node|fm_generic_feature|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('NSO Showcase Audio');

  return $field_groups;
}
