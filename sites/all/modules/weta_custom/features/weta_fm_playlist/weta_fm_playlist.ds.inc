<?php
/**
 * @file
 * weta_fm_playlist.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function weta_fm_playlist_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|fm_playlist|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'fm_playlist';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h4',
        'class' => '',
      ),
    ),
  );
  $export['node|fm_playlist|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|fm_playlist|now_playing';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'fm_playlist';
  $ds_fieldsetting->view_mode = 'now_playing';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '17',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => '',
        'class' => '',
      ),
    ),
  );
  $export['node|fm_playlist|now_playing'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|fm_playlist|teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'fm_playlist';
  $ds_fieldsetting->view_mode = 'teaser';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'node_link' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|fm_playlist|teaser'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function weta_fm_playlist_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|fm_playlist|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'fm_playlist';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_feature',
        1 => 'field_countdown_number',
        2 => 'field_start_time',
        3 => 'title',
        4 => 'field_composer',
        5 => 'field_artist',
        6 => 'field_album',
        7 => 'field_album_title',
        8 => 'field_playlist_linktext',
        9 => 'field_buy_url',
      ),
    ),
    'fields' => array(
      'field_feature' => 'ds_content',
      'field_countdown_number' => 'ds_content',
      'field_start_time' => 'ds_content',
      'title' => 'ds_content',
      'field_composer' => 'ds_content',
      'field_artist' => 'ds_content',
      'field_album' => 'ds_content',
      'field_album_title' => 'ds_content',
      'field_playlist_linktext' => 'ds_content',
      'field_buy_url' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|fm_playlist|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|fm_playlist|now_playing';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'fm_playlist';
  $ds_layout->view_mode = 'now_playing';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'group_text_wrapper',
        1 => 'field_composer',
        2 => 'group_title',
        3 => 'title',
        4 => 'field_playlist_linktext',
        5 => 'field_buy_url',
      ),
    ),
    'fields' => array(
      'group_text_wrapper' => 'ds_content',
      'field_composer' => 'ds_content',
      'group_title' => 'ds_content',
      'title' => 'ds_content',
      'field_playlist_linktext' => 'ds_content',
      'field_buy_url' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 0,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|fm_playlist|now_playing'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|fm_playlist|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'fm_playlist';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'node_link',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'node_link' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|fm_playlist|teaser'] = $ds_layout;

  return $export;
}
