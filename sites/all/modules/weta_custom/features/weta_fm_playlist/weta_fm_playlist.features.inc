<?php
/**
 * @file
 * weta_fm_playlist.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function weta_fm_playlist_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function weta_fm_playlist_node_info() {
  $items = array(
    'fm_playlist' => array(
      'name' => t('FM Playlist'),
      'base' => 'node_content',
      'description' => t('FM Playlist items'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
