<?php
/**
 * @file
 * weta_fm_playlist.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function weta_fm_playlist_taxonomy_default_vocabularies() {
  return array(
    'fm_feature' => array(
      'name' => 'FM Feature',
      'machine_name' => 'fm_feature',
      'description' => 'FM Feature',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
