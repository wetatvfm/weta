<?php
/**
 * @file
 * weta_fm_playlist.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function weta_fm_playlist_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_text_wrapper|node|fm_playlist|now_playing';
  $field_group->group_name = 'group_text_wrapper';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'fm_playlist';
  $field_group->mode = 'now_playing';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Now Playing',
    'weight' => '14',
    'children' => array(
      0 => 'field_composer',
      1 => 'field_playlist_linktext',
      2 => 'field_buy_url',
      3 => 'group_title',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Now Playing',
      'instance_settings' => array(
        'classes' => '',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'id' => 'node_fm_playlist_now_playing_group_text_wrapper',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_text_wrapper|node|fm_playlist|now_playing'] = $field_group;

  return $export;
}
