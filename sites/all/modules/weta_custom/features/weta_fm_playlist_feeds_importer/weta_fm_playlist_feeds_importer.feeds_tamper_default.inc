<?php
/**
 * @file
 * weta_fm_playlist_feeds_importer.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function weta_fm_playlist_feeds_importer_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'fm_playlist-blank_source_1-explode';
  $feeds_tamper->importer = 'fm_playlist';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'explode';
  $feeds_tamper->settings = array(
    'separator' => '|',
    'limit' => '',
    'real_separator' => '|',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Explode Artists';
  $export['fm_playlist-blank_source_1-explode'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'fm_playlist-blank_source_1-rewrite';
  $feeds_tamper->importer = 'fm_playlist';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'rewrite';
  $feeds_tamper->settings = array(
    'text' => '[xpathparser:9]|[xpathparser:10]|[xpathparser:11]|[xpathparser:13]|[xpathparser:14]|[xpathparser:15]|[xpathparser:16]|[xpathparser:17]|[xpathparser:18]|[xpathparser:19]|[xpathparser:20]|[xpathparser:21]|[xpathparser:22]|[xpathparser:23]|[xpathparser:24]|[xpathparser:25]|[xpathparser:26]|[xpathparser:27]|[xpathparser:28]|[xpathparser:29]',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Rewrite Artists';
  $export['fm_playlist-blank_source_1-rewrite'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'fm_playlist-blank_source_2-explode_features';
  $feeds_tamper->importer = 'fm_playlist';
  $feeds_tamper->source = 'Blank source 2';
  $feeds_tamper->plugin_id = 'explode';
  $feeds_tamper->settings = array(
    'separator' => '|',
    'limit' => '',
    'real_separator' => '|',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Explode Features';
  $export['fm_playlist-blank_source_2-explode_features'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'fm_playlist-blank_source_2-rewrite_features';
  $feeds_tamper->importer = 'fm_playlist';
  $feeds_tamper->source = 'Blank source 2';
  $feeds_tamper->plugin_id = 'rewrite';
  $feeds_tamper->settings = array(
    'text' => '[xpathparser:30]|[xpathparser:31]|[xpathparser:32]|[xpathparser:33]|[xpathparser:34]',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Rewrite Features';
  $export['fm_playlist-blank_source_2-rewrite_features'] = $feeds_tamper;

  return $export;
}
