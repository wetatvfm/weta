<?php
/**
 * @file
 * weta_fm_playlist_upload.features.inc
 */

/**
 * Implements hook_node_info().
 */
function weta_fm_playlist_upload_node_info() {
  $items = array(
    'fm_playlist_upload' => array(
      'name' => t('FM Playlist Upload'),
      'base' => 'node_content',
      'description' => t('FM Playlist Upload'),
      'has_title' => '1',
      'title_label' => t('Date'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
