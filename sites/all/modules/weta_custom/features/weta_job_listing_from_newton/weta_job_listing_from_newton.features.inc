<?php
/**
 * @file
 * weta_job_listing_from_newton.features.inc
 */

/**
 * Implements hook_node_info().
 */
function weta_job_listing_from_newton_node_info() {
  $items = array(
    'job_listing' => array(
      'name' => t('Job Listing'),
      'base' => 'node_content',
      'description' => t('Job listings imported from Newton'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
