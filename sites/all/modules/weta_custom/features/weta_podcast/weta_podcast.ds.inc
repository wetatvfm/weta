<?php

/**
 * @file
 * weta_podcast.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function weta_podcast_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|podcast|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'podcast';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'two_col_3_9';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_podcast_image',
      ),
      'right' => array(
        1 => 'title',
        2 => 'field_podcast_display_date',
        3 => 'field_podcast_short_description',
        4 => 'field_podcast_episode',
      ),
    ),
    'fields' => array(
      'field_podcast_image' => 'left',
      'title' => 'right',
      'field_podcast_display_date' => 'right',
      'field_podcast_short_description' => 'right',
      'field_podcast_episode' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|podcast|teaser'] = $ds_layout;

  return $export;
}
