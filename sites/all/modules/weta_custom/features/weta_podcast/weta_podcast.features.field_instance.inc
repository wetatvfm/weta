<?php

/**
 * @file
 * weta_podcast.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function weta_podcast_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-podcast-field_podcast_display_date'.
  $field_instances['node-podcast-field_podcast_display_date'] = array(
    'bundle' => 'podcast',
    'deleted' => 0,
    'description' => 'The date a podcast episode becomes visible to the public. Note: The episode must also be published!',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long_date_only',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
          'show_remaining_days' => 0,
          'show_repeat_rule' => 'show',
        ),
        'type' => 'date_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long_date_only',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
          'show_remaining_days' => 0,
          'show_repeat_rule' => 'show',
        ),
        'type' => 'date_default',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'p',
    'field_name' => 'field_podcast_display_date',
    'label' => 'Display Date',
    'required' => 1,
    'settings' => array(
      'default_value' => 'now',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'display_all_day' => 0,
        'increment' => 15,
        'input_format' => 'm/d/Y - g:i:sa',
        'input_format_custom' => '',
        'label_position' => 'above',
        'no_fieldset' => 1,
        'repeat_collapsed' => 0,
        'text_parts' => array(),
        'year_range' => '-0:+1',
      ),
      'type' => 'date_popup',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'node-podcast-field_podcast_duration'.
  $field_instances['node-podcast-field_podcast_duration'] = array(
    'bundle' => 'podcast',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Format: hh:mm:ss',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 7,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_podcast_duration',
    'label' => 'Duration',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 8,
      ),
      'type' => 'text_textfield',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'node-podcast-field_podcast_episode'.
  $field_instances['node-podcast-field_podcast_episode'] = array(
    'bundle' => 'podcast',
    'deleted' => 0,
    'description' => 'The podcast episode audio file. The filename cannot contain spaces but may contain hyphens or underscores.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'jplayer',
        'settings' => array(
          'autoplay' => 0,
          'backgroundColor' => '000000',
          'continuous' => 0,
          'mode' => 'playlist',
          'muted' => FALSE,
          'preload' => 'metadata',
          'repeat' => 'none',
          'solution' => 'html, flash',
          'volume' => 80,
        ),
        'type' => 'jplayer_player',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'jplayer',
        'settings' => array(
          'autoplay' => 0,
          'backgroundColor' => '000000',
          'continuous' => 0,
          'mode' => 'playlist',
          'muted' => FALSE,
          'preload' => 'metadata',
          'repeat' => 'none',
          'solution' => 'html, flash',
          'volume' => 80,
        ),
        'type' => 'jplayer_player',
        'weight' => 16,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_podcast_episode',
    'label' => 'Episode',
    'required' => 1,
    'settings' => array(
      'description_field' => 0,
      'file_directory' => 'podcasts',
      'file_extensions' => 'mp3',
      'max_filesize' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'file',
      'settings' => array(
        'filefield_sources' => array(
          'filefield_sources' => array(
            'attach' => 'attach',
            'clipboard' => 0,
            'reference' => 0,
            'remote' => 0,
            'upload' => 'upload',
          ),
          'source_attach' => array(
            'absolute' => 0,
            'attach_mode' => 'move',
            'path' => 'podcast_upload',
          ),
          'source_imce' => array(
            'imce_mode' => 0,
          ),
          'source_reference' => array(
            'autocomplete' => 0,
            'search_all_fields' => 0,
          ),
        ),
        'insert' => 0,
        'insert_absolute' => 0,
        'insert_class' => '',
        'insert_default' => 'auto',
        'insert_styles' => array(
          'auto' => 'auto',
          'icon_link' => 0,
          'image' => 0,
          'image_16x9_feature' => 0,
          'image_calendar_basic' => 0,
          'image_calendar_highlight' => 0,
          'image_cropped_banner' => 0,
          'image_feature_square' => 0,
          'image_flexslider_full' => 0,
          'image_flexslider_thumbnail' => 0,
          'image_flyout_highlight' => 0,
          'image_fm_feature_logo' => 0,
          'image_full' => 0,
          'image_full_width' => 0,
          'image_hero' => 0,
          'image_hero_slide' => 0,
          'image_highlight_20-80' => 0,
          'image_kids_crop' => 0,
          'image_large' => 0,
          'image_media_thumbnail' => 0,
          'image_medium' => 0,
          'image_merlin' => 0,
          'image_mini_feature' => 0,
          'image_now_playing_logo' => 0,
          'image_productions_highlight' => 0,
          'image_sidebar_strip' => 0,
          'image_skinny_strip' => 0,
          'image_square_thumbnail' => 0,
          'image_squarish' => 0,
          'image_thumbnail' => 0,
          'image_tight_crop' => 0,
          'image_tv_banner' => 0,
          'image_wanted' => 0,
          'image_wanted_thumbnail' => 0,
          'image_weta_player' => 0,
          'image_wide_feature' => 0,
          'link' => 0,
        ),
        'insert_width' => '',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'file_generic',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'node-podcast-field_podcast_episode_number'.
  $field_instances['node-podcast-field_podcast_episode_number'] = array(
    'bundle' => 'podcast',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 9,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'div',
    'field_name' => 'field_podcast_episode_number',
    'label' => 'Episode Number',
    'required' => 1,
    'settings' => array(
      'max' => '',
      'min' => '',
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'node-podcast-field_podcast_episode_subtitle'.
  $field_instances['node-podcast-field_podcast_episode_subtitle'] = array(
    'bundle' => 'podcast',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'A single, descriptive sentence of the episode.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 5,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'div',
    'field_name' => 'field_podcast_episode_subtitle',
    'label' => 'Episode Subtitle',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 7,
    ),
  );

  // Exported field_instance: 'node-podcast-field_podcast_episode_type'.
  $field_instances['node-podcast-field_podcast_episode_type'] = array(
    'bundle' => 'podcast',
    'default_value' => array(
      0 => array(
        'value' => 'full',
      ),
    ),
    'deleted' => 0,
    'description' => 'What type of episode is this? It is almost always going to be a Regular Episode, but could be a trailer or bonus content.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 8,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'div',
    'field_name' => 'field_podcast_episode_type',
    'label' => 'Episode Type',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'node-podcast-field_podcast_full_description'.
  $field_instances['node-podcast-field_podcast_full_description'] = array(
    'bundle' => 'podcast',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'A full description of the podcast episode. This should still be concise and no more than one paragraph. Additional information can be included in the Show Notes field, which is unlimited.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 14,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'div',
    'field_name' => 'field_podcast_full_description',
    'label' => 'Full Description',
    'required' => 1,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 9,
    ),
  );

  // Exported field_instance: 'node-podcast-field_podcast_image'.
  $field_instances['node-podcast-field_podcast_image'] = array(
    'bundle' => 'podcast',
    'deleted' => 0,
    'description' => 'Artwork for this episode. Requirements: 
<ul>
<li>A minimum size of 1400 x 1400 pixels and a maximum size of 3000 x 3000 pixels</li>
<li>72 dpi</li>
<li>JPEG or PNG format with appropriate file extensions (.jpg, .png)</li>
<li> RGB colorspace</li>
</ul>
To optimize images for mobile devices, you should compress your image files.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => 'calendar_basic',
        ),
        'type' => 'image',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => 'content',
          'image_style' => 'calendar_basic',
        ),
        'type' => 'image',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'div',
    'field_name' => 'field_podcast_image',
    'label' => 'Image',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 23784,
      'file_directory' => 'podcasts',
      'file_extensions' => 'png jpg',
      'max_filesize' => '',
      'max_resolution' => '3000x3000',
      'min_resolution' => '1400x1400',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'allowed_schemes' => array(
          'private' => 0,
          'public' => 'public',
        ),
        'allowed_types' => array(
          'audio' => 0,
          'document' => 0,
          'image' => 'image',
          'video' => 0,
        ),
        'browser_plugins' => array(
          'media_default--media_browser_1' => 'media_default--media_browser_1',
          'media_default--media_browser_my_files' => 'media_default--media_browser_my_files',
          'upload' => 'upload',
        ),
        'filefield_sources' => array(
          'filefield_sources' => array(),
          'source_attach' => array(
            'absolute' => 0,
            'attach_mode' => 'move',
            'path' => 'file_attach',
          ),
          'source_imce' => array(
            'imce_mode' => 0,
          ),
          'source_reference' => array(
            'autocomplete' => 0,
          ),
        ),
        'insert' => 0,
        'insert_absolute' => FALSE,
        'insert_class' => '',
        'insert_default' => array(
          0 => 'auto',
        ),
        'insert_styles' => array(
          0 => 'auto',
        ),
        'insert_width' => '',
        'manualcrop_crop_info' => 1,
        'manualcrop_default_crop_area' => 1,
        'manualcrop_enable' => 0,
        'manualcrop_filter_insert' => TRUE,
        'manualcrop_inline_crop' => 0,
        'manualcrop_instant_crop' => FALSE,
        'manualcrop_instant_preview' => 1,
        'manualcrop_keyboard' => 1,
        'manualcrop_maximize_default_crop_area' => 0,
        'manualcrop_require_cropping' => array(),
        'manualcrop_styles_list' => array(),
        'manualcrop_styles_mode' => 'include',
        'manualcrop_thumblist' => 0,
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'node-podcast-field_podcast_short_description'.
  $field_instances['node-podcast-field_podcast_short_description'] = array(
    'bundle' => 'podcast',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'A short description of the podcast episode. It should be no more than 280 characters and will be the default text for sharing on social media.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 6,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 3,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_podcast_short_description',
    'label' => 'Short Description',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 8,
    ),
  );

  // Exported field_instance: 'node-podcast-field_podcast_show_notes'.
  $field_instances['node-podcast-field_podcast_show_notes'] = array(
    'bundle' => 'podcast',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Any additional information about the podcast episode -- links, footnotes, video clips, tangents, etc.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 4,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 15,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'div',
    'field_name' => 'field_podcast_show_notes',
    'label' => 'Show Notes',
    'required' => 0,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 10,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('A full description of the podcast episode. This should still be concise and no more than one paragraph. Additional information can be included in the Show Notes field, which is unlimited.');
  t('A short description of the podcast episode. It should be no more than 280 characters and will be the default text for sharing on social media.');
  t('A single, descriptive sentence of the episode.');
  t('Any additional information about the podcast episode -- links, footnotes, video clips, tangents, etc.');
  t('Artwork for this episode. Requirements: 
<ul>
<li>A minimum size of 1400 x 1400 pixels and a maximum size of 3000 x 3000 pixels</li>
<li>72 dpi</li>
<li>JPEG or PNG format with appropriate file extensions (.jpg, .png)</li>
<li> RGB colorspace</li>
</ul>
To optimize images for mobile devices, you should compress your image files.');
  t('Display Date');
  t('Duration');
  t('Episode');
  t('Episode Number');
  t('Episode Subtitle');
  t('Episode Type');
  t('Format: hh:mm:ss');
  t('Full Description');
  t('Image');
  t('Short Description');
  t('Show Notes');
  t('The date a podcast episode becomes visible to the public. Note: The episode must also be published!');
  t('The podcast episode audio file. The filename cannot contain spaces but may contain hyphens or underscores.');
  t('What type of episode is this? It is almost always going to be a Regular Episode, but could be a trailer or bonus content.');

  return $field_instances;
}
