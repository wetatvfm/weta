<?php
/**
 * @file
 * weta_responsive_lightbox.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function weta_responsive_lightbox_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_resp_lb_addtext_button1'.
  $field_bases['field_resp_lb_addtext_button1'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_resp_lb_addtext_button1',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_resp_lb_addtext_button2'.
  $field_bases['field_resp_lb_addtext_button2'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_resp_lb_addtext_button2',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_resp_lb_addtext_button3'.
  $field_bases['field_resp_lb_addtext_button3'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_resp_lb_addtext_button3',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_resp_lb_bgimage_desktop'.
  $field_bases['field_resp_lb_bgimage_desktop'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_resp_lb_bgimage_desktop',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'fid' => array(
        0 => 'fid',
      ),
    ),
    'locked' => 0,
    'module' => 'image',
    'settings' => array(
      'default_image' => 0,
      'uri_scheme' => 'public',
    ),
    'translatable' => 0,
    'type' => 'image',
  );

  // Exported field_base: 'field_resp_lb_bgimage_mobile'.
  $field_bases['field_resp_lb_bgimage_mobile'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_resp_lb_bgimage_mobile',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'fid' => array(
        0 => 'fid',
      ),
    ),
    'locked' => 0,
    'module' => 'image',
    'settings' => array(
      'default_image' => 0,
      'uri_scheme' => 'public',
    ),
    'translatable' => 0,
    'type' => 'image',
  );

  // Exported field_base: 'field_resp_lb_button_alignment'.
  $field_bases['field_resp_lb_button_alignment'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_resp_lb_button_alignment',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'left' => 'Left',
        'center' => 'Center',
        'right' => 'Right',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_resp_lb_buttons'.
  $field_bases['field_resp_lb_buttons'] = array(
    'active' => 1,
    'cardinality' => 3,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_resp_lb_buttons',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'link',
    'settings' => array(
      'attributes' => array(
        'class' => '',
        'rel' => '',
        'target' => 'default',
      ),
      'display' => array(
        'url_cutoff' => 80,
      ),
      'enable_tokens' => 1,
      'title' => 'optional',
      'title_maxlength' => 128,
      'title_value' => '',
      'url' => 0,
    ),
    'translatable' => 0,
    'type' => 'link_field',
  );

  // Exported field_base: 'field_resp_lb_footer'.
  $field_bases['field_resp_lb_footer'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_resp_lb_footer',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_resp_lb_footer_color'.
  $field_bases['field_resp_lb_footer_color'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_resp_lb_footer_color',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'white' => 'White',
        'black' => 'Black',
        'red' => 'Red',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_resp_lb_form'.
  $field_bases['field_resp_lb_form'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_resp_lb_form',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'nid' => array(
        0 => 'nid',
      ),
    ),
    'locked' => 0,
    'module' => 'node_reference',
    'settings' => array(
      'referenceable_types' => array(
        'audio_file' => 0,
        'basic_page' => 0,
        'blog_post' => 0,
        'board_meetings' => 0,
        'bracket_match' => 0,
        'bracket_team' => 0,
        'calendar_event' => 0,
        'canvasser' => 0,
        'carousel' => 0,
        'cove_video' => 0,
        'feature_block' => 0,
        'featured_links' => 0,
        'flyout_links' => 0,
        'fm_cd_pick' => 0,
        'fm_channels' => 0,
        'fm_classical_conversations' => 0,
        'fm_generic_feature' => 0,
        'fm_opera_at_eight' => 0,
        'fm_opera_at_eight_upload' => 0,
        'fm_opera_encore' => 0,
        'fm_opera_encore_upload' => 0,
        'fm_opera_house' => 0,
        'fm_playlist' => 0,
        'fm_playlist_upload' => 0,
        'fm_shifts' => 0,
        'fm_theme_events' => 0,
        'fm_themes' => 0,
        'giveaway' => 0,
        'homepage_dates' => 0,
        'house_ad' => 0,
        'internship' => 0,
        'job_listing' => 0,
        'job_opening' => 0,
        'lightbox' => 0,
        'map_points' => 0,
        'pmp_story' => 0,
        'pmp_video' => 0,
        'preroll' => 0,
        'press_contact' => 0,
        'press_image' => 0,
        'press_page' => 0,
        'press_program' => 0,
        'press_release' => 0,
        'quiz_personality' => 0,
        'quiz_results' => 0,
        'quiz_trivia' => 0,
        'responsive_lightbox' => 0,
        'slideshow' => 0,
        'station_relations_page' => 0,
        'station_relations_subpage' => 0,
        'status_update' => 0,
        'timeline_item' => 0,
        'tv_channels' => 0,
        'tv_series' => 0,
        'tv_series_custom' => 0,
        'venue' => 0,
        'video_upload' => 0,
        'volunteer_sponsor' => 0,
        'volunteer_spotlight' => 0,
        'wanted_poster' => 0,
        'webform' => 'webform',
        'webform_display' => 0,
        'weta_player_ad_hack' => 0,
        'youtube_promos' => 0,
      ),
      'view' => array(
        'args' => array(),
        'display_name' => '',
        'view_name' => '',
      ),
    ),
    'translatable' => 0,
    'type' => 'node_reference',
  );

  // Exported field_base: 'field_resp_lb_headline'.
  $field_bases['field_resp_lb_headline'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_resp_lb_headline',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_resp_lb_headline_alignment'.
  $field_bases['field_resp_lb_headline_alignment'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_resp_lb_headline_alignment',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'full_center' => 'Full Width, Centered',
        'full_left' => 'Full Width, Left Aligned',
        'half_left' => 'Half Width, Left Aligned',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_resp_lb_headline_color'.
  $field_bases['field_resp_lb_headline_color'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_resp_lb_headline_color',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'white' => 'White',
        'black' => 'Black',
        'red' => 'Red',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_resp_lb_lightbox_type'.
  $field_bases['field_resp_lb_lightbox_type'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_resp_lb_lightbox_type',
    'field_permissions' => array(
      'type' => 1,
    ),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'buttons' => 'Buttons',
        'form' => 'Form',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_resp_lb_main_text'.
  $field_bases['field_resp_lb_main_text'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_resp_lb_main_text',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'text_long',
  );

  // Exported field_base: 'field_resp_lb_main_text_align'.
  $field_bases['field_resp_lb_main_text_align'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_resp_lb_main_text_align',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'full_center' => 'Full Width, Centered',
        'full_left' => 'Full Width, Left Aligned',
        'half_left' => 'Half Width, Left Aligned',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_resp_lb_main_text_color'.
  $field_bases['field_resp_lb_main_text_color'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_resp_lb_main_text_color',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'white' => 'White',
        'black' => 'Black',
        'red' => 'Red',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_resp_lb_main_text_image'.
  $field_bases['field_resp_lb_main_text_image'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_resp_lb_main_text_image',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'fid' => array(
        0 => 'fid',
      ),
    ),
    'locked' => 0,
    'module' => 'image',
    'settings' => array(
      'default_image' => 0,
      'uri_scheme' => 'public',
    ),
    'translatable' => 0,
    'type' => 'image',
  );

  // Exported field_base: 'field_resp_lb_mobile_bg_color'.
  $field_bases['field_resp_lb_mobile_bg_color'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_resp_lb_mobile_bg_color',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 6,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_resp_lb_schedule'.
  $field_bases['field_resp_lb_schedule'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_resp_lb_schedule',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'date',
    'settings' => array(
      'cache_count' => 4,
      'cache_enabled' => 0,
      'granularity' => array(
        'day' => 'day',
        'hour' => 'hour',
        'minute' => 'minute',
        'month' => 'month',
        'second' => 0,
        'year' => 'year',
      ),
      'repeat' => 0,
      'timezone_db' => '',
      'todate' => 'required',
      'tz_handling' => 'none',
    ),
    'translatable' => 0,
    'type' => 'datetime',
  );

  return $field_bases;
}
