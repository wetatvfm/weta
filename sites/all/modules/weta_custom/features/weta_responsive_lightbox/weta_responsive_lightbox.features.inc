<?php
/**
 * @file
 * weta_responsive_lightbox.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function weta_responsive_lightbox_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function weta_responsive_lightbox_node_info() {
  $items = array(
    'responsive_lightbox' => array(
      'name' => t('Lightbox (Responsive)'),
      'base' => 'node_content',
      'description' => t('Responsive lightbox with multiple display options'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
