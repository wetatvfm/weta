<?php
/**
 * @file
 * weta_responsive_lightbox.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function weta_responsive_lightbox_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = TRUE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_addtext_buttons|node|responsive_lightbox|form';
  $field_group->group_name = 'group_addtext_buttons';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'responsive_lightbox';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Additional Button Text',
    'weight' => '12',
    'children' => array(
      0 => 'field_resp_lb_addtext_button1',
      1 => 'field_resp_lb_addtext_button2',
      2 => 'field_resp_lb_addtext_button3',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Additional Button Text',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-addtext-buttons field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_addtext_buttons|node|responsive_lightbox|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_background_image|node|responsive_lightbox|form';
  $field_group->group_name = 'group_background_image';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'responsive_lightbox';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Background Image',
    'weight' => '8',
    'children' => array(
      0 => 'field_resp_lb_bgimage_desktop',
      1 => 'field_resp_lb_bgimage_mobile',
      2 => 'field_resp_lb_mobile_bg_color',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-background-image field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_background_image|node|responsive_lightbox|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_footer|node|responsive_lightbox|form';
  $field_group->group_name = 'group_footer';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'responsive_lightbox';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Footer',
    'weight' => '18',
    'children' => array(
      0 => 'field_resp_lb_footer',
      1 => 'field_resp_lb_footer_color',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-footer field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_footer|node|responsive_lightbox|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_headline|node|responsive_lightbox|form';
  $field_group->group_name = 'group_headline';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'responsive_lightbox';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Headline',
    'weight' => '9',
    'children' => array(
      0 => 'field_resp_lb_headline',
      1 => 'field_resp_lb_headline_color',
      2 => 'field_resp_lb_headline_alignment',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-headline field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_headline|node|responsive_lightbox|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_main_text|node|responsive_lightbox|form';
  $field_group->group_name = 'group_main_text';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'responsive_lightbox';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Main Text',
    'weight' => '10',
    'children' => array(
      0 => 'field_resp_lb_main_text',
      1 => 'field_resp_lb_main_text_color',
      2 => 'field_resp_lb_main_text_align',
      3 => 'field_resp_lb_main_text_image',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-main-text field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_main_text|node|responsive_lightbox|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Additional Button Text');
  t('Background Image');
  t('Footer');
  t('Headline');
  t('Main Text');

  return $field_groups;
}
