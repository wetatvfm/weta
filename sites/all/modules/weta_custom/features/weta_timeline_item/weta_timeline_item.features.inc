<?php
/**
 * @file
 * weta_timeline_item.features.inc
 */

/**
 * Implements hook_node_info().
 */
function weta_timeline_item_node_info() {
  $items = array(
    'timeline_item' => array(
      'name' => t('Timeline Item'),
      'base' => 'node_content',
      'description' => t('Items for historical timelines'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
