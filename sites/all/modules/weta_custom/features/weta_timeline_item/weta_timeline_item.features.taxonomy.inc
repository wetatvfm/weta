<?php
/**
 * @file
 * weta_timeline_item.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function weta_timeline_item_taxonomy_default_vocabularies() {
  return array(
    'timeline' => array(
      'name' => 'Timeline',
      'machine_name' => 'timeline',
      'description' => 'For filtering timeline views',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
