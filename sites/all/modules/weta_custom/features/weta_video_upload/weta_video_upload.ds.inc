<?php
/**
 * @file
 * weta_video_upload.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function weta_video_upload_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|video_upload|highlight_20_80';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'video_upload';
  $ds_fieldsetting->view_mode = 'highlight_20_80';
  $ds_fieldsetting->settings = array(
    'modal_player_link' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h3',
        'class' => '',
      ),
    ),
  );
  $export['node|video_upload|highlight_20_80'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|video_upload|teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'video_upload';
  $ds_fieldsetting->view_mode = 'teaser';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h3',
        'class' => '',
      ),
    ),
  );
  $export['node|video_upload|teaser'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function weta_video_upload_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|video_upload|highlight_20_80';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'video_upload';
  $ds_layout->view_mode = 'highlight_20_80';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'field_feature_image',
        2 => 'group_text_wrapper',
        3 => 'field_description',
        4 => 'field_video',
        5 => 'modal_player_link',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'field_feature_image' => 'ds_content',
      'group_text_wrapper' => 'ds_content',
      'field_description' => 'ds_content',
      'field_video' => 'ds_content',
      'modal_player_link' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|video_upload|highlight_20_80'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|video_upload|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'video_upload';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'field_video',
        2 => 'field_description',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'field_video' => 'ds_content',
      'field_description' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|video_upload|teaser'] = $ds_layout;

  return $export;
}
