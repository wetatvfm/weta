<?php
/**
 * @file
 * weta_video_upload.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function weta_video_upload_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-video_upload-field_description'
  $field_instances['node-video_upload-field_description'] = array(
    'bundle' => 'video_upload',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'highlight_20_80' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'div',
    'field_name' => 'field_description',
    'label' => 'Description',
    'required' => 0,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'node-video_upload-field_feature_image'
  $field_instances['node-video_upload-field_feature_image'] = array(
    'bundle' => 'video_upload',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 2,
      ),
      'highlight_20_80' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => 'highlight_20-80',
        ),
        'type' => 'image',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 14,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'div',
    'field_name' => 'field_feature_image',
    'label' => 'Image',
    'required' => 0,
    'settings' => array(
      'alt_field' => 1,
      'default_image' => 0,
      'file_directory' => 'video_upload',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'filefield_sources' => array(
          'filefield_sources' => array(
            'attach' => 0,
            'clipboard' => 0,
            'reference' => 0,
            'remote' => 0,
            'upload' => 'upload',
          ),
          'source_attach' => array(
            'absolute' => 0,
            'attach_mode' => 'move',
            'path' => 'file_attach',
          ),
          'source_imce' => array(
            'imce_mode' => 0,
          ),
          'source_reference' => array(
            'autocomplete' => 0,
          ),
        ),
        'insert' => 0,
        'insert_absolute' => 0,
        'insert_class' => '',
        'insert_default' => 'auto',
        'insert_styles' => array(
          'auto' => 'auto',
          'icon_link' => 0,
          'image' => 0,
          'image_calendar_basic' => 0,
          'image_calendar_highlight' => 0,
          'image_cropped_banner' => 0,
          'image_feature_square' => 0,
          'image_flexslider_full' => 0,
          'image_flyout_highlight' => 0,
          'image_fm_feature_logo' => 0,
          'image_full' => 0,
          'image_full_width' => 0,
          'image_highlight_20-80' => 0,
          'image_kids_crop' => 0,
          'image_large' => 0,
          'image_media_thumbnail' => 0,
          'image_medium' => 0,
          'image_merlin' => 0,
          'image_mini_feature' => 0,
          'image_now_playing_logo' => 0,
          'image_productions_highlight' => 0,
          'image_squarish' => 0,
          'image_thumbnail' => 0,
          'image_tight_crop' => 0,
          'image_tv_banner' => 0,
          'image_wanted' => 0,
          'image_wanted_thumbnail' => 0,
          'image_weta_player' => 0,
          'image_wide_feature' => 0,
          'link' => 0,
        ),
        'insert_width' => '',
        'manualcrop_crop_info' => 1,
        'manualcrop_default_crop_area' => 1,
        'manualcrop_enable' => 1,
        'manualcrop_filter_insert' => 1,
        'manualcrop_inline_crop' => 0,
        'manualcrop_instant_crop' => 0,
        'manualcrop_instant_preview' => 1,
        'manualcrop_keyboard' => TRUE,
        'manualcrop_maximize_default_crop_area' => FALSE,
        'manualcrop_require_cropping' => array(
          'highlight_20-80' => 'highlight_20-80',
        ),
        'manualcrop_styles_list' => array(
          'highlight_20-80' => 'highlight_20-80',
        ),
        'manualcrop_styles_mode' => 'exclude',
        'manualcrop_thumblist' => 0,
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 8,
    ),
  );

  // Exported field_instance: 'node-video_upload-field_video'
  $field_instances['node-video_upload-field_video'] = array(
    'bundle' => 'video_upload',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'jw_player',
        'settings' => array(
          'check_support' => FALSE,
          'jwplayer_preset' => 'default',
        ),
        'type' => 'jw_player',
        'weight' => 0,
      ),
      'highlight_20_80' => array(
        'label' => 'hidden',
        'module' => 'jw_player',
        'settings' => array(
          'check_support' => 0,
          'jwplayer_preset' => 'default',
        ),
        'type' => 'jw_player',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'jw_player',
        'settings' => array(
          'check_support' => FALSE,
          'jwplayer_preset' => 'default',
        ),
        'type' => 'jw_player',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'div',
    'field_name' => 'field_video',
    'label' => 'Video',
    'required' => 1,
    'settings' => array(
      'description_field' => 0,
      'file_directory' => 'video_upload',
      'file_extensions' => 'mp4 flv',
      'max_filesize' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'file',
      'settings' => array(
        'filefield_sources' => array(
          'filefield_sources' => array(
            'attach' => 0,
            'clipboard' => 0,
            'reference' => 0,
            'remote' => 0,
            'upload' => 'upload',
          ),
          'source_attach' => array(
            'absolute' => 0,
            'attach_mode' => 'move',
            'path' => 'file_attach',
          ),
          'source_imce' => array(
            'imce_mode' => 0,
          ),
          'source_reference' => array(
            'autocomplete' => 0,
          ),
        ),
        'insert' => 0,
        'insert_absolute' => 0,
        'insert_class' => '',
        'insert_default' => 'auto',
        'insert_styles' => array(
          'auto' => 'auto',
          'icon_link' => 0,
          'image' => 0,
          'image_calendar_basic' => 0,
          'image_calendar_highlight' => 0,
          'image_cropped_banner' => 0,
          'image_feature_square' => 0,
          'image_flexslider_full' => 0,
          'image_flyout_highlight' => 0,
          'image_fm_feature_logo' => 0,
          'image_full' => 0,
          'image_full_width' => 0,
          'image_highlight_20-80' => 0,
          'image_kids_crop' => 0,
          'image_large' => 0,
          'image_media_thumbnail' => 0,
          'image_medium' => 0,
          'image_merlin' => 0,
          'image_mini_feature' => 0,
          'image_now_playing_logo' => 0,
          'image_productions_highlight' => 0,
          'image_squarish' => 0,
          'image_thumbnail' => 0,
          'image_tight_crop' => 0,
          'image_tv_banner' => 0,
          'image_wanted' => 0,
          'image_wanted_thumbnail' => 0,
          'image_weta_player' => 0,
          'image_wide_feature' => 0,
          'link' => 0,
        ),
        'insert_width' => '',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'file_generic',
      'weight' => 9,
    ),
  );

  // Exported field_instance: 'node-video_upload-field_video_series'
  $field_instances['node-video_upload-field_video_series'] = array(
    'bundle' => 'video_upload',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 3,
      ),
      'highlight_20_80' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 12,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'div',
    'field_name' => 'field_video_series',
    'label' => 'Video Series',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 7,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Description');
  t('Image');
  t('Video');
  t('Video Series');

  return $field_instances;
}
