<?php
/**
 * @file
 * weta_video_upload.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function weta_video_upload_taxonomy_default_vocabularies() {
  return array(
    'video_series' => array(
      'name' => 'Video Series',
      'machine_name' => 'video_series',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
