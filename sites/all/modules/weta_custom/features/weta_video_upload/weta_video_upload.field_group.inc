<?php
/**
 * @file
 * weta_video_upload.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function weta_video_upload_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_text_wrapper|node|video_upload|highlight_20_80';
  $field_group->group_name = 'group_text_wrapper';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'video_upload';
  $field_group->mode = 'highlight_20_80';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Text Wrapper',
    'weight' => '1',
    'children' => array(
      0 => 'field_description',
      1 => 'modal_player_link',
      2 => 'title',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Text Wrapper',
      'instance_settings' => array(
        'classes' => 'span_9 col',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'id' => 'node_video_upload_highlight_20_80_group_text_wrapper',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_text_wrapper|node|video_upload|highlight_20_80'] = $field_group;

  return $export;
}
