<?php
/**
 * @file
 * weta_video_upload.jw_player_presets.inc
 */

/**
 * Implements hook_default_jw_player_presets().
 */
function weta_video_upload_default_jw_player_presets() {
  $export = array();

  $jw_player_preset = new stdClass();
  $jw_player_preset->disabled = FALSE; /* Edit this to true to make a default jw_player_preset disabled initially */
  $jw_player_preset->api_version = 1;
  $jw_player_preset->preset_name = 'Default';
  $jw_player_preset->machine_name = 'default';
  $jw_player_preset->description = '';
  $jw_player_preset->settings = array(
    'mode' => 'html5',
    'width' => '480',
    'height' => '360',
    'controlbar' => 'over',
    'skin' => '',
    'autoplay' => 0,
  );
  $export['default'] = $jw_player_preset;

  return $export;
}
