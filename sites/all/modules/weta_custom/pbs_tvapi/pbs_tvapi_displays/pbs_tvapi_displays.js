(function ($) {
  // TV Episodes Functions
  Drupal.behaviors.WETATruncateTVListings = {
    attach: function (context, settings) {

      /**
       * initializes eTruncate to truncate all the elements with the class
       * "more" within the div#block-pbs-tvapi-upcoming-byseries table.episodes
       * it also places the button inside the element with the id
       * "buttonContainer" (created dynamically before)
       * and changes the default button label.
      */

      $("#block-pbs-tvapi-displays-upcoming-byseries table.episodes").once().eTruncate({
        buttonContainer: "#block-pbs-tvapi-displays-upcoming-byseries .episodes-bottom p",
        showText: "See More Episodes",
        hideText: "See Fewer Episodes"
      });
    }
  };
}(jQuery));
