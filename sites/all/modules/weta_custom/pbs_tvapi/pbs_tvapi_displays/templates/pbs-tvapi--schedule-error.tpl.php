<?php

/**
 * @file
 * Theme error messages for schedule grid.
 *
 * @param $message
 *   The error message.
 *
 * @ingroup themeable
 */

?>

<div class="container">
<div class="span_12 col">
  <div class="alert_symbol">
    <div class="alert">
      <div class="ds-1col clearfix">
        <?php print $message; ?>
      </div>
    </div>
  </div>
</div>
</div>
