<?php
/**
 * @file
 * Custom Rules code: actions, conditions and events.
 */



/**
 * Implements hook_rules_action_info().
 *
 * Declares any meta-data about actions for Rules.
 */

function rules_custom_rules_action_info() {
  $actions = array(
    'rules_custom_node_clear_cache' => array(
      'label' => t('Clear all caches for a specific node on save'),
      'group' => t('Rules custom'),
      'parameter' => array(
        'node' => array(
          'type' => 'node',
          'label' => t('Node'),
          'save' => TRUE,
        ),
      ),
    ),
  );

  return $actions;
}


/**
 * The action function for 'rules_custom_node_clear_cache'.
 */
function rules_custom_node_clear_cache($node) {
  $pages = array();

  // Construct system paths.
  $system_path = 'node/' . $node->nid;
  $pages[] = $system_path;
  $cid = url($system_path, array('absolute' => TRUE));

  // Construct alias paths.
  if (isset($node->path) && !empty($node->path['alias'])) {
    $alias_path = $node->path['alias'];
    $pages[] = $alias_path;
    // Because the alias always takes precedence, we can overwrite the $cid.
    $cid = url($alias_path, array('absolute' => TRUE));
  }

  // Clear the Drupal cache for this page.
  cache_clear_all($cid, 'cache_page');

  // Clear the Varnish cache for this page.
  varnish_purge_paths($_SERVER['HTTP_HOST'], $pages);
  watchdog('rules_custom', t('The cache has been cleared for %bundle: %link.', array('%bundle' => $node->type, '%link' => $cid)), NULL, WATCHDOG_INFO, NULL);
}
