<?php
/**
 * @file
 * volunteer_spotlight.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function volunteer_spotlight_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|volunteer_spotlight|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'volunteer_spotlight';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'group_image',
        1 => 'title',
        2 => 'field_teaser',
        3 => 'field_volunteer_image',
        4 => 'node_link',
        5 => 'group_text_wrapper',
      ),
    ),
    'fields' => array(
      'group_image' => 'ds_content',
      'title' => 'ds_content',
      'field_teaser' => 'ds_content',
      'field_volunteer_image' => 'ds_content',
      'node_link' => 'ds_content',
      'group_text_wrapper' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|volunteer_spotlight|teaser'] = $ds_layout;

  return $export;
}
