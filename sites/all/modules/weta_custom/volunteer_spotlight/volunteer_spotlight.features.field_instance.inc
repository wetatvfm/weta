<?php
/**
 * @file
 * volunteer_spotlight.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function volunteer_spotlight_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-volunteer_spotlight-field_description'
  $field_instances['node-volunteer_spotlight-field_description'] = array(
    'bundle' => 'volunteer_spotlight',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 14,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_description',
    'label' => 'Bio',
    'required' => 0,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 9,
    ),
  );

  // Exported field_instance: 'node-volunteer_spotlight-field_short_title'
  $field_instances['node-volunteer_spotlight-field_short_title'] = array(
    'bundle' => 'volunteer_spotlight',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_short_title',
    'label' => 'Volunteer Name',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 7,
    ),
  );

  // Exported field_instance: 'node-volunteer_spotlight-field_teaser'
  $field_instances['node-volunteer_spotlight-field_teaser'] = array(
    'bundle' => 'volunteer_spotlight',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Short teaser for the full bio.  This appears on the Volunteer landing page with a link to click through to read the full bio.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_teaser',
    'label' => 'Teaser Text',
    'required' => 1,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 8,
    ),
  );

  // Exported field_instance: 'node-volunteer_spotlight-field_volunteer_image'
  $field_instances['node-volunteer_spotlight-field_volunteer_image'] = array(
    'bundle' => 'volunteer_spotlight',
    'deleted' => 0,
    'description' => 'The cropped version of this image will appear in the teaser on the Volunteer landing page.  You can also insert the full image into the bio by placing your cursor within the WYSIWYG where you\'d like the image to appear, then clicking the "Insert" button. Then click on the image and from the Styles dropdown choose whether it should be aligned left or right.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => 'content',
          'image_style' => 'highlight_20-80',
        ),
        'type' => 'image',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_volunteer_image',
    'label' => 'Volunteer Image',
    'required' => 0,
    'settings' => array(
      'alt_field' => 1,
      'default_image' => 0,
      'file_directory' => 'volunteers',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'filefield_sources' => array(
          'filefield_sources' => array(
            'attach' => 0,
            'clipboard' => 0,
            'reference' => 0,
            'remote' => 0,
            'upload' => 'upload',
          ),
          'source_attach' => array(
            'absolute' => 0,
            'attach_mode' => 'move',
            'path' => 'file_attach',
          ),
          'source_imce' => array(
            'imce_mode' => 0,
          ),
          'source_reference' => array(
            'autocomplete' => 0,
          ),
        ),
        'insert' => 1,
        'insert_absolute' => 0,
        'insert_class' => '',
        'insert_default' => 'image_large',
        'insert_styles' => array(
          'auto' => 0,
          'icon_link' => 0,
          'image' => 0,
          'image_16x9_feature' => 0,
          'image_calendar_basic' => 0,
          'image_calendar_highlight' => 0,
          'image_cropped_banner' => 0,
          'image_feature_square' => 0,
          'image_flexslider_full' => 0,
          'image_flyout_highlight' => 0,
          'image_fm_feature_logo' => 0,
          'image_full' => 0,
          'image_full_width' => 0,
          'image_highlight_20-80' => 0,
          'image_kids_crop' => 0,
          'image_large' => 0,
          'image_media_thumbnail' => 0,
          'image_medium' => 0,
          'image_merlin' => 0,
          'image_mini_feature' => 0,
          'image_now_playing_logo' => 0,
          'image_productions_highlight' => 0,
          'image_sidebar_strip' => 0,
          'image_squarish' => 0,
          'image_thumbnail' => 0,
          'image_tight_crop' => 0,
          'image_tv_banner' => 0,
          'image_wanted' => 0,
          'image_wanted_thumbnail' => 0,
          'image_weta_player' => 0,
          'image_wide_feature' => 0,
          'link' => 0,
        ),
        'insert_width' => '',
        'manualcrop_crop_info' => 1,
        'manualcrop_default_crop_area' => 1,
        'manualcrop_enable' => 1,
        'manualcrop_filter_insert' => 0,
        'manualcrop_inline_crop' => 0,
        'manualcrop_instant_crop' => 0,
        'manualcrop_instant_preview' => 1,
        'manualcrop_keyboard' => 1,
        'manualcrop_maximize_default_crop_area' => 0,
        'manualcrop_require_cropping' => array(
          'highlight_20-80' => 'highlight_20-80',
        ),
        'manualcrop_styles_list' => array(
          'highlight_20-80' => 'highlight_20-80',
        ),
        'manualcrop_styles_mode' => 'include',
        'manualcrop_thumblist' => 0,
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 10,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Bio');
  t('Short teaser for the full bio.  This appears on the Volunteer landing page with a link to click through to read the full bio.');
  t('Teaser Text');
  t('The cropped version of this image will appear in the teaser on the Volunteer landing page.  You can also insert the full image into the bio by placing your cursor within the WYSIWYG where you\'d like the image to appear, then clicking the "Insert" button. Then click on the image and from the Styles dropdown choose whether it should be aligned left or right.');
  t('Volunteer Image');
  t('Volunteer Name');

  return $field_instances;
}
