<?php
/**
 * @file
 * volunteer_spotlight.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function volunteer_spotlight_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function volunteer_spotlight_node_info() {
  $items = array(
    'volunteer_spotlight' => array(
      'name' => t('Volunteer Spotlight'),
      'base' => 'node_content',
      'description' => t('Featured volunteers'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
