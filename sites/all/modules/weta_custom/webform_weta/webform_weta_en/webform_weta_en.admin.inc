<?php

/**
 * @file
 * Form functions for settings form for the Engaging Networks API.
 */

/**
 * Settings form for Engaging Networks API.
 */
function webform_weta_en_admin_site_settings($form, &$form_state) {
  $form = array();

  $form['webform_weta_en_settings'] = array(
    '#title' => 'Engaging Networks API Settings',
    '#type' => 'fieldset',
  );

  $form['webform_weta_en_settings']['webform_weta_en_key'] = array(
    '#title' => 'Engaging Networks API Key',
    '#type' => 'textfield',
    '#required' => TRUE,
    '#description' => t('Enter your Engaging Networks API key.'),
    '#default_value' => variable_get('webform_weta_en_key', ''),
  );

  $form['webform_weta_en_timeout'] = array(
    '#title' => 'Time out',
    '#type' => 'textfield',
    '#required' => TRUE,
    '#size' => 5,
    '#maxlength' => 3,
    '#description' => 'Number of seconds for the request to wait (default is 15)',
    '#default_value' => variable_get('webform_weta_en_api_timeout', 15),
  );

  return system_settings_form($form);
}

/**
 * Validate the Engaging Networks API settings form.
 */
function webform_weta_en_admin_site_settings_validate($form, &$form_state) {
  $values = $form_state['values'];

  // Make sure the timeout is a number
  if (!is_int((int)$values['webform_weta_en_timeout'])) {
    form_set_error('webform_weta_en_timeout', t('Timeout must be an integer between 1-999'));
  }
  elseif ((int)$values['webform_weta_en_timeout'] < 0 || (int)$values['webform_weta_en_timeout'] > 999) {
    form_set_error('webform_weta_en_timeout', t('Timeout must be an integer between 1-999'));
  }

  // Validate the key against Engaging Network's API
  $api_key = $values['webform_weta_en_key'];

  $response = webform_weta_en_get_token($api_key);

  if (!$response) {
    form_set_error('webform_weta_en_settings', t('Failed to connect to the Engaging Networks API with those credentials.'));
  }
  else {
    drupal_set_message(t('Successfully connected with the Engaging Networks API. Settings have been saved.'));
  }
}


/**
 * Method test form for Engaging Networks API.
 */
function webform_weta_en_admin_tests($form, &$form_state) {

  $selectedendpoint = NULL;
  $selectedemail= NULL;
  $selectedid = NULL;

  // get defaults
  if (isset($form_state['values'])) {
    $values = $form_state['values'];
    $selectedendpoint = $values['endpoint'];
    $selectedemail= $values['email'];
    $selectedid = $values['selected_id'];
  }


  // build form
  $form = array();

  $form['webform_weta_en_tests'] = array(
    '#title' => 'Test Engaging Networks API Results',
    '#type' => 'fieldset',
  );

  // endpoint options
  $endpoints = array();
  $endpoints['authenticate'] = 'Authenticate';
  $endpoints['supporter'] = 'Get Supporter';

  // build methods select element
  $form['webform_weta_en_tests']['endpoint_options'] = array(
    '#type' => 'value',
    '#value' => $endpoints,
  );

  $form['webform_weta_en_tests']['endpoint'] = array(
    '#type' => 'select',
    '#title' => t('Endpoint'),
    '#name' => 'endpoint',
    '#options' => $form['webform_weta_en_tests']['endpoint_options']['#value'],
    '#default_value' => $selectedendpoint,
    '#required' => TRUE,
  );

  // only show email element if Supporter is selected
  $form['webform_weta_en_tests']['email'] = array(
    '#type' => 'textfield',
    '#title' => 'Email',
    '#default_value' => $selectedemail,
    '#states' => array(
      'visible' => array(
        ':input[name="endpoint"]' => array('value' => 'supporter'),
      ),
    ),
  );

  // only show email element if Supporter is selected
  $form['webform_weta_en_tests']['supporter_id'] = array(
    '#type' => 'textfield',
    '#title' => 'Supporter ID',
    '#default_value' => $selectedid,
    '#states' => array(
      'visible' => array(
        ':input[name="endpoint"]' => array('value' => 'supporter'),
      ),
    ),
  );



  // use an AJAX callback to display results without refreshing the page
  $form['submit'] = array(
    '#type' => 'submit',
    '#ajax' => array(
      'callback' => 'webform_weta_en_admin_test_submit_driven_callback',
      'wrapper' => 'box',
      'name' => 'submit1',
    ),
    '#value' => t('Get response'),
  );

  // results will be displayed here
  $form['box'] = array(
    '#type' => 'markup',
    '#prefix' => '<div id="box">',
    '#suffix' => '</div>',
    '#markup' => '',
  );

  return $form;
}

/**
 * Validate the Engaging Networks API test form.
 */
function webform_weta_en_admin_tests_validate($form, &$form_state) {
    $values = $form_state['values'];

    $selectedendpoint = $values['endpoint'];

    $selectedemail = NULL;
    if (isset($values['email'])) {
      $selectedemail = $values['email'];
    }

    $selectedid = NULL;
    if (isset($values['selected_id'])) {
      $selectedid = $values['selected_id'];
    }


    $pass = TRUE;
    switch ($selectedendpoint) {
      case 'supporter':
        if (!empty($selectedemail)) {
          if (!filter_var($selectedemail, FILTER_VALIDATE_EMAIL)) {
            $message = 'Please enter a valid email address.';
            $pass = FALSE;
          }

        }
        break;
      default:
        $pass = TRUE;
    }

  if (!$pass) {
    form_set_error('webform_weta_en_tests', t($message));
  }
}


/**
 * Select the 'box' element, change the markup in it, and return it as a
 * renderable array.
 *
 * @return renderable array (the box element)
 */
function webform_weta_en_admin_test_submit_driven_callback($form, $form_state) {

  $token = webform_weta_en_get_token();
  $id = NULL;
  $type = 'GET';
  $params = array();

  $values = $form_state['values'];
  $endpoint = $values['endpoint'];
  $email = $values['email'];
  $supporter_id = $values['supporter_id'];

  if ($endpoint == 'supporter') {
    if (!empty($email)) {
      // set parameter
      $params['email'] = $email;
    }

    if (!empty($supporter_id)) {
      $id = $supporter_id;
    }
  }


  // call the API
  $output = webform_weta_en_call($token, $endpoint, $id, $type, $params);


  // alter the form to display the results
  $element = $form['box'];
  $element['#markup'] = '<pre>' . print_r($output,TRUE) .'</pre>';

  return $element;
}
