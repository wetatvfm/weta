<?php

/**
 * Implements hook_flush_caches().
 */
function weta_migrate_flush_caches() {
  weta_migrate_register_migrations();
}

/**
 * Register all D5->D7 migrations.
 */
function weta_migrate_register_migrations() {
  /**
   * Each migration being registered takes an array of arguments, some required
   * and some optional. Start with the common arguments required by all - the
   * source_connection (connection key, set up in settings.php, pointing to
   * the Drupal 5 database), source_version (major version of Drupal), and
   * group_name (a.k.a. import job).
   */
  $common_arguments = array(
    'source_connection' => 'for_migration',
    'source_version' => 5,
    'group_name' => 'WETA',
  );

  // Node migrations - each has its own class derived from the migrate_d2d class,
  // specifying its particular field mappings and transformations. source_type
  // and destination_type are required arguments.
  $node_arguments = array(
    array(
      'class_name' => 'VenueMigration',
      'description' => t('Migration of venue nodes from Drupal 5'),
      'machine_name' => 'venue',
      'source_type' => 'venue',
      'destination_type' => 'venue',
    ),
    array(
      'class_name' => 'CalendarMigration',
      'description' => t('Migration of calendar nodes from Drupal 5'),
      'machine_name' => 'calendar',
      'source_type' => 'cal_event',
      'destination_type' => 'calendar_event',
      'dependencies' => array(
        'WETATerm9',
        'venue',
      ),
    ),
    array(
      'class_name' => 'WETAEventMigration',
      'description' => t('Migration of calendar nodes from Drupal 5'),
      'machine_name' => 'weta_calendar',
      'source_type' => 'event',
      'destination_type' => 'calendar_event',
      'dependencies' => array(
        'WETATerm9',
        'venue',
      ),
    ),
    array(
      'class_name' => 'MapPointsMigration',
      'description' => t('Migration of map point nodes from Drupal 5'),
      'machine_name' => 'map_points',
      'source_type' => 'map_items',
      'destination_type' => 'map_points',
    ),
    array(
      'class_name' => 'CCNodeMigration',
      'description' => t('Migration of CC nodes'),
      'machine_name' => 'cc_nodes',
      'source_type' => 'audio2',
      'destination_type' => 'fm_classical_conversations',
    ),
    array(
      'class_name' => 'SRSubpageMigration',
      'description' => t('Migration of Station Relation subpages'),
      'machine_name' => 'sr_nodes',
      'source_type' => 'national_projects_post',
      'destination_type' => 'station_relations_subpage',
    ),
    array(
      'class_name' => 'PressReleaseMigration',
      'description' => t('Migration of Press Releases'),
      'machine_name' => 'press_releases',
      'source_type' => 'press_release',
      'destination_type' => 'press_release',
      'dependencies' => array(
        'WETANodepress_contacts',
      ),
    ),
    array(
      'class_name' => 'PressImageMigration',
      'description' => t('Migration of press images'),
      'machine_name' => 'press_images',
      'source_type' => 'image',
      'destination_type' => 'press_image',
    ),
    array(
      'class_name' => 'PressPageMigration',
      'description' => t('Migration of additional press pages'),
      'machine_name' => 'press_pages',
      'source_type' => 'press_page',
      'destination_type' => 'press_page',
      'dependencies' => array(
        'WETANodepress_contacts',
      ),
    ),
    array(
      'class_name' => 'PressProgramMigration',
      'description' => t('Migration of press programs'),
      'machine_name' => 'press_kits',
      'source_type' => 'press_kit',
      'destination_type' => 'press_program',
      'dependencies' => array(
        'WETANodepress_contacts',
        'press_pages',
        'press_releases',
        'press_images'
      ),
    ),
    array(
      'class_name' => 'OperaHouseMigration',
      'description' => t('Migration of Classical WETA Opera House'),
      'machine_name' => 'opera',
      'source_type' => 'opera',
      'destination_type' => 'fm_opera_house',
    ),
    array(
      'class_name' => 'NSOShowcaseMigration',
      'description' => t('Migration of NSO Showcase nodes'),
      'machine_name' => 'nso_showcase',
      'source_type' => 'nso',
      'destination_type' => 'fm_generic_feature'
    ),
    array(
      'class_name' => 'FrontRowWashingtonMigration',
      'description' => t('Migration of Front Row Washington nodes'),
      'machine_name' => 'frontrow',
      'source_type' => 'frontrow',
      'destination_type' => 'fm_generic_feature'
    ),
    array(
      'class_name' => 'ChoralShowcaseMigration',
      'description' => t('Migration of Choral Showcase Nodes'),
      'machine_name' => 'choral_showcase',
      'source_type'=> 'choralshowcase',
      'destination_type' => 'fm_generic_feature',
    ),
    array(
      'class_name' => 'FMPlaylistMigration',
      'description' => t('Migration of FM Playlist items'),
      'machine_name' => 'fm_playlist',
      'source_type' => 'fm_playlist',
      'destination_type' => 'fm_playlist',
    ),
    array(
      'class_name' => 'VLVPlaylistMigration',
      'description' => t('Migration of VLV Playlist items'),
      'machine_name' => 'vlv_playlist',
      'source_type' => 'fm_vlv_playlist',
      'destination_type' => 'fm_playlist',
    ),
    array(
      'class_name' => 'TimelineMigration',
      'description' => t('Migration of Timeline items'),
      'machine_name' => 'timeline',
      'source_type' => 'timeline',
      'destination_type' => 'timeline_item',
    ),
    array(
      'class_name' => 'WantedPosterMigration',
      'description' => t('Migration of Wanted Poster items'),
      'machine_name' => 'wanted',
      'source_type' => 'wanted_poster',
      'destination_type' => 'wanted_poster',
    ),
    array(
      'class_name' => 'JobPostingsMigration',
      'description' => t('Migration of Job Posting items'),
      'machine_name' => 'jobs',
      'source_type' => 'job_opening',
      'destination_type' => 'job_opening',
    ),
    array(
      'class_name' => 'InternshipMigration',
      'description' => t('Migration of Internship items'),
      'machine_name' => 'internships',
      'source_type' => 'internship',
      'destination_type' => 'internship',
    ),
    array(
      'class_name' => 'GiveawaysMigration',
      'description' => t('Migration of Giveaway items'),
      'machine_name' => 'giveaways',
      'source_type' =>  'giveaway',
      'destination_type' => 'giveaway',
      'dependencies' => array(
        'calendar',
      ),
    ),
    array(
      'class_name' => 'COVEMigration',
      'description' => t('Migration of video nodes with COVE IDs'),
      'machine_name' => 'cove_videos',
      'source_type' => 'video',
      'destination_type' => 'cove_video',
      'dependencies' => array(
        'WETATerm16',
      ),
    ),
    array(
      'class_name' => 'LegacyVideoMigration',
      'description' => t('Migration of video nodes without COVE IDs'),
      'machine_name' => 'legacy_videos',
      'source_type' => 'video',
      'destination_type' => 'video_upload',
    ),
    array(
      'class_name' => 'BoardMeetingMigration',
      'description' => t('Migration of board meeting nodes'),
      'machine_name' => 'board_meetings',
      'source_type' => 'board_meeting',
      'destination_type' => 'board_meetings',
    ),

  );
  // Tell the node migrations where the users are coming from, so they can
  // set up the dependency and resolve D5->D7 uids.
  $common_node_arguments = $common_arguments + array(
    'user_migration' => 'WETAUser',
  );
  foreach ($node_arguments as $arguments) {
    $arguments = array_merge_recursive($arguments, $common_node_arguments);
    Migration::registerMigration($arguments['class_name'], $arguments['machine_name'], $arguments);
  }


}

/**
 * Implements hook_migrate_api().
 */
function weta_migrate_migrate_api() {
  $api = array(
    'api' => 2,
  );
  return $api;
}