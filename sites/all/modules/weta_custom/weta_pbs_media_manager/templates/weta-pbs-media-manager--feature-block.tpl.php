<?php

/**
 * @file
 * Theme individual video as a feature block.
 *
 * This isn't being used.  See
 * panels-pane--fieldable-panels-pane--media-manager-feature-block.tpl.php and
 * weta_preprocess_panels_pane() in template.php.
 *
 * @param $video
 *   An associative array containing:
 *   - url
 *   - image
 *   - passport status
 *
 * @param $slug
 *   The show slug, for creating the URL
 *
 * @ingroup themeable
 */

?>

<div class="span_3 col p30">
  <div class="red video">
    <div class="video">
      <a href="/mmplayer/a-show-of-unity-rcaf1t" title="Victoria, Season 3">
        <span class="screenshot"><span class="play-overlay"></span></span>
        <img src="https://weta.org/sites/default/files/styles/feature_square/public/Victoria3_EP5_1.jpg?itok=szJoP6rM&amp;c=c4de1d02ab5f73058ebb06e1397506d2" alt="Credit: Courtesy of Justin Slee/ITV Plc for MASTERPIECE  Photo Caption: Queen Victoria played by Jenna Coleman" title="Credit: Courtesy of Justin Slee/ITV Plc for MASTERPIECE  Photo Caption: Queen Victoria played by Jenna Coleman">
      </a>
    </div>
    <div id="node-feature-block-basic-square-group-text-wrapper" class="text_wrapper group-text-wrapper field-group-div">
      <div class="field-linked-title">
        <a href="https://watch.weta.org/video/a-show-of-unity-rcaf1t/" rel="lightvideo">Victoria, Season 3</a>
      </div>
      <p>Stream the latest episode of the popular Masterpiece drama now!</p>
    </div>
  </div>
</div>
