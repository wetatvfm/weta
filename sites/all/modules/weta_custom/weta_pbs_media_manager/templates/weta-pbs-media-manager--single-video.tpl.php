<?php

/**
 * @file
 * Theme a single video with player.
 *
 * @param $video
 *   A string containing the partner player as provided by the API.
 *
 * @ingroup themeable
 */
?>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-P633L6"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P633L6');</script>
<!-- End Google Tag Manager -->

<div class="cove_promo">
  <button class="mfp-close" type="button" title="Close (Esc)">×</button>
  <div class="field-cove-video-id">
    <div class="partner-player">
      <?php print $video; ?>
    </div>
  </div>
</div>
