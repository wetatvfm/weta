<?php

/**
 * @file
 * Theme individual videos for lists.
 *
 * @param $videos
 *   An associative array by delta containing:
 *   - title
 *   - description
 *   - image
 *   - passport status
 *   - slug
 *
 * @param $slug
 *   The show slug, for creating the URL
 *
 * @ingroup themeable
 */

?>
<?php foreach ($videos as $i => $video) :

  // embed a player for the first item
  if ($i == 0) :

    $player = $video['player_code'];
  ?>
  <h2><?php print $video['title']; ?></h2>
  <div class="mm_wrapper">

      <?php print $player; ?>

</div>
<?php print $video['description']; ?>
  <?php

  else :


    $linked_image = '';
    if (isset($video['image']) && !empty($video['image'])) :
      $overlay = '<span class="screenshot"><span class="play-overlay"></span></span>';
      $image = theme('image', $video['image']);


      $link = 'mmplayer/' . $video['slug'];

      $linked_image = l($overlay . $image, $link, array('attributes' => array('class' => array('mm_player')), 'html' => TRUE));
    endif;

    $passport_icon = '';
    if ($video['is_passport']) :
      $passport_icon = '<img src="/' . drupal_get_path('module', 'weta_pbs_media_manager') . '/templates/passport_sm.png" class="passport_icon" width="15" height="15" alt="WETA Passport" title="WETA Passport" />';
    endif;

    // we need to add a clearfix after the fourth result.
    $clear = '';

    if ($i % 4 == 0) :
      $clear = 'clearfix';
    endif;

    if ($i == 1) :
    ?>
    <div class="divider2"></div>
    <?php
    endif;
  ?>

  <div class="span_4 col p20-10-10 <?php print $clear; ?>">
    <div class="feature_box trans cove_listing">
      <div class="video"><?php if ($linked_image) : print $linked_image; endif;?></div>
      <h3><?php print $passport_icon . $video['title']; ?></h3>
      <p><?php print $video['description']; ?></p>
    </div>
  </div>
  <?php
  endif;
  ?>
<?php endforeach; ?>

<div class="span_12 col p20-10-10">
  <?php print l(t('All Videos'), 'https://pbs.org/show/' . $slug, array('attributes' => array('class' => array('button cove_button')))); ?>
</div>
