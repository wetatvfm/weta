/**
 * @file
 * Load weta streaming audio players.
 */
(function ($) {
  Drupal.behaviors.wetaPlayerLoader = {
    attach: function(context, settings) {

      var wetaPlayerLoaded = false;
      wetaPlayerLoad();

      // Load player
      function wetaPlayerLoad() {

        if (wetaPlayerLoaded) {
          return;
        }

        wetaPlayerLoaded = true;

        var swfPath = Drupal.settings.wetaPlayerSettings.swfPath;
        var streamPath = Drupal.settings.wetaPlayerSettings.streamPath;
        var streamPlayerId = Drupal.settings.wetaPlayerSettings.streamPlayerId;

        var vlvStreamPath = Drupal.settings.wetaPlayerSettings.vlvStreamPath;
        var vlvPlayerId = Drupal.settings.wetaPlayerSettings.vlvPlayerId;

        // Detect browser for flash fallback.
        var detectBrowser = function() {
          var N = navigator.appName, ua= navigator.userAgent, tem;
          var M = ua.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);
          if(M && (tem= ua.match(/version\/([\.\d]+)/i))!= null) M[2]= tem[1];
          M = M ? [M[1], M[2]] : [N, navigator.appVersion,'-?'];
          return M;
        };
        var browserSolution = 'html, flash';

        if (detectBrowser()[0] == 'Netscape') {
          browserSolution = 'flash, html';
        }
        var stream = {
          title: 'WETA',
          mp3: streamPath
        };
        var vlvStream = {
          title: 'Viva La Voce',
          mp3: vlvStreamPath
        };

        var ready = false;
        var vlvReady = false;
        var pass = false;
        var vlvPass = false;

        // Streaming player.
        $('#jquery_jplayer_' + streamPlayerId).jPlayer({
          ready: function (event) {
            ready = true;
            pass = true;
            $(this).jPlayer('setMedia', stream).jPlayer('play');
          },
          pause: function() {
            if (!pass) {
              return false;
            }
            $(this).jPlayer('clearMedia');
          },
          error: function(event) {
            if(ready && event.jPlayer.error.type === $.jPlayer.error.URL_NOT_SET) {
              // Set up the media stream again and play it.
              $(this).jPlayer('setMedia', stream).jPlayer('play');
            }
          },
          ended: function () {
            pass = true;
            $(this).jPlayer('setMedia', stream).jPlayer('play');
          },
          volumechange: function(event) {

            // If the volume is set to zero manually, mute the player.
            if(!event.jPlayer.options.volume) {
              event.jPlayer.options.muted = true;
            }

            // If the player is muted, stop the stream
            if(event.jPlayer.options.muted) {
              $(this).jPlayer('clearMedia');
            }

            // If the player is unmuted, restart the stream
            if(!event.jPlayer.options.muted) {
              $(this).jPlayer('setMedia', stream).jPlayer('play');
            }
          },
          swfPath: swfPath,
          supplied: 'mp3',
          preload: 'none',
          wmode: 'window',
          solution: browserSolution,
          cssSelectorAncestor: "#jp_container_" + streamPlayerId
        });

        // Viva La Voce streaming player.
        $('#jquery_jplayer_' + vlvPlayerId).jPlayer({
          ready: function (event) {
            vlvReady = true;
            vlvPass = true;
            $(this).jPlayer('setMedia', vlvStream).jPlayer('play');
          },
          pause: function() {
            if (!vlvPass) {
              return false;
            }
            $(this).jPlayer('clearMedia');
          },
          error: function(event) {
            if(vlvReady && event.jPlayer.error.type === $.jPlayer.error.URL_NOT_SET) {
              // Setup the media stream again and play it.
              $(this).jPlayer('setMedia', vlvStream).jPlayer('play');
            }
          },
          ended: function () {
            vlvPass = true;
            $(this).jPlayer('setMedia', vlvStream).jPlayer('play');
          },
          volumechange: function(event) {
            // If the volume is set to zero manually, mute the player.
            if(!event.jPlayer.options.volume) {
              event.jPlayer.options.muted = true;
            }
            // If the player is muted stop the stream.
            if(event.jPlayer.options.muted) {
              $(this).jPlayer('clearMedia');
            }
            // If the player is unmuted, restart the stream.
            if(!event.jPlayer.options.muted) {
              $(this).jPlayer('setMedia', vlvStream).jPlayer('play');
            }
          },
          swfPath: swfPath,
          supplied: 'mp3',
          preload: 'none',
          wmode: 'window',
          solution: browserSolution,
          cssSelectorAncestor: "#jp_container_" + vlvPlayerId
        });
      }
    }
  }
}(jQuery));
