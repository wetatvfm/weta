/**
 * @file
 * Ajax fetch now playing info for the weta streaming player.
 */
(function ($) {
  Drupal.behaviors.wetaPlayerNowPlaying = {
    attach: function(context, settings) {
      var container = $('#now-playing');
      var page = Drupal.settings.wetaPlayerSettings.playerPage;
      var base = Drupal.settings.wetaPlayerSettings.basePath;
      var url = base + 'listen-live/now-playing-xml/' + page;
      var errorMessage = '<div><p>Playlist information is not currently available.</p></div>';

      // All dates need to be in America/New_York
      moment.tz.setDefault("America/New_York");


      /**
       * Function to load the XML file from the page created by Views.
       */
      function loadXML() {
        $.ajax({
          type: "get",
          url: url,
          dataType: "xml",
          success: function(data) {

            // get the path to the XML file
            var xml = $(data).find('Source').text();

            if (xml) {
              // send it to the playlist loader
              loadPlaylist(xml);
            }
            else {
              //console.log('no playlist available');
              // Build out HTML for an error message.
              var npHtml = errorMessage;
              // Insert the HTML into the now-playing div
              $('#now-playing').html(npHtml);

              // try again in five minutes.
              var reloadXML = setTimeout(loadXML(), 300000);
            }
          },
          error: function(xhr, status) {
            //console.log('page not available');
            // Build out HTML for an error message.
            var npHtml = errorMessage;
            // Insert the HTML into the now-playing div
            $('#now-playing').html(npHtml);

            // Record the actual error in the console for debugging.
            //console.log(status);

            // try again in five minutes.
            var reloadXML = self.setTimeout(loadXML, 300000);
          }
        }); // end ajax
      } // end loadXML()

      // Do the initial load of the XML file.
      loadXML();

      /**
       * Interval function for looping through the XML file.
       *
       * @param interval
       *    The time in milliseconds to rerun the included function. Set to
       *    false to clear the interval.
       */
      function nowPlayingTimer(interval, data) {
        if (interval) {
          nowPlaying = self.setInterval(function() {reload(data)}, interval);
        }
        else {
          clearInterval(nowPlaying);
        }
      }

      /**
       * Reload the information for the Now Playing block.
       *
       * @param data
       *   The data generated by loadPlaylist()
       */
      function reload(data) {

        // only continue if the XML is for today
        if (playlistCheck(data)) {
          if (playlistLoop(data) && playlistLoop(data).length >= 1) {
            // loop through the XML
            var np = playlistLoop(data);

            // Build out HTML
            var npHtml = '<div>';

            if (np[0]['composer']) {
              npHtml += '<h5>' + np[0]['composer'] + '</h5>';
            }

            if (np[0]['title']) {
              npHtml += '<p class="italic spaced">' + np[0]['title'] + '</p>';
             }

            if (np[0]['buyURL']) {
              npHtml += '<a href="' + np[0]['buyURL'] + '" target="_blank">' + np[0]['buyText'] + ' &rarr;</a>';
            }

            npHtml += '</div>';
          }
          else {
            // Build out HTML for an error message instead.
            var npHtml = errorMessage;
          }
        }
        else {

          // Build out HTML for an error message instead.
          var npHtml = errorMessage;
          console.log('wrong playlist');
        }

        // Insert the HTML into the now-playing div
        $('#now-playing').html(npHtml);
      }


      /**
       * Load the playlist items
       *
       * @param xml
       *   The XML file generated by loadXML();.
       */
      function loadPlaylist(xml) {
        $.ajax({
          type: "get",
          url: xml,
          dataType: "xml",
          success: function(data) {
            // start the timer to refresh every minute so the current
            // piece will display
            nowPlayingTimer(60000, data);
            reload(data);
          },
          error: function(xhr, status) {
            // Build out HTML for an error message.
            var npHtml = errorMessage;
            // Insert the HTML into the now-playing div
            $('#now-playing').html(npHtml);

            // Record the actual error in the console for debugging.
            console.log(status);
          }
        }); // end ajax

      } // end loadPlaylist()


      /**
       * Check to see if the XML matches today's playlist.
       *
       * @param data
       *   The data generated by reload();
       */
      function playlistCheck(data) {

        // get the current time
        var today = moment().format('YYYY-MM-DD');

        // select the first item from the XML and extract the date
        var firstDate = $(data).find('PlaylistWebReportQuery').find('PlayDate').first(function() {}).text();

        // format it so we only get the date part
        firstDate = moment(firstDate).format('YYYY-MM-DD');

        // compare the date from the playlist with today
        if (firstDate == today) {
          return true;
        }
        else {
          // playlistCheck fails: cancel reload()
          nowPlayingTimer(false);

          // try again -- in two minutes so there's time for the cache to clear
          var tryXML = self.setTimeout(loadXML, 120000);

          return false;
        }

      } // end playlistCheck()


      /**
       * Loop through the playlist data to get the current piece.
       *
       * @param data
       *   The data generated by reload();
       */
      function playlistLoop(data) {

        // get the current time
        var now = moment().format('x');

        // figure out what page we're on for constructing the Buy Link later
        switch (page) {
          case '90-9FM':
            var src = 'WETA';
            break;
          case 'viva-la-voce':
            var src = 'VLV';
            break;
          default:
            var src = 'WETA';
        }

        // loop through the results, only selecting the relevant items
        var nowPlayingData = $(data).find('PlaylistWebReportQuery').map(function() {

          // individual playlist entry
          var $item = $(this);

          // get the date
          var date = $item.find('PlayDate').text();

          // convert it to a timestamp in America/New_York
          var d = moment(date).format('x');

          var nonMusic = $item.find('Non-Music').text();

          // we only want the music item's that have already played
          if (d <= now && nonMusic == 'N') {
            var thisTitle = $item.find('Title').text();
            var timestamp = moment(date).format('X');
            var thisComposer = $item.find('Composer').text();

            switch ($item.find('BuyLink').text()) {
              case '1':
                var thisBuyText = 'Buy the CD';

                // get the album
                var album = encodeURI($item.find('Album').text());

                // format the date
                var displayDate = moment(date).format('YYYYMMDDHHmm');

                // construct the URL
                var thisBuyURL = 'http://www.arkivmusic.com/classical/Playlist?source=' + src + '&amp;date=' + displayDate + '&amp;labelcat=' + album;

                break;
              case '2':
                var thisBuyURL = '/fm/playlists/notavailable';
                var thisBuyText = 'CD not available';
                break;
              case '3':
                var thisBuyURL = '/donatefm';
                var thisBuyText = 'CD available with Classical CD pledge';
                break;
              case '4':
                var thisBuyURL = '/fm/features/cdpick';
                var thisBuyText = 'Visit the CD Pick page for buying information';
                break;
              default:
                var thisBuyURL = null;
                var thisBuyText = null;
            }

            var filtered = {title:thisTitle, date:timestamp, composer:thisComposer, buyURL:thisBuyURL, buyText:thisBuyText};
          }

          // return the filtered array
          return filtered;

        }).last(function() {});  // we only want the last item

        // return the results of the loop
        return nowPlayingData;

      } // end Playlistloop()
    }
  }
}(jQuery));
