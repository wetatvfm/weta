<?php
/**
 * @file
 * Template to render the weta streaming audio player page.
 */
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title><?php print $title; ?></title>
    <?php print $includes; ?>
  </head>
  <body>
    <?php print $banner; ?>
    <div class="player-wrapper">
      <div id="js-tabs">
        <div id="right-ad">
          <?php print $right_ad; ?>
        </div>
        <?php print $tabs; ?>
        <div id="weta-player"><?php print $content; ?></div>
        <div id="extra_text"><?php print $extra_text; ?></div>
      </div>
    </div>
    <div id="jp-footer">
      <div class="classical-weta"><a target="_blank" href="<?php print $classical_weta_link; ?>">Classical WETA</a></div>
      <div class="donate-now"><a target="_blank" href="<?php print $donate_now_link; ?>">Donate Now</a></div>
    </div>
  </body>
</html>
