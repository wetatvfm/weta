<?php

/**
 * @file
 * Builds the player html for the WETA streaming audio player.
 */

/**
 * Variables
 * $player_id: The unique id number of the player on the page.
 * $stream_title: The title of the player stream.
 * $error_title: The title of the error message.
 * $error_message: The message to display to the end user if they cannot play
 *   the audio.
 * $flash_link_src: The url of the adobe flash player download.
 * $flash_link_text: The text to make into the flash player link.
 */

?>

<div id="jp_container_<?php print $player_id; ?>" class="jp-audio-stream">
  <div class="jp-type-single">
    <div class="jp-title">
        <h2 class="stream-title"><?php print $stream_title; ?></h2>
      <div id="now-playing"></div>
    </div>
    <div id="jquery_jplayer_<?php print $player_id; ?>" class="jp-jplayer"></div>
    <div class="jp-gui jp-interface">
      <ul class="jp-controls">
        <li><a href="javascript:;" class="jp-play" tabindex="1">play</a></li>
        <li><a href="javascript:;" class="jp-pause" tabindex="1">pause</a></li>
      </ul>
      <div id="volume-control-<?php print $player_id; ?>">
        <ul class="jp-volume-controls">
          <li><a href="javascript:;" class="jp-mute" tabindex="1" title="mute">mute</a></li>
          <li><a href="javascript:;" class="jp-unmute" tabindex="1" title="unmute">unmute</a></li>
        </ul>
        <div class="max-volume"><a href="javascript:;" class="jp-volume-max" tabindex="1" title="max volume">max volume</a></div>
        <div class="jp-volume-bar">
          <div class="jp-volume-bar-value"></div>
        </div>
      </div>
    </div>
    <div class="jp-no-solution">
      <span><?php print $error_title; ?></span>
      <p><?php print $error_message; ?></p>
      <p><a href="<?php print $flash_link_src; ?>" target="_blank"><?php print $flash_link_text; ?></a></p>
    </div>
  </div>
</div>
