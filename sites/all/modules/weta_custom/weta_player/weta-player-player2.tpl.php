<?php

/**
 * @file
 * Builds the player html for the WETA classical conversations audio player.
 */

/**
 * Variables
 * $player_id: The unique player_id number of the player on the page.
 * $error_title: The title of the error message.
 * $error_message: The message to display to the end user if they cannot play
 *   the audio.
 * $flash_link_src: The url of the adobe flash player download.
 * $flash_link_text: The text to make into the flash player link.
 */

?>
<h2 class="stream-title"><?php print $title; ?></h2>
<div class="classical-conversation-image"><?php print $image; ?></div>
<div class="classical-conversation-text"><?php print $text; ?></div>
<div id="jp_container_<?print $player_id; ?>" class="jp-audio">
  <div class="jp-type-playlist">
    <div id="jquery_jplayer_<?php print $player_id; ?>" class="jp-jplayer"></div>
    <div class="jp-gui jp-interface">
      <ul class="jp-controls">
        <li><a href="javascript:;" class="jp-previous" tabindex="1">previous</a></li>
        <li><a href="javascript:;" class="jp-play" tabindex="1">play</a></li>
        <li><a href="javascript:;" class="jp-pause" tabindex="1">pause</a></li>
        <li><a href="javascript:;" class="jp-next" tabindex="1">next</a></li>
        <li><a href="javascript:;" class="jp-stop" tabindex="1">stop</a></li>
      </ul>
      <div class="jp-progress">
        <div class="jp-seek-bar">
          <div class="jp-play-bar"></div>
        </div>
      </div>
      <div class="jp-current-time"></div>
      <div class="jp-duration"></div>
      <ul class="jp-volume-controls">
        <li><a href="javascript:;" class="jp-mute" tabindex="1" title="mute">mute</a></li>
        <li><a href="javascript:;" class="jp-unmute" tabindex="1" title="unmute">unmute</a></li>
        <li><a href="javascript:;" class="jp-volume-max" tabindex="1" title="max volume">max volume</a></li>
      </ul>
      <div class="jp-volume-bar">
        <div class="jp-volume-bar-value"></div>
      </div>
      <ul class="jp-toggles">
        <li><a href="javascript:;" class="jp-shuffle" tabindex="1" title="shuffle">shuffle</a></li>
        <li><a href="javascript:;" class="jp-shuffle-off" tabindex="1" title="shuffle off">shuffle off</a></li>
        <li><a href="javascript:;" class="jp-repeat" tabindex="1" title="repeat">repeat</a></li>
        <li><a href="javascript:;" class="jp-repeat-off" tabindex="1" title="repeat off">repeat off</a></li>
      </ul>
    </div>
    <div class="jp-playlist">
      <ul>
        <li></li>
      </ul>
    </div>
    <div class="jp-no-solution">
      <span><?php print $error_title; ?></span>
      <p><?php print $error_message; ?></p>
      <p><a href="<?php print $flash_link_src; ?>" target="_blank"><?php print $flash_link_text; ?></a>.</p>
    </div>
  </div>
</div>
