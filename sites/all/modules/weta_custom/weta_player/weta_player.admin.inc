<?php

/**
 * @file
 * Provide the admin related functions for the WETA player module.
 */

/**
 * Defines the admin config form for the audio players.
 */
function weta_player_config_form() {
  $form = array();

  $form['global_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Global Player Settings'),
    '#collapsible' => TRUE,
  );
  $form['global_settings']['weta_player_donate_link'] = array(
    '#title' => t('Donate Link Path'),
    '#type' => 'textfield',
    '#description' => t('The path that the donate now link should go to. DO NOT add a leading "/".'),
    '#default_value' => variable_get('weta_player_donate_link', ''),
  );
  $form['global_settings']['weta_player_classical_link'] = array(
    '#title' => t('Classical WETA Link Path'),
    '#type' => 'textfield',
    '#description' => t('The path that the classical weta link should go to. DO NOT add a leading "/".'),
    '#default_value' => variable_get('weta_player_classical_link', ''),
  );

  // BEGIN 90.9 STREAM SETTINGS.
  $form['weta_player'] = array(
    '#type' => 'fieldset',
    '#title' => t('Stream Settings'),
    '#collapsible' => TRUE,
  );

  $form['weta_player']['weta_player_page_title'] = array(
    '#title' => t('Page Title'),
    '#description' => t('The HTML page title of the WETA Player page.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('weta_player_page_title', 'WETA - Listen Live'),
  );

  $form['weta_player']['weta_player_steam_url'] = array(
    '#title' => t('Stream URL'),
    '#description' => t("The URL of the WETA audio stream from the ShoutCast server. This should usually end with a semicolon. Example: 'http://stream.weta.org:9000/;'"),
    '#type' => 'textfield',
    '#default_value' => variable_get('weta_player_steam_url', 'http://stream.weta.org:9000/;'),
  );

  $form['weta_player']['weta_player_stream_title'] = array(
    '#title' => t('Stream Title'),
    '#description' => t('The player will show this as the title of the audio being played.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('weta_player_stream_title', 'WETA Streaming Audio'),
  );

  $form['weta_player']['weta_player_default_preroll'] = array(
    '#title' => t('Default Preroll Node Id'),
    '#description' => t("A default preroll node nid should be supplied in the event that no preroll can be found."),
    '#type' => 'textfield',
    '#default_value' => variable_get('weta_player_default_preroll', NULL),
  );

  $form['weta_player']['weta_player_extra_text'] = array(
    '#title' => t('Extra Text'),
    '#description' => t('This can be used to display links below the player. The text will be filtered to avoid Cross Site Scripting vulnerabilities but adding links is fine.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('weta_player_extra_text', ''),
  );

  // BEGIN VIVA LA VOCE STREAM SETTINGS.
  $form['weta_player_vlv'] = array(
    '#type' => 'fieldset',
    '#title' => t('Viva La Voce Stream Settings'),
    '#collapsible' => TRUE,
  );

  $form['weta_player_vlv']['weta_player_page_title_vlv'] = array(
    '#title' => t('Page Title'),
    '#description' => t('The HTML page title of the Viva La Voce Player page.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('weta_player_page_title_vlv', 'Viva La Voce - Listen Live'),
  );

  $form['weta_player_vlv']['weta_player_steam_url_vlv'] = array(
    '#title' => t('Stream URL'),
    '#description' => t("The URL of the Viva La Voce audio stream from the ShoutCast server. This should usually end with a semicolon. Example: 'http://stream.weta.org:9000/;'"),
    '#type' => 'textfield',
    '#default_value' => variable_get('weta_player_steam_url_vlv', 'http://stream.weta.org:9000/;'),
  );

  $form['weta_player_vlv']['weta_player_stream_title_vlv'] = array(
    '#title' => t('Stream Title'),
    '#description' => t('The player will show this as the title of the audio being played.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('weta_player_stream_title_vlv', 'Viva La Voce Streaming Audio'),
  );

  $form['weta_player_vlv']['weta_player_default_preroll_vlv'] = array(
    '#title' => t('Default Preroll Node Id'),
    '#description' => t("A default preroll node nid should be supplied in the event that no preroll can be found."),
    '#type' => 'textfield',
    '#default_value' => variable_get('weta_player_default_preroll_vlv', NULL),
  );

  $form['weta_player_vlv']['weta_player_vlv_extra_text'] = array(
    '#title' => t('Extra Text'),
    '#description' => t('This can be used to display links below the player. The text will be filtered to avoid Cross Site Scripting vulnerabilities but adding links is fine.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('weta_player_vlv_extra_text', ''),
  );

  // BEGIN CLASSICAL CONVERSATION SETTINGS.
  $form['weta_player_cc'] = array(
    '#type' => 'fieldset',
    '#title' => t('Audio Features Settings'),
    '#collapsible' => TRUE,
  );
  $form['weta_player_cc']['weta_player_cc_extra_text'] = array(
    '#title' => t('Extra Text'),
    '#description' => t('This can be used to display links below the player. The text will be filtered to avoid Cross Site Scripting vulnerabilities but adding links is fine.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('weta_player_cc_extra_text', ''),
  );

  // BEGIN ERROR SETTINGS.
  $form['weta_player_error'] = array(
    '#type' => 'fieldset',
    '#title' => t('Error settings'),
    '#collapsible' => TRUE,
  );

  $form['weta_player_error']['weta_player_flash_link_text'] = array(
    '#title' => t('Flash Download Link Text'),
    '#description' => t('Text to be displayed as the link to the flash download.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('weta_player_flash_link_text', 'Update Flash Now.'),
  );

  $form['weta_player_error']['weta_player_flash_url'] = array(
    '#title' => t('Flash Download URL'),
    '#description' => t('The path to direct users to find the adobe flash player.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('weta_player_flash_url', 'http://get.adobe.com/flashplayer/'),
  );

  $form['weta_player_error']['weta_player_error_title'] = array(
    '#title' => t('Error Title'),
    '#description' => t('If an error occurs and the user does not have a browser capable of playing audio, an error will be displayed with this as the title.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('weta_player_error_title', 'Update Required'),
  );

  $form['weta_player_error']['weta_player_error_message'] = array(
    '#title' => t('Error Message'),
    '#description' => t('If an error occurs and the user does not have a browser capable of playing audio, the following message will be displayed'),
    '#type' => 'textarea',
    '#default_value' => variable_get('weta_player_error_message', 'To play the media you will need to either update your browser to a recent version or update your Flash Player.'),
  );

  return system_settings_form($form);
}

/**
 * Defines the admin config form for the ad server.
 */
function weta_player_ad_config_form() {
  $form = array();

  $form['global_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Global Ad Settings'),
    '#collapsible' => FALSE,
  );
  $form['global_settings']['weta_player_dfp_account_id'] = array(
    '#title' => t('DFP Account Id'),
    '#type' => 'textfield',
    '#description' => t('The id of the DFP account.'),
    '#required' => TRUE,
    '#default_value' => variable_get('weta_player_dfp_account_id', ''),
  );
  $form['global_settings']['weta_player_preroll_m3u_title'] = array(
    '#title' => t('M3U Preroll Text'),
    '#type' => 'textfield',
    '#description' => t('This text will be displayed as part of the playlist that is sent to users of third-party streaming sites. Some players do not display it. Others will show it as the name of the song.'),
    '#default_value' => variable_get('weta_player_preroll_m3u_title', 'Thanks to our sponsors.'),
  );

  $form['fm909_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('90.9 FM Stream Ad Settings'),
    '#collapsible' => TRUE,
  );
  $form['fm909_settings']['weta_player_banner_ad'] = array(
    '#title' => t('Bannder Ad Name'),
    '#type' => 'textfield',
    '#description' => t('The machine name of the banner ad. This should be a string with no special characters. It is found on DFP under the "Inventory" section. Example: "weta_player_banner_ad".'),
    '#required' => TRUE,
    '#default_value' => variable_get('weta_player_banner_ad', ''),
  );
  $form['fm909_settings']['weta_player_right_ad'] = array(
    '#title' => t('Right Side Ad Name'),
    '#type' => 'textfield',
    '#description' => t('The machine name of the right side ad. This should be a string with no special characters. It is found on DFP under the "Inventory" section. Example: "weta_player_side_ad".'),
    '#required' => TRUE,
    '#default_value' => variable_get('weta_player_right_ad', ''),
  );

  $form['vlv_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Viva La Voce Stream Ad Settings'),
    '#collapsible' => TRUE,
  );
  $form['vlv_settings']['weta_player_banner_ad_vlv'] = array(
    '#title' => t('Bannder Ad Name'),
    '#type' => 'textfield',
    '#description' => t('The machine name of the banner ad. This should be a string with no special characters. It is found on DFP under the "Inventory" section. Example: "weta_player_banner_ad".'),
    '#required' => TRUE,
    '#default_value' => variable_get('weta_player_banner_ad_vlv', ''),
  );
  $form['vlv_settings']['weta_player_right_ad_vlv'] = array(
    '#title' => t('Right Side Ad Name'),
    '#type' => 'textfield',
    '#description' => t('The machine name of the right side ad. This should be a string with no special characters. It is found on DFP under the "Inventory" section. Example: "weta_player_side_ad".'),
    '#required' => TRUE,
    '#default_value' => variable_get('weta_player_right_ad_vlv', ''),
  );

  $form['cc_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Classical Converstations Player Ad Settings'),
    '#collapsible' => TRUE,
  );
  $form['cc_settings']['weta_player_banner_ad_cc'] = array(
    '#title' => t('Bannder Ad Name'),
    '#type' => 'textfield',
    '#description' => t('The machine name of the banner ad. This should be a string with no special characters. It is found on DFP under the "Inventory" section. Example: "weta_player_banner_ad".'),
    '#required' => TRUE,
    '#default_value' => variable_get('weta_player_banner_ad_cc', ''),
  );
  $form['cc_settings']['weta_player_right_ad_cc'] = array(
    '#title' => t('Right Side Ad Name'),
    '#type' => 'textfield',
    '#description' => t('The machine name of the right side ad. This should be a string with no special characters. It is found on DFP under the "Inventory" section. Example: "weta_player_side_ad".'),
    '#required' => TRUE,
    '#default_value' => variable_get('weta_player_right_ad_cc', ''),
  );

  return system_settings_form($form);
}
