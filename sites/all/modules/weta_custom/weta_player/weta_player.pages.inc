<?php

/**
 * @file
 * Page callbacks for the weta_player module.
 */

/**
 * Page callback for listen-live page.
 * @see weta_player.module, weta_player_menu()
 */
function weta_player_build_page($page_arg, $cc_nid_arg = NULL) {
  // Set default page and override if we get something else valid.
  $page = '90-9FM';
  $allowed_paths = array(
    '90-9FM',
    'viva-la-voce',
    'audio-features',
  );
  if (in_array($page_arg, $allowed_paths)) {
    $page = $page_arg;
  }

  // Get path to the module as a variable since we use it a few times.
  $path = drupal_get_path('module', 'weta_player');

  // Make unique player ids to keep track of multiple players.
  $id = 0;
  $stream_player_id = ++$id;
  $vlv_player_id    = ++$id;
  $cc_player_id     = ++$id;

  // Reset the static functions.
  drupal_static_reset('drupal_add_js');
  drupal_static_reset('drupal_add_css');
  drupal_add_js('misc/drupal.js', array('group' => JS_LIBRARY, 'weight' => -99));
  drupal_add_js('misc/jquery.js', array('group' => JS_LIBRARY, 'weight' => -100));

  // Add JPlayer and Jplayer playlist.
  // Note: using drupal_add_library did not work in initial tests.
  $player_library_dir = variable_get('jplayer_directory', 'sites/all/libraries/jplayer');
  $jplayer = $player_library_dir . '/jquery.jplayer.min.js';
  $jplayer_playlist = $player_library_dir . '/add-on/jplayer.playlist.min.js';
  drupal_add_js($jplayer);
  drupal_add_js($jplayer_playlist);

  // Add custom player script.
  drupal_add_js($path . '/js/load_player.js');

  // Add js loader for the playlist info.
  if ($page == '90-9FM' || $page == 'viva-la-voce') {
    drupal_add_js($path . '/js/now_playing.js');
    // Add the Moment and Moment Timezone libraries because Microsoft is terrible and Edge and IE don't support timezone calculations natively.
    drupal_add_js($path . '/js/moment.min.js');
    drupal_add_js($path . '/js/moment-timezone-with-data-2012-2022.min.js');
  }

  // Load ads using DFP.
  $banner_block = 'fm_player_classicalweta_728x90';
  $banner_delta = 'fm_728x90';
  $right_block = 'fm_player_classicalweta_300x250';
  $right_delta = 'fm_300x250';

  if ($page == 'viva-la-voce') {
    $banner_block = 'fm_player_vlv_728x90';
    $banner_delta = 'fm_vlv_728x90';
    $right_block = 'fm_player_vlv_300x250';
    $right_delta = 'fm_vlv_300x250';
  }

  $block = module_invoke('dfp', 'block_view', $banner_block);
  $block['module'] = 'dfp';
  $block['delta'] = $banner_delta;
  $block['title'] = '';
  $block['region'] = 'none';
  $block_vars = array(
    'elements' => array(
      '#block' => (object) $block,
      '#children' => render($block['content'])
    )
  );
  $banner = theme('block', $block_vars);

  $block = module_invoke('dfp', 'block_view', $right_block);
  $block['module'] = 'dfp';
  $block['delta'] = $right_delta;
  $block['title'] = '';
  $block['region'] = 'none';
  $block_vars = array(
    'elements' => array(
      '#block' => (object) $block,
      '#children' => render($block['content'])
    )
  );
  $right_ad = theme('block', $block_vars);

  // Fetch stream URL and other variables and send it to the player as a js
  // setting.
  $cc_media = weta_player_get_cc_audio_data($cc_nid_arg);
  $stream_path = variable_get('weta_player_steam_url', 'https://stream.weta.org:8010/fmlive');
  $vlv_stream_path = variable_get('weta_player_steam_url_vlv', 'https://stream.weta.org:8008/viva');

  // Fetch the preroll path
  if (is_array($banner) && isset($banner['ad'])) {
    $preroll_path = weta_player_get_preroll_by_ad_id($banner['ad']);
  }
  // If nothing was found, load the default.
  else {
    $default_preroll_nid = weta_player_get_default_preroll_node($page);
    $preroll_path = weta_player_get_preroll_by_nid($default_preroll_nid);
  }
  // If we still have a null path, then there's no default set and we want to
  // send null into the JS file. If we have a path though, make it into a
  // usable URL.
  if ($preroll_path) {
    $preroll_path = file_create_url($preroll_path);
  }

  $jplayer_swf_path = url($player_library_dir . '/Jplayer.swf', array('absolute' => TRUE));
  drupal_add_js(array('wetaPlayerSettings' => array(
    'basePath'       => base_path(),
    'playerPage'     => $page,
    'swfPath'        => $jplayer_swf_path,
    'streamPath'     => $stream_path,
    'vlvStreamPath'  => $vlv_stream_path,
    'prerollPath'    => $preroll_path,
    'streamPlayerId' => $stream_player_id,
    'vlvPlayerId'    => $vlv_player_id,
    'ccPlayerId'     => $cc_player_id,
    'ccMedia'        => $cc_media['media'],
  )), 'setting');

  weta_player_add_preroll_choices();

  // Prepare settings to pass to player tempates.
  $settings = array();
  $settings['stream_title']    = variable_get('weta_player_stream_title');
  $settings['flash_link_text'] = variable_get('weta_player_flash_link_text');
  $settings['flash_link_src']  = variable_get('weta_player_flash_url');
  $settings['error_title']     = variable_get('weta_player_error_title');
  $settings['error_message']   = variable_get('weta_player_error_message');
  $settings['player_id']       = $stream_player_id;

  // Theme the main content based on the incoming page argument.
  switch ($page) {
    case 'viva-la-voce':
      // Prepare settings to pass to viva la voce player tempates.
      // Override some of the settings.
      $settings['stream_title'] = variable_get('weta_player_stream_title_vlv');
      $settings['player_id']   = $vlv_player_id;
      $content = theme('weta_player_streaming_player_html', $settings);
      $extra_text = variable_get('weta_player_vlv_extra_text', '');
      break;

    case 'audio-features':
      // Classical conversations player markup generation.
      $settings['player_id']  = $cc_player_id;
      $settings['text']       = $cc_media['markup']['text'];
      $settings['image']      = $cc_media['markup']['image'];
      $settings['title']      = $cc_media['markup']['title'];
      $content = theme('weta_player_cc_player_html', $settings);
      $extra_text = variable_get('weta_player_cc_extra_text', '');
      break;

    case '90-9FM':
    default:
      // Grab the player markup from the theme system.
      $content = theme('weta_player_streaming_player_html', $settings);
      $extra_text = variable_get('weta_player_extra_text', '');
  }

  // Add styles.
  drupal_add_css($path . '/css/reset.css');
  drupal_add_css($path . '/css/player_styles.css');
  _dfp_js_global_settings();

  // Load the JS and CSS.
  $script = drupal_get_js();
  $css = drupal_get_css();

  $classical_weta_link = weta_player_prep_link(variable_get('weta_player_classical_link', '#'));
  $donate_now_link = weta_player_prep_link(variable_get('weta_player_donate_link', '#'));

  // Build out page variables and send to theme system.
  $variables['includes'] = $script . $css;
  $variables['title'] = variable_get('weta_player_page_title', 'WETA - Listen Live');
  $variables['classical_weta_link'] = $classical_weta_link;
  $variables['donate_now_link'] = $donate_now_link;
  $variables['tabs'] = weta_player_generate_tabs($page);
  $variables['banner'] = $banner;
  $variables['right_ad'] = $right_ad;
  $variables['content']  = $content;
  $variables['extra_text']  = $extra_text;
  print theme('weta_player_listen_live_html', $variables);
  return NULL;
}

/**
 * Page callback to generate the "now playing" information.
 */
function weta_player_playlist($arg) {
  switch ($arg) {
    case '90-9FM':
      $display = 'classical_weta';
      break;

    case 'viva-la-voce':
      $display = 'viva_la_voce';
      break;

    default:
      $display = 'classical_weta';
  }
  // This is built from a view so just pull the proper view and then we'll load
  // it with JS/Ajax.
  $view = views_get_view('weta_player_now_playing');
  $view->set_display($display);
  $view->pre_execute();
  $view->execute();
  print $view->render();
  return NULL;
}

/*
 * Create the m3u playlist for Classical WETA
 *
 */
function weta_player_m3u_playlist() {

  $view = views_get_view('m3u_prerolls');
  $view->set_display('cw_m3u');
  $view->pre_execute();
  $view->execute();

  drupal_add_http_header('Content-Type', 'audio/mpegurl');

  $output = '';
  $output = $view->render();
  print $output;

  return NULL;
}

/*
 * Create the m3u playlist for Vivalavoce
 *
 * This should probably be handled by arguments so there's only one function.
 *
 */
function weta_player_vlv_m3u_playlist() {

  $view = views_get_view('m3u_prerolls');
  $view->set_display('vlv_m3u');
  $view->pre_execute();
  $view->execute();

  drupal_add_http_header('Content-Type', 'audio/mpegurl');

  $output = '';
  $output = $view->render();
  print $output;

  return NULL;
}

/*
 * Create the asx playlist for Classical WETA
 *
 */
function weta_player_asx_playlist() {

  $view = views_get_view('m3u_prerolls');
  $view->set_display('cw_asx');
  $view->pre_execute();
  $view->execute();

  drupal_add_http_header('Content-Type', 'video/x-ms-asf');

  $output = '';
  $output = $view->render();
  print $output;

  return NULL;
}

/*
 * Create the asx playlist for Vivalavoce
 *
 * This should probably be handled by arguments so there's only one function.
 *
 */
function weta_player_vlv_asx_playlist() {

  $view = views_get_view('m3u_prerolls');
  $view->set_display('vlv_asx');
  $view->pre_execute();
  $view->execute();

  drupal_add_http_header('Content-Type', 'video/x-ms-asf');

  $output = '';
  $output = $view->render();
  print $output;

  return NULL;
}

function weta_player_plaintext_addline(&$text, $add) {
  $text = $text . "\n" . $add;
}

/**
 * Helper funtion to make the tabs markup.
 *
 * @param string $page
 *   The incoming page arg.
 *
 * @return string
 *   Rendered markup of the tab menu.
 *
 * Removed Audio Features tab. Jess 9/25/2013. See also weta-player.css.
 */
function weta_player_generate_tabs($page) {
  $options = array(
    '90-9FM' => array(),
    'viva-la-voce' => array(),
    //'audio-features' => array(),
  );
  // We have alternate ways to get to the players so we need to set the active
  // class on links that meet these.
  // listen-live => listen-live/90-9FM
  // audio-features => audio-features/{nid}
  if (isset($options[$page])) {
    $options[$page]['attributes'] = array(
      'class' => array(
        'active',
      ),
    );
  }
  $items = array();
  $items[] = l(t('CLASSICAL WETA'), 'listen-live', $options['90-9FM']);
  $items[] = l(t('VIVALAVOCE'), 'listen-live/viva-la-voce', $options['viva-la-voce']);
  //$items[] = l(t('AUDIO FEATURES'), 'listen-live_z/audio-features', $options['audio-features']);
  return theme('item_list', array('items' => $items));
}

/**
 * Helper funtion to get the Classical Conversatiosn audio data.
 *
 * @param int $arg
 *   The arg passed to the page. This should be a node nid.
 *
 * @return array
 *   -media: a list of mp3 urls for cc audio features.
 *   -markup: an array of data to be displayed on the page.
 *     -image: A rendered image preview.
 *     -text: Rendered HTML of the description.
 */
function weta_player_get_cc_audio_data($arg) {
  // Sanitize argument.
  $cc_nid = FALSE;
  if (is_numeric($arg) && intval($arg) == $arg && $arg > 0) {
    $cc_nid = $arg;
  }
  // Get the classical conversation view. Pull the data off of it and make it
  // into an array to pass in as a Drupal setting.
  $cc_view = views_get_view('weta_player_classical_conversations', 'master');
  // If we got a valid argument, pass it to views.
  if ($cc_nid) {
    $cc_view->set_arguments(array($cc_nid));
  }
  $cc_view->execute();
  $cc_data = $cc_view->result;
  // We must render the path first as it is passed into the description as a
  // token.
  $cc_view->render_field('path', 0);
  $cc_title = $cc_view->render_field('title', 0);
  $cc_image = $cc_view->render_field('field_image', 0);
  $cc_text = $cc_view->render_field('field_short_description', 0);
  $media = array();
  // Technically we should only have one of these. But some flexability never
  // hurt. This will however need to be adapted to add the text and image for
  // multiples.
  foreach ($cc_data as $conversation) {
    // The player JS expects an array of objects.
    $resource = new stdClass();
    $resource->mp3 = file_create_url($conversation->file_managed_file_usage_uri);
    $resource->title = $conversation->node_title;
    $media[] = $resource;
  }
  $cc_return = array('title' => $cc_title, 'image' => $cc_image, 'text' => $cc_text);
  return array('media' => $media, 'markup' => $cc_return);
}

/**
 * Get a preroll audio file path from a preroll based on the associated ad id
 * from DFP.
 *
 * @param string/integer $ad_id
 *   The DFP ad id.
 *
 * @return string
 *   The url of the preroll audio file or NULL if the request is bad.
 */
function weta_player_get_preroll_by_ad_id($ad_id) {

  if (!$ad_id || !is_numeric($ad_id)) {
    return NULL;
  }
  // Generate a query to get the nid form the associated preroll's ad id.
  $query = db_select('node', 'n')
    ->fields('n', array('nid'))
    ->condition('n.type', 'preroll', '=');
  $query->join('field_data_field_ad_id', 'adid', 'n.nid = adid.entity_id AND n.vid = adid.revision_id');
  $query->condition('adid.entity_type', 'node', '=');
  $query->condition('adid.field_ad_id_value', $ad_id, '=');
  $query->range(0, 1);
  $result = $query->execute();
  $nid = $result->fetchField();

  // To prevent errors drop out if we don't get a node id.
  if (!$nid) {
    return NULL;
  };

  $node = node_load($nid);
  $langcode = field_language('node', $node, 'field_preroll_mp3');
  $path = $node->field_preroll_mp3[$langcode][0]['uri'];
  return $path;
}

/**
 * Use the nid of a preroll node to get the path to it's audio file.
 *
 * @param integer $nid
 *   The node nid.
 *
 * @return string
 *   The url of the preroll audio file or NULL if the request is bad.
 */
function weta_player_get_preroll_by_nid($nid) {
  if (!$nid || !is_numeric($nid)) {
    return NULL;
  }
  $node = node_load($nid);
  if (empty($node) || $node->type != 'preroll') {
    return NULL;
  }
  $langcode = field_language('node', $node, 'field_preroll_mp3');
  $path = $node->field_preroll_mp3[$langcode][0]['uri'];
  return $path;
}

/**
 * Adds preroll-to-DFP creative ID mappings to Drupal.settings.
 */
function weta_player_add_preroll_choices() {

  $mapping = array();

  // Load all published preroll nodes.

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'preroll')
    ->propertyCondition('status', 1);
  $result = $query->execute();

  if (isset($result['node'])) {

    // Get the ad ID and preroll URL from each node.

    $prerolls = $result['node'];
    $fields = field_info_instances('node', 'preroll');
    field_attach_load('node', $prerolls, FIELD_LOAD_CURRENT, array('field_id' => $fields['field_preroll_mp3']['field_id']));
    field_attach_load('node', $prerolls, FIELD_LOAD_CURRENT, array('field_id' => $fields['field_ad_id']['field_id']));

    foreach ($prerolls as $preroll) {
      $preroll_url = file_create_url($preroll-> field_preroll_mp3[LANGUAGE_NONE][0]['uri']);
      $ad_id = $preroll->field_ad_id[LANGUAGE_NONE][0]['safe_value'];
      $mapping[$ad_id] = $preroll_url;
    }

  }

  drupal_add_js(array('weta_player' => array(
    'creativeIdToPreroll' => $mapping,
  )), 'setting');

}


/**
 * Get the markup for an ad from DFP.
 * See http://support.google.com/dfp_premium/bin/answer.py?hl=en&answer=1638620
 * for more on this.
 *
 * @param string $region
 *   There are two ad regions, banner and right. This string designates which ad
 *   to generate.
 * @param string $page
 *   The argument supplied to the page to build the correct player.
 *
 * @return array
 *   -script: The script header to give back to the header
 *   -markup: The ad markup
 *   -ad: ad id pulled from the markup
 *
 * For reference a sample response from DFP follows:
 *
 <html><head><style><!--
 a:link { color: #000000 }a:visited { color: #000000 }a:hover { color: #000000 }a:active { color: #000000 }  --></style><script><!--
 (function(){window.ss=function(){};})();var viewReq = new Array();function vu(u) {var i=new Image();i.src=u.replace("&amp;","&");viewReq.push(i);}(function(){var b=!1;var d=function(a,f,k){var c=document;c.addEventListener?c.addEventListener(a,f,k||b):c.attachEvent&&c.attachEvent("on"+a,f)};var e,g=b,h=b;d("mousedown",function(){g=!0});d("keydown",function(){h=!0});document.addEventListener&&d("click",function(a){e=a},!0);window.accbk=function(){var a=e?e:window.event;return!a?b:!g&&!h?(a.preventDefault?a.preventDefault():a.returnValue=b,!0):b};})();function st(id) {var a = document.getElementById(id);if (a) {}}function ha(a,x){  if (accbk()) return;}function ca(a) {  top.location.href=document.getElementById(a).href;}function ia(a,e,x) {if (accbk()) return;}function ga(o,e,x) {if (document.getElementById) {var a=o.id.substring(1),p="",r="",g=e.target,t,f,h;if (g) {t=g.id;f=g.parentNode;if (f) {p=f.id;h=f.parentNode;if (h)r=h.id;}} else {h=e.srcElement;f=h.parentNode;if (f)p=f.id;t=h.id;}if (t==a||p==a||r==a)return true;ia(a,e,x);top.location.href=document.getElementById(a).href;}}
 //-->
 </script></head><body leftMargin="0" topMargin="0" marginwidth="0" marginheight="0" style="background:transparent" ><div id="google_image_div" style="overflow:hidden; position:absolute"><script>vu("http://weta.org/adsink/?wetaAdId\x3d22402863242")</script><a id="aw0" target="_top" href="http://googleads.g.doubleclick.net/aclk?sa=L&ai=COG3VaKJuUcawC6He0AHepYCQBrrswqwDAAAQASDyhY8dUP6T8LYCYMn2pozQpOQPyAEC4AIAqAMBqgRQT9BZqghlunMr0rBVhqNUfY7F8EXxzp3PpAtb9XB3zXyooBy9eE3C5KrER_6eVBROmDF1kpg9qMPvaQ8VzcTb_t7MIfM3M0IVRmMQwn8sX_LgBAGgBhQ&num=0&sig=AOD64_3ld4mbGfeYd9ZCeRZV8yLPcoZWDA&client=ca-pub-0207519598676346&adurl=http://bing.com" onFocus="ss('aw0')" onMouseDown="st('aw0')" onMouseOver="ss('aw0')" onClick="ha('aw0')"><img src="http://pagead2.googlesyndication.com/simgad/12853439704647283739" border="0" width="728" height="90" alt="something alternative." class="img_ad"  /></a><script>var onDrtLoad = function() {if (typeof posWidgetIframeList != 'undefined' && posWidgetIframeList) {for (var i = 0; i < posWidgetIframeList.length; i++) {posWidgetIframeList[i].G_handleAdDoritosFlowDone();}}};</script><iframe frameborder=0 height=0 width=0 src="http://googleads.g.doubleclick.net/pagead/drt/s?v=r20120211" style="position:absolute" onload="onDrtLoad()"></iframe></div></body></html>
 *
 */
function weta_player_get_dfp_ad($region, $page = '90-9FM') {
  if ($region == 'banner') {
    $size = '728x90';
    switch ($page) {
      case '90-9FM':
        $ad_container = variable_get('weta_player_banner_ad', '');
        $cache = 'weta_player_banner_cache';
        $cache_time = 'weta_banner_cache_time';
        break;

      case 'viva-la-voce':
        $ad_container = variable_get('weta_player_banner_ad_vlv', '');
        $cache = 'weta_player_banner_cache_vlv';
        $cache_time = 'weta_banner_cache_time_vlv';
        break;

      case 'audio-features':
        $ad_container = variable_get('weta_player_banner_ad_cc', '');
        $cache = 'weta_player_banner_cache_cc';
        $cache_time = 'weta_banner_cache_time_cc';
        break;

      default:
        $ad_container = variable_get('weta_player_banner_ad', '');
    }
  }
  elseif ($region == 'right') {
    $size = '300x250';
    switch ($page) {
      case '90-9FM':
        $ad_container = variable_get('weta_player_right_ad', '');
        $cache = 'weta_player_right_cache';
        $cache_time = 'weta_right_cache_time';
        break;

      case 'viva-la-voce':
        $ad_container = variable_get('weta_player_right_ad_vlv', '');
        $cache = 'weta_player_right_cache_vlv';
        $cache_time = 'weta_right_cache_time_vlv';
        break;

      case 'audio-features':
        $ad_container = variable_get('weta_player_right_ad_cc', '');
        $cache = 'weta_player_right_cache_cc';
        $cache_time = 'weta_right_cache_time_cc';
        break;

      default:
        $ad_container = variable_get('weta_player_right_ad', '');
    }
  }

  $account_id = variable_get('weta_player_dfp_account_id', '');
  $random = rand(0, 1000000);
  $url = 'http://pubads.g.doubleclick.net/gampad/adx?iu=/' . $account_id . '/' . $ad_container . '&sz=' . $size . '&c=' . $random . 'm=text/xml';
  $ad_response = drupal_http_request($url, array('timeout' => 5));

  if (!empty($ad_response->data)) {
    $ad_markup = $ad_response->data;
    // Stash the response away in case DFP doesn't give us something next time.
    // Do this only after ten minutes. We don't need every response.
    if (REQUEST_TIME - variable_get($cache_time, 0) > 600) {
      variable_set($cache, $ad_response->data);
      variable_set($cache_time, REQUEST_TIME);
    }
  }
  else {
    // DFP punked out on us. So pull something from the cache.
    watchdog('WETA Player', 'Served an ad from cache. Ad was: %adc, served in region: %reg, of page: %page, at time: %time.', array(
      '%adc' => $ad_container,
      '%reg' => $region,
      '%page' => $page,
      '%time' => date('M d Y - H:i:s', REQUEST_TIME)
    ));
    $ad_markup = variable_get($cache, '');
  }
  // Strip all the junk of the response.
  // A sample response for reference is at the top of this function.
  $search = array(
    '/\<head/',
    '/\<html/',
    '/\<body/',
    '/\<\/head/',
    '/\<\/html/',
    '/\<\/body/',
  );
  $replace = array(
    '<div',
    '<div',
    '<div',
    '</div',
    '</div',
    '</div',
  );
  $ad_markup = preg_replace($search, $replace, $ad_markup);

  $ad_id = weta_player_parse_ad_id($ad_markup);

  // Pull off the first script tag.
  $script_start = strpos($ad_markup, '<script><!--');
  $script_end = strpos($ad_markup, '//-->');
  $script = substr($ad_markup, $script_start + strlen('<script><!--') , $script_end - ($script_start + strlen('<script><!--')));

  return array('script' => $script, 'markup' => $ad_markup, 'ad' => $ad_id);
}

/**
 * TODO
 *
 */
function weta_player_parse_ad_id($text) {
  // Find the ad id from the token we placed on DFP.
  $ad_id_param = 'wetaAdId\\x3d';
  $ad_id_pos = strpos($text, $ad_id_param);
  $ad_id = NULL;
  if ($ad_id_pos !== FALSE) {
    $end_pos = strpos($text, '"', $ad_id_pos);
    $ad_id = substr($text, $ad_id_pos + strlen($ad_id_param), $end_pos - ($ad_id_pos + strlen($ad_id_param)));
  }
  return $ad_id;
}

/**
 * Helper function to get the defautl preroll nid based on the incoming path.
 *
 * @param string $page
 *   The argument passed to the menu callback to build the page.
 *
 * @return int
 *   Node nid of the deault preroll node. NULL if one was not set.
 */
function weta_player_get_default_preroll_node($page) {
  $nid = NULL;
  switch ($page) {
    case '90-9FM':
      $nid = variable_get('weta_player_default_preroll', NULL);
      break;
    case 'viva-la-voce':
      $nid = variable_get('weta_player_default_preroll_vlv', NULL);
      break;
    case 'audio-features':
      $nid = variable_get('weta_player_default_preroll', NULL);
      break;
    default:
      $nid = variable_get('weta_player_default_preroll', NULL);
  }
  return $nid;
}

/**
 * Helper function to deal with internal vs extneral link structure.
 */
function weta_player_prep_link($url) {
  if (strpos($url, 'http://') === 0 || strpos($url, 'https://') === 0) {
    return $url;
  }
  else {
    // internal link
    return base_path() . $url;
  }
}
