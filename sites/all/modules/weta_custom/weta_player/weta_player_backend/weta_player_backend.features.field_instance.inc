<?php
/**
 * @file
 * weta_player_backend.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function weta_player_backend_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-preroll-field_ad_id'
  $field_instances['node-preroll-field_ad_id'] = array(
    'bundle' => 'preroll',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'This is the Creative ID for the 728x90 banner creative in DFP. The ID for each creative is displayed on the line item listing. This same value will need to be set either at the end of the alt text or as the final parameter to the third party URL configuration options.<br />
Examples:<br />
Third Party URL: http://weta.org/adsink?wetaAdId=xxxxxx<br />
Alt Text: This is an image of a bottle of soda wetaAdId=xxxxxx<br />',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'div',
    'field_name' => 'field_ad_id',
    'label' => 'Ad Id',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'node-preroll-field_display_dates'
  $field_instances['node-preroll-field_display_dates'] = array(
    'bundle' => 'preroll',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
          'show_repeat_rule' => 'show',
        ),
        'type' => 'date_default',
        'weight' => 4,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'div',
    'field_name' => 'field_display_dates',
    'label' => 'Display Dates',
    'required' => 1,
    'settings' => array(
      'default_value' => 'now',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'display_all_day' => 0,
        'increment' => 15,
        'input_format' => 'm/d/Y - g:i:sa',
        'input_format_custom' => '',
        'label_position' => 'above',
        'repeat_collapsed' => 0,
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_popup',
      'weight' => 33,
    ),
  );

  // Exported field_instance: 'node-preroll-field_fallback'
  $field_instances['node-preroll-field_fallback'] = array(
    'bundle' => 'preroll',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 5,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'div',
    'field_name' => 'field_fallback',
    'label' => 'Fallback',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 0,
      ),
      'type' => 'options_buttons',
      'weight' => 34,
    ),
  );

  // Exported field_instance: 'node-preroll-field_fm_station'
  $field_instances['node-preroll-field_fm_station'] = array(
    'bundle' => 'preroll',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'div',
    'field_name' => 'field_fm_station',
    'label' => 'Station',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 32,
    ),
  );

  // Exported field_instance: 'node-preroll-field_preroll_mp3'
  $field_instances['node-preroll-field_preroll_mp3'] = array(
    'bundle' => 'preroll',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'file',
        'settings' => array(),
        'type' => 'file_default',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'div',
    'field_name' => 'field_preroll_mp3',
    'label' => 'Preroll MP3',
    'required' => 1,
    'settings' => array(
      'description_field' => 0,
      'file_directory' => 'preroll',
      'file_extensions' => 'mp3',
      'insert' => array(
        'manualcrop_filter_insert' => 0,
      ),
      'max_filesize' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'media',
      'settings' => array(
        'allowed_schemes' => array(
          'private' => 'private',
          'public' => 'public',
        ),
        'allowed_types' => array(
          'audio' => 0,
          'document' => 0,
          'image' => 'image',
          'video' => 0,
        ),
        'browser_plugins' => array(
          'media_default--media_browser_1' => 'media_default--media_browser_1',
          'media_default--media_browser_my_files' => 'media_default--media_browser_my_files',
          'upload' => 'upload',
        ),
        'manualcrop_crop_info' => 1,
        'manualcrop_default_crop_area' => 1,
        'manualcrop_enable' => 0,
        'manualcrop_filter_insert' => TRUE,
        'manualcrop_inline_crop' => FALSE,
        'manualcrop_instant_crop' => FALSE,
        'manualcrop_instant_preview' => 1,
        'manualcrop_keyboard' => TRUE,
        'manualcrop_maximize_default_crop_area' => FALSE,
        'manualcrop_require_cropping' => array(
          'calendar_basic' => 0,
          'calendar_highlight' => 0,
          'cropped_banner' => 0,
          'feature_square' => 0,
          'flyout_highlight' => 0,
          'full_width' => 0,
          'highlight_20-80' => 0,
          'kids_crop' => 0,
          'merlin' => 0,
          'mini_feature' => 0,
          'now_playing_logo' => 0,
          'productions_highlight' => 0,
          'squarish' => 0,
          'tight_crop' => 0,
          'tv_banner' => 0,
          'wide_feature' => 0,
        ),
        'manualcrop_styles_list' => array(
          'calendar_basic' => 0,
          'calendar_highlight' => 0,
          'cropped_banner' => 0,
          'feature_square' => 0,
          'flyout_highlight' => 0,
          'full_width' => 0,
          'highlight_20-80' => 0,
          'kids_crop' => 0,
          'merlin' => 0,
          'mini_feature' => 0,
          'now_playing_logo' => 0,
          'productions_highlight' => 0,
          'squarish' => 0,
          'tight_crop' => 0,
          'tv_banner' => 0,
          'wide_feature' => 0,
        ),
        'manualcrop_styles_mode' => 'exclude',
        'manualcrop_thumblist' => 0,
        'progress_indicator' => 'throbber',
      ),
      'type' => 'media_generic',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'taxonomy_term-fm_channel-field_feature_image'
  $field_instances['taxonomy_term-fm_channel-field_feature_image'] = array(
    'bundle' => 'fm_channel',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 0,
      ),
      'image_only' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => 'now_playing_logo',
        ),
        'type' => 'image',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_feature_image',
    'label' => 'Image',
    'required' => 0,
    'settings' => array(
      'alt_field' => 1,
      'default_image' => 0,
      'epsacrop' => array(
        'styles' => array(
          'merlin' => 0,
          'now_playing_logo' => 'now_playing_logo',
          'squarish' => 0,
        ),
      ),
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'filefield_sources' => array(
          'filefield_sources' => array(
            'attach' => 0,
            'clipboard' => 0,
            'reference' => 0,
            'remote' => 0,
          ),
          'source_attach' => array(
            'absolute' => 0,
            'attach_mode' => 'move',
            'path' => 'file_attach',
          ),
          'source_imce' => array(
            'imce_mode' => 0,
          ),
          'source_reference' => array(
            'autocomplete' => 0,
          ),
        ),
        'insert' => 0,
        'insert_absolute' => 0,
        'insert_class' => '',
        'insert_default' => 'auto',
        'insert_styles' => array(
          'auto' => 'auto',
          'icon_link' => 0,
          'image' => 0,
          'image_epsacrop_thumb' => 0,
          'image_flexslider_full' => 0,
          'image_large' => 0,
          'image_medium' => 0,
          'image_merlin' => 0,
          'image_now_playing_logo' => 0,
          'image_square_thumbnail' => 0,
          'image_squarish' => 0,
          'image_thumbnail' => 0,
          'link' => 0,
        ),
        'insert_width' => '',
        'manualcrop_crop_info' => TRUE,
        'manualcrop_default_crop_area' => TRUE,
        'manualcrop_enable' => FALSE,
        'manualcrop_filter_insert' => TRUE,
        'manualcrop_inline_crop' => FALSE,
        'manualcrop_instant_crop' => FALSE,
        'manualcrop_instant_preview' => TRUE,
        'manualcrop_keyboard' => TRUE,
        'manualcrop_maximize_default_crop_area' => FALSE,
        'manualcrop_require_cropping' => array(),
        'manualcrop_styles_list' => array(),
        'manualcrop_styles_mode' => 'include',
        'manualcrop_thumblist' => FALSE,
        'preview_image_style' => 'now_playing_logo',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'taxonomy_term-fm_channel-field_horizontal_image'
  $field_instances['taxonomy_term-fm_channel-field_horizontal_image'] = array(
    'bundle' => 'fm_channel',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 1,
      ),
      'image_only' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'fences_wrapper' => 'div',
    'field_name' => 'field_horizontal_image',
    'label' => 'Horizontal Image',
    'required' => 0,
    'settings' => array(
      'alt_field' => 1,
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'filefield_sources' => array(
          'filefield_sources' => array(
            'attach' => 0,
            'clipboard' => 0,
            'reference' => 0,
            'remote' => 0,
            'upload' => 'upload',
          ),
          'source_attach' => array(
            'absolute' => 0,
            'attach_mode' => 'move',
            'path' => 'file_attach',
          ),
          'source_imce' => array(
            'imce_mode' => 0,
          ),
          'source_reference' => array(
            'autocomplete' => 0,
          ),
        ),
        'insert' => 0,
        'insert_absolute' => 0,
        'insert_class' => '',
        'insert_default' => 'auto',
        'insert_styles' => array(
          'auto' => 'auto',
          'icon_link' => 0,
          'image' => 0,
          'image_calendar_basic' => 0,
          'image_calendar_highlight' => 0,
          'image_cropped_banner' => 0,
          'image_feature_square' => 0,
          'image_flexslider_full' => 0,
          'image_flyout_highlight' => 0,
          'image_fm_feature_logo' => 0,
          'image_full' => 0,
          'image_full_width' => 0,
          'image_highlight_20-80' => 0,
          'image_kids_crop' => 0,
          'image_large' => 0,
          'image_media_thumbnail' => 0,
          'image_medium' => 0,
          'image_merlin' => 0,
          'image_mini_feature' => 0,
          'image_now_playing_logo' => 0,
          'image_productions_highlight' => 0,
          'image_squarish' => 0,
          'image_thumbnail' => 0,
          'image_tight_crop' => 0,
          'image_tv_banner' => 0,
          'image_wanted' => 0,
          'image_wanted_thumbnail' => 0,
          'image_weta_player' => 0,
          'image_wide_feature' => 0,
          'link' => 0,
        ),
        'insert_width' => '',
        'manualcrop_crop_info' => 1,
        'manualcrop_default_crop_area' => 1,
        'manualcrop_enable' => 0,
        'manualcrop_filter_insert' => 0,
        'manualcrop_inline_crop' => 0,
        'manualcrop_instant_crop' => 0,
        'manualcrop_instant_preview' => 1,
        'manualcrop_keyboard' => TRUE,
        'manualcrop_maximize_default_crop_area' => 0,
        'manualcrop_require_cropping' => array(),
        'manualcrop_styles_list' => array(),
        'manualcrop_styles_mode' => 'include',
        'manualcrop_thumblist' => 0,
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 6,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Ad Id');
  t('Display Dates');
  t('Fallback');
  t('Horizontal Image');
  t('Image');
  t('Preroll MP3');
  t('Station');
  t('This is the Creative ID for the 728x90 banner creative in DFP. The ID for each creative is displayed on the line item listing. This same value will need to be set either at the end of the alt text or as the final parameter to the third party URL configuration options.<br />
Examples:<br />
Third Party URL: http://weta.org/adsink?wetaAdId=xxxxxx<br />
Alt Text: This is an image of a bottle of soda wetaAdId=xxxxxx<br />');

  return $field_instances;
}
