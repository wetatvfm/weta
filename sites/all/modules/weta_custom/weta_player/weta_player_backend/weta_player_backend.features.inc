<?php
/**
 * @file
 * weta_player_backend.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function weta_player_backend_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function weta_player_backend_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function weta_player_backend_image_default_styles() {
  $styles = array();

  // Exported image style: weta_player.
  $styles['weta_player'] = array(
    'name' => 'weta_player',
    'label' => 'WETA Player (50x50)',
    'effects' => array(
      19 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 50,
          'height' => 50,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function weta_player_backend_node_info() {
  $items = array(
    'preroll' => array(
      'name' => t('Preroll'),
      'base' => 'node_content',
      'description' => t('A preroll is an audio file that is associated with a sponsorship. It will be played before the audio stream on the online player. A preroll is associated with an advertisement that will be shown on the player.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
