<?php
/**
 * @file
 * weta_player_backend.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function weta_player_backend_taxonomy_default_vocabularies() {
  return array(
    'fm_channel' => array(
      'name' => 'FM Channel',
      'machine_name' => 'fm_channel',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
