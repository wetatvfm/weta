<?php
/**
 * @file
 * weta_player_backend.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function weta_player_backend_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_m3u|node|preroll|form';
  $field_group->group_name = 'group_m3u';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'preroll';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'For m3u playlist',
    'weight' => '4',
    'children' => array(
      0 => 'field_fm_station',
      1 => 'field_display_dates',
      2 => 'field_fallback',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-m3u field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_m3u|node|preroll|form'] = $field_group;

  return $export;
}
