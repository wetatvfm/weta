<?php
/**
 * @file
 * weta_player_backend.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function weta_player_backend_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'weta_player_classical_conversations';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'WETA Player Classical Conversations';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: File Usage: File */
  $handler->display->display_options['relationships']['node_to_file']['id'] = 'node_to_file';
  $handler->display->display_options['relationships']['node_to_file']['table'] = 'file_usage';
  $handler->display->display_options['relationships']['node_to_file']['field'] = 'node_to_file';
  $handler->display->display_options['relationships']['node_to_file']['required'] = TRUE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: File: Path */
  $handler->display->display_options['fields']['uri']['id'] = 'uri';
  $handler->display->display_options['fields']['uri']['table'] = 'file_managed';
  $handler->display->display_options['fields']['uri']['field'] = 'uri';
  $handler->display->display_options['fields']['uri']['relationship'] = 'node_to_file';
  $handler->display->display_options['fields']['uri']['label'] = '';
  $handler->display->display_options['fields']['uri']['element_label_colon'] = FALSE;
  /* Field: Field: Image */
  $handler->display->display_options['fields']['field_image']['id'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['table'] = 'field_data_field_image';
  $handler->display->display_options['fields']['field_image']['field'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['label'] = '';
  $handler->display->display_options['fields']['field_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_image']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_image']['settings'] = array(
    'image_style' => 'weta_player',
    'image_link' => '',
  );
  /* Field: Content: Short description */
  $handler->display->display_options['fields']['field_short_description']['id'] = 'field_short_description';
  $handler->display->display_options['fields']['field_short_description']['table'] = 'field_data_field_short_description';
  $handler->display->display_options['fields']['field_short_description']['field'] = 'field_short_description';
  $handler->display->display_options['fields']['field_short_description']['label'] = '';
  $handler->display->display_options['fields']['field_short_description']['alter']['max_length'] = '150';
  $handler->display->display_options['fields']['field_short_description']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['field_short_description']['alter']['html'] = TRUE;
  $handler->display->display_options['fields']['field_short_description']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_short_description']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_short_description']['settings'] = array(
    'trim_length' => '150',
  );
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['nid']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['nid']['validate']['type'] = 'node';
  $handler->display->display_options['arguments']['nid']['validate_options']['types'] = array(
    'fm_classical_conversations' => 'fm_classical_conversations',
  );
  $handler->display->display_options['arguments']['nid']['validate_options']['access'] = TRUE;
  $handler->display->display_options['arguments']['nid']['validate']['fail'] = 'ignore';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'fm_classical_conversations' => 'fm_classical_conversations',
  );
  $export['weta_player_classical_conversations'] = $view;

  $view = new view();
  $view->name = 'weta_player_now_playing';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Weta Player Now Playing';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Now Playing';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'access weta player';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Composer */
  $handler->display->display_options['fields']['field_composer']['id'] = 'field_composer';
  $handler->display->display_options['fields']['field_composer']['table'] = 'field_data_field_composer';
  $handler->display->display_options['fields']['field_composer']['field'] = 'field_composer';
  $handler->display->display_options['fields']['field_composer']['label'] = '';
  $handler->display->display_options['fields']['field_composer']['element_type'] = 'h5';
  $handler->display->display_options['fields']['field_composer']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_composer']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_composer']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_composer']['settings'] = array(
    'linked_field' => array(
      'linked' => 0,
      'destination' => '',
      'advanced' => array(
        'title' => '',
        'target' => '',
        'rel' => '',
        'text' => '',
        'foo' => 'bar',
        'class' => '',
      ),
    ),
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = '0';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_wrapper_type'] = 'p';
  $handler->display->display_options['fields']['title']['element_wrapper_class'] = 'italic spaced';
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Playlist Type */
  $handler->display->display_options['fields']['field_playlist_type']['id'] = 'field_playlist_type';
  $handler->display->display_options['fields']['field_playlist_type']['table'] = 'field_data_field_playlist_type';
  $handler->display->display_options['fields']['field_playlist_type']['field'] = 'field_playlist_type';
  $handler->display->display_options['fields']['field_playlist_type']['label'] = '';
  $handler->display->display_options['fields']['field_playlist_type']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_playlist_type']['element_type'] = 'h4';
  $handler->display->display_options['fields']['field_playlist_type']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_playlist_type']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_playlist_type']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_playlist_type']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_playlist_type']['settings'] = array(
    'linked_field' => array(
      'linked' => 0,
      'destination' => '',
      'advanced' => array(
        'title' => '',
        'target' => '',
        'rel' => '',
        'text' => '',
        'foo' => 'bar',
        'class' => '',
      ),
    ),
  );
  /* Field: Content: Buy URL */
  $handler->display->display_options['fields']['field_buy_url']['id'] = 'field_buy_url';
  $handler->display->display_options['fields']['field_buy_url']['table'] = 'field_data_field_buy_url';
  $handler->display->display_options['fields']['field_buy_url']['field'] = 'field_buy_url';
  $handler->display->display_options['fields']['field_buy_url']['label'] = '';
  $handler->display->display_options['fields']['field_buy_url']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_buy_url']['element_type'] = '0';
  $handler->display->display_options['fields']['field_buy_url']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_buy_url']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_buy_url']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_buy_url']['type'] = 'computed_field_unsanitized';
  $handler->display->display_options['fields']['field_buy_url']['settings'] = array(
    'linked_field' => array(
      'linked' => 0,
      'destination' => '',
      'advanced' => array(
        'title' => '',
        'target' => '',
        'rel' => '',
        'text' => '',
        'foo' => 'bar',
        'class' => '',
      ),
    ),
  );
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['nid']['alter']['text'] = 'Buy the CD &rarr;';
  $handler->display->display_options['fields']['nid']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['nid']['alter']['path'] = '[field_buy_url]';
  $handler->display->display_options['fields']['nid']['alter']['external'] = TRUE;
  $handler->display->display_options['fields']['nid']['alter']['trim_whitespace'] = TRUE;
  $handler->display->display_options['fields']['nid']['alter']['alt'] = 'Buy the CD';
  $handler->display->display_options['fields']['nid']['alter']['target'] = '_blank';
  $handler->display->display_options['fields']['nid']['element_type'] = '0';
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['nid']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['nid']['element_default_classes'] = FALSE;
  /* Sort criterion: Content: Time (field_start_time) */
  $handler->display->display_options['sorts']['field_start_time_value']['id'] = 'field_start_time_value';
  $handler->display->display_options['sorts']['field_start_time_value']['table'] = 'field_data_field_start_time';
  $handler->display->display_options['sorts']['field_start_time_value']['field'] = 'field_start_time_value';
  $handler->display->display_options['sorts']['field_start_time_value']['order'] = 'DESC';
  /* Sort criterion: Content: Day of the Week (field_day) */
  $handler->display->display_options['sorts']['field_day_value']['id'] = 'field_day_value';
  $handler->display->display_options['sorts']['field_day_value']['table'] = 'field_data_field_day';
  $handler->display->display_options['sorts']['field_day_value']['field'] = 'field_day_value';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'fm_playlist' => 'fm_playlist',
  );
  /* Filter criterion: Content: Time (field_start_time) */
  $handler->display->display_options['filters']['field_start_time_value']['id'] = 'field_start_time_value';
  $handler->display->display_options['filters']['field_start_time_value']['table'] = 'field_data_field_start_time';
  $handler->display->display_options['filters']['field_start_time_value']['field'] = 'field_start_time_value';
  $handler->display->display_options['filters']['field_start_time_value']['operator'] = '<=';
  $handler->display->display_options['filters']['field_start_time_value']['granularity'] = 'second';
  $handler->display->display_options['filters']['field_start_time_value']['default_date'] = 'now';
  $handler->display->display_options['filters']['field_start_time_value']['default_to_date'] = 'now +1 day';
  $handler->display->display_options['filters']['field_start_time_value']['year_range'] = '-3:+1';
  /* Filter criterion: Content: Non-Music? (field_non_music) */
  $handler->display->display_options['filters']['field_non_music_value']['id'] = 'field_non_music_value';
  $handler->display->display_options['filters']['field_non_music_value']['table'] = 'field_data_field_non_music';
  $handler->display->display_options['filters']['field_non_music_value']['field'] = 'field_non_music_value';
  $handler->display->display_options['filters']['field_non_music_value']['value'] = 'N';
  /* Filter criterion: Content: Playlist Type (field_playlist_type) */
  $handler->display->display_options['filters']['field_playlist_type_tid']['id'] = 'field_playlist_type_tid';
  $handler->display->display_options['filters']['field_playlist_type_tid']['table'] = 'field_data_field_playlist_type';
  $handler->display->display_options['filters']['field_playlist_type_tid']['field'] = 'field_playlist_type_tid';
  $handler->display->display_options['filters']['field_playlist_type_tid']['value'] = array(
    0 => '1',
  );
  $handler->display->display_options['filters']['field_playlist_type_tid']['vocabulary'] = 'fm_channel';

  /* Display: Content pane - Classical Weta */
  $handler = $view->new_display('panel_pane', 'Content pane - Classical Weta', 'classica_weta');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Now Playing - Classical Weta';
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;

  /* Display: Content pane - Viva La Voce */
  $handler = $view->new_display('panel_pane', 'Content pane - Viva La Voce', 'viva_la_voce');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Now Playing - Viva La Voce';
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'fm_playlist' => 'fm_playlist',
  );
  /* Filter criterion: Content: Time (field_start_time) */
  $handler->display->display_options['filters']['field_start_time_value']['id'] = 'field_start_time_value';
  $handler->display->display_options['filters']['field_start_time_value']['table'] = 'field_data_field_start_time';
  $handler->display->display_options['filters']['field_start_time_value']['field'] = 'field_start_time_value';
  $handler->display->display_options['filters']['field_start_time_value']['operator'] = '<=';
  $handler->display->display_options['filters']['field_start_time_value']['granularity'] = 'second';
  $handler->display->display_options['filters']['field_start_time_value']['default_date'] = 'now';
  $handler->display->display_options['filters']['field_start_time_value']['default_to_date'] = 'now +1 day';
  $handler->display->display_options['filters']['field_start_time_value']['year_range'] = '-3:+1';
  /* Filter criterion: Content: Non-Music? (field_non_music) */
  $handler->display->display_options['filters']['field_non_music_value']['id'] = 'field_non_music_value';
  $handler->display->display_options['filters']['field_non_music_value']['table'] = 'field_data_field_non_music';
  $handler->display->display_options['filters']['field_non_music_value']['field'] = 'field_non_music_value';
  $handler->display->display_options['filters']['field_non_music_value']['value'] = 'N';
  /* Filter criterion: Content: Playlist Type (field_playlist_type) */
  $handler->display->display_options['filters']['field_playlist_type_tid']['id'] = 'field_playlist_type_tid';
  $handler->display->display_options['filters']['field_playlist_type_tid']['table'] = 'field_data_field_playlist_type';
  $handler->display->display_options['filters']['field_playlist_type_tid']['field'] = 'field_playlist_type_tid';
  $handler->display->display_options['filters']['field_playlist_type_tid']['value'] = array(
    0 => '2',
  );
  $handler->display->display_options['filters']['field_playlist_type_tid']['vocabulary'] = 'fm_channel';
  $export['weta_player_now_playing'] = $view;

  return $export;
}
