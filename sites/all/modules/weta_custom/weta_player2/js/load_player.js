/**
 * @file
 * Load weta streaming audio players.
 */
(function ($) {
  Drupal.behaviors.wetaPlayerLoader = {
    attach: function(context, settings) {
      var swfPath = Drupal.settings.wetaPlayerSettings.swfPath;
      var streamPath = Drupal.settings.wetaPlayerSettings.streamPath;
      var streamPlayerId = Drupal.settings.wetaPlayerSettings.streamPlayerId;
      var prerollPath = Drupal.settings.wetaPlayerSettings.prerollPath;
      var vlvStreamPath = Drupal.settings.wetaPlayerSettings.vlvStreamPath;
      var vlvPlayerId = Drupal.settings.wetaPlayerSettings.vlvPlayerId;

      var ccPlayerId = Drupal.settings.wetaPlayerSettings.ccPlayerId;
      var ccMedia = Drupal.settings.wetaPlayerSettings.ccMedia;

      // Detect browser for flash fallback.
      var detectBrowser = function() {
        var N = navigator.appName, ua= navigator.userAgent, tem;
        var M = ua.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);
        if(M && (tem= ua.match(/version\/([\.\d]+)/i))!= null) M[2]= tem[1];
        M = M ? [M[1], M[2]] : [N, navigator.appVersion,'-?'];
        return M;
      };
      var browserSolution = 'flash, html';
      if (detectBrowser()[0] == 'Firefox' || detectBrowser()[0] == 'Chrome') {
        browserSolution = 'flash, html';
      }
      var stream = {
        title: 'WETA',
        mp3: streamPath
      };
      var vlvStream = {
        title: 'Viva La Voce',
        mp3: vlvStreamPath
      };
      var preroll = {
        mp3: prerollPath
      };

      var ready = false;
      var vlvReady = false;
      var pass = false;
      var vlvPass = false;

      // Streaming player.
      $('#jquery_jplayer_' + streamPlayerId).jPlayer({
        ready: function (event) {
          ready = true;
          $(this).jPlayer('setMedia', preroll).jPlayer('play');
          // If no preroll, just jump to the stream.
          if (!preroll.mp3) {
            pass = true;
            $(this).jPlayer('setMedia', stream).jPlayer('play');
          }
        },
        pause: function() {
          if (!pass) {
            return false;
          }
          $(this).jPlayer('clearMedia');
        },
        error: function(event) {
          if(ready && event.jPlayer.error.type === $.jPlayer.error.URL_NOT_SET) {
            // Setup the media stream again and play it.
            $(this).jPlayer('setMedia', stream).jPlayer('play');
          }
        },
        ended: function () {
          pass = true;
          $(this).jPlayer('setMedia', stream).jPlayer('play');
        },
        swfPath: swfPath,
        supplied: 'mp3',
        preload: 'none',
        wmode: 'window',
        solution: browserSolution,
        cssSelectorAncestor: "#jp_container_" + streamPlayerId
      });

      // Viva La Voce streaming player.
      $('#jquery_jplayer_' + vlvPlayerId).jPlayer({
        ready: function (event) {
          vlvReady = true;
          $(this).jPlayer('setMedia', preroll).jPlayer('play');
          // If no preroll, just jump to the stream.
          if (!preroll.mp3) {
            vlvPass = true;
            $(this).jPlayer('setMedia', vlvStream).jPlayer('play');
          }
        },
        pause: function() {
          if (!vlvPass) {
            return false;
          }
          $(this).jPlayer('clearMedia');
        },
        error: function(event) {
          if(ready && event.jPlayer.error.type === $.jPlayer.error.URL_NOT_SET) {
            // Setup the media stream again and play it.
            $(this).jPlayer('setMedia', vlvStream).jPlayer('play');
          }
        },
        ended: function () {
          vlvPass = true;
          $(this).jPlayer('setMedia', vlvStream).jPlayer('play');
        },
        swfPath: swfPath,
        supplied: 'mp3',
        preload: 'none',
        wmode: 'window',
        solution: browserSolution,
        cssSelectorAncestor: "#jp_container_" + vlvPlayerId
      });

      // Classical Conversations Player.
      new jPlayerPlaylist({
          jPlayer: "#jquery_jplayer_" + ccPlayerId,
          cssSelectorAncestor: "#jp_container_" + ccPlayerId
        },
        ccMedia,
        {
          swfPath: swfPath,
          supplied: "mp3",
          solution: browserSolution
        }
      );
    }
  }
}(jQuery));
