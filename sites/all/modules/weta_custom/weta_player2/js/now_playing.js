/**
 * @file
 * Ajax fetch now playing info for the weta streaming player.
 */
(function ($) {
  Drupal.behaviors.wetaPlayerNowPlaying = {
    attach: function(context, settings) {
      var container = $('#now-playing');
      var page = Drupal.settings.wetaPlayerSettings.playerPage;
      var base = Drupal.settings.wetaPlayerSettings.basePath;
      // Set a variable that will self reload every 60 seconds.
      var nowPlaying = self.setInterval(function() {reload()}, 60000);
      function reload() {
        container.load(base + 'listen-live/now-playing/' + page);
      }
      // Inital load of the data.
      reload();
    }
  }
}(jQuery));
