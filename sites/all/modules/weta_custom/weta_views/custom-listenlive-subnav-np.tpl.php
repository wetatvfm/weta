<?php
  // listen live button
  $popuplink = '/listen-live';
  $popup = custom_listenlive_popup($popuplink);
  $listenlink = l('Listen Live', $popuplink, array('attributes' => array('onclick' => $popup, 'id' => 'listenlive'), 'html' => TRUE));
  //$listenlink = l('Listen Live', $popuplink, array('html' => TRUE));
?>

<ul class="radio">
  <li><?php print $listenlink; ?></li>
  <li><a href="#" id="nowplay">Now Playing</a>
    <div id="np_flyout">
      <ul>
      <li>CLASSICAL WETA <?php print $cw_nowplaying['content']; ?></li>
      <li>VIVALAVOCE <?php print $vlv_nowplaying['content']; ?></li>
      </ul>
    </div>
  </li>
</ul>