<?php
  // listen live button
  $popuplink = '/listen-live';
  $popup = weta_views_listenlive_popup($popuplink);
  $listenlink = l('Listen Live', $popuplink, array('attributes' => array('onclick' => $popup, 'id' => 'listenlive'), 'html' => TRUE));
  //$listenlink = l('Listen Live', $popuplink, array('html' => TRUE));
?>

<ul class="radio">
  <li><?php print $listenlink; ?></li>
  <li>&nbsp;</li>
</ul>