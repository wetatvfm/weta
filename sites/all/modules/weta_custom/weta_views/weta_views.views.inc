<?php
/**
 * Implements hook_views_data().
 */
function weta_views_views_data() {
  $data['weta_views']['table']['group'] = t('WETA');
  $data['weta_views']['table']['join'] = array(
    // Exist in all views.
    '#global' => array(),
  );

  $data['weta_views']['weta_todays_date'] = array(
    'title' => t('Today\'s date'),
    'help' => t('Displays today\'s date.'),
    'field' => array(
      'handler' => 'views_handler_weta_todays_date',
    ),
  );

  return $data;
}
