<?php
    // Add eTruncate plug-in
    drupal_add_js('sites/all/libraries/eTruncate/jquery.eTruncate.min.js');
    drupal_add_js('sites/all/libraries/eTruncate/jquery.custom.js');
?>

<?php if ($top): ?>
  <div class="span_12 col">
    <?php print $top; ?>
  </div>
<?php endif; ?>

<?php if ($left): ?>
  <div class="span_8 col p0-60-0-0">
    <?php print $left; ?>
  </div>
<?php endif; ?>

<?php if ($right): ?>
  <div class="span_4 col p0-60-0-0">
    <?php print $right; ?>
  </div>
<?php endif; ?>

<div class="span_12 col ptop40">
  <div class="divider1"></div>
  <div class="caldates prevdates">
  <div class="calitems">
  <?php if ($bottom): ?>
    <?php print $bottom; ?>
  <?php endif; ?>
  </div>
  </div>
  <?php if (!empty($drupal_render_children)): ?>
    <?php print $drupal_render_children ?>
  <?php endif; ?>
</div>
