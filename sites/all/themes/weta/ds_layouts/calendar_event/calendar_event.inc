<?php
function ds_calendar_event() {
  return array(
    'label' => t('Calendar Entry'),
    'regions' => array(
      'top' => t('Top'),
      'left' => t('Left'),
      'right' => t('Right'),
      'bottom' => t('Bottom'),
    ),
  );
}