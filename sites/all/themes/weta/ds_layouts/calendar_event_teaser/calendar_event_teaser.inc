<?php
function ds_calendar_event_teaser() {
  return array(
    'label' => t('Calendar Teaser'),
    'regions' => array(
      'left' => t('Left'),
      'right' => t('Right'),
    ),
  );
}