<div class="white_bg">
<?php if (isset($title_suffix['contextual_links'])): ?>
<?php print render($title_suffix['contextual_links']); ?>
<?php endif; ?>

<?php if ($date): ?>
<div class="big_date">
<?php print $date; ?>
</div>
<?php endif; ?>

<?php if ($event): ?>
<div class="big_text">
<?php print $event; ?>
</div>
<?php endif; ?>
</div>