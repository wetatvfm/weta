<?php
function ds_day_in_history() {
  return array(
    'label' => t('Day in History'),
    'regions' => array(
      'date' => t('Date'),
      'event' => t('Event'),
    ),
  );
}