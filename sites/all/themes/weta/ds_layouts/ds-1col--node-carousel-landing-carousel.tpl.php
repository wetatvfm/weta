<?php
/**
 * @file
 * Display Suite 1 column template.
 */
?>
<?php
// Add Flexslider library
drupal_add_library('flexslider', 'flexslider');
?>
<<?php print $ds_content_wrapper; print $layout_attributes; ?> class="<?php print $classes; ?>">
<div class="slides">
<?php print $ds_content; ?>
</div>
</<?php print $ds_content_wrapper ?>>