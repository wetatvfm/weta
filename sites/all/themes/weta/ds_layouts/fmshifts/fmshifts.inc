<?php
function ds_fmshifts() {
  return array(
    'label' => t('FM Shifts'),
    'regions' => array(
      'time' => t('Time'),
      'host' => t('Host/Feature'),
    ),
  );
}