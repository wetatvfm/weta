<?php if (isset($title_suffix['contextual_links'])): ?>
<?php print render($title_suffix['contextual_links']); ?>
<?php endif; ?>

<div class="span_6 col">
<?php print $image; ?>
</div>

<div class="span_6 col p20">
<?php print $text; ?>
</div>