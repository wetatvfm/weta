<?php
function ds_productions_feature() {
  return array(
    'label' => t('Productions Feature'),
    'regions' => array(
      'image' => t('Image'),
      'text' => t('Text'),
    ),
  );
}