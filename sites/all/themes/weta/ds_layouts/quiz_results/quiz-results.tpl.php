<?php if (isset($title_suffix['contextual_links'])): ?>
<?php print render($title_suffix['contextual_links']); ?>
<?php endif; ?>

<?php if ($top): ?>
  <div class="span_12 col">
    <?php print $top; ?>
  </div>
<?php endif; ?>

<?php if ($left): ?>
  <div class="span_9 col">
    <?php print $left; ?>
  </div>
<?php endif; ?>

<?php if ($right): ?>
  <div class="span_3 col p20">
    <?php print $right; ?>
  </div>
<?php endif; ?>

<?php if ($bottom): ?>
  <div class="span_12 col">
    <?php print $bottom; ?>
  </div>
<?php endif; ?>
