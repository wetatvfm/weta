<?php
function ds_quiz_results() {
  return array(
    'label' => t('Quiz Results'),
    'regions' => array(
      'top' => t('Top'),
      'left' => t('Left'),
      'right' => t('Right'),
      'bottom' => t('Bottom'),
    ),
  );
}