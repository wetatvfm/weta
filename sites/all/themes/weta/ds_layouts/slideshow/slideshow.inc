<?php
function ds_slideshow() {
  return array(
    'label' => t('Slideshow'),
    'regions' => array(
      'image' => t('Image'),
      'caption' => t('Caption')
    ),
  );
}