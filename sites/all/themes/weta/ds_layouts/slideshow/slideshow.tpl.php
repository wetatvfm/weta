<div class="slide">
<?php if (isset($title_suffix['contextual_links'])): ?>
<?php print render($title_suffix['contextual_links']); ?>
<?php endif; ?>

<?php if ($image): ?>
<div class="span_6 col">
<div class="slide-wrap">
<?php print $image; ?>
</div>
</div>
<?php endif; ?>

<?php if ($caption): ?>
<div class="span_5 col slide_captions">
<div class="flex-caption">
<?php print $caption; ?>
</div>
</div>
<?php endif; ?>
</div>