<?php
function ds_timeline() {
  return array(
    'label' => t('Timeline item'),
    'regions' => array(
      'id' => t('ID'),
      'ds_content' => t('Content'),
    ),
  );
}