
<div class="content_wrapper">

<?php if (isset($title_suffix['contextual_links'])): ?>
<?php print render($title_suffix['contextual_links']); ?>
<?php endif; ?>

<?php
  $right_class = 'span_12 col';
?>
<?php if ($left): ?>
  <?php
    $right_class = 'span_9 col';
  ?>
  <div class="span_3 col p10">
    <?php print $left; ?>
  </div>
<?php endif; ?>

<?php if ($right): ?>
  <div class="<?php print $right_class; ?>">
    <?php print $right; ?>
  </div>
<?php endif; ?>

</div>
<div class="clearfix"></div>

<?php if (!empty($drupal_render_children)): ?>
<?php print $drupal_render_children ?>
<?php endif; ?>
