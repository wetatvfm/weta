<?php
function ds_two_col_3_9() {
  return array(
    'label' => t('Two Column: 3/9'),
    'regions' => array(
      'left' => t('Left'),
      'right' => t('Right'),
    ),
  );
}
