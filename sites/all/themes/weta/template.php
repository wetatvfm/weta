<?php
/**
 * Add body classes if certain regions have content.
 */
function weta_preprocess_html(&$variables) {
  if (!empty($variables['page']['featured'])) {
    $variables['classes_array'][] = 'featured';
  }

  if (!empty($variables['page']['triptych_first'])
    || !empty($variables['page']['triptych_middle'])
    || !empty($variables['page']['triptych_last'])) {
    $variables['classes_array'][] = 'triptych';
  }

  if (!empty($variables['page']['footer_firstcolumn'])
    || !empty($variables['page']['footer_secondcolumn'])
    || !empty($variables['page']['footer_thirdcolumn'])
    || !empty($variables['page']['footer_fourthcolumn'])) {
    $variables['classes_array'][] = 'footer-columns';
  }

  // Figure out the site section, then add the appropriate class
  $alias = drupal_get_path_alias(current_path());
  $sections = explode('/', $alias);
  switch ($sections[0]) {
    case 'fm':
      $variables['classes_array'][] = 'fm';
      break;
    case 'tv':
      $variables['classes_array'][] = 'tv';
      break;
    case 'support':
      $variables['classes_array'][] = 'support';
      break;
    case 'about':
      $variables['classes_array'][] = 'about';
      break;
    case 'listen-live':
      // add a theme suggestion for m3u view
      if (isset($sections[1]) && $sections[1] == 'classicalweta' && isset($sections[2]) && $sections[2] == 'playlist.m3u') {
        $variables['theme_hook_suggestions'][] = ' html__listen_live__classicalweta__playlist_m3u';
      }
      break;
    case 'lightbox':
      $variables['classes_array'][] = 'lightbox';
      break;
  }

}

/**
 * Add additional classes to certain regions
 */
function weta_preprocess_region(&$variables) {

  // Figure out the site section
  $alias = drupal_get_path_alias(current_path());
  $sections = explode('/', $alias);

  if ($variables['region'] == 'subnav') {
    $variables['classes_array'][] = 'subnav';

    // Add the appropriate class based on section
    switch ($sections[0]) {
      case 'tv':
        $variables['classes_array'][] = 'header_tv';
        break;
      case 'kids':
        $variables['classes_array'][] = 'subnav-kids';
        break;
    }
  }

  if ($variables['region'] == 'content' && $sections[0] == 'lightbox') {
    $variables['theme_hook_suggestions'][] = 'region__content__lightbox';
  }

}

/**
 * Override or insert variables into the page template for HTML output.
 */
function weta_process_html(&$variables) {
  // Hook into color.module.
  if (module_exists('color')) {
    _color_html_alter($variables);
  }
}
/**
 * Override or insert variables into the page template.
 */
function weta_preprocess_page(&$variables) {

  // Initialize $is_landing variable to allow landing pages
  // to be treated like $is_front
  $variables['is_landing'] = FALSE;

  // Figure out the site section
  $alias = drupal_get_path_alias(current_path());
  $sections = explode('/', $alias);

  // If there are no args past $section[0], this is a landing page
  $landing = array(
    'fm',
    'tv',
    'kids',
    'support',
    'schedule',
    'local',
    'about',
  );
  if (empty($sections[1]) && in_array($sections[0], $landing)) {
    $variables['is_landing'] = TRUE;
  }


  // Treat TV Series pages as landing pages
  if (isset($variables['node']->type) &&  ($variables['node']->type == 'tv_series' || $variables['node']->type == 'tv_series_custom')) {
    $variables['is_landing'] = TRUE;
  }

  // add template suggestion for bracket match pages
  if (isset($variables['node']->type) && $variables['node']->type == 'bracket_match') {
    $variables['theme_hook_suggestions'][] = 'page__node__bracket_match';
  }


  // add template suggestion for lightbox related basic pages
  if (isset($variables['node']->type) && $variables['node']->type == 'basic_page' && $sections[0] == 'lightbox') {
    $variables['theme_hook_suggestions'][] = 'page__lightbox';
  }

  // add template suggestion for carousel pages
  if (isset($variables['node']->type) && $variables['node']->type == 'carousel') {
    $variables['theme_hook_suggestions'][] = 'page__node__carousel';
  }

  // podcast pages
  if (isset($variables['node']->type) && $variables['node']->type == 'podcast') {
    // add template suggestion for podcast pages
    $variables['theme_hook_suggestions'][] = 'page__node__podcast';
    // get the episode number and insert it before the page title
    if (isset($variables['node']->field_podcast_episode_number)) {
      $episode_number = field_get_items('node', $variables['node'], 'field_podcast_episode_number');
      $episode_number = field_view_value('node', $variables['node'], 'field_podcast_episode_number', $episode_number[0]);
      $episode_number = render($episode_number) . '. ';
      $variables['title_prefix']['weta']['#children'] = $episode_number;
    }
  }

  // Add classes based on site section to control nav highlighting
  // Also as is_landing variable as necessary
  switch ($sections[0]) {
    case 'fm':
      $variables['wrapclasses'][] = 'fm_wrap';
      break;
    case 'tv':
      $variables['wrapclasses'][] = 'tv_wrap';
      // uk is a special, special snowflake
      if (isset($sections[1]) && $sections[1] == 'uk'):
        $variables['is_landing'] = FALSE;
        if (!isset($sections[2])):
          $variables['is_landing'] = TRUE;
        endif;
      endif;
      break;
    case 'schedule':
      $variables['wrapclasses'][] = 'schedule_wrap';
      $variables['is_landing'] = TRUE;
      break;
    case 'kids':
      $variables['wrapclasses'][] = 'kid_wrap';
      break;
    case 'support':
      $variables['wrapclasses'][] = 'support_wrap';
      break;
    case 'about':
      $variables['wrapclasses'][] = 'about_wrap';
      break;
    case 'local':
      $variables['wrapclasses'][] = 'local_wrap';
      // calendar is a special, special snowflake
      if (isset($sections[1]) && $sections[1] == 'calendar'):
        $variables['is_landing'] = FALSE;
        if (!isset($sections[2])):
          $variables['is_landing'] = TRUE;
        endif;
      endif;
      break;


  }

}

/**
 * Override or insert variables into the page template.
 */
function weta_process_page(&$variables) {
  // Hook into color.module.
  if (module_exists('color')) {
    _color_page_alter($variables);
  }
  // Always print the site name and slogan, but if they are toggled off, we'll
  // just hide them visually.
  $variables['hide_site_name']   = theme_get_setting('toggle_name') ? FALSE : TRUE;
  $variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
  if ($variables['hide_site_name']) {
    // If toggle_name is FALSE, the site_name will be empty, so we rebuild it.
    $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
  }
  if ($variables['hide_site_slogan']) {
    // If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.
    $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
  }
  // Since the title and the shortcut link are both block level elements,
  // positioning them next to each other is much simpler with a wrapper div.
  if (!empty($variables['title_suffix']['add_or_remove_shortcut']) && $variables['title']) {
    // Add a wrapper div using the title_prefix and title_suffix render elements.
    $variables['title_prefix']['shortcut_wrapper'] = array(
      '#markup' => '<div class="shortcut-wrapper clearfix">',
      '#weight' => 100,
    );
    $variables['title_suffix']['shortcut_wrapper'] = array(
      '#markup' => '</div>',
      '#weight' => -99,
    );
    // Make sure the shortcut link is the first item in title_suffix.
    $variables['title_suffix']['add_or_remove_shortcut']['#weight'] = -100;
  }

}

/**
 * Implements hook_preprocess_maintenance_page().
 */
function weta_preprocess_maintenance_page(&$variables) {
  // By default, site_name is set to Drupal if no db connection is available
  // or during site installation. Setting site_name to an empty string makes
  // the site and update pages look cleaner.
  // @see template_preprocess_maintenance_page()
  if (!$variables['db_is_active']) {
    $variables['site_name'] = '';
  }
  drupal_add_css(drupal_get_path('theme', 'bartik') . '/css/maintenance-page.css');
}

/**
 * Override or insert variables into the maintenance page template.
 */
function weta_process_maintenance_page(&$variables) {
  // Always print the site name and slogan, but if they are toggled off, we'll
  // just hide them visually.
  $variables['hide_site_name']   = theme_get_setting('toggle_name') ? FALSE : TRUE;
  $variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
  if ($variables['hide_site_name']) {
    // If toggle_name is FALSE, the site_name will be empty, so we rebuild it.
    $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
  }
  if ($variables['hide_site_slogan']) {
    // If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.
    $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
  }
}

/**
 * Override or insert variables into the node template.
 */
function weta_preprocess_node(&$variables) {
  // add theme hook suggestion for view modes
  $variables['theme_hook_suggestions'][] = 'node__' . $variables['type'] . '__' . $variables['view_mode'];

  if ($variables['view_mode'] == 'homepage_carousel' && $variables['type'] == 'carousel') {
    $variables['classes_array'][] = 'flexslider col';
  }

  if ($variables['view_mode'] == 'full' && node_is_page($variables['node'])) {
    $variables['classes_array'][] = 'node-full';
  }

  // LIGHTBOXES
  if ($variables['type'] == 'responsive_lightbox' && $variables['view_mode'] == 'generic') {
    $options = array(
      'group' => CSS_THEME,
      'every_page' => TRUE,

    );
    drupal_add_css(drupal_get_path('theme', 'weta') . '/templates/css/weta-lightbox.css', $options);


    $desktop_background_url = file_create_url($variables['field_resp_lb_bgimage_desktop'][0]['uri']);
    $mobile_background_url = file_create_url($variables['field_resp_lb_bgimage_mobile'][0]['uri']);

    $background_css = '#rlb #wrapper { background-image: url(' . $mobile_background_url . '); } ';
    $background_css .= '@media (min-width: 768px) { #rlb #wrapper { background-image: url(' . $desktop_background_url . '); } }';


    drupal_add_css($background_css, array('group' => CSS_THEME, 'type' => 'inline'));

  }


  // add flexsider classes to landing page carousels.

  if ($variables['type'] == 'carousel' && $variables['view_mode'] == 'landing_carousel') {
    $variables['classes_array'][] = 'flexslider col';

    // The kids carousel is special
    if (arg(0) == 'kids') {
      $variables['classes_array'][] = 'kids-slider';
    }
    else {
      $variables['classes_array'][] = 'split-slider';
    }
    // The support carousel also has the "support" class.
    if (arg(0) == 'support') {
      $variables['classes_array'][] = 'support';
    }

    // Also remove non-tv slides from tv carousel.
    if (arg(0) == 'tv') {
      /**
       * Category IDs:
       * 97: WETA Television
       * 235: WETA UK
       */
      $categories = array('97', '235');

      foreach ($variables['field_slides'] as $i => $slide) {
        $tid = $slide['node']->field_feature_category['und'][0]['tid'];
        if (!in_array($tid, $categories)) {
          // if the slide isn't in one of the categories, remove it from the carousel
          unset($variables['content']['field_slides'][$i]);
        }
      }
    }

  }
  $text_boxes = array('text_box', 'text_box_h3', 'text_box_with_header');
  if (in_array($variables['view_mode'], $text_boxes)) {
    $variables['classes_array'][] = 'ds-1col span_4 col p20-10-10';
  }

  if ($variables['view_mode'] == 'image_top_with_short_description' && isset($variables['view']) && $variables['view']->name == 'weta_kids_feature_strip') {
    $colors = array('green', 'purple', 'orange');
    $color_class = $colors[$variables['view']->row_index];
    $variables['box_class'] = $color_class;
    $variables['classes_array'][] = 'span_4 col p20-10-10';
  }

  // Only show the last date if there are repeating or multiple dates
  if (($variables['view_mode'] == 'image_top_with_description' || $variables['view_mode'] == 'sidebar_feature') && $variables['type'] == 'calendar_event') {

    // Repeating dates
    if (!empty($variables['content']['field_cal_repdates']['#items'])) {

      // If there are multiple dates, only show the last one
      if (count($variables['content']['field_cal_repdates']['#items']) > 1) {
        // pop out the last date
        $last = array_pop($variables['content']['field_cal_repdates']);
        // get the array keys
        $keys = array_keys($variables['content']['field_cal_repdates']);
        // set up a temporary array
        $temp = array();
        // if the key isn't numeric, we want to keep it, so we stick it in the temp array
        foreach ($keys as $key) {
          if (!is_numeric($key)) {
            $temp[$key] = $variables['content']['field_cal_repdates'][$key];
          }
        }
        // add the last date to the temp array
        $temp[0] = $last;
        // replace the original array with the temp array
        $variables['content']['field_cal_repdates'] = $temp;
      }
    }

    // Multiple dates
    if (!empty($variables['content']['field_cal_multipledates']['#items'])) {

      // If there are multiple dates, only show the last one
      if (count($variables['content']['field_cal_multipledates']['#items']) > 1) {
        // pop out the last date
        $last = array_pop($variables['content']['field_cal_multipledates']);
        // get the array keys
        $keys = array_keys($variables['content']['field_cal_multipledates']);
        // set up a temporary array
        $temp = array();
        // if the key isn't numeric, we want to keep it, so we stick it in the temp array
        foreach ($keys as $key) {
          if (!is_numeric($key)) {
            $temp[$key] = $variables['content']['field_cal_multipledates'][$key];
          }
        }
        // add the last date to the temp array
        $temp[0] = $last;
        // replace the original array with the temp array
        $variables['content']['field_cal_multipledates'] = $temp;
      }
    }
  }

  // For FM Playlist
  if ($variables['type'] == 'fm_playlist') {

    // decode entities in titles (e.g. &amp;amp;)
    $variables['content']['title'][0]['#markup'] = html_entity_decode($variables['content']['title'][0]['#markup']);


    if ($variables['view_mode'] == 'full') {
      // Concatenate album and album title. Then hide extra album title.
      if (isset($variables['content']['field_album_title'][0]['#markup'])) {
        $album_title = ' <em>(' . $variables['content']['field_album_title'][0]['#markup'] . ')</em>';
        $variables['content']['field_album'][0]['#markup'] .= $album_title;
        unset($variables['content']['field_album_title']);
      }
      $variables['attributes_array']['id'] = $variables['nid'];
    }

    if ($variables['view_mode'] == 'full' || $variables['view_mode'] == 'now_playing') {
      // Link to Arkiv
      if (isset($variables['content']['field_playlist_linktext'][0]['#markup']) && isset($variables['content']['field_buy_url'][0]['#markup'])) {
        $url = html_entity_decode($variables['content']['field_buy_url'][0]['#markup']);

        $text = $variables['content']['field_playlist_linktext'][0]['#markup'];
        if ($variables['view_mode'] == 'full') {
          $text .= ' &rarr;';
        }
        // add classes if CD Not Available
        $options['html'] = TRUE;
        if ($variables['content']['field_playlist_linktext']['#items'][0]['value'] == 2) {
          $options['attributes']['class'] = array('cd_na_popup');
        }
        // replace link text with link
        $variables['content']['field_playlist_linktext'][0]['#markup'] = l($text, $url, $options);
        // hide extra url
        unset($variables['content']['field_buy_url']);
        if (isset($variables['elements']['field_buy_url'])) {
          unset($variables['elements']['field_buy_url']);
        }
      }

      if (isset($variables['field_playlist_linktext'][0]) && $variables['field_playlist_linktext'][0]['value'] == 0) {
        unset($variables['content']['field_playlist_linktext'][0]['#markup']);
      }
    }
  }
  // Feature Blocks
  if ($variables['type'] == 'feature_block') {

    // Full width promos
    if ($variables['view_mode'] == 'full_width_promo') {

      // Primary link
      if (isset($variables['content']['field_primary_link']['#items'][0]['display_url'])) {

        // Add button class
        $variables['content']['field_primary_link'][0]['#element']['attributes']['class'] = 'button';

        // Link image to primary link
        if (isset($variables['content']['field_feature_image'][0])) {
          $link = $variables['content']['field_primary_link']['#items'][0]['display_url'];
          $variables['content']['field_feature_image'][0]['#path'] = array(
            'path' => $link
          );
        }

      }
    }



    $modes = array('basic_square', 'basic_square_blue', 'green_square', 'carousel', 'highlight_50_50', 'home_50_50', 'productions_feature', 'highlight_20_80', 'flyout_feature', 'homepage_carousel');
    if (in_array($variables['view_mode'], $modes)) {

      if ((isset($variables['content']['field_primary_link']['#items'][0]['display_url']) || isset($variables['content']['group_text_wrapper']['field_primary_link']['#items'][0]['display_url'])) && isset($variables['content']['field_feature_image'][0])) {

        // check to see if video
        if (isset($variables['content']['field_primary_link']['#items'][0]['display_url'])) {
          $link = $variables['content']['field_primary_link']['#items'][0]['display_url'];
        }
        else {
          $link = $variables['content']['group_text_wrapper']['field_primary_link']['#items'][0]['display_url'];
        }

        $parsed_link = parse_url($link);
        $youtube_check = FALSE;
        $cove_check = FALSE;

        // add modal popup to youtube videos
        if ($parsed_link['host'] == 'www.youtube.com' && $parsed_link['path'] == '/watch') {
          $youtube_check = TRUE;
          $parsed_url = drupal_parse_url($link);
          $link = $parsed_url['path'];
          $class = array('video_preview');
          $options['attributes']['class'] = $class;
          if (!empty($parsed_url['query']) && isset($parsed_url['query']['v'])) {
            $options['query']['v'] = $parsed_url['query']['v'];
          }
        }

        // add modal popup to COVE videos
        if (($parsed_link['host'] == 'watch.weta.org' || $parsed_link['host'] == 'www.pbs.org') && strpos($parsed_link['path'], '/video/') === 0) {
          $cove_check = TRUE;
          $get_id = explode('/', $parsed_link['path']);
          $pbs_id = $get_id[2];
          if (is_numeric($pbs_id)) {
            $class = array('mm_player');
            $options['attributes']['class'] = $class;
            $link = 'player/' . $pbs_id;
          }
          // Media Manager ID
          else {
            $class = array('mm_player');
            $options['attributes']['class'] = $class;
            $link = 'mmplayer/' . $pbs_id;
          }
        }

        // add node title as link title attribute
        $options['attributes']['title'] = $variables['title'];


        // add options to primary link array for theming
        if ($variables['view_mode'] == 'carousel' || $variables['view_mode'] == 'homepage_carousel') {

          $variables['content']['field_primary_link'][0]['#element']['url'] = $link;
          if (isset($class) && is_array($class)) {
            $variables['field_primary_link'][0]['attributes']['class'] = $class;
            $variables['content']['field_primary_link'][0]['#element']['attributes']['class'] = implode(' ', $class);
          }

          // add tracking code to options for carousel primary link and image
          $track = TRUE;
          $parsed_carousel_url = parse_url($link);
          if (isset($parsed_carousel_url['host'])) {
            $track = FALSE;
            $carousel_url_array = explode('.', $parsed_carousel_url['host']);
            $excluded_subdomains = array('support', 'watch');
            if (in_array('weta', $carousel_url_array) && !in_array($carousel_url_array[0], $excluded_subdomains)) {
              $track = TRUE;
            }
          }

          if ($track) {
            $variables['field_primary_link'][0]['query']['src'] = 'carousel';
            $variables['content']['field_primary_link'][0]['#element']['query']['src'] = 'carousel';
            $options['query']['src'] = 'carousel';
          }
        }

        // add path and path options to image array for theming

        $variables['content']['field_feature_image'][0]['#path'] = array (
          'path' => $link,
          'options' => $options,
        );

        // theme it
        $markup = theme('image_formatter', $variables['content']['field_feature_image'][0]);

        // add video styling
        $exceptions = array('home_50_50', 'highlight_50_50', 'highlight_20_80', 'flyout_feature', 'homepage_carousel');
        if (($youtube_check || $cove_check) && (!in_array($variables['view_mode'], $exceptions))) {
          $button = '<span class="screenshot"><span class="play-overlay"></span></span>';
          $markup = str_replace('<img', $button . '<img', $markup);

          $squares = array('basic_square', 'basic_square_blue', 'green_square', 'productions_feature');
          if (in_array($variables['view_mode'], $squares)) {
            $markup = '<div class="video">' . $markup . '</div>';
          }

          unset($variables['content']['field_feature_image'][0]);
          $variables['content']['field_feature_image'][0]['#markup'] = $markup;
        }


        // remove primary link from display
        if (isset($variables['content']['field_primary_link']['#formatter']) && $variables['content']['field_primary_link']['#formatter'] == 'link_plain') {
          unset($variables['content']['field_primary_link']);
        }
      }
    }

    // add video icon to additional links in the homepage carousel
    if ($variables['view_mode'] == 'homepage_carousel' && !empty($variables['content']['field_links'])) {

      foreach ($variables['content']['field_links']['#items'] as $i => $item) {
        $class = NULL;
        $options = NULL;

        // check to see if video
        $link = $item['display_url'];

        $parsed_link = parse_url($link);

        // add modal popup to youtube videos
        if ($parsed_link['host'] == 'www.youtube.com' && $parsed_link['path'] == '/watch') {
          $parsed_url = drupal_parse_url($link);
          $link = $parsed_url['path'];
          $class = array('video_preview');
        }

        // add modal popup to COVE videos
        if (($parsed_link['host'] == 'watch.weta.org' || $parsed_link['host'] == 'www.pbs.org') && strpos($parsed_link['path'], '/video/') === 0) {
          $get_id = explode('/', $parsed_link['path']);
          $pbs_id = $get_id[2];
          // COVE legacy ID
          if (is_numeric($pbs_id)) {
            $class = array('mm_player');
            $link = 'player/' . $pbs_id;
          }
          // Media Manager ID
          else {
            $class = array('mm_player');
            $link = 'mmplayer/' . $pbs_id;
          }
        }

        // add options to link array for theming
        $variables['content']['field_links']['#items'][$i]['url'] = $link;
        if (isset($class) && is_array($class)) {
          //$variables['field_links'][$i]['attributes']['class'] = $class;
          $variables['content']['field_links'][$i]['#element']['attributes']['class'] = implode(' ', $class);
        }

        // add tracking code to options for link
        $track = TRUE;
        $parsed_carousel_url = parse_url($link);
        if (isset($parsed_carousel_url['host'])) {
          $track = FALSE;
          $carousel_url_array = explode('.', $parsed_carousel_url['host']);
          $excluded_subdomains = array('support', 'watch');
          if (in_array('weta', $carousel_url_array) && !in_array($carousel_url_array[0], $excluded_subdomains)) {
            $track = TRUE;
          }
        }

        if ($track) {
          $variables['field_links'][$i]['query']['src'] = 'carousel';
          $variables['content']['field_links'][$i]['#element']['query']['src'] = 'carousel';
        }
      }


    }
  }

  // House Ad
  if ($variables['type'] == 'house_ad') {
    if (isset($variables['content']['field_primary_link']['#items'][0]['display_url']) && isset($variables['content']['field_image'][0])) {

      // add node title as link title attribute
      $options['attributes']['title'] = $variables['title'];
      $link = $variables['content']['field_primary_link']['#items'][0]['display_url'];
      // add path and path options to image array for theming
      $variables['content']['field_image'][0]['#path']['path'] = $link;
      $variables['content']['field_image'][0]['#path']['options'] = $options;

      // remove primary link from display
      if (isset($variables['content']['field_primary_link']['#formatter']) && $variables['content']['field_primary_link']['#formatter'] == 'link_plain') {
        unset($variables['content']['field_primary_link']);
      }
    }

  }

  // Add arrows to read more link
  if ((($variables['type'] == 'fm_classical_conversations' && $variables['view_mode'] != 'basic_square') || $variables['type'] == 'station_relations_page' || $variables['type'] == 'giveaway' || $variables['type'] == 'volunteer_spotlight') && isset($variables['content']['node_link'][0]['#markup'])) {
    $variables['content']['node_link'][0]['#markup'] = str_replace('</a>', ' &rarr; </a>', $variables['content']['node_link'][0]['#markup']);


  }

  // COVE videos
  if ($variables['type'] == 'cove_video') {
    if ($variables['view_mode'] == 'highlight_20_80') {
      $cove_id = $variables['content']['field_cove_id']['#items'][0]['safe_value'];
      $title = 'Watch now &rarr;';
      $link = '/player/' . $cove_id;
      $options = array('attributes' => array('class' => array('cove_player')),'html' => TRUE);



      $variables['content']['field_cove_id'][0]['#markup'] = l($title, $link, $options);


      if (isset($variables['content']['field_feature_image'][0]) && isset($cove_id)) {



        // add node title as link title attribute
        $path_options['attributes']['title'] = $variables['title'];



        // add magnific popup class
        $path_options['attributes']['class'][] = 'cove_player';



        // add path and path options to image array for theming
        $variables['content']['field_feature_image'][0]['#path']['path'] = $link;
        $variables['content']['field_feature_image'][0]['#path']['options'] = $path_options;

        // theme it
        $markup = theme('image_formatter', $variables['content']['field_feature_image'][0]);



        //unset($variables['content']['field_feature_image'][0]);
        $variables['content']['field_feature_image'][0]['#markup'] = $markup;


      }
    }
  }

  // Webform Display
  if ($variables['type'] == 'webform_display') {

    // default to submissions being open
    $submissions_open = TRUE;

    // is there a webform?
    if (isset($variables['content']['field_webform']) && !empty($variables['content']['field_webform'])) {

      // get the nid
      $webform_id = $variables['content']['field_webform']['#items'][0]['nid'];

      // check to see if there's a max submission limit, and if it's been reached
      $submissions_check = custom_rsvp_count_check($webform_id);


      // is there a waitlist?
      if (isset($variables['content']['field_waitlist_form']) && !empty($variables['content']['field_waitlist_form'])) {
        $waitlist = $variables['content']['field_waitlist_form'][0]['webform'];
      }

      /*
       * if submissions_check is TRUE,
       * -- show webform
       * -- if the waitlist form exists, hide it
       * -- submissions_open = TRUE
       * if submissions_check is FALSE
       * -- hide webform
       * -- submissions_open = FALSE
       * -- if the waitlist form exists
       *    -- show waitlist
       *    -- submissions_open = TRUE
       * if submissions are completely closed
       * -- hide webform
       * -- hide waitlist
       * -- submissions_open = FALSE
       */
      switch ($submissions_check) {
        case TRUE: //open
          // if there's a waitlist, hide it
          if (isset($waitlist)) {
            unset($variables['content']['field_waitlist_form']);
          }
          $submissions_open = TRUE;
          break;
        case FALSE: //closed
          unset($variables['content']['field_webform']);
          $submissions_open = FALSE;
          if (isset($waitlist)) {
            // if there's a waitlist, hide closed text
            $submissions_open = TRUE;
          }
          break;
        default:
          break;
      }

      // check to see if the end date has passed
      if (isset($variables['field_end_date']['und'][0]['value']) && !empty($variables['field_end_date']['und'][0]['value'])) {
        $end_date = strtotime($variables['field_end_date']['und'][0]['value'] . ' ' . $variables['field_end_date']['und'][0]['timezone_db']);
        $now = time();
;
        // if so, disable the webform
        if ($end_date < $now) {
          $submissions_open = FALSE;
        }
      }
    }

    // if submissions are open, hide the submissions closed text
    if ($submissions_open) {
      unset($variables['content']['field_submissions_closed_text']);
    }
    else {
      unset($variables['content']['field_webform']);
      if (isset($waitlist)) {
        unset($variables['content']['field_waitlist_form']);
      }
    }
  }

  // Podcasts
  if ($variables['type'] == 'podcast') {
    if ($variables['teaser']) {
      // get the episode number and insert it into the title
      if (isset($variables['content']['field_podcast_episode_number'])) {
        $episode_number = render($variables['content']['field_podcast_episode_number']);
        $episode_number = strip_tags($episode_number);
        $episode_number = trim($episode_number);
        $new_title = $episode_number . '. ' . $variables['title'];
        $markup = $variables['content']['title'][0]['#markup'];
        $new_markup = str_replace($variables['title'], $new_title, $markup);
        $variables['content']['title'][0]['#markup'] = $new_markup;
      }

      // add right arrow to read more link
      if (isset($variables['content']['node_link'][0]['#markup'])) {
        $variables['content']['node_link'][0]['#markup'] = str_replace('</a>', ' &rarr; </a>', $variables['content']['node_link'][0]['#markup']);
      }


    }
  }

}

/*
 * Override or insert variables into the field template.
 */
function weta_preprocess_field(&$variables, $hook) {

  // Modify primary links
  if ($variables['element']['#field_name'] == 'field_primary_link') {

    // Add arrows and localize
    foreach ($variables['element']['#items'] as $i => $item) {
      if (isset($variables['items'][$i]['#markup'])) {
        // don't add arrows to flyout_links content type
        if ($variables['element']['#bundle'] != 'flyout_links') {
          $item['title'] .= ' &rarr;';
          $item['attributes'] = array('html' => TRUE);
        }
      $variables['items'][$i]['#markup'] = l($item['title'], $item['url'], $item['attributes']);
      }
      else {
        // don't add arrows to flyout_links content type
        if ($variables['element']['#bundle'] != 'flyout_links') {
          $item['title'] .= ' &rarr;';
          $variables['items'][$i]['#element']['title'] = $item['title'];
          $variables['items'][$i]['#element']['html'] = TRUE;
        }
      }
    }

    // Add p tags to Home 50/50 view mode
    if ($variables['element']['#view_mode'] == 'home_50_50') {
      if (isset($variables['items'][0]['#markup'])) {
        $variables['items'][0]['#markup'] = '<p>' . $variables['items'][0]['#markup'] . '</p>';
      }
    }

    // Add inline class to wrapper element in flyout_links content type
    if ($variables['element']['#bundle'] == 'flyout_links') {
      $variables['classes_array'][] = 'inline';
    }
 }

  // Add arrows and localize additional links
  if ($variables['element']['#field_name'] == 'field_links') {

    foreach ($variables['element']['#items'] as $i => $item) {

      $item['title'] .= ' &rarr;';
      $item['attributes'] = array('html' => 'TRUE');

      if (custom_is_pbs($item['url'])) {
        if ($variables['element']['#bundle'] == 'tv_series' || $variables['element']['#bundle'] == 'tv_series_custom') {
           $item['title'] = '<img src="' . base_path() . path_to_theme() . '/templates/images/pbs-black.gif" alt="">&nbsp;&nbsp;&nbsp;' . $item['title'];
        }
      }
      if (isset($variables['items'][$i]['#markup'])) {
        $variables['items'][$i]['#markup'] = l($item['title'], $item['url'], $item['attributes']);
      }
      else {
        $variables['items'][$i]['#element']['url'] = $item['url'];
        $variables['items'][$i]['#element']['title'] = $item['title'];
        $variables['items'][$i]['#element']['html'] = TRUE;
      }
    }

  }

  // Modify homepage slideshow elements

  if ($variables['element']['#view_mode'] == 'carousel') {

    // Subhead: replace <p> with <h5>
    if ($variables['element']['#field_name'] == 'field_subhead') {
      $variables['items'][0]['#markup'] = str_replace('p>', 'h5>', $variables['items'][0]['#markup']);
    }

    // Add class to taxonomy div
    if ($variables['element']['#field_name'] == 'field_feature_category') {
      $variables['classes_array'][] = 'medium';
    }

    // Add tracking parameter to additional links
    if ($variables['element']['#field_name'] == 'field_links') {
      foreach ($variables['items'] as &$item) {
        if (strpos($item['#element']['url'], 'http') === FALSE) {
          $item['#element']['query']['src'] = 'carousel';
        }
      }

    }
  }

  // Modify homepage day in history block elements

  if ($variables['element']['#bundle'] == 'homepage_dates') {
    if ($variables['element']['#field_name'] == 'field_date') {
      $date = strip_tags($variables['items'][0]['#markup']);
      $year = '<div class="year">' . date('Y', $date) . '</div>';
      if ($variables['element']['#view_mode'] == 'full') {
        $monthday = date('F j', $date);
        $variables['items'][0]['#markup'] = $monthday . $year;
      }
      elseif ($variables['element']['#view_mode'] == 'teaser') {
        $variables['items'][0]['#markup'] = $year;
      }
    }
  }

  // Modify FM Playlist now playing widget elements

  if ($variables['element']['#view_mode'] == 'now_playing') {

    // Link text
    if ($variables['element']['#field_name'] ==  'field_playlist_linktext') {
      // add arrow to link
      foreach ($variables['element']['#items'] as $i => $item) {
        if (isset($variables['items'][$i]['#markup'])) {
          $variables['items'][$i]['#markup'] = str_replace('</a>', ' &rarr; </a>', $variables['items'][$i]['#markup']);
        }
        else {
          $variables['items'][$i]['#markup'] = '';
        }

      }


      // add listen live link
      $listen_link = '';
      if ($variables['element']['#object']->field_playlist_type['und'][0]['tid'] == 1) {
        $popuplink = '/listen-live';
        $playlist = 'fm/playlists';
      } elseif ($variables['element']['#object']->field_playlist_type['und'][0]['tid'] == 2) {
        $popuplink = '/listen-live/viva-la-voce';
        $playlist = 'fm/vlvplaylist';
      }
      $popup = weta_views_listenlive_popup($popuplink);
      $listen_link = l('Listen Live &rarr;', $popuplink, array('attributes' => array('onclick' => $popup, 'id' => 'listenlive'), 'html' => TRUE));
      $variables['items'][0]['#markup'] .= '<br />' . $listen_link;

      // add playlist link
      $playlist_link = l('View the playlist &rarr;', $playlist, array('fragment' => $variables['element']['#object']->nid, 'html' => TRUE));
      $variables['items'][0]['#markup'] .= '<br />' . $playlist_link;

      // wrap it in a div
      $variables['items'][0]['#markup'] = '<div class="np_links">' . $variables['items'][0]['#markup'] . '</div>';


      }

    // Composer
    if ($variables['element']['#field_name'] == 'field_composer') {
      $variables['items'][0]['#markup'] = '<h5>' . $variables['items'][0]['#markup'] . '</h5>';
    }

  }

  if ($variables['element']['#bundle'] == 'fm_playlist' && $variables['element']['#view_mode'] == 'full') {
    // Composer
    if ($variables['element']['#field_name'] == 'field_composer') {
       $variables['items'][0]['#markup'] = '<h5>' . $variables['items'][0]['#markup'] . '</h5>';
    }
    // Artist
    if ($variables['element']['#field_name'] == 'field_artist') {
      // strip out empty artists
      foreach ($variables['items'] as $i => $item) {
        if (strlen($item['#markup']) == 0) {
          unset($variables['items'][$i]);
        }
      }
      // add pipes between artists
      $total = count($variables['items']);
      if ($total > 1) {
        foreach ($variables['items'] as $i => $item) {
          if (($i + 1) < $total) {
            $variables['items'][$i]['#markup'] .= ' | ';
          }
        }
      }
    } // end Artist
    // Countdown Number
    if ($variables['element']['#field_name'] == 'field_countdown_number') {
      foreach ($variables['items'] as $i => $item) {
        $number = $variables['items'][$i]['#markup'];
        $number = '<p>' . l('Classical Countdown #' . $number, '/fm/features/classical-countdown') . '</p>';
        $variables['items'][$i]['#markup'] = $number;
      }
    }

  }

  // Add arrows to Related Series links
  if ($variables['element']['#field_name'] == 'field_related_series') {
    foreach ($variables['items'] as $delta => $item):
      $variables['items'][$delta]['#title'] .= ' &rarr;';
      $variables['items'][$delta]['#options']['html'] = TRUE;
    endforeach;
  }

  if ($variables['element']['#field_name'] == 'field_short_description' && $variables['element']['#bundle'] == 'status_update') {
    $markup = $variables['items'][0]['#markup'];
    $markup = str_replace('<p>', '', $markup);
    $markup = str_replace('</p>', '', $markup);
    $variables['items'][0]['#markup'] = $markup;
  }

  // Add 'date' class to blog post date
  if ($variables['element']['#field_name'] == 'post_date' && $variables['element']['#bundle'] == 'blog_post') {
    $variables['classes_array'][] = 'date';
  }

  // Add 'date' class to pmp date
  if ($variables['element']['#field_name'] == 'field_pmp_published' && $variables['element']['#bundle'] == 'pmp_story' && ($variables['element']['#view_mode'] == 'generic' || $variables['element']['#view_mode'] == 'mini_feature')) {
    $variables['classes_array'][] = 'date';
  }

  // Add custom theme hook suggestion due to namespace conflicts.
  // See #1137024 comment 32 on d.o
  if ($variables['element']['#field_name'] == 'field_featured_performances' && $variables['element']['#bundle'] == 'fm_generic_feature') {
    $variables['theme_hook_suggestions'][] = 'field__collection_featured_performances';
  }

  // Format highlighted events
  if ($variables['element']['#bundle'] == 'calendar_event' && ($variables['element']['#view_mode'] == 'image_top_with_description' || $variables['element']['#view_mode'] == 'sidebar_feature')) {
    if ($variables['element']['#field_name'] == 'field_cal_repdates' ||  $variables['element']['#field_name'] == 'field_cal_multipledates') {

      // date formatting
      $markup = $variables['element'][0]['#markup'];
      $markup = strip_tags($markup);
      if (count($variables['element']['#items']) > 1) {
        $markup = 'Through ' . $markup;
      }
      $variables['items'][0]['#markup'] = '<div class="date">' . $markup . '</div>';
    }

    // add arrow to read more links
    if ($variables['element']['#field_name'] == 'node_link') {
      foreach ($variables['element']['#items'] as $i => $item) {
        $arrow = '&rarr;';
        $arrowcheck = strpos($variables['items'][$i]['#markup'], $arrow);
        if ($arrowcheck !== FALSE) {
          $variables['items'][$i]['#markup'] = str_replace('</a>', ' &rarr; </a>', $variables['items'][$i]['#markup']);
        }
      }
    }

  }

  // Rewrite WETA Special Event output for calendar
  if ($variables['element']['#field_name'] == 'field_weta_special_event') {
    if ($variables['element']['#items'][0]['value'] == 1) {
      $variables['items'][0]['#markup'] = '<h3 class="special_event ' . $variables['element']['#view_mode'] . '">WETA SPECIAL EVENT</h3>';
    }
  }

  // Rewrite WETA Giveaway output for calendar
  if ($variables['element']['#field_name'] == 'field_giveaway' && $variables['element']['#view_mode'] == 'teaser') {
      if (isset($variables['element']['#object']->field_giveaway['und'][0]['node']->field_giveaway_dates['und'][0]['value']) && isset($variables['element']['#object']->field_giveaway['und'][0]['node']->field_giveaway_dates['und'][0]['value2'])) {
      $start = strtotime($variables['element']['#object']->field_giveaway['und'][0]['node']->field_giveaway_dates['und'][0]['value']);
      $end = strtotime($variables['element']['#object']->field_giveaway['und'][0]['node']->field_giveaway_dates['und'][0]['value2']);
      $today = time();

      if ($today >= $start && $today <= $end) {
        $variables['theme_hook_suggestions'][] = 'field__field_giveaway__teaseronly';
      }
      else {
      $variables['classes_array'][] = 'hidden';
      }
    }
  }

  // Call a specific template file for repeating and multiple date fields in the event calendar
  if (($variables['element']['#field_name'] == 'field_cal_repdates' || $variables['element']['#field_name'] == 'field_cal_multipledates') && $variables['element']['#view_mode'] == 'full') {
    $variables['theme_hook_suggestions'][] = 'field__field_cal_repdates__calendar_event__full';
  }

  // Calendar link to giveaway
  if ($variables['element']['#field_name'] == 'field_giveaway' && $variables['element']['#view_mode'] == 'full') {



    if (isset($variables['element']['#object']->field_giveaway['und'][0]['node']->field_giveaway_dates['und'][0]['value']) && isset($variables['element']['#object']->field_giveaway['und'][0]['node']->field_giveaway_dates['und'][0]['value2'])) {
      $start = strtotime($variables['element']['#object']->field_giveaway['und'][0]['node']->field_giveaway_dates['und'][0]['value']);
      $end = strtotime($variables['element']['#object']->field_giveaway['und'][0]['node']->field_giveaway_dates['und'][0]['value2']);
      $today = time();



      if ($today >= $start && $today <= $end) {
        $variables['label'] = 'Win tickets to this event!';
        $variables['items'][0]['#title'] = 'Enter today &rarr;';
        $variables['items'][0]['#options']['html'] = TRUE;
      }
      else {
        $variables['label'] = '';
        $variables['classes_array'][] = 'hidden';
      }
    }
  }

  // Calendar event website
  if ($variables['element']['#bundle'] == 'calendar_event' && $variables['element']['#field_name'] == 'field_event_website') {
    $variables['items'][0]['#element']['title'] = 'Event website &rarr;';
    $variables['items'][0]['#element']['html'] = TRUE;


  }

  // Calendar event email -- replace visible address with text
  if ($variables['element']['#bundle'] == 'calendar_event' && $variables['element']['#field_name'] == 'field_event_email') {
    $a = new SimpleXMLElement($variables['items'][0]['#markup']);
    $url = $a['href'];
    $title = 'Event email &rarr;';
    $variables['items'][0]['#markup'] = l($title, $url, array('html' => TRUE));
  }

  // Rewrite calendar category links to point to views filter
  if ($variables['element']['#bundle'] == 'calendar_event' && $variables['element']['#field_name'] == 'field_cal_categories') {
    $links = array();
    foreach ($variables['element']['#items'] as $item) {
      $term = taxonomy_term_load($item['tid']);
      $links[] = l($term->name, 'local/calendar', $options = array('query' => array('field_cal_categories_tid' =>  $item['tid'])));
    }
    $variables['items'][0]['#markup'] = implode(', ', $links);
    $variables['label'] = '<strong>' . $variables['label'] . '</strong>';

    // remove inline class
    $inline = array_search('inline', $variables['classes_array']);
    if (!empty($inline)) {
      unset($variables['classes_array'][$inline]);
    }

  }

  // Add class to calendar venue
  if ($variables['element']['#bundle'] == 'calendar_event' && $variables['element']['#view_mode'] = 'teaser' && $variables['element']['#field_name'] == 'field_venue') {
    $variables['classes_array'][] = 'sub';
  }

  // add arrow to calendar read more links
  if ($variables['element']['#bundle'] == 'calendar_event' && $variables['element']['#view_mode'] = 'teaser' && $variables['element']['#field_name'] == 'node_link') {
    foreach ($variables['element']['#items'] as $i => $item) {
      $variables['items'][$i]['#markup'] = str_replace('</a>', ' &rarr; </a>', $variables['items'][$i]['#markup']);
    }
  }




  // Link to event from giveaway
  if ($variables['element']['#bundle'] == 'giveaway' && $variables['element']['#field_name'] == 'field_event') {
    $eventid = $variables['element']['#items'][0]['nid'];
    $variables['items'][0]['#markup'] = '<p>' . l('Learn more about this event &rarr;', 'node/' . $eventid, array('html' => TRUE)) . '</p>';
  }


  // Giveaway deadline
  if ($variables['element']['#bundle'] == 'giveaway' && $variables['element']['#field_name'] == 'field_giveaway_dates') {
    $variables['label'] = 'This giveaway ends on';
    $variables['items'][0]['#markup'] = '<strong>' . $variables['items'][0]['#markup'] . '</strong>';
  }

  // strip tags from custom Pipe field
  if ($variables['element']['#field_name'] == 'pipe') {
    $variables['items'][0]['#markup'] = strip_tags($variables['items'][0]['#markup']);
  }
  // Station Relations pages
  if ($variables['element']['#bundle'] == 'station_relations_page' || $variables['element']['#bundle'] == 'station_relations_subpage') {
    // remove dividers from signature image and body fields
    $ul_fields = array('field_station_relations_logos', 'field_staion_relations_downloads', 'field_additional_information', 'field_links');
    if (in_array($variables['element']['#field_name'], $ul_fields)) {
      $variables['theme_hook_suggestions'][] = 'field__ul__station_relations_page';
    }
  }

  // Highlight 20/80 Image Style
  $image_fields = array('field_image', 'field_feature_image', 'field_host_image', 'field_header_image');
  if (in_array($variables['element']['#field_name'], $image_fields)) {
    foreach ($variables['items'] as $item) {
      if (isset($item['#image_style']) && $item['#image_style'] == 'highlight_20-80') {
          $variables['classes_array'][] = 'span_3 col p10';
      }
    }
  }

  // PMP Images in Stories -- only show the first image
  if ($variables['element']['#field_name'] == 'field_pmp_image_reference') {
    if ($variables['element']['#view_mode'] == 'full') {
      foreach ($variables['items'] as $delta => $item) {
        if ($delta > 0) {
          unset($variables['items'][$delta]);
        }
      }
    }
  }

  // PMP Images Alt Tags -- decode entities
  if ($variables['element']['#field_name'] == 'field_file_image_alt_text') {
    foreach ($variables['items'] as $delta => $item) {
      $variables['items'][$delta]['#markup'] = html_entity_decode($item['#markup']);
    }
  }

  // Calendar Square Image Style for WETA Event List
  if ($variables['element']['#field_name'] == 'field_cal_image' && isset($variables['element']['#object']->view) && $variables['element']['#object']->view->name == 'weta_events') {
    foreach ($variables['items'] as $item) {
      if (isset($item['#image_style']) && $item['#image_style'] == 'calendar_basic') {
          $variables['classes_array'][] = 'span_3 col p10';
      }
    }
  }

  // Opera house
  if ($variables['element']['#bundle'] == 'fm_opera_house') {
    // remove 'inline' class because we just want the label inline, not the field
    $inline = array_search('inline', $variables['classes_array']);
    if (!empty($inline)) {
      unset($variables['classes_array'][$inline]);
    }

    // make cast list inline by adding class to first p tag
    if ($variables['element']['#field_name'] == 'field_performance_info') {
      $markup = $variables['items'][0]['#markup'];
      $markup = preg_replace('/<p>/', '<p class="inline">', $markup, 1);
      $variables['items'][0]['#markup'] = $markup;
    }

    // add arrow to more info link
    if ($variables['element']['#field_name'] == 'field_offsite_link') {
      foreach ($variables['items'] as $i => $item) {
        $variables['items'][$i]['#element']['title'] .= ' &rarr;';
        $variables['items'][$i]['#element']['html'] = TRUE;
      }
    }

    // TEASER
    if ($variables['element']['#view_mode'] == 'teaser') {

      // add arrow to read more links
      if ($variables['element']['#field_name'] == 'node_link') {
        foreach ($variables['element']['#items'] as $i => $item) {
          $variables['items'][$i]['#markup'] = str_replace('</a>', ' &rarr; </a>', $variables['items'][$i]['#markup']);
        }
      }

    }
  }

  // Timeline Items
  if ($variables['element']['#bundle'] == 'timeline_item' && $variables['element']['#view_mode'] == 'teaser') {
    if ($variables['element']['#field_name'] == 'date_id') {
      $variables['items'][0]['#markup'] = strip_tags($variables['items'][0]['#markup']);
    }
  }


  // Job opening
  if ($variables['element']['#bundle'] == 'job_opening') {
    // remove 'inline' class because we just want the label inline, not the field
    $inline = array_search('inline', $variables['classes_array']);
    if (!empty($inline)) {
      unset($variables['classes_array'][$inline]);
    }

        // add arrow to read more links
    if ($variables['element']['#field_name'] == 'node_link') {
      foreach ($variables['element']['#items'] as $i => $item) {
          $variables['items'][$i]['#markup'] = str_replace('</a>', ' &rarr; </a>', $variables['items'][$i]['#markup']);
      }
    }

  }

  // PMP Stories and Videos
  if ($variables['element']['#bundle'] == 'pmp_story' || $variables['element']['#bundle'] == 'pmp_video') {
    // add arrow to read more links
    if ($variables['element']['#field_name'] == 'node_link') {
      foreach ($variables['element']['#items'] as $i => $item) {
          $variables['items'][$i]['#markup'] = str_replace('</a>', ' &rarr; </a>', $variables['items'][$i]['#markup']);
      }
    }
  }

  // Quiz results
  if ($variables['element']['#bundle'] == 'field_quiz_result_options') {
    if ($variables['element']['#field_name'] == 'field_quiz_result_description') {
      $variables['classes_array'][] = 'span_9 col';
    }
    if ($variables['element']['#field_name'] == 'field_quiz_share_buttons') {
      $variables['classes_array'][] = 'span_3 col p20';
    }
  }

  if ($variables['element']['#bundle'] == 'quiz_results') {
    if ($variables['element']['#field_name'] == 'field_parent_quiz' && isset($variables['items'][0]['#type']) && $variables['items'][0]['#type'] == 'link') {
      $variables['items'][0]['#title'] = 'Take the quiz again &rarr;';
      $variables['items'][0]['#options']['html'] = TRUE;
    }
    if ($variables['element']['#field_name'] == 'field_quiz_tweet') {
      // sanitize output
      $tweet = html_entity_decode($variables['element']['#items'][0]['safe_value'], $flags = ENT_QUOTES);
      $tweet_text = urlencode($tweet);

      // get current alias and construct URL
      $alias = drupal_get_path_alias(current_path());
      $url = $GLOBALS['base_url'] . '/' . $alias;

      // convert to bitly URL
      $short_url = custom_get_bitly_short_url($url);

      // construct widget
      $twitter_widget = '<a href="https://twitter.com/intent/tweet?url=' .  $short_url . '&text=' . $tweet_text . '" class="twitter-share-button" data-count="vertical" data-text="' . $tweet . '" data-url="' . $short_url . '" data-counturl="' . $url . '" data-via="wetatvfm">Tweet</a>';

      // construct FB like button
      /*
      $fb = '<div id="fb-root" style="z-index:100;"></div>';
      $fb .= '<div class="fb-like" data-href="' . $url . '" data-send="false" data-layout="box_count" data-width="450" data-show-faces="false" data-font="verdana"></div>';
      */
      // construct FB share button
      $fb = '<div class="fb-share-button" data-href="' . $url . '" data-layout="box_count"></div>';

      // put them together
      $buttons = $twitter_widget . '<br /><br />' . $fb;
      // replace markup

      $variables['items'][0]['#markup'] = $buttons;
    }
  }

  if ($variables['element']['#bundle'] == 'video_upload' && $variables['element']['#field_name'] == 'modal_player_link') {
    $nid = trim(strip_tags($variables['items'][0]['#markup']));
    $title = 'Watch now &rarr;';
    $url = 'watchbox/' . $nid;
    $options = array();
    $options['attributes']['class'] = array('jw_popup');
    $options['html'] = TRUE;
    $link = l($title, $url, $options);
    $variables['items'][0]['#markup'] = '<p>' . $link . '</p>';
  }


  if ($variables['element']['#bundle'] == 'video_upload' && $variables['element']['#field_name'] == 'field_video' && $variables['element']['#view_mode'] == 'highlight_20_80') {
    $variables['classes_array'][] = 'jw_popup_box';
  }

  if ($variables['element']['#bundle'] == 'bracket_match') {
    if ($variables['element']['#field_name'] == 'field_tweet') {
      // sanitize output
      $tweet = html_entity_decode($variables['element']['#items'][0]['safe_value'], $flags = ENT_QUOTES);

      // get current alias and construct URL
      $alias = drupal_get_path_alias(current_path());
      $url = $GLOBALS['base_url'] . '/' . $alias;

      // convert to bitly URL
      $short_url = custom_get_bitly_short_url($url);

      // construct widget
      $twitter_widget = '<a href="https://twitter.com/share?url=' .  $short_url . '" class="twitter-share-button" data-count="vertical" data-text="' . $tweet . '" data-url="' . $short_url . '" data-counturl="' . $url . '" data-via="wetatvfm">Tweet</a><script type="text/javascript" src="https://platform.twitter.com/widgets.js"></script>';

      // construct FB like button
      $fb = '<div id="fb-root" style="z-index:100;"></div>';
      $fb .= '<div class="fb-like" data-href="' . $url . '" data-send="false" data-layout="box_count" data-width="450" data-show-faces="false" data-font="verdana"></div>';

      // put them together
      $buttons = $twitter_widget . '<br /><br />' . $fb;
      // replace markup

      $variables['items'][0]['#markup'] = $buttons;
    }
  }

  // add arrow to additional link field
  if ($variables['element']['#field_name'] == 'field_additional_link') {

    // Add arrows and localize
    foreach ($variables['element']['#items'] as $i => $item) {

      if (isset($variables['items'][$i]['#markup'])) {
        $item['title'] .= ' &rarr;';
        $item['attributes'] = array('html' => TRUE);
        $variables['items'][$i]['#markup'] = l($item['title'], $item['url'], $item['attributes']);
      }
      else {
        $item['title'] .= ' &rarr;';
        $variables['items'][$i]['#element']['title'] = $item['title'];
        $variables['items'][$i]['#element']['html'] = TRUE;
      }
    }
  }

  // TV Series
  if ($variables['element']['#bundle'] == 'tv_series') {
    if ($variables['element']['#view_mode'] == 'highlight_20_80') {
      // add arrow to read more links
      if ($variables['element']['#field_name'] == 'node_link') {
        foreach ($variables['element']['#items'] as $i => $item) {
          $variables['items'][$i]['#markup'] = str_replace('</a>', ' &rarr; </a>', $variables['items'][$i]['#markup']);
        }
      }
    }
  }

  // Podcasts
  if ($variables['element']['#bundle'] == 'podcast') {
    // Add the podtrac prefix to the embedded player
    if ($variables['element']['#field_name'] == 'field_podcast_episode') {
      // Because there's no way to gracefully step in earlier in the process,
      // we're going to need to re-theme the player. So, first step:
      // set up all the necessary elements
      $entity_type = $variables['element']['#entity_type'];
      $entity = $variables['element']['#object'];
      $field_name = $variables['element']['#field_name'];

      // We need to go into the field instance to get the display settings
      $field_info = field_info_instance($entity_type, $variables['element']['#field_name'], $variables['element']['#bundle']);
      $display = array('settings' => $field_info['display']['default']['settings']);

      // Now that we have all the other pieces, update the file URL.
      $items = $variables['element']['#items'];
      $url = $items[0]['uri'];
      $podtrac_prefix = 'https://dts.podtrac.com/redirect.mp3/weta.org/sites/default/files/';
      $new_url = str_replace('public://', $podtrac_prefix, $url);
      $items[0]['uri'] = $new_url;

      $markup = theme(
          'jplayer',
          array(
            'entity_type' => $entity_type,
            'entity' => $entity,
            'field_name' => $field_name,
            'items' => $items,
            'settings' => $display['settings'],
          )
        );
      $variables['items'][0]['#markup'] = $markup;
    }
    if ($variables['element']['#field_name'] == 'field_podcast_image') {
      $variables['classes_array'][] = 'span_3 col';
    }
    if ($variables['element']['#field_name'] == 'field_podcast_full_description') {
      $variables['classes_array'][] = 'span_9 col';
    }
    if ($variables['element']['#field_name'] == 'field_podcast_show_notes') {
      $variables['classes_array'][] = 'clearfix';
    }
  }

  // Slideshow Hero Carousels
  if ($variables['element']['#bundle'] == 'slideshow') {
    // Add a template override for the images field.
    if ($variables['element']['#view_mode'] == 'homepage_carousel' && $variables['element']['#field_name'] == 'field_slideshow_images') {
      $variables['theme_hook_suggestions'][] = 'field__field_slideshow_images__slideshow__homepage_carousel';
    }
  }

  // Album of the Week
  if ($variables['element']['#bundle'] == 'fm_cd_pick') {
    if ($variables['element']['#field_name'] == 'field_primary_link') {
      foreach ($variables['element']['#items'] as $i => $item) {
        $url = $item['url'];
        $parsed = parse_url($url);
        // check to see if the link is to arkivmusic
        $arkiv = strpos($parsed['host'], 'arkivmusic.com');
        // if it IS to arkiv, then check to see if the source=WETA parameter already exists
        if ($arkiv !== FALSE && !empty($parsed['query'])) {
          parse_str($parsed['query'], $query);
          // if not, add it and rebuild the URL
          if (!isset($query['source'])) {
            $query['source'] = 'WETA';
            $parsed['query'] = http_build_query($query);
            $new_url = $parsed['scheme'] . '://' . $parsed['host'] . $parsed['path'] . '?' . $parsed['query'];

            // replace the URL in the array
            $variables['items'][$i]['#element']['url'] = $new_url;
          }
        }


      }

    }
  }
}

/**
 * Override or insert variables into the file entity template.
 */
function weta_preprocess_file_entity(&$variables) {
  // suggest specific template for pmpapi audio files
  if ($variables['type'] == 'audio' && isset($variables['pmpapi_guid']) && !empty($variables['pmpapi_guid'])) {
    $variables['theme_hook_suggestions'][] = 'file__audio__pmpapi';
  }
}

/**
 * Override or insert variables into the block template.
 */
function weta_preprocess_block(&$variables) {

  $alias = drupal_get_path_alias(current_path());
  $sections = explode('/', $alias);

  if ($variables['block']->module == 'system' && $sections[0] == 'lightbox') {
    $variables['theme_hook_suggestions'][] = 'block__system__main__lightbox';
  }

  // In the header region visually hide block titles.
  if (isset($variables['block']->region) && $variables['block']->region == 'header') {
    $variables['title_attributes_array']['class'][] = 'element-invisible';
  }

  // station relations logged in block
  if ($variables['block_html_id'] == 'block-custom-station-relations-logged-in') {
    if ($variables['logged_in'] == FALSE) {
      // remove sideblock class
      if (in_array('sideblock', $variables['classes_array'])) {
        $classes = array_flip($variables['classes_array']);
        unset($variables['classes_array'][$classes['sideblock']]);
      }
      // remove title
      $variables['block']->subject = '';
    }
  }
}

/**
 * Implements theme_menu_tree().
 */
function weta_menu_tree($variables) {
  unset($shortcut_check);
  $shortcut_check = substr_count($variables['theme_hook_original'], 'shortcut_set');
  $ul_start = '<ul>';
  if (!empty($shortcut_check)) {
    $ul_start = '<ul class="menu">';
  }
  //return '<ul class="menu clearfix">' . $variables['tree'] . '</ul>';
  return $ul_start . $variables['tree'] . '</ul>';


}

/**
 * Menu overrides
 */
// adding primary_nav_links id
function weta_menu_tree__main_menu($variables) {
  return '<ul id="primary_nav_links">' . $variables['tree'] . '</ul>';
}

// adding mobile_nav id
function weta_menu_tree__menu_mobile_menu($variables) {
  return '<ul id="mobile_nav">' . $variables['tree'] . '</ul>';
}

// remove menu clearfix
function weta_menu_tree__menu_about_us($variables) {
  return '<ul>' . $variables['tree'] . '</ul>';
}

// adding radio class to listen live menu
function weta_menu_tree__menu_listen_live($variables) {
  return '<ul class="radio">' . $variables['tree'] . '</ul>';
}

// splitting subnav menus into thirds
function weta_menu_tree__menu_subnav_radio($variables) {
  $chunks = 3;
  $newtree = custom_subnav_reformat($variables['tree'], $chunks);
  // return the formatted menu
  return $newtree;
}

function weta_menu_tree__menu_subnav_about($variables) {
  $chunks = 4;
  $newtree = custom_subnav_reformat($variables['tree'], $chunks);
  // return the formatted menu
  return $newtree;
}

function weta_menu_tree__menu_subnav_kids($variables) {
  $chunks = 3;
  $newtree = custom_subnav_reformat($variables['tree'], $chunks);
  // return the formatted menu
  return $newtree;
}

function weta_menu_tree__menu_subnav_local($variables) {
  $chunks = 4;
  $newtree = custom_subnav_reformat($variables['tree'], $chunks);
  // return the formatted menu
  return $newtree;
}

function weta_menu_tree__menu_subnav_television($variables) {
  $chunks = 4;
  $newtree = custom_subnav_reformat($variables['tree'], $chunks);
  // return the formatted menu
  return $newtree;
}
function weta_menu_tree__menu_subnav_education($variables) {
  $chunks = 4;
  $newtree = custom_subnav_reformat($variables['tree'], $chunks);
  // return the formatted menu
  return $newtree;
}

function weta_menu_tree__menu_subnav_support($variables) {
  $chunks = 5;
  $newtree = custom_subnav_reformat($variables['tree'], $chunks);
  // return the formatted menu
  return $newtree;
}


// removing classes from menu list items
function weta_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';

  // remove the classes
  unset($element['#attributes']['class']);

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }

  // add today's date to the Schedule main nav item
  if ($element['#original_link']['mlid'] == '4777') {
    $today = date('D. M j', time());
    $element['#title'] .= '<br /><span>' . $today . '</span>';
    $element['#localized_options']['html'] = TRUE;
  }

  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

/**
 * Implements hook_form_alter().
 */
function weta_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'search_block_form') {
    $form['#id'] = 'search_form';
    $form['search_block_form']['#id'] = 'search_field';
  }
}

/**
 * Alters image tags to remove width and height attributes
 * so images will scale properly and responsively
 */

function weta_image($vars) {
  $attributes = $vars['attributes'];
  $attributes['src'] = file_create_url($vars['path']);

  // remove width and height attributes
  foreach (array('alt', 'title') as $key) {
    if (isset($vars[$key])) {
      $attributes[$key] = $vars[$key];
    }
  }

  // add class to image in the now playing widget
  if (isset($vars['style_name']) && $vars['style_name'] == 'now_playing_logo') {
    $attributes['class'] = 'fltlt';
  }

  // add class to image in tv banner house ad
  if (isset($vars['style_name']) && $vars['style_name'] == 'tv_banner') {
    $attributes['class'] = 'full_width';
  }

  // add class to image in the mini feature block
  if (isset($vars['style_name']) && $vars['style_name'] == 'mini_feature') {
    $attributes['class'] = 'fltlt';
  }

  // add class to image in the highlight 20/80 feature block
  if (isset($vars['style_name']) && $vars['style_name'] == 'highlight_20-80') {
    $attributes['class'] = 'full_width';
  }

  // add class to image in the Productions Feature and Full Width displays
  if (isset($vars['style_name']) && ($vars['style_name'] == 'productions_highlight' || $vars['style_name'] == 'full_width')) {
    $attributes['class'] = 'full_width';
  }

  // add class to image in the Wide Feature display
  if (isset($vars['style_name']) && $vars['style_name'] == 'wide_feature') {
    $attributes['class'] = 'fltlt full_width';
  }

  // add class to image in the Flyout Highlight display
  if (isset($vars['style_name']) && $vars['style_name'] == 'flyout_highlight') {
    $attributes['class'] = 'fltlt';
  }

  // add class to image in the FM Feature Logo display
  if (isset($vars['style_name']) && $vars['style_name'] == 'fm_feature_logo') {
    $attributes['class'] = 'imgleft';
  }

  // add class to Wanted Posters
  if (isset($vars['style_name']) && ($vars['style_name'] == 'wanted' || $vars['style_name'] == 'wanted_thumb')) {
    $attributes['class'] = 'full_width';
  }

  if (isset($vars['style_name']) && $vars['style_name'] == '16x9_feature') {
    $attributes['class'] = 'fltlt full_width';
  }

  // add class to Skinny Strip
  if (isset($vars['style_name']) && $vars['style_name'] == 'skinny_strip') {
    $attributes['class'] = 'full_width';
  }


  return '<img' . drupal_attributes($attributes) . ' />';
}

function weta_preprocess_panels_pane(&$vars) {

  // Add class to the homepage what's on now block title
  if ($vars['pane']->type == 'block' && $vars['pane']->subtype == 'quicktabs-what_s_on_now_homepage_block') {
    $vars['title_attributes_array']['class'][] = 'gray';
  }

  // Add classes to newsletter signup block
  if ($vars['pane']->type == 'block' && $vars['pane']->subtype == 'block-15') {
    $vars['classes_array'][] = 'news_signup p10 mtop_40';
  }

  // Add H1 tags to title
  if ($vars['pane']->type == 'node_title' && $vars['display']->layout == 'tvseries') {
    $vars['content'] = '<h1>' . $vars['content'] . '</h1>';
  }

  // Add SHARE label since panels strips it out
  if ($vars['pane']->type == 'block' && $vars['pane']->subtype == 'sharethis-sharethis_block') {
    $vars['content'] = '<div class="share_label">SHARE:</div>' . $vars['content'];

    // Float right on calendar landing page
    if ($vars['pane']->pid == '276') {
      $vars['classes_array'][] = 'share_right';
    }
  }


  // Add classes to From Our Blogs widget
  if ($vars['pane']->type == 'panels_mini' && $vars['pane']->subtype == 'from_our_blogs') {
    $vars['classes_array'][] = 'border_wrap mright_20';
  }

  // Add classes to Classical News widget
  if ($vars['pane']->type == 'views_panes' && $vars['pane']->subtype == 'classical_news-panel_pane_2') {
    //$vars['classes_array'][] = 'border_wrap';
  }

  if ($vars['pane']->type == 'views_panes' && $vars['pane']->subtype == 'classical_news-classical_news_ticker') {
    $vars['title_attributes_array']['class'][] = 'span_3 col mright_20';
    $title_image = '<img src="' . base_path() . path_to_theme() . '/templates/images/ClassicalNews_640x205.jpg" alt="Classical Music News" class="full_width" />';
    $vars['title'] = l($title_image, '/fm/news', array('html' => TRUE));
  }

  // Add class and logo to WETA Kids branding block
  if ($vars['pane']->type == 'block'  && $vars['pane']->subtype == 'block-17') {
    $vars['classes_array'][] = 'kids-logo col p30';
    $vars['content'] = '<img src="' . base_path() . path_to_theme() . '/templates/images/PBSKids2_181x140.png" alt="WETA PBS Kids logo" class="full_width" /><br />' . $vars['content'];
  }

  // Add class to What's On Now block on Kids landing page
  if ($vars['pane']->type == 'block' &&  $vars['pane']->subtype == 'quicktabs-whats_on_now_weta_kids') {
    $vars['classes_array'][] = 'top-bar-orange';
  }

  if ($vars['pane']->type == 'node' && $vars['content']['#view_mode'] == 'highlight_20_80' && $vars['pane']->did == '631') {
    $vars['classes_array'][] = 'top-bar-purple';
  }

  // Support branding block
  if ($vars['pane']->type == 'block' && $vars['pane']->subtype == 'block-18') {
    $vars['classes_array'][] = 'links';
    $vars['classes_array'][] = 'nopad';
    $vars['pane_prefix'] = '<div class="kids-logo col link_nav">';
    $vars['pane_suffix'] = '</div>';
    $vars['title_prefix'] = '<h1>' . $vars['display']->title . '</h1>';
  }
  // About branding block
  if ($vars['pane']->type == 'block' && $vars['pane']->subtype == 'block-28') {
    $vars['classes_array'][] = 'links';
    $vars['classes_array'][] = 'nopad';
    $vars['pane_prefix'] = '<div class="kids-logo col link_nav">';
    $vars['pane_suffix'] = '</div>';
    $vars['title_prefix'] = '<h1>' . $vars['display']->title . '</h1>';
  }
    // Local branding block
  if ($vars['pane']->type == 'block' && $vars['pane']->subtype == 'block-25') {
    $vars['classes_array'][] = 'links';
    $vars['classes_array'][] = 'nopad';
    $vars['pane_prefix'] = '<div class="kids-logo col link_nav">';
    $vars['pane_suffix'] = '</div>';
    $vars['title_prefix'] = '<h1>' . $vars['display']->title . '</h1>';
  }

  if ($vars['pane']->type == 'block' && $vars['pane']->subtype == 'block-19') {
    $vars['classes_array'][] = 'message_box_text_wrap';
    $vars['pane_prefix'] = '<div class="message_box hide-mobile">';
    $vars['pane_suffix'] = '</div>';
    $vars['title_attributes_array'] = array();
    $vars['title_header'] = 'h3';
  }

 // WETA UK branding block
  if ($vars['pane']->type == 'block' && $vars['pane']->subtype == 'block-24') {
    $vars['classes_array'][] = 'links';
    $vars['classes_array'][] = 'nopad';
    $vars['pane_prefix'] = '<div class="kids-logo col link_nav">';
    $vars['pane_suffix'] = '</div>';
    $vars['content'] = '<img src="' . base_path() . path_to_theme() . '/templates/images/weta_uk_logo.png" alt="WETA UK logo" class="full_width" /><br />' . $vars['content'];
  }

  // switch title header from h2 to h4 in flyout 50/25/25 layout
  if ($vars['pane']->type == 'block' && $vars['display']->layout == 'flyout_50_25_25' && ($vars['pane']->panel == 'right1' || $vars['pane']->panel == 'right2')) {
    $vars['title_header'] = 'h4';
  }

  // switch title header from h2 to h1 in calendar custom pane (intro)
  if ($vars['pane']->pid == '277') {
    $vars['title_header'] = 'h1';
  }

  if ($vars['pane']->type == 'node' && $vars['content']['#bundle'] == 'slideshow') {
    $vars['title_header'] = 'h3';
  }

  if ($vars['pane']->type == 'fieldable_panels_pane') {
    if ($vars['content']['#bundle'] == 'media_manager_feature_block') {
      // override the template
      $vars['theme_hook_suggestions'][] = 'panels_pane__fieldable_panels_pane__media_manager_feature_block';

      // set up the title field, pulling the URL from the video field.
      if (isset($vars['content']['field_mm_video']) && isset($vars['content']['field_mm_video']['#items'][0]['url'])) {
        $vars['title_prefix'] = '<a href="' . $vars['content']['field_mm_video']['#items'][0]['url'] . '">';
        $vars['title_suffix'] = '</a>';
      }

      // set up the image
      if (isset($vars['content']['field_mm_video']) && isset($vars['content']['field_mm_video'][0]['#data']['image'])) {
        $vars['content']['image'] = theme_image($vars['content']['field_mm_video'][0]['#data']['image']);
      }

      // set up passport icon
      $vars['content']['passport'] = '';
      if (isset($vars['content']['field_mm_video']) && $vars['content']['field_mm_video'][0]['#data']['is_passport']) {
        $vars['content']['passport'] = '<img src="/sites/all/modules/weta_custom/weta_pbs_media_manager/templates/passport_sm.png" class="passport_icon" alt="WETA Passport" title="WETA Passport">';
      }
    }
  }
  // add span_4 class to dfp wrapper level so as to center the country music ad
  if ($vars['pane']->type == 'block' && $vars['pane']->subtype == 'dfp-tv_countrymusic_300x250') {
    $vars['content']['dfp_wrapper']['#attributes']['class'][] = 'span_4';
  }

}

/**
 * Theme function to output tablinks for jQuery UI style tabs.
 * From the Quicktabs module
 */
function weta_qt_ui_tabs_tabset($vars) {
  $output = '<ul>';
  foreach ($vars['tabset']['tablinks'] as $i => $tab) {
    if (is_array($tab)) {
      $link = drupal_render($tab);
      $link_array = explode('>' , $link, 2);
      $link = implode('><span>', $link_array);
      $link = str_replace('</a>', '</span></a>', $link);
      $output .= '<li class="tab-' . $i . '">' . $link . '</li>';
    }
  }
  $output .= '</ul>';

  return $output;
}

/**
 * Theme function to output content for jQuery UI style tabs.
 *
 * @ingroup themeable
 */
function weta_qt_ui_tabs($variables) {
  $element = $variables['element'];

  // add classes to tv episodes widget
  if ($element['#options']['attributes']['id'] == 'quicktabs-episodes_by_series') {
    $element['#options']['attributes']['class'] .= ' weta-tabs tabs-schedule tabs3';
  }

  if ($element['#options']['attributes']['id'] == 'quicktabs-whats_on_now_weta_kids') {
    $element['#options']['attributes']['class'] .= ' weta-tabs tabs-schedule tabs2';
  }

  $output = '<div ' . drupal_attributes($element['#options']['attributes']) . '>';
  $output .= drupal_render($element['tabs']);

  if (isset($element['active'])) {
    $output .= drupal_render($element['active']);
  }
  else {
    foreach ($element['divs'] as $div) {
      $output .= drupal_render($div);
    }
  }

  $output .= '</div>';
  return $output;
}

/**
 * Override or insert variables into the views template for HTML output.
 */

function weta_preprocess_views_view(&$variables) {

  // modify featured links blocks on landing pages
  if ($variables['name'] == 'featured_links' && $variables['display_id'] == 'panel_pane_1') {

    // add classes
    $variables['classes_array'][] = 'kids-logo col link_nav';

    // add title
    $variables['title'] = '<h3>' . t('Today\'s Features') . '</h3>';

    // add header logos and link classes
    switch ($variables['view']->args[0]) {
      // FM
      case '98':
        // Anniversary Logo with horn
        //$variables['header'] = '<img src="' . base_path() . path_to_theme() . '/templates/images/classical weta horizontal logo-02b-02.png" alt="Classical WETA logo"  class="full_width hide-mobile"/>';
        // Standard CW logo
        $variables['header'] = '<img src="' . base_path() . path_to_theme() . '/templates/images/FM_Level1_classical_logo.png" alt="Classical WETA logo"  class="full_width hide-mobile"/>';
        $variables['div_classes'] = 'links';
        break;
      // TV
      case '97':
        $variables['header'] = '<img src="' . base_path() . path_to_theme() . '/templates/images/weta_tv_logo.png" alt="WETA Television logo"  class="full_width hide-mobile"/>';
        $variables['div_classes'] = 'links light_blue';
        break;
    }

    // remove <div class="item-list"> because theme_item_list is stupid.
    $variables['rows'] = strip_tags($variables['rows'], '<ul><li><a>');


  }

  // modify CW now playing block on FM landing page
  if ($variables['name'] == 'now_playing' && $variables['display_id'] == 'panel_pane_1') {

    // add classes
    $variables['classes_array'][] = 'span_4 col p20 adMid';

    if (!($variables['is_front'])) {
      // add title
      $variables['title'] = '<h2 class="show-mobile">' . t('What\'s On') . '</h2>';
    }
  }

  // modify VLV now playing block on FM landing page
  if ($variables['name'] == 'now_playing' && $variables['display_id'] == 'panel_pane_2') {

  // add classes
  $variables['classes_array'][] = 'span_4 col p20 adLast';

  // add title
  $variables['title'] = '<div class="divider1 show-mobile"></div>';
  }

  // modify on-air host block on FM landing page
  if ($variables['name'] == 'now_playing' && ($variables['display_id'] == 'attachment_2' || $variables['display_id'] == 'attachment_3')) {
    // add classes
    $variables['classes_array'][] = 'one_tweet';
  }

  // add class to From Our Blogs headers
  if ($variables['name'] == 'blogs') {
    $variables['classes_array'][] = 'block_head';
  }

  if ($variables['name'] == 'weta_kids_feature_strip' && $variables['display_id'] == 'panel_pane_1') {
    $variables['classes_array'][] = 'feature-row kids-row';
  }

  // reset title for FM Playlists and Giveaways, since they seem to have gone missing
  if ($variables['name'] == 'fm_playlists') {
    $view = $variables['view'];
    $variables['title'] = filter_xss_admin($view->get_title());
  }

  // formatting for Classical Conversations red feature
  if ($variables['name'] == 'fm_classical_conversations' && $variables['display_id'] == 'panel_pane_1') {
    $variables['classes_array'][] = 'span_3 col p30';
  }

  if ($variables['name'] == 'new_calendar' && $variables['display_id'] == 'panel_pane_1') {
    $variables['classes_array'][] = 'calendar_table';
  }

  if ($variables['name'] == 'cd_picks' && $variables['display_id'] == 'page') {

    // For reasons, they decided to hard code attachment titles with h2 tags.
    // I want them to be h3.
    $variables['attachment_after'] = str_replace('h2>', 'h3>', $variables['attachment_after']);

  }
}




/**
 * Alter a flipped table style view.
 */
function weta_preprocess_views_flipped_table(&$vars) {
  // modify tv and radio channel blocks
  if ($vars['view']->name == 'weta_television_channel_guide' || $vars['view']->name == 'weta_radio_channel_guide') {
    $vars['classes_array'][] = 'channel-guide responsive';
  }
}

/**
 * Override or insert variables into the views template for HTML output.
 */
function weta_preprocess_views_view_table(&$variables) {

  if ($variables['view']->name == 'new_calendar') {
    // add class
    $variables['classes_array'][] = 'calendar_table';
  }
  if ($variables['view']->name == 'administration_calendar_events') {
    $variables['classes_array'][] = 'calendar_admin_table';
  }
}

/**
 * Override or insert variables into the views template for HTML output.
 */
function weta_preprocess_views_view_unformatted(&$variables) {



  if ($variables['view']->name == 'fm_classical_conversations' && $variables['view']->current_display == 'panel_pane_1') {
    $variables['classes_array'][0] .= ' red';
  }

}

/**
 * Returns HTML for a form element.
 *
 * see includes/form.inc Line 4011
 */

function weta_form_element($variables) {
  $element = &$variables['element'];

  // Community Calendar date exposed filter
  if ($element['#id'] == 'edit-date-filter-value-datepicker-popup-0') {
    // insert icon
    $element['#children'] .= '<img class="ui-datepicker-trigger" src="' . base_path() . path_to_theme() . '/templates/images/calendar.gif" alt="..." title="...">';

    // remove description
    unset($element['#description']);
  }

  // Playlist exposed filter
  if ($element['#id'] == 'edit-playlist-date-value-datepicker-popup-1') {
    // insert icon
    $element['#children'] .= '<img class="ui-datepicker-trigger" src="' . base_path() . path_to_theme() . '/templates/images/calendar.gif" alt="..." title="...">';

    // remove description
    unset($element['#description']);
  }
  // ORIGINAL CODE

  // This function is invoked as theme wrapper, but the rendered form element
  // may not necessarily have been processed by form_builder().
  $element += array(
    '#title_display' => 'before',
  );

  // Add element #id for #type 'item'.
  if (isset($element['#markup']) && !empty($element['#id'])) {
    $attributes['id'] = $element['#id'];
  }
  // Add element's #type and #name as class to aid with JS/CSS selectors.
  $attributes['class'] = array('form-item');
  if (!empty($element['#type'])) {
    $attributes['class'][] = 'form-type-' . strtr($element['#type'], '_', '-');
  }
  if (!empty($element['#name'])) {
    $attributes['class'][] = 'form-item-' . strtr($element['#name'], array(' ' => '-', '_' => '-', '[' => '-', ']' => ''));
  }
  // Add a class for disabled elements to facilitate cross-browser styling.
  if (!empty($element['#attributes']['disabled'])) {
    $attributes['class'][] = 'form-disabled';
  }

  // need to add class for bracket voting
  if ($element['#type'] == 'radio' && isset($element['#form_type']) && $element['#form_type'] == 'bracket') {

    $attributes['class'][] = 'span_5 p20-10-10';
    // add clearfix to third element so second row lines up nicely
    // get last character of id
    $radio_id = substr($element['#id'], -1);
    if ($radio_id == '3') {
      $attributes['style'][] = 'clear: both;';
    }
  }
  $output = '<div' . drupal_attributes($attributes) . '>' . "\n";

  // If #title is not set, we don't display any label or required marker.
  if (!isset($element['#title'])) {
    $element['#title_display'] = 'none';
  }
  $prefix = isset($element['#field_prefix']) ? '<span class="field-prefix">' . $element['#field_prefix'] . '</span> ' : '';
  $suffix = isset($element['#field_suffix']) ? ' <span class="field-suffix">' . $element['#field_suffix'] . '</span>' : '';

  // need to put the input inside the label for bracket voting
  if ($element['#type'] == 'radio' && isset($element['#form_type']) && $element['#form_type'] == 'bracket') {
    $variables['rendered_element'] = ' ' . $prefix . $element['#children'] . $suffix . "\n";
    $output .= theme('bracket_form_element_label', $variables);
  }
  else {
    switch ($element['#title_display']) {
      case 'before':
      case 'invisible':
        $output .= ' ' . theme('form_element_label', $variables);
        $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
        break;

      case 'after':
        $output .= ' ' . $prefix . $element['#children'] . $suffix;
        $output .= ' ' . theme('form_element_label', $variables) . "\n";
        break;

      case 'none':
      case 'attribute':
        // Output no label and no required marker, only the children.
        $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
        break;
    }
  }

  if (!empty($element['#description'])) {
    $output .= '<div class="description">' . $element['#description'] . "</div>\n";
  }

  $output .= "</div>\n";



  return $output;
}

/**
 * Remove the 'all day' label by blanking out the theme function since there
 * doesn't seem to be a better way.
 */
function weta_date_all_day_label() {
}

/**
 * Preprocess function for DFP tags.
 *
 * Sizes are stored in the format [[w, h], [w, h]]
 */
function weta_preprocess_dfp_tag(&$variables) {
  // Add theme template suggestsions based on ad width
  if (isset($variables['tag']->size)) {
    //separate multiple sizes
    $sizes = explode('], ', $variables['tag']->size);
    $widths = array();
    foreach($sizes as $i => $size) {
      //strip brackets
      $size = str_replace('[', '', $size);
      $size = str_replace(']', '', $size);
      //extract widths
      $dimensions = explode(',', $size);
      $widths[] = $dimensions[0];
    }
    // de-dupe widths
    $widths = array_unique($widths);

    //add theme template suggestions
    foreach ($widths as $i => $width) {
      $variables['theme_hook_suggestions'][] = 'dfp_tag__' . $width;
    }
  }
}

/**
 * Preprocess function for flexslider container.
 *
 * NOTE: I think this is deprecated. It doesn't seem to be doing anything.
 */

function weta_preprocess_flexslider_container(&$vars) {
  switch($vars['settings']['flexslider_optionset']) {
    case 'slideshow':
      $vars['theme_hook_suggestions'][] = 'flexslider_container__slideshow';
    break;
    case 'slideshow_no_autoplay':
      $vars['theme_hook_suggestions'][] = 'flexslider_container__slideshow_no_autoplay';
    break;
  }
}

function weta_preprocess_weta_player_listen_live_html(&$variables) {
  $path = $GLOBALS['base_url'] . base_path() . path_to_theme() . '/templates/css/';

  $css = '<link media="all" href="' . $path . 'fonts.css" rel="stylesheet" type="text/css" />';
  $css .= '<link media="all" href="' . $path . 'gridpak.css" rel="stylesheet" type="text/css" />';
  //$css .= '<link media="all" href="' . $path . 'weta.css" rel="stylesheet"  type="text/css" />';
  $css .= '<link media="all" href="' . $path . 'weta-player.css" rel="stylesheet"  type="text/css" />';

  $css .= '<link media="all" href="' . $path . 'player.css" rel="stylesheet" type="text/css" />';



  $variables['includes'] .= $css;
}


function weta_html_head_alter(&$head_elements) {
  //only run this if we're on a webform result page
  if (arg(0) == 'node' && is_numeric(arg(1)) && arg(2) == 'done') {
    // and if there's actually a result
    if (isset($_GET['sid']) && !empty($_GET['sid']) && is_numeric($_GET['sid'])) {
      //load the node
      $node = node_load(arg(1));

      if ($node->type == 'quiz_trivia') {
        $url = drupal_lookup_path('alias', 'node/' . $node->nid);
        $head_elements['metatag_og:url']['#value'] = $GLOBALS['base_url'] . '/' . $url;
      }
    }
  }
}

/**
 * Template pre-process function.
 *
 * Preprocess the chart page to build all raw data necessary for rendering
 * using the template file:  webform-chart-page.tpl.php.
 */
function weta_preprocess_webform_chart_page(&$variables) {
  if ($variables['node']->type == 'bracket_match'):
    // override the template
    $variables['theme_hook_suggestions'][] = 'webform_chart_page__bracket';
  endif;
}


/**
 * Template pre-process function.
 *
 * Preprocess a chart item to build all raw data necessary for rendering using
 * the template file:  webform-chart-item.tpl.php.
 */
function weta_preprocess_webform_chart_item(&$variables) {
  // get the node type, since it isn't passed to the array
  $nid = $variables['raw_item']['component_data']['nid'];
  $node = node_load($nid);

  // only modify the 'bracket_match' content type
  if ($node->type == 'bracket_match'):
    $variables['item']['#title'] = '';
  endif;
}


/**
 * Output the Webform into the node content.
 *
 * @param $node
 *   The webform node object.
 * @param $page
 *   If this webform node is being viewed as the main content of the page.
 * @param $form
 *   The rendered form.
 * @param $enabled
 *   If the form allowed to be completed by the current user.
 */
function weta_webform_view($variables) {
  // If the Webform Scheduler module is enabled, allow for custom messages
  // for when the form is not available.
  // Requires hidden components with the keys before_message and after_message.
  if (module_exists('webform_scheduler')) {
    if (!empty($variables['webform']['#node']->webform['scheduler'])) {
      // get current time
      $now = time();

      // if the form isn't open yet, get the contents of before_message
      if ($now < $variables['webform']['#node']->webform['scheduler']['begin'] && isset($variables['webform']['#form']['submitted']['before_message']['#value'])) {
        $message = check_markup($variables['webform']['#form']['submitted']['before_message']['#value'], 'wysiwyg');
      }

      // if the form has already closed, get the contents of after_message
      elseif ($now > $variables['webform']['#node']->webform['scheduler']['end'] && isset($variables['webform']['#form']['submitted']['after_message']['#value'])) {
        $message = check_markup($variables['webform']['#form']['submitted']['after_message']['#value'], 'wysiwyg');
      }
    }

    // Only show the form if this user is allowed access.
    if ($variables['webform']['#enabled']) {
      return drupal_render($variables['webform']['#form']);
    }
    // Otherwise, if there's a message, display it.
    elseif (isset($message)) {
      return $message;
    }
  }
}

/**
 * Alter messages on the fly
 */
function weta_preprocess_status_messages(&$variables) {
  // Hide the closed webform message since we're displaying it through other
  // means.  See weta_webform_view().

  $message = 'Submissions for this form are closed.';
  if (isset($_SESSION['messages'])) {
    foreach ($_SESSION['messages'] as $type => $messages) {
      if ($type == 'warning') {
        $pos = array_search($message, $messages);
        if ($pos !== FALSE) {
          unset($_SESSION['messages'][$type][$pos]);
          if (empty($_SESSION['messages'][$type])) {
            unset($_SESSION['messages'][$type]);
          }
        }
      }
    }
  }
}
