<?php
/**
 * @file
 * Theme implementation to display Responsive Lightbox Launcher block.
 *
 * Stripped of everything but the identifier
 */
?>
<div id="<?php print $block_html_id; ?>">
<?php print $content ?>
</div>
