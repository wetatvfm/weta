<?php
  /**
   * @file
   * Default template for dfp tags.
   */
?>
<div class="ad300">
<div class="feature_box">
<div class="adImg">
<div <?php print drupal_attributes($placeholder_attributes) ?>>
  <script type="text/javascript">
    googletag.cmd.push(function() {
      googletag.display("<?php print $tag->placeholder_id ?>");
    });
  </script>

</div>
</div>
</div>
</div>
<div class="clearfix"></div>
