<?php
/**
 * @file
 * Displays the search form block.
 *
 * Available variables:
 * - $search_form: The complete search form ready for print.
 * - $search: Associative array of search elements. Can be used to print each
 *   form element separately.
 *
 * Default elements within $search:
 * - $search['search_block_form']: Text input area wrapped in a div.
 * - $search['actions']: Rendered form buttons.
 * - $search['hidden']: Hidden form elements. Used to validate forms when
 *   submitted.
 *
 * @see template_preprocess_search_block_form()
 */
?>
<?php if (empty($variables['form']['#block']->subject)): ?>
<h2 class="element-invisible"><?php print t('Search form'); ?></h2>
<?php endif; ?>
<?php print $search_form; ?>