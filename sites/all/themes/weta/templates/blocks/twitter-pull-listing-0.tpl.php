<?php
/**
 * @file
 * Theme template for a list of tweets.
 *
 * Available variables in the theme include:
 *
 * 1) An array of $tweets, where each tweet object has:
 *   $tweet->id
 *   $tweet->username
 *   $tweet->userphoto
 *   $tweet->text
 *   $tweet->timestamp
 *   $tweet->time_ago
 *
 * 2) $twitkey string containing initial keyword.
 *
 * 3) $title
 *
 */
?>
<?php if ($lazy_load): ?>
<?php print $lazy_load; ?>
<?php else: ?>
<div id="tweet_bird"></div>
<?php if (!empty($title)): ?>
<h3><?php print $title; ?></h3>
<?php endif; ?>
<?php if (is_array($tweets)):?>
<?php $tweet_count = count($tweets); ?>
<?php foreach ($tweets as $tweet_key => $tweet): ?>
<p><?php print twitter_pull_add_links($tweet->text); ?></p>
<?php if ($tweet_key < $tweet_count - 1): ?>
<div class="tweet-divider"></div>
<?php endif; ?>
<?php endforeach; ?>
<?php endif; ?>
<p><a href="http://twitter.com/wetatvfm">Follow @wetatvfm &rarr;</a></p>
<?php endif; ?>