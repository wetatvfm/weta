<?php
/**
 * @file field--fences-ul.tpl.php
 * Wrap each field value in the <li> element and all of them in the <ul> element.
 *
 * @see http://developers.whatwg.org/grouping-content.html#the-ul-element
 */
?>
<?php if ($element['#label_display'] == 'inline'): ?>
  <span class="field-label"<?php print $title_attributes; ?>>
    <?php print $label; ?>:
  </span>
<?php elseif ($element['#label_display'] == 'above'): ?>
  <h3 class="field-label"<?php print $title_attributes; ?>>
    <?php print $label; ?>
  </h3>
<?php endif; ?>

<?php

  // flag previous events with a class and count them
  $today = time();
  $prevtotal = 0;
  foreach ($element['#items'] as $i => $date) {
    $thisdate = strtotime($date['value']);
    if ($thisdate < $today) {
      $item_attributes[$i] = ' class="prevdate"';
      $prevtotal++;
    }
  }

  // determine how many dates there are total and upcoming
  $total = count($items);
  $upcomingtotal = $total - $prevtotal;

  // if there are more than 10 upcoming dates, flag extras with a class
  $start = $prevtotal + 9; // less one because array starts at 0
  if ($upcomingtotal > 10) {
    foreach ($items as $i => $item) {
      if ($i >= $start) {
        $item_attributes[$i] = ' class="more"';
      }
    }
  }


?>
<ul class="<?php print $classes; ?>"<?php print $attributes; ?>>

  <?php foreach ($items as $delta => $item): ?>
    <li<?php print $item_attributes[$delta]; ?>>
      <?php print render($item); ?>
    </li>
  <?php endforeach; ?>
</ul>
