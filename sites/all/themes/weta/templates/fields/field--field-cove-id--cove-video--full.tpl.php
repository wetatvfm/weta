<?php
/**
 * @file field--field-cove-id--cove-video.tpl.php
 * Display COVE viral player
 *
 */
?>

<?php foreach ($items as $delta => $item): ?>
<div class="<?php print $classes; ?> cove_wrapper"<?php print $attributes; ?>>

<?php
$cove_id = strip_tags(render($item));
?>

<iframe width="512" height="376" src="http://watch.weta.org/viralplayer/<?php print $cove_id; ?>" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" seamless></iframe>

</div>
<?php
endforeach;
?>