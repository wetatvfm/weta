<?php
/**
 * @file field--fences-div.tpl.php
 * Wrap each field value in the <div> element.
 *
 * @see http://developers.whatwg.org/grouping-content.html#the-div-element
 */
?>
<?php
// Fences overreaches, so strip divs from title fields
if ($element['#field_name'] == 'title') {
foreach ($items as $delta => $item):
print render($item);
endforeach;
} else {

if ($element['#label_display'] == 'inline'): ?>
<span class="field-label"<?php print $title_attributes; ?>>
<?php print $label; ?>:
</span>
<?php elseif ($element['#label_display'] == 'above'): ?>
<h3 class="field-label"<?php print $title_attributes; ?>>
<?php print $label; ?>
</h3>
<?php endif; ?>

<?php foreach ($items as $delta => $item): ?>
<?php
// add div only for highlight 20/80 image style
if (isset($item['#image_style']) && $item['#image_style'] == 'highlight_20-80') {
?>
<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
<?php
}
?>


<?php print render($item); ?>

<?php
// close div only for highlight 20/80 image style
if (isset($item['#image_style']) && $item['#image_style'] == 'highlight_20-80') {
?>
</div>
<?php
} // endif
endforeach;
}
?>