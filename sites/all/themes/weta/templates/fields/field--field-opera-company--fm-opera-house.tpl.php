<?php
/**
 * @file field--fences-h4.tpl.php
 * Wrap each field value in the <h4> element.
 *
 * @see http://developers.whatwg.org/sections.html#the-h1,-h2,-h3,-h4,-h5,-and-h6-elements
 */
?>

<?php
$header_label = 'h2';
if ($element['#view_mode'] == 'teaser') {
  $header_label = 'h3';
}

?>

<?php if ($element['#label_display'] == 'above'): ?>
  <h3 class="field-label"<?php print $title_attributes; ?>>
    <?php print $label; ?>
  </h3>
<?php endif; ?>

<?php foreach ($items as $delta => $item): ?>
  <<?php print $header_label; ?> class="<?php print $classes; ?>"<?php print $attributes; ?>>

  <?php if ($element['#label_display'] == 'inline'): ?>
  <span class="field-label"<?php print $title_attributes; ?>>
    <?php print $label; ?>:
  </span>
  <?php endif; ?>
    <?php print render($item); ?>
  </<?php print $header_label; ?>>
<?php endforeach; ?>
