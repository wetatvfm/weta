<?php
/**
 * @file field--fences-no-wrapper.tpl.php
 * Render each field value with no wrapper element.
 *
 * Switched inline label wrapper to h4
 */
?>
<?php if ($element['#label_display'] == 'inline'): ?>
  <h4 class="field-label inline">
    <?php print $label; ?>:
  </h4>
<?php elseif ($element['#label_display'] == 'above'): ?>
  <h3 class="field-label"<?php print $title_attributes; ?>>
    <?php print $label; ?>
  </h3>
<?php endif; ?>

<?php foreach ($items as $delta => $item): ?>
  <?php print render($item); ?>
<?php endforeach; ?>
