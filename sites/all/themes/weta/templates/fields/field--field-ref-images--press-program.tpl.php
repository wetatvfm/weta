<?php
/**
 * @file field--fences-ul.tpl.php
 * Wrap each field value in the <li> element and all of them in the <ul> element.
 *
 * Switch label wrapper to h2
 * Add divider
 *
 * @see http://developers.whatwg.org/grouping-content.html#the-ul-element
 */
?>
<div class="divider2"></div>
<?php if ($element['#label_display'] == 'inline'): ?>
  <span class="field-label"<?php print $title_attributes; ?>>
    <?php print $label; ?>:
  </span>
<?php elseif ($element['#label_display'] == 'above'): ?>
  <h2 class="field-label"<?php print $title_attributes; ?>>
    <?php print $label; ?>
  </h2>
<?php endif; ?>

<table class="images_table <?php print $classes; ?>"<?php print $attributes; ?>>

  <?php foreach ($items as $delta => $item): ?>
    <?php
    $zebra = 'even';
    $eo_check = $delta % 2;
    if ($eo_check == 1) {
      $zebra = 'odd';
    }

    ?>
    <tr class="<?php print $zebra; ?>">
      <?php print render($item); ?>
    </tr>
  <?php endforeach; ?>

</table>
