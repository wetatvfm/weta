<?php
/**
 * @file field--fences-div.tpl.php
 * Wrap each field value in the <div> element.
 *
 * @see http://developers.whatwg.org/grouping-content.html#the-div-element
 *
 * Added variable $label_header to control label header
 */
?>


<?php
// Add Flexslider library
drupal_add_library('flexslider', 'flexslider');
?>
<div class="hero slideshow">
  <div class="flexslider">
    <div class="slides">
      <?php foreach ($items as $delta => $item): ?>

      <div class="<?php print $classes; ?> slide"<?php print $attributes; ?>>
        <div class="span_12 col">
          <div class="slide-wrap">
            <?php print render($item); ?>
            <?php if (isset($item['#item']['title'])):?>
              <div class="flex-caption">
                <div class="caption-main">
                  <div class="caption-frame">
                    <div class="caption-frame-inner">
                      <p><?php print $item['#item']['title']; ?></p>
                    </div>
                  </div>
                </div>
              </div>
            <?php endif; ?>
          </div>
        </div>
      </div>
      <?php endforeach;?>
    </div>
  </div>
</div>
