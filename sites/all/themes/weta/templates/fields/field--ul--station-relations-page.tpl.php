<?php
/**
 * @file field--fences-ul.tpl.php
 * Wrap each field value in the <li> element and all of them in the <ul> element.
 *
 * Make label wrapper dynamic
 * Add dynamic divider
 *
 * @see http://developers.whatwg.org/grouping-content.html#the-ul-element
 */
?>
<?php
  $label_header = 'h2';
  if (isset($element['#label_header'])):
    $label_header = $element['#label_header'];
  endif;
  $divider = '<div class="divider2"></div>';
  if (isset($element['#divider'])):
    $divider = $element['#divider'];
  endif;
?>

<?php print $divider; ?>

<?php if ($element['#label_display'] == 'inline'): ?>
  <span class="field-label"<?php print $title_attributes; ?>>
    <?php print $label; ?>:
  </span>
<?php elseif ($element['#label_display'] == 'above'): ?>
  <<?php print $label_header; ?> class="field-label"<?php print $title_attributes; ?>>
    <?php print $label; ?>
  </<?php print $label_header; ?>>
<?php endif; ?>

<ul class="<?php print $classes; ?>"<?php print $attributes; ?>>

  <?php foreach ($items as $delta => $item): ?>
    <li<?php print $item_attributes[$delta]; ?>>
      <?php print render($item); ?>
    </li>
  <?php endforeach; ?>

</ul>
