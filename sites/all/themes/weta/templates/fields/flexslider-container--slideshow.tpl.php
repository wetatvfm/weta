<?php
/**
 * @file
 * Default output for a Flex Slider node.
*/
?>

  <div class="flexslider photo-gallery" id="flexslider-<?php print $id; ?>">
    <div class="slides">
    <?php
    $total = count($items);
    $thisitem = 1;
    ?>
    <?php foreach($items as $item) : ?>
      <div class="slide"><?php print render($item); ?>
        <?php if(!empty($item['#item']['title']) || !empty($item['#item']['alt'])) : ?>
         <p class="flex-caption"><strong>Photo <?php print $thisitem; ?> of <?php print $total; ?></strong>&nbsp;//&nbsp;<?php print $item['#item']['title']; ?></p>
        <?php endif; ?>
      </div>
      <?php $thisitem++; ?>
    <?php endforeach; ?>
    </div>
  </div>
