<?php
/**
 * @file
 * Template to render the weta streaming audio player page.
 *
 * removed now_playing.js for performance reasons
 */
?>
<?php
$base_url = $GLOBALS['base_url'] . base_path();
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
    <title><?php print $head_title; ?></title>

<?php
// targeting script for Washington Post ad -- June 2014.
?>
<script type="text/javascript" src="http://js.washingtonpost.com/wp-adv/units/wp-plus/062014/WETA_Retargeting/tag.js"></script>
<script type="text/javascript" src="<?php print $base_url; ?>sites/all/libraries/jplayer/jquery.jplayer.min.js"></script>
<script type="text/javascript" src="<?php print $base_url; ?>sites/all/libraries/jplayer/add-on/jplayer.playlist.min.js"></script>
<script type="text/javascript" src="<?php print $base_url; ?>sites/all/modules/weta_custom/weta_player2/js/load_player.js"></script>
<script type="text/javascript" src="<?php print $base_url; ?>sites/all/modules/weta_custom/weta_player2/js/now_playing.js"></script>

<script type="text/javascript">
<!--//--><![CDATA[//><!--

(function(){window.ss=function(){};})();var viewReq = new Array();function vu(u) {var i=new Image();i.src=u.replace("&amp;","&");viewReq.push(i);}(function(){var c=function(a,e,h){var b=document;b.addEventListener?b.addEventListener(a,e,h||!1):b.attachEvent&&b.attachEvent("on"+a,e)};var d,f=!1,g=!1;c("mousedown",function(){f=!0});c("keydown",function(){g=!0});document.addEventListener&&c("click",function(a){d=a},!0);window.accbk=function(){var a=d?d:window.event;return a?f||g?!1:(a.preventDefault?a.preventDefault():a.returnValue=!1,!0):!1};})();function st(id) {var a = document.getElementById(id);if (a) {}}function ha(a,x){  if (accbk()) return;}function ca(a) {window.open(document.getElementById(a).href);}function ia(a,e,x) {if (accbk()) return;}function ga(o,e,x) {if (document.getElementById) {var a=o.id.substring(1),p="",r="",g=e.target,t,f,h;if (g) {t=g.id;f=g.parentNode;if (f) {p=f.id;h=f.parentNode;if (h)r=h.id;}} else {h=e.srcElement;f=h.parentNode;if (f)p=f.id;t=h.id;}if (t==a||p==a||r==a)return true;ia(a,e,x);window.open(document.getElementById(a).href);}}

//--><!]]>
</script>
<?php print variable_get('preroll'); ?>
<style type="text/css" media="all">@import url("<?php print $base_url; ?>sites/all/modules/weta_custom/weta_player2/css/reset.css?mw9gfy");
@import url("<?php print $base_url; ?>sites/all/modules/weta_custom/weta_player2/css/player_styles.css?mw9gfy");</style>
<link media="all" href="<?php print $base_url; ?>sites/all/themes/weta/templates/css/fonts.css" rel="stylesheet" type="text/css"><link media="all" href="<?php print $base_url; ?>sites/all/themes/weta/templates/css/gridpak.css" rel="stylesheet" type="text/css"><link media="all" href="<?php print $base_url; ?>sites/all/themes/weta/templates/css/weta-player.css" rel="stylesheet" type="text/css"><link media="all" href="<?php print $base_url; ?>sites/all/themes/weta/templates/css/player.css" rel="stylesheet" type="text/css">
<?php
$args = arg();
$icon = 'classical_weta_homescreen_icon.png';
$sicon = 'classical_weta_shortcut_icon.png';
if (isset($args[1]) && $args[1] == 'viva-la-voce') {
  $icon = 'vlv_homescreen_icon.png';
  $sicon = 'vlv_homescreen_icon.png';
}
?>

<link rel="apple-touch-icon" href="<?php print $base_url; ?>sites/all/themes/weta/templates/images/<?php print $icon; ?>" />
<link rel="shortcut icon" sizes="196x196" href="<?php print $base_url; ?>sites/all/themes/weta/templates/images/<?php print $sicon; ?>">
  </head>
  <body class="player">

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-P633L6"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P633L6');</script>
<!-- End Google Tag Manager -->

<?php print $page; ?>



  </body>
</html>
