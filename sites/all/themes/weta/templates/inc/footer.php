<footer>

<div class="container">
<div class="span_12 col show-mobile">
<?php print render($page['mobile_subnav']); ?>
</div>


<div class="span_5 col">
<?php print render($page['footerleft']); ?>
</div>


<div class="span_2 col">
<img src="<?php print base_path() . path_to_theme(); ?>/templates/images/weta_footer_logo.jpg" alt="" />
<?php print render($page['footercenter']); ?>
</div>

<div class="span_5 col">

<div class="foot_col">
<?php print render($page['footernavleft']); ?>
</div>

<div class="foot_col">
<?php print render($page['footernavcenter']); ?>
</div>

<div class="foot_col">
<?php print render($page['footernavright']); ?>
</div>

<div class="bottom_logos">
<a href="https://pbs.org/"><img src="<?php print base_path() . path_to_theme(); ?>/templates/images/PBS_wblue_pms293_72x24-V2.png" alt="" /></a>
<a href="https://npr.org/"><img src="<?php print base_path() . path_to_theme(); ?>/templates/images/npr_logo.jpg" alt="" /></a>
<?php print render($page['footerbelownav']); ?>
</div>
</div>

<div class="span_12 col foot_row mtop_20">
<ul id="social_foot">
  <li><a href="https://facebook.com/wetatvfm"><img src="<?php print base_path() . path_to_theme(); ?>/templates/images/fb_foot.jpg" alt="Facebook" /></a></li>
  <li><a href="https://twitter.com/wetatvfm"><img src="<?php print base_path() . path_to_theme(); ?>/templates/images/twitter_foot.png" alt="Twitter" /></a></li>
  <li><a href="https://pinterest.com/wetatvfm/"><img src="<?php print base_path() . path_to_theme(); ?>/templates/images/pinterest_foot.png" alt="Pinterest" width="48" /></a></li>
  <!--<li><a href="/rss.xml"><img src="<?php //print base_path() . path_to_theme(); ?>/templates/images/rss_foot.jpg" alt="" /></a></li>-->
</ul>
<?php print render($page['footersocial']); ?>
</div>

<div class="span_12 col foot_row">
<?php print render($page['footerbottom']); ?>
</div>

</div>
</footer>
</div>
