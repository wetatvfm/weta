<header>
<div class="container">


<div class="span_2 col">
<?php if ($logo): ?>
<a href="<?php print base_path(); ?>"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>"  id="logo" /></a>
<?php endif; ?>
</div>


<div class="span_2 col">
<img src="<?php print base_path() . path_to_theme(); ?>/templates/images/tagline.png" alt="" class="tagline fltlt" />

</div>


<div id="logo_mobile">
<a href="<?php print base_path(); ?>"><img src="<?php print base_path() . path_to_theme(); ?>/templates/images/logo.jpg" alt=""  /></a>
</div>
<div class="span_5 col">
<div id="head_top">
  <!--<a href="/rss.xml" class="fltrt social rss"></a>-->
  <a href="http://pinterest.com/wetatvfm/" class="fltrt social pin"></a>
  <a href="http://twitter.com/wetatvfm" class="fltrt social twit"></a>
  <a href="http://www.facebook.com/wetatvfm" class="fltrt social fb"></a>
  <?php
    // listen live button
    $popuplink = '/listen-live';
    $popup = weta_views_listenlive_popup($popuplink);
    $button = '<div id="listen_live"><span class="smaller">Classical WETA</span><br />LISTEN LIVE</div>';
    print l($button, $popuplink, array('attributes' => array('onclick' => $popup, 'class' => array('fltrt listen_live_btn')), 'html' => TRUE));

  ?>
  <a href="/donate" id="donate-btn" class="show-mobile">DONATE</a>
</div>
</div>
<div id="mobile_nav_wrapper">
  <div class="menu_toggle"><img src="<?php print base_path() . path_to_theme(); ?>/templates/images/now_playing.png" alt=""> menu</div>
</div>
<?php print render($page['top']); ?>


</div>

<div class="container">
<?php print render($page['mobile_nav']); ?>
</div>


<div class="container" id="desktop_nav">

<div class="span_1 col">
&nbsp;
</div>


<div class="span_11 col">


<nav>

<?php print render($page['nav']); ?>

</nav>
</div>

<div class="span_12 col" id="flyout_anchor">

</div>

<div class="span_12 col">
<?php print render($page['subnav']); ?>
</div>

</div>

<div class="container">
<div class="show-mobile span_12 col">
<?php print render($page['mobile_np']); ?>
</div>
</div>


</header>
