jQuery(document).ready(function() {
	/*$(function() {
		$( "#datepicker" ).datepicker({
			showOn: "button",
			buttonImage: "images/calendar.gif",
			buttonImageOnly: true
		});
	});
	*/



	jQuery(".show").fancybox({
		maxWidth	: 550,
		maxHeight	: 600,
		fitToView	: false,
		width		: '90%',
		height		: '380px',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});

  jQuery(".show-4x3").fancybox({
		maxWidth	: 550,
		maxHeight	: 600,
		fitToView	: false,
		width		: '90%',
		height		: '440px',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});

});

