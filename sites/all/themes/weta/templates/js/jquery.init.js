(function ($) {
$(document).ready(function(){

	// tabs init
	$(function() {
		$( ".tabs" ).tabs();
	});

	$('#mobile_nav').accordion({
			header: '.mobile_menu',
			active: false,
			alwaysOpen: false,
			animated: true,
			autoheight: false
		});

	$('#mobile_nav').hide();

	$('.menu_toggle').attr('title','show');

	$('.menu_toggle').click(function(){
		if($(this).attr('title')==='show'){
		$('#mobile_nav').slideDown('slow');
		$(this).attr('title','hide');
		}
		else {
		$('#mobile_nav').slideUp('slow');
		$(this).attr('title','show');
		}
	});



	//scrollpane parts
	var scrollPane = $( ".scroll-pane" ),
			scrollContent = $( ".scroll-content" );

	//build slider
	var scrollbar = $( ".scroll-bar" ).slider({
			slide: function( event, ui ) {
				if ( scrollContent.width() > scrollPane.width() ) {
					scrollContent.css( "margin-left", Math.round(
						ui.value / 100 * ( scrollPane.width() - scrollContent.width() )
					) + "px" );
				} else {
					scrollContent.css( "margin-left", 0 );
				}
			}
	});



});
}(jQuery));