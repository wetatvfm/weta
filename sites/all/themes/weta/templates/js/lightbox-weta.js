jQuery(document).ready(function() {

  if ($.cookie('weta_lightbox') == null) {
    $(".show-lb").fancybox({
      openEffect    : 'fade',
      closeEffect   : 'fade',
      openSpeed     : 500,
      closeSpeed    : 250,
      maxWidth      : 850,
      maxHeight     : 650,
      width         : '100%',
      height        : '100%',
      closeClick    : false,
      closeBtn      : true,
      helpers: {
        overlay: {
          locked : true,
          closeClick: true,
          css: {
            'background' : 'rgba(45, 45, 45, .8)'
          }
        }
      }
    });

    $(".show-lb").eq(0).trigger('click');

    // set cookie for whole site that expires in 1 day
    $.cookie('weta_lightbox', '1', {expires:1, path:'/'});
  }
});

