(function ($) {
$(document).ready(function() {

  // generic magnific lightbox
  if ($('.show-lb').length) {
    if ($.cookie('weta_lightbox') === null) {
      $('.show-lb').magnificPopup({
        type: 'inline',
        // other options
        verticalFit: true,
        disableOn: 490,
        closeOnBgClick: true
      });

      $(".show-lb").eq(0).trigger('click');

      // set cookie for whole site that expires in 1 day
      $.cookie('weta_lightbox', '1', {expires:1, path:'/'});
    }
  }

  function openResponsivePopup() {
    $('.show-rlb').magnificPopup({
            type: 'iframe',
            verticalFit: true,
            closeOnBgClick: true,
            showCloseBtn: true,
            disableOn: 779,
            mainClass: 'responsive-lightbox',

            iframe: {
              markup: '<div class="mfp-iframe-scaler">'+
                    '<div class="mfp-close"></div>'+
                    '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
                    '</div>', // HTML markup of popup, `mfp-close` will be replaced by the close button
            },

            // Add event to Google Tag Manager when lightbox opens.
            callbacks: {
              open: function() {
                var lightbox_id = $('.show-rlb').attr('data-mfp-src');
                dataLayer.push({
                  'event':'LightboxDisplayed',
                  'lightboxId': lightbox_id
                });
              },
              close: function() {
                var lightbox_id = $('#rlb').attr('data-nid');
                dataLayer.push({
                  'event':'LightboxClosed',
                  'lightboxId': lightbox_id
                });
              }
            }

          }).magnificPopup('open');

  }

    function openResponsivePopupInline() {
    $.magnificPopup.open({
            items: {
              src: '#rlb',
            },
            type: 'inline',
            verticalFit: true,
            closeOnBgClick: true,
            showCloseBtn: true,
            disableOn: 779,
            mainClass: 'responsive-lightbox',

            // Add event to Google Tag Manager when lightbox opens.

            callbacks: {
              open: function() {
                var lightbox_id = $('#rlb').attr('data-nid');
                dataLayer.push({
                  'event':'LightboxDisplayed',
                  'lightboxId': lightbox_id
                });
              },
              close: function() {
                var lightbox_id = $('#rlb').attr('data-nid');
                dataLayer.push({
                  'event':'LightboxClosed',
                  'lightboxId': lightbox_id
                });
              }
            }

          });

  }

  function openResponsivePopupInlineSimple(lightbox_id) {
    $.magnificPopup.open({
      items: {
        src: '#'+lightbox_id,
      },
      type: 'inline',
      verticalFit: true,
      closeOnBgClick: true,
      showCloseBtn: true,
      disableOn: 779,
      mainClass: 'responsive-lightbox',

      // Add event to Google Tag Manager when lightbox opens.

      callbacks: {
        open: function() {
          dataLayer.push({
            'event':'LightboxDisplayed',
            'lightboxId': lightbox_id
          });
        },
        close: function() {
          dataLayer.push({
            'event':'LightboxClosed',
            'lightboxId': lightbox_id
          });
        }
      }

    });

  }

  // responsive lightbox
  if ($('.show-rlb').length) {
    if ($.cookie('weta_lightbox') == null || $('.show-rlb').hasClass('preview')) {

      setTimeout(openResponsivePopup, 5000);

      // set cookie for whole site that expires in 18 hours, but not if preview.
      // 18 hours is 18 * 60 * 60 * 1000 miliseconds.
      // Add that to the current date to specify an expiration date 18 hours in the future.
      if (!$('.show-rlb').hasClass('preview')) {
        var date = new Date();
        var hours = 18;
        date.setTime(date.getTime() + (hours * 60 * 60 * 1000));
        $.cookie('weta_lightbox', '1', {expires:date, path:'/'});
      }
    }
  }


  // responsive lightbox (inline)
  if ($('.rlb-lightbox #rlb').length) {
    var viewportWidth = window.innerWidth;
    if (($.cookie('weta_lightbox') == null || $('.rlb-lightbox').hasClass('preview')) && viewportWidth > 779) {

      setTimeout(openResponsivePopupInline, 5000);

      // set cookie for whole site that expires in 18 hours, but not if preview.
      // 18 hours is 18 * 60 * 60 * 1000 miliseconds.
      // Add that to the current date to specify an expiration date 18 hours in the future.
      if (!$('.rlb-lightbox').hasClass('preview')) {
        var date = new Date();
        var hours = 18;
        date.setTime(date.getTime() + (hours * 60 * 60 * 1000));
        $.cookie('weta_lightbox', '1', {expires:date, path:'/'});
      }
    }
  }

  // simple responsive lightbox (inline)
  if ($('#rlb-simple').length) {
    // only run the lightbox script if the cookie isn't set and the
    // screen is wide enough.
    var viewportWidth = window.innerWidth;
    if ($.cookie('weta_lightbox') == null && viewportWidth > 779) {
      // Get the current time.
      var today = new Date();
      var now = today.getTime();

      // Get the start and end dates for each lightbox.
      ids = $('#rlb-simple > div').map(function() {
        var startdate = new Date($(this).attr('data-start'));
        var start = startdate.getTime();

        var enddate = new Date($(this).attr('data-end'));
        var end = enddate.getTime();

        // If we're in the window, we want to display this lightbox.
        if (now >= start && now <= end) {
          return this.id;
        }
      }).get();

      // There may be more than one lightbox in the window. Pick
      // one at random.
      if (ids) {
        lightbox_id = ids[Math.floor(Math.random() * ids.length)];
      }

      // If we've picked a lightbox, then display it and set a cookie.
      if (lightbox_id) {
        setTimeout(openResponsivePopupInlineSimple(lightbox_id), 5000);

        // set cookie for whole site that expires in 18 hours.
        // 18 hours is 18 * 60 * 60 * 1000 miliseconds.
        // Add that to the current date to specify an expiration date 18 hours in the future.
        var hours = 18;
        today.setTime(now + (hours * 60 * 60 * 1000));
        $.cookie('weta_lightbox', '1', {expires:today, path:'/'});

      }

    }
  }

  // responsive lightbox click-to-launch preview
  if ($('.preview-rlb').length) {
    $('.preview-rlb').magnificPopup({
      /* type: 'iframe', */
      type: 'inline',
      // other options
      verticalFit: true,
      closeOnBgClick: true,
      showCloseBtn: true,
      disableOn: 779,
      mainClass: 'responsive-lightbox',
      /*
      iframe: {
        markup: '<div class="mfp-iframe-scaler">'+
              '<div class="mfp-close"></div>'+
              '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
              '</div>', // HTML markup of popup, `mfp-close` will be replaced by the close button
      },
      */
    });
  }


  // Basic video previews
  $('.video_preview').magnificPopup({
    type: 'iframe',
    // other options
    verticalFit: true,
    disableOn: 490,
    closeOnBgClick: true,
    showCloseBtn: true
  });

  // COVE video previews
  $('.cove_preview').magnificPopup({
    type: 'iframe',
    // other options

    verticalFit: false,
    disableOn: 530,
    closeOnBgClick: true,
    showCloseBtn: true,

    iframe: {
      markup: '<div class="cove_popup">'+
            '<div class="mfp-iframe-scaler">'+
            '<div class="mfp-close"></div>'+
            '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
            '</div>'+
            '</div>', // HTML markup of popup, `mfp-close` will be replaced by the close button
      patterns: {
        cove: {
          index: 'watch.weta.org',
          id: '/video/',
          src: 'http://watch.weta.org/viralplayer/%id%'
        },
        pbs: {
          index: 'www.pbs.org',
          id: '/video/',
          src: 'http://player.pbs.org/viralplayer/%id%'
        }
      }
    }
  });

  // COVE TV Schedule previews via COVE API
  $('.open_cove_promo').magnificPopup({
    type:'inline',
    // other options
    verticalFit: true,
    closeOnBgClick: true,
    closeBtnInside: true,
    midClick: true // Allow opening popup on middle mouse click.
  });

  // Full COVE API integration
  /* launch popup player even if lazy loaded */
  Drupal.behaviors.LaunchCOVEPlayer = {
    attach: function (context, settings) {
      $('.cove_player').magnificPopup({
        type: 'ajax',
        // other options
        verticalFit: true,
        closeOnBgClick: true,
        closeBtnInside: true,
        midClick: true, // Allow opening popup on middle mouse click.
        ajax: {
          settings: null,
          cursor: 'mfp-ajax-cur',
          tError: '<a href="%url%">The video</a> could not be loaded.'
        }
      });

      $('.mm_player').magnificPopup({
        type: 'ajax',
        // other options
        verticalFit: true,
        closeOnBgClick: true,
        closeBtnInside: true,
        midClick: true, // Allow opening popup on middle mouse click.
        ajax: {
          settings: null,
          cursor: 'mfp-ajax-cur',
          tError: '<a href="%url%">The video</a> could not be loaded.'
        }
      });

    }
  };

  $('.cove_player').magnificPopup({
    type: 'ajax',
    // other options
    verticalFit: true,
    closeOnBgClick: true,
    closeBtnInside: true,
    midClick: true, // Allow opening popup on middle mouse click.
    ajax: {
      settings: null,
      cursor: 'mfp-ajax-cur',
      tError: '<a href="%url%">The video</a> could not be loaded.'
    }
  });


  $('.mm_player').magnificPopup({
    type: 'ajax',
    // other options
    verticalFit: true,
    closeOnBgClick: true,
    closeBtnInside: true,
    midClick: true, // Allow opening popup on middle mouse click.
    ajax: {
      settings: null,
      cursor: 'mfp-ajax-cur',
      tError: '<a href="%url%">The video</a> could not be loaded.'
    }
  });

  $('.image-popup').magnificPopup({
    type: 'image',
    closeOnContentClick: true,
    closeBtnInside: false,
    fixedContentPos: true,
    image: {
      verticalFit: true
    },
  });

  // Program details for schedule grid
  $('.sched_detail').magnificPopup({
    type: 'inline',
    // other options
    verticalFit: true,
    closeOnBgClick: true,
    showCloseBtn: true
  });

  // CD Not Available
  $('.cd_na_popup').magnificPopup({
    items: {
      src: '<div class="cd_na">'+
      '<div class="mfp-close">X CLOSE&nbsp;</div>'+
      '<div class="content"><p>Thanks to our partnership with <a href="http://arkivmusic.com/">Arkiv music</a>, it is easy for listeners to find and purchase most of the music that they hear on Classical WETA and VivaLaVoce. However, some pieces are simply not available on CD, due to rights restrictions or because they are unique, live recordings.</p></div>'+
      '</div>',
      type: 'inline'
    },
    // other options
    verticalFit: true,
    closeOnBgClick: true,
    showCloseBtn: true
  });

  // JW popup for Video Upload content
  $('.jw_popup').magnificPopup({
    type: 'ajax',
    closeOnBgClick: false,
    showCloseBtn: true,
    mainClass: 'weta_jw',
    disableOn: 400,
    closeMarkup: '<div class="mfp-close">X CLOSE&nbsp;</div>'
  });
});
}(jQuery));
