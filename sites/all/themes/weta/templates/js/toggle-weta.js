(function ($) {
  $(document).ready(function() {
    $('label.option').click(function(){
      $(this).children('div.node-bracket-team').addClass('input-checked');
      $(this).children('div.node-bracket-team').children('div').children('div.thisvote').text('Selected');
      $(this).parent('.form-item-submitted-options').siblings('.form-item-submitted-options').children('label').children('div.node-bracket-team').removeClass('input-checked');
      $(this).parent('.form-item-submitted-options').siblings('.form-item-submitted-options').children('label').children('div.node-bracket-team').children('div').children('div.thisvote').text('Select');
    });

    /* get all the bracket webforms */
    var forms = document.querySelectorAll('div#node-bracket-match-full-group-webform');

    for (var f = 0; f < forms.length; f++) {

      /* get all the <p> tags within the form */
      var choices = forms[f].querySelectorAll('.node-bracket-match .form-item-submitted-options p');

      /* initialize an array to hold the heights of those <p> tags */
      var heights = [];

      /* iterate through the <p> tags and get the heights */
      for (var i = 0; i < choices.length; i++) {
        heights[i] = $(choices[i]).height();
      }

      /* sort the array in descending order to get the largest height */
      heights.sort(function(a, b){return b-a});

      /* set the height for each <p> tag to the largest height */
      for (var i = 0; i < choices.length; i++) {
        $(choices[i]).css("height", heights[0]);
      }

      /* after voting, scroll to the top so you can see the chart */
      $(forms[f].querySelector('.webform-submit')).click(function () {
        scrollToElement('#node-bracket-match-full-group-webform', 250);
      });
    }

    /* scroll up or down to an element */
    function scrollToElement(selector, time, verticalOffset) {
      time = typeof(time) != 'undefined' ? time : 1000;
      verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
      element = $(selector);
      offset = element.offset();
      offsetTop = offset.top + verticalOffset;
      $('html, body').animate({
          scrollTop: offsetTop
      }, time);
    }
  })
})(jQuery);
