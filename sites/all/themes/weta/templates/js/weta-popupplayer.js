/**
* @file
* Provides custom JS functions
*/
/**
* Handle all custom control of the player
*/
(function ($) {
$(document).ready(function () {
// Handle the popout links
$('a.popout-player').click(function() {
// Get the playlist from the clicked player
// Get the url to open
var url = $(this).attr("play");
var windowSizeArray = [
"width=650,height=200",
"scrollbars=no,menubar=no,height=200,width=650,resizable=no,toolbar=no,location=no,status=no"
];
window.open(url, 'popUpPlayer', windowSizeArray);
return false;
});
});
})(jQuery);
;