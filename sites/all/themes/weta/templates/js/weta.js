(function ($) {
$(document).ready(function() {

  MenuMiniPanels.setCallback('onShow', function(qTip, event, content) {
    if($('#mini-panel-schedule_flyout')) {
      var grid_w = $('.qtip').width();
      grid_w = grid_w -158;
      $('.full-width-row').width(grid_w);
    }
  });

 	$('#nowplay').click(
 		function(e){
 		e.preventDefault();
 		//$('#np_flyout').slideDown();
 		$('#np_flyout').toggle();
 		}
 	);

 	function equalHeight(group) {
  	var tallest = 0;
  	group.each(function() {
  		var thisHeight = $(this).height();
  		if(thisHeight > tallest) {
  			tallest = thisHeight;
  		}
  	});
	 group.height(tallest);
	}


 	if($('.tabs')){
 	  $('.tabs ul').each(function(){
 		  equalHeight($(this).find("li a"));
 		});
 	}


 	$('.close_menu').click(function(){
 	  $('.subnav-home').hide();
 	});

  $('.subnav-home').hide();



  $('.flexslider').flexslider({
  	touch: true,
  	selector: ".slides > .slide",
  	/*controlsContainer: ".flexnav",*/
  	animationSpeed: 1000
  	}
  );


  var page_width =  $(window).width();
  if (page_width<479) {
    $( ".grid" ).accordion({
      header: '.show-mobile-schd-head',
    	active: false,
    	collapsible: true,
      autoHeight: false,
      clearStyle: true
    	});
   }

  $(window).resize(function() {
		fix();
	});

	function fix() {
		var slide_height = $('.flex-active-slide img').height();
		var slide_width = $('.flex-active-slide img').width();
		var control_width = 	$('.flex-control-nav').width();
    var hero_slide_height = slide_height - 17
    var half_width = control_width / 2;
    var hero_width = (slide_width / 2) - half_width;
		slide_height = slide_height  + 17;
    if (page_width < 479) {
      hero_slide_height = slide_height + 5;
    }
    else if (page_width < 960) {
      hero_slide_height = slide_height;
    }

		$('.flex-control-nav').css('width',control_width);
		$('.flex-control-nav').css('top', slide_height);
    $('.node-slideshow .flex-control-nav').css('top', slide_height - 8);
		$('.flex-control-nav').css('left', (slide_width - control_width));

    $('.hero .flex-control-nav').css('top', hero_slide_height);
    $('.hero .flex-control-nav').css('left', hero_width);

	}

	function fixonload() {
		var slide_height = $('.flex-active-slide img').height();
		var slide_width = $('.flex-active-slide img').width();
		var control_width = 	$('.flex-control-nav').width();
    var half_width = control_width / 2;

    var hero_slide_height = slide_height - 17;
    var hero_width = slide_width - control_width - 10;

    slide_height = slide_height  + 17;
    if (page_width < 479) {
      hero_width = (slide_width / 2) - half_width;
      hero_slide_height = slide_height - 20;
    }
    else if (page_width < 960) {
      hero_width = (slide_width / 2) - half_width;
      hero_slide_height = slide_height - 20;
    }

		$('.flex-control-nav').css('top', slide_height);
    $('.node-slideshow .flex-control-nav').css('top', slide_height - 8);
		$('.flex-control-nav').css('left', (slide_width - control_width));

    $('.hero .flex-control-nav').css('top', hero_slide_height);
    $('.hero .flex-control-nav').css('left', hero_width);
	}

	setTimeout(function(){fixonload()}, 500);

	$('.mobile-np').click(function(){
		$('.mobile-np-player').toggle();
	});

  /* set all COVE API video listings to the same height */
  Drupal.behaviors.COVEVideoHeights = {
    attach: function (context, settings) {

      var vids = document.querySelectorAll('.pane-weta-coveapi');

      for (var f = 0; f < vids.length; f++) {

        /* get all the divs which hold images */
        var images = vids[f].querySelectorAll('div.cove_listing div.video');

        /* initialize a variable to hold the width */
        var img_width = $(images[0]).width();

        /* caclulate what the height should be */
        var img_height = Math.round((img_width/16)*9);

        var styles = {
          height : img_height,
          overflow: "hidden"
        };

        /* iterate through the divs and set the height */
        for (var i = 0; i < images.length; i++) {
          $(images[i]).css(styles);
        }

        /* get all the <div> tags with the class cove_listing */
        var choices = vids[f].querySelectorAll('div.cove_listing');

        /* initialize an array to hold the heights of those <div> tags */
        var heights = [];


        /* iterate through the <p> tags and get the heights */
        for (var i = 0; i < choices.length; i++) {
          heights[i] = $(choices[i]).height();
        }

        /* sort the array in descending order to get the largest height */
        heights.sort(function(a, b){return b-a});

        /* set the height for each <p> tag to the largest height */
        for (var i = 0; i < choices.length; i++) {
          $(choices[i]).css("height", heights[0]);
        }
      }
    }
  };
});
}(jQuery));
