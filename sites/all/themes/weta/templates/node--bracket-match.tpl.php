<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

<?php
hide($content['comments']);
hide($content['links']);

// pre-loading chart due to ajax conflicts
if (module_exists('webform_chart')) {
  $chart = webform_chart_page($node);
  //print $chart;
}

print render($content);

?>

</div>