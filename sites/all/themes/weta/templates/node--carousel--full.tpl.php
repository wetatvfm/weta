<?php

$hero_view = node_view($node, 'homepage_carousel');
$lp_view = node_view($node, 'landing_carousel');

print '<h2>Homepage Preview</h2>';
print '<div class="hero">';
print drupal_render($hero_view);
print '</div>';

print '<div class="divider2"></div>';

print '<h2>Landing Page Preview</h2>';
print drupal_render($lp_view);

?>
