<?php
hide($content['comments']);
hide($content['links']);
//dpm($content,'content');
//print render($content);

//dpm($content['field_feature_image'], 'image');


//dpm($formatted_image_url, 'formatted image url');
?>


<div class="span_12 col full_width_promo">
<?php if (isset($title_suffix['contextual_links'])): ?>
<?php print render($title_suffix['contextual_links']); ?>
<?php endif; ?>
<div class="span_8 col p20">
<?php print render($content['field_feature_image']); ?>
</div>

<div class="span_4 col p20">
<?php print $title; ?>
<?php print render($content['field_description']); ?>
<?php print render($content['field_primary_link']); ?>
</div>
</div>
