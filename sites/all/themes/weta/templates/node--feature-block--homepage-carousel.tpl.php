<?php
hide($content['comments']);
hide($content['links']);
//dpm($content,'content');
//print render($content);
//dpm($content['field_feature_image'], 'image');


//dpm($formatted_image_url, 'formatted image url');
?>


<div class="slide">
<?php if (isset($title_suffix['contextual_links'])): ?>
<?php print render($title_suffix['contextual_links']); ?>
<?php endif; ?>


<div class="span_12 col">
<div class="slide-wrap">
<?php print render($content['field_feature_image']); ?>

<div class="flex-caption">
  <div class="caption-main">
    <div class="caption-frame">
      <div class="caption-frame-inner">
        <div class="subtitle">
          <?php print render($content['field_subhead']); ?>
        </div>
        <h2><?php print $title; ?></h2>

        <?php print render($content['field_short_description']); ?>
        <div class="hero-links">
          <?php print render($content['field_primary_link']); ?>
          <?php print render($content['field_links']); ?>
        </div>

      </div>
    </div>
  </div>
</div>

</div>
</div>



</div>
