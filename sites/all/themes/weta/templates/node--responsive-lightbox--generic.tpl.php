<?php

// manipulate the fields to get what we need

/**
 * Simplify variables
 */
$background_color = trim(render($content['field_resp_lb_mobile_bg_color']));
$color = trim(render($content['field_resp_lb_headline_color']));
$alignment = trim(render($content['field_resp_lb_headline_alignment']));

$text_color = trim(render($content['field_resp_lb_main_text_color']));
$text_alignment = trim(render($content['field_resp_lb_main_text_align']));

$headline = render($content['field_resp_lb_headline']);
$headline = strip_tags($headline, '<em><br>');

$main_text = render($content['field_resp_lb_main_text']);

$button_alignment = trim(render($content['field_resp_lb_button_alignment']));
if (empty($button_alignment)) {
  $button_alignment = 'center';
}

$footer = '';
if (isset($content['field_resp_lb_footer'][0]['#markup'])) {
  // Only <a> tags are allowed in the footer.
  $content['field_resp_lb_footer'][0]['#markup'] = strip_tags($content['field_resp_lb_footer'][0]['#markup'], '<a>');
  $footer = render($content['field_resp_lb_footer']);
}

$footer_color = trim(render($content['field_resp_lb_footer_color']));

if (empty($footer_color)) {
  $footer_color = 'white';
}


/**
 * Remove width and height from main text images
 */
if (!empty($main_text)) {
  $doc_inline = new DOMDocument();
  $doc_inline->loadHTML($main_text);
  $mt_image_tags = $doc_inline->getElementsByTagName('img');

  foreach($mt_image_tags as $tag) {
    $tag->removeAttribute('width');
    $tag->removeAttribute('height');
  }

  $main_text = $doc_inline->saveHTML();
  $main_text = strip_tags($main_text, '<em><br><img>');
}

/**
 * Button logic and formatting
 */
$lightbox_type = $content['field_resp_lb_lightbox_type']['#items'][0]['value'];
switch ($lightbox_type) {
  case 'form':
    $action = render($content['field_resp_lb_form'][0]);
    break;
  case 'buttons':
    $button_count = count($content['field_resp_lb_buttons']['#items']);

    $buttons = '';
    switch ($button_count) {
      case 3:
        $prefix = '<div class="span_4 col multiple">';
        $suffix = '</div>';

        foreach ($content['field_resp_lb_buttons']['#items'] as $i => $item) {
          $thisbutton = $i + 1;
          $addtext = render($content['field_resp_lb_addtext_button' . $thisbutton]);
          if (!empty($addtext)) {
            $addtext = '<div class="addtext">' . $addtext . '</div>';
          }

          $content['field_resp_lb_buttons'][$i]['#element']['attributes']['class'] = 'button lightbox-button';
          $content['field_resp_lb_buttons'][$i]['#element']['attributes']['target'] = '_top';
          $title_prefix = '<div class="btn-text">';
          $title_suffix = '</div>';
          $title = $content['field_resp_lb_buttons'][$i]['#element']['title'];

          $content['field_resp_lb_buttons'][$i]['#element']['title'] = $title_prefix . $title . $addtext . $title_suffix;

          $content['field_resp_lb_buttons'][$i]['#element']['html'] = TRUE;

          $buttons .= $prefix . render($content['field_resp_lb_buttons'][$i]) . $suffix;
        }
        break;
      case 2:
        $prefix = '<div class="span_6 col multiple">';
        $suffix = '</div>';

        foreach ($content['field_resp_lb_buttons']['#items'] as $i => $item) {
          $thisbutton = $i + 1;
          $addtext = render($content['field_resp_lb_addtext_button' . $thisbutton]);
          if (!empty($addtext)) {
            $addtext = '<div class="addtext">' . $addtext . '</div>';
          }

          $content['field_resp_lb_buttons'][$i]['#element']['attributes']['class'] = 'button lightbox-button';
          $content['field_resp_lb_buttons'][$i]['#element']['attributes']['target'] = '_top';
          $title_prefix = '<div class="btn-text">';
          $title_suffix = '</div>';
          $title = $content['field_resp_lb_buttons'][$i]['#element']['title'];
          $content['field_resp_lb_buttons'][$i]['#element']['title'] = $title_prefix . $title . $addtext . $title_suffix;

          $content['field_resp_lb_buttons'][$i]['#element']['html'] = TRUE;

          $buttons .= $prefix . render($content['field_resp_lb_buttons'][$i]) . $suffix;
        }
        break;
      case 1:
        foreach ($content['field_resp_lb_buttons']['#items'] as $i => $item) {
          $thisbutton = $i + 1;
          $addtext = render($content['field_resp_lb_addtext_button' . $thisbutton]);
          if (!empty($addtext)) {
            $addtext = '<div class="addtext">' . $addtext . '</div>';
          }

          $content['field_resp_lb_buttons'][$i]['#element']['attributes']['class'] = 'button lightbox-button';
          $content['field_resp_lb_buttons'][$i]['#element']['attributes']['target'] = '_top';
          $title_prefix = '<div class="btn-text">';
          $title_suffix = '</div>';
          $title = $content['field_resp_lb_buttons'][$i]['#element']['title'];
          $content['field_resp_lb_buttons'][$i]['#element']['title'] = $title_prefix . $title . $addtext . $title_suffix;
          $content['field_resp_lb_buttons'][$i]['#element']['html'] = TRUE;
          $buttons = render($content['field_resp_lb_buttons']);
        }
        switch ($button_alignment) {
          case 'left':
            $prefix = '<div class="span_6 col multiple">';
            $suffix = '</div>';
            $suffix .= '<div class="span_6 col">&nbsp;</div>';
            $buttons = $prefix . $buttons . $suffix;
            break;
          case 'center':
            $prefix = '<div class="span_3 col">&nbsp;</div>';
            $prefix .= '<div class="span_6 col center multiple">';
            $suffix = '</div>';
            $suffix .= '<div class="span_3 col">&nbsp;</div>';
            $buttons = $prefix . $buttons . $suffix;
            break;
          case 'right':
            $prefix = '<div class="span_6 col">&nbsp;</div>';
            $prefix .= '<div class="span_6 col multiple">';
            $suffix = '</div>';
            $buttons = $prefix . $buttons . $suffix;
            break;
          default:
            break;
        }
        break;
      default:
        $buttons = render($content['field_resp_lb_buttons']);
      }
    $action = $buttons;
    break;
  default:
    $action = '';
}

?>


<div id="rlb" class="mfp-hide" data-nid="<?php print $node->nid; ?>">
  <div id="wrapper" class="container-fluid">
    <div id="topper" class="<?php print $text_color; ?>">
      <!-- Variable content here. -->
      <div class="headline <?php print $color . ' ' . $alignment; ?>">
        <h2><?php print $headline; ?></h2>
        <p class="<?php print $text_alignment; ?>"><?php print $main_text; ?></p>
      </div>
    </div>
    <div id="lower">
      <div id="bottom" class="<?php print $text_color . ' ' . $text_alignment; ?>">
      </div>
      <div id="buttons" class="row">
        <?php print $action; ?>
        <div class="footer-<?php print $footer_color; ?>">
          <?php print $footer; ?>
        </div>
      </div>
    </div>
  </div>
</div>
