<?php
hide($content['comments']);
hide($content['links']);

$preview_link = '<a href="#rlb" class="preview-rlb">Click to preview the lightbox</a>';

print '<h3>Preview</h3>';
//print '<p>' . l('Click to preview the lightbox', $node_url, array('attributes' => array('data-mfp-src' => '/lightbox-preview/' . $node->nid, 'class' => array('preview-rlb')), 'fragment' => 'lightbox')) . '</p>';
print '<p>' . $preview_link . '</p>';
print render($content);

?>
