<?php

// manipulate the fields to get what we need

/**
 * Simplify variables
 */
$background_color = trim(render($content['field_resp_lb_mobile_bg_color']));
$color = trim(render($content['field_resp_lb_headline_color']));
$alignment = trim(render($content['field_resp_lb_headline_alignment']));

$text_color = trim(render($content['field_resp_lb_main_text_color']));
$text_alignment = trim(render($content['field_resp_lb_main_text_align']));

$headline = render($content['field_resp_lb_headline']);
$headline = strip_tags($headline, '<em><br>');

$main_text = render($content['field_resp_lb_main_text']);

$button_alignment = trim(render($content['field_resp_lb_button_alignment']));
if (empty($button_alignment)) {
  $button_alignment = 'center';
}

$footer = '';
if (isset($content['field_resp_lb_footer'][0]['#markup'])) {
  // Only <a> tags are allowed in the footer.
  $content['field_resp_lb_footer'][0]['#markup'] = strip_tags($content['field_resp_lb_footer'][0]['#markup'], '<a>');
  $footer = render($content['field_resp_lb_footer']);
}

$footer_color = trim(render($content['field_resp_lb_footer_color']));

if (empty($footer_color)) {
  $footer_color = 'white';
}


/**
 * Get the image URLs for the desktop CSS background
 */
$doc_desktop = new DOMDocument();
$desktop_background_image = render($content['field_resp_lb_bgimage_desktop']);
$doc_desktop->loadHTML(trim($desktop_background_image));
$image_tags = $doc_desktop->getElementsByTagName('img');

foreach($image_tags as $tag) {
  $desktop_background_url = $tag->getAttribute('src');
}

/**
 * Get the image URLs for the mobile CSS background
 */
$doc_mobile = new DOMDocument();
$mobile_background_image = render($content['field_resp_lb_bgimage_mobile']);
$doc_mobile->loadHTML(trim($mobile_background_image));
$m_image_tags = $doc_mobile->getElementsByTagName('img');

foreach($m_image_tags as $tag) {
  $mobile_background_url = $tag->getAttribute('src');
}


/**
 * Remove width and height from main text images
 */
if (!empty($main_text)) {
  $doc_inline = new DOMDocument();
  $doc_inline->loadHTML($main_text);
  $mt_image_tags = $doc_inline->getElementsByTagName('img');

  foreach($mt_image_tags as $tag) {
    $tag->removeAttribute('width');
    $tag->removeAttribute('height');
  }

  $main_text = $doc_inline->saveHTML();
  $main_text = strip_tags($main_text, '<em><br><img>');
}

/**
 * Button logic and formatting
 */
$lightbox_type = $content['field_resp_lb_lightbox_type']['#items'][0]['value'];
switch ($lightbox_type) {
  case 'form':
    $action = render($content['field_resp_lb_form'][0]);
    break;
  case 'buttons':
    $button_count = count($content['field_resp_lb_buttons']['#items']);

    $buttons = '';
    switch ($button_count) {
      case 3:
        $prefix = '<div class="col-xs-12 col-sm-4 col-sm-offset-0 multiple">';
        $suffix = '</div>';

        foreach ($content['field_resp_lb_buttons']['#items'] as $i => $item) {
          $thisbutton = $i + 1;
          $addtext = render($content['field_resp_lb_addtext_button' . $thisbutton]);
          if (!empty($addtext)) {
            $addtext = '<div class="addtext">' . $addtext . '</div>';
          }

          $content['field_resp_lb_buttons'][$i]['#element']['attributes']['class'] = 'btn btn-danger btn-lg btn-block';
          $content['field_resp_lb_buttons'][$i]['#element']['attributes']['target'] = '_top';
          $title_prefix = '<div class="btn-text">';
          $title_suffix = '</div>';
          $title = $content['field_resp_lb_buttons'][$i]['#element']['title'];

          $content['field_resp_lb_buttons'][$i]['#element']['title'] = $title_prefix . $title . $addtext . $title_suffix;

          $content['field_resp_lb_buttons'][$i]['#element']['html'] = TRUE;

          $buttons .= $prefix . render($content['field_resp_lb_buttons'][$i]) . $suffix;
        }
        break;
      case 2:
        $prefix = '<div class="col-xs-12 col-sm-6 col-sm-offset-0 multiple">';
        $suffix = '</div>';

        foreach ($content['field_resp_lb_buttons']['#items'] as $i => $item) {
          $thisbutton = $i + 1;
          $addtext = render($content['field_resp_lb_addtext_button' . $thisbutton]);
          if (!empty($addtext)) {
            $addtext = '<div class="addtext">' . $addtext . '</div>';
          }

          $content['field_resp_lb_buttons'][$i]['#element']['attributes']['class'] = 'btn btn-danger btn-lg btn-block';
          $content['field_resp_lb_buttons'][$i]['#element']['attributes']['target'] = '_top';
          $title_prefix = '<div class="btn-text">';
          $title_suffix = '</div>';
          $title = $content['field_resp_lb_buttons'][$i]['#element']['title'];
          $content['field_resp_lb_buttons'][$i]['#element']['title'] = $title_prefix . $title . $addtext . $title_suffix;

          $content['field_resp_lb_buttons'][$i]['#element']['html'] = TRUE;

          $buttons .= $prefix . render($content['field_resp_lb_buttons'][$i]) . $suffix;
        }
        break;
      case 1:
        foreach ($content['field_resp_lb_buttons']['#items'] as $i => $item) {
          $thisbutton = $i + 1;
          $addtext = render($content['field_resp_lb_addtext_button' . $thisbutton]);
          if (!empty($addtext)) {
            $addtext = '<div class="addtext">' . $addtext . '</div>';
          }

          $content['field_resp_lb_buttons'][$i]['#element']['attributes']['class'] = 'btn btn-danger btn-lg';
          $content['field_resp_lb_buttons'][$i]['#element']['attributes']['target'] = '_top';
          $content['field_resp_lb_buttons'][$i]['#element']['title'] .= $addtext;
          $content['field_resp_lb_buttons'][$i]['#element']['html'] = TRUE;
          $buttons = render($content['field_resp_lb_buttons']);
        }
        switch ($button_alignment) {
          case 'left':
            $prefix = '<div class="col-xs-12 col-sm-6 col-sm-offset-0 full">';
            $suffix = '</div>';
            $suffix .= '<div class="col-xs-12 col-sm-3 col-sm-offset-0"></div>';
            $suffix .= '<div class="col-xs-12 col-sm-3 col-sm-offset-0"></div>';
            $buttons = $prefix . $buttons . $suffix;
            break;
          case 'center':
            $prefix = '<div class="col-xs-12 col-sm-3 col-sm-offset-0"></div>';
            $prefix .= '<div class="col-xs-12 col-sm-6 col-sm-offset-0 full">';
            $suffix = '</div>';
            $suffix .= '<div class="col-xs-12 col-sm-3 col-sm-offset-0"></div>';
            $buttons = $prefix . $buttons . $suffix;
            break;
          case 'right':
            $prefix = '<div class="col-xs-12 col-sm-3 col-sm-offset-0"></div>';
            $prefix .= '<div class="col-xs-12 col-sm-3 col-sm-offset-0"></div>';
            $prefix .= '<div class="col-xs-12 col-sm-6 col-sm-offset-0 full">';
            $suffix = '</div>';
            $buttons = $prefix . $buttons . $suffix;
            break;
          default:
            break;
        }
        break;
      default:
        $buttons = render($content['field_resp_lb_buttons']);
      }
    $action = $buttons;
    break;
  default:
    $action = '';
}

?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Lightbox</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Crimson+Text' rel='stylesheet' type='text/css'>

    <style>

    /* Fonts */
    body { font-family: 'Montserrat', sans-serif;  }
    #topper h1 { font-family: 'Arvo', serif; }

    /* Backgrounds */
    html { background: #<?php print $background_color; ?>; }

    body {
      padding-bottom: 70px;
      background-color: #<?php print $background_color; ?>;
    }

    #wrapper {
      background-image: url(<?php print "'" . $mobile_background_url . "'"; ?>);
      background-position: top center;
      background-size: 100% auto;
      background-repeat: no-repeat;
    }

    .element-invisible {
      display: none;
    }

    .field-resp-lb-footer a {
      text-decoration: underline;
      font-size: .9em;
    }

    .white .field-resp-lb-footer {
      color: #fff;
    }

    .white .field-resp-lb-footer a {
      color: #fff;
    }

    .black .field-resp-lb-footer {
      color: #000;
    }

    .black .field-resp-lb-footer a {
      color: #000;
    }

    .red .field-resp-lb-footer {
      color: #ad000c;
    }

    .red .field-resp-lb-footer a {
      color: #ad000c;
    }

    .field-resp-lb-footer {
      clear: both;
      font-size: 0.9em;
      margin: auto auto auto 10px;
      text-align: center;
      width: 100%;
    }

    #topper .headline h2 {
      font-family: 'Montserrat', sans-serif;
      color: white;
      /*margin-top: 140px;*/
      text-align: center;
      font-size: 53px;
      font-weight: bold;
    }

    #topper .headline.black h2 {
      color: #000;
    }

    #topper .headline.red h2 {
      color: #ad000c;
    }

    .btn-danger {
      background-color: #ad000c;
      display: -webkit-box;
      display: -webkit-flex;
      display: -moz-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-flex-align: center;
      -ms-flex-align: center;
      -webkit-align-items: center;
      align-items: center;
      -ms-justify-content: center;
      justify-content: center;
      -webkit-flex-flow: column;
      flex-flow: column;
    }

    .btn-danger p {
      max-width: 100%;
      box-sizing: border-box;
      margin: 0;
      text-align: center;
    }


    #buttons {
      margin: 10px;
    }
    #buttons a {
      font-size: 2rem;
      vertical-align: middle;
      text-transform: uppercase;
      display: inline-block;
      white-space: normal;
    }

    #buttons .multiple a {
      background-color: #ad000c;
      box-shadow: 2px 2px 0 0 rgba(0, 0, 0, 0.4);
      font-size: 2rem;
      font-weight: bold;
      height: 60px;
      line-height: 1.1;
      margin-bottom: 2rem;
      padding: 5px;
      white-space: normal;
    }

    .full a {
      width: 100%;
    }

    .btn-text {
      position: relative;
      top: 50%;
      transform: translateY(-50%);
    }

    .btn-text .addtext {
      font-weight: normal;
      font-size: 14px;
    }

    form {
      width: 100%;
      margin: 0 auto;

    }

    .form-item input {
      width: 100%;
      margin: 3px 10px;
      padding: 20px;
    }

    .form-submit {
      background: #ad000c none repeat scroll 0 0;
      border: medium none;
      color: #fff;
      padding: 20px;
      margin: 3px 10px;
      text-transform: uppercase;
      width: 100%;
    }

    #bottom {
      margin: 10px;
    }
    #bottom p, #topper p {
      font-size: 35px;
      font-weight: bold;
      text-align: center;
      margin: 20px auto;
    }

    #bottom.white p, #topper.white p {
      color: white;
    }

    #bottom.black p, #topper.black p {
      color: #000;
    }

    #bottom.red p, #topper.red p {
      color: #ad000c;
    }

    #bottom img {
      max-width: 100%;
    }


    #blip {
      border-top: 4px solid #a18463;
      margin: 20px auto;
      width: 25%;
    }

    /* honeypot */
    .homepage-textfield {
      display: none !important;
    }

    @media (min-width: 768px) {
      body {
        padding-bottom: 0;
      }
      #wrapper {
        background: #999 url(<?php print "'" . $desktop_background_url . "'"; ?>) top center / 100% no-repeat;
        height: 430px;
      }
      #buttons a {
        font-size: 1.9rem;
        margin-left: 10px;
      }

      #buttons .field-resp-lb-footer a {
        font-size: 1em;
        margin-left: auto;
        text-transform: none;
        vertical-align: bottom;
      }

      #topper .headline h2 {
        font-size: 60px;
        margin-top: 30px;
      }

      #topper .headline.full_center h2 {
        width: 100%;
        max-width: 100%;
        margin: 20px auto 0;
        text-align: center;
      }

      #topper .headline.full_left h2 {
        width: 100%;
        max-width: 85%;
        margin-top: 20px;
        text-align: left;
      }

      #topper .headline.half_left h2, #topper p.half_left {
        width: 60%;
        text-align: left;
      }

      #lower {
        position: absolute;
        bottom: 10px;
        width: 95%;
      }

      #bottom img {
        max-width: 50%;
      }
      #bottom {
        /*margin-top: 90px;*/
      }
      #bottom::before {
        display: none;
      }
      #bottom p, #topper p {
        margin: 5px 0;
      }

      #bottom.full_center p, #topper.full_center p {
        text-align: center;
      }

      #bottom.full_left p, #topper .full_left p {
        text-align: left;
      }

      #bottom.half_left p, #topper.half_left p {
        text-align: left;
        max-width: 60%;
      }

      #blip {
        display: none;
      }
      #buttons {
        margin: 0;
      }

      .form-item input {
        width: 20%;
        float: left;
        margin: 15px 10px;
      }

      .form-submit {
        width: auto;
        margin: 15px 10px;
      }
    }

    @media (min-width: 575px) and (max-width: 779px) {
      #topper .headline h2 {
        margin-top: 200px;
      }
      #buttons .field-resp-lb-footer a {
        font-size: 1em;
        margin-left: auto;
        text-transform: none;
        vertical-align: bottom;
      }
    }

    </style>
  </head>
  <body>
    <div id="wrapper" class="container-fluid">
      <div id="topper" class="<?php print $text_color; ?>">
        <!-- Variable content here. -->
        <div class="headline <?php print $color . ' ' . $alignment; ?>">
          <h2><?php print $headline; ?></h2>
          <p class="<?php print $text_alignment; ?>"><?php print $main_text; ?></p>
        </div>
      </div>
      <div id="lower">
        <div id="bottom" class="<?php print $text_color . ' ' . $text_alignment; ?>">
        </div>
        <div id="buttons" class="row">
          <?php print $action; ?>
          <div class="<?php print $footer_color; ?>">
            <?php print $footer; ?>
          </div>
        </div>
      </div>
    </div>

  </body>
</html>
