<?php
hide($content['comments']);
hide($content['links']);
hide($content['field_timeline_type']);
hide($content['field_feature_image']);
hide($content['field_year']);
hide($content['field_date_display']);
hide($content['field_media_manager_video']);
hide($content['field_youtube_video']);


// get the date type
$date_type = trim(render($content['field_date_display']));

// get the date and convert it to a timestamp
$date = strip_tags(render($content['field_year']));
$date = strtotime($date);

// set up $display_date based on the date type
switch($date_type) {
  case 'year':
    $display_date = date('Y', $date);
    break;
  case 'month':
    $display_date = date('F Y', $date);
    break;
  case 'full':
    $display_date = date('F j, Y', $date);
    break;
  default:
    $display_date = date('F j, Y', $date);
}

$image = FALSE;
$this_image = render($content['field_feature_image']);
if (!empty($this_image)) {
  $image = TRUE;
}

// initialize variables to hold additional content
$addclass = NULL;
$prefix = NULL;
$suffix = NULL;

// default display type is inline.
$display_type = 'inline';

// get the timeline type
$timeline_type = trim(render($content['field_timeline_type']));

switch($timeline_type) {
  case 'image':
    // open a larger version of the image in a magnific popup
    $options = array(
      'attributes' => array(
        'class' => array('image-popup'),
        'title' => $content['field_feature_image'][0]['#item']['title'],
      ),
    );
    $content['field_feature_image'][0]['#path']['options'] = $options;
    $suffix = '<p class="note">Click to enlarge.</p>';
    break;
  case 'link':
    // link the image to the link destination
    $link = $content['field_primary_link'][0]['#element']['display_url'];
    $options = array(
      'attributes' => array(
        'target' => '_blank',
      ),
    );
    if ($image) {
      $content['field_feature_image'][0]['#path']['path'] = $link;
      $content['field_feature_image'][0]['#path']['options'] = $options;
    }
    break;
  case 'youtube':
    // open the video in a magnific popup
    $link = $content['field_youtube_video'][0]['#element']['display_url'];
    $content['field_feature_image'][0]['#path']['path'] = $link;
    $options = array(
      'attributes' => array(
        'class' => array('video_preview'),
      ),
    );
    $content['field_feature_image'][0]['#path']['options'] = $options;

    // Change image style to a 16x9 ratio.
    $content['field_feature_image'][0]['#image_style'] = '16x9_feature';

    // add additional content
    $addclass = 'video';
    $prefix = '<span class="screenshot"><span class="play-overlay"></span></span>';

    // override display type
    $display_type = 'video';
    break;
  case 'pbs_video':
    // open the player in a magnific popup
    $slug = $content['field_media_manager_video']['#items'][0]['slug'];
    $content['field_feature_image'][0]['#path']['path'] = 'mmplayer/' . $slug;
    $options = array(
      'attributes' => array(
        'class' => array('mm_player'),
      ),
    );
    $content['field_feature_image'][0]['#path']['options'] = $options;

    // Change image style to a 16x9 ratio.
    $content['field_feature_image'][0]['#image_style'] = '16x9_feature';

    // add additional content
    $addclass = 'video';
    $prefix = '<span class="screenshot"><span class="play-overlay"></span></span>';

    // override display type
    $display_type = 'video';
  break;
  default:
    break;
}

?>

<div class="span_12 col">

  <?php
  switch($display_type) {
    case 'inline':
      if ($image) {
    ?>
      <h3><?php print $display_date; ?></h3>
      <h4><?php print $node->title; ?></h4>
       <div class="timeline-image span_3 col p10 <?php print $addclass; ?>">
        <?php print $prefix; ?>
        <?php print render($content['field_feature_image']); ?>
        <?php print $suffix; ?>
      </div>
      <div class="timeline-content span_9 col">
        <?php print render($content); ?>
      </div>

    <?php
      }
      else {
    ?>
        <h3><?php print $display_date; ?></h3>
        <h4><?php print $node->title; ?></h4>
        <div class="timeline-content">
          <?php print render($content); ?>
        </div>
    <?php
      }
      break;
    case 'video':
    ?>
      <div class="timeline-content">
        <h3><?php print $display_date; ?></h3>
        <h4><?php print $node->title; ?></h4>
        <?php print render($content); ?>
      </div>
       <div class="timeline-image clearfix <?php print $addclass; ?>">
        <?php print $prefix; ?>
        <?php print render($content['field_feature_image']); ?>
        <?php print $suffix; ?>
      </div>
    <?php
    break;
  }
  ?>

</div>
