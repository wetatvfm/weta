<?php
/*
 * Removed extraneous title since title is in date pager
 */
?>

<?php if ($page['top_banner']): ?>
<div class="top_banner">
<?php print render($page['top_banner']); ?>
</div>
<?php endif; ?>

<?php if ($messages): ?>
<div id="messages">
<?php print $messages; ?>
</div> <!-- /.section, /#messages -->
<?php endif; ?>

<?php
$classes = '';
if (isset($wrapclasses)) {
  $classes = implode(' ', $wrapclasses);
}
?>

<div id="wrap" class="<?php print $classes; ?>">
<?php include 'inc/header.php'; ?>
<section>

<?php if (!$is_front && !$is_landing) { ?>
<div class="container content_top">

<div class="<?php if($page['sidebar_first'] || $page['sidebar_second']){print 'span_8';}else{print 'span_12';} ?> col p0-60">


<?php
if ($tabs):
print '<div class="drupal-tabs">';
print render($tabs);
print '</div>';
endif;

?>


<?php if ($action_links): ?>
<ul class="action-links">
<?php print render($action_links); ?>
</ul>
<?php endif; ?>



<?php
print render($page['content']); ?>
</div>

<?php if($page['sidebar_first'] || $page['sidebar_second']): ?>
<div class="span_4 col p0-60-0-0">
<?php print render($page['sidebar_first']); ?>
<?php print render($page['sidebar_second']); ?>
</div>
<?php endif; ?>
</div>

<?php
} else {

print render($page['content']);
}
?>
</section>

<?php include 'inc/footer.php'; ?>