
<?php if ($messages): ?>
<div class="span_12">
<div id="messages">
<?php print $messages; ?>
</div>
</div> <!-- /.section, /#messages -->
<?php endif; ?>

<?php
print render($page['content']);
?>