<?php
/*
 * Edited to remove sidebar_first region and add cal_form id.
 */
?>

<?php if ($page['top_banner']): ?>
<div class="top_banner">
<?php print render($page['top_banner']); ?>
</div>
<?php endif; ?>

<?php if ($messages): ?>
<div id="messages">
<?php print $messages; ?>
</div> <!-- /.section, /#messages -->
<?php endif; ?>

<?php
$classes = '';
if (isset($wrapclasses)) {
  $classes = implode(' ', $wrapclasses);
}
?>

<div id="wrap" class="<?php print $classes; ?>">
<?php include 'inc/header.php'; ?>
<section>


<div class="container content_top">

<div class="span_12 col p0-60" id="cal_form">
<?php if ($title): ?>
<h1><?php print $title; ?></h1>
<?php endif; ?>



<?php
if ($tabs):
print '<div class="drupal-tabs">';
print render($tabs);
print '</div>';
endif;

?>


<?php if ($action_links): ?>
<ul class="action-links">
<?php print render($action_links); ?>
</ul>
<?php endif; ?>



<?php
print render($page['content']); ?>
</div>


</div>


</section>

<?php include 'inc/footer.php'; ?>