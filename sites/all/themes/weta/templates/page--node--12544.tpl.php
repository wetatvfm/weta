<?php
/*
 * Stripped down for CD Not Available modal window for FM playlists.
 *
 * This has been overridden.  Text can now be edited in magnific-weta.js.
 */
?>
<?php if ($messages): ?>
<div id="messages"><div class="section clearfix">
<?php print $messages; ?>
</div></div> <!-- /.section, /#messages -->
<?php endif; ?>
<?php
print render($page['content']);
?>
