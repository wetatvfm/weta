<?php
/*
 * Stripped down for PBS Kids Player page
 */
?>
<?php if ($page['top_banner']): ?>
<div class="top_banner">
<?php print render($page['top_banner']); ?>
</div>
<?php endif; ?>



<?php
$classes = '';
if (isset($wrapclasses)) {
  $classes = implode(' ', $wrapclasses);
}
?>

<div id="wrap" class="<?php print $classes; ?>">
<?php include 'inc/header.php'; ?>
<section>

<div class="container content_top">
<div class="p0-60">
<?php if ($title): ?>
<h1><?php print $title; ?></h1>
<?php endif; ?>

<?php if ($messages): ?>
<div id="messages"><div class="section clearfix">
<?php print $messages; ?>
</div></div> <!-- /.section, /#messages -->
<?php endif; ?>

<?php
if ($tabs):
print '<div class="drupal-tabs">';
print render($tabs);
print '</div>';
endif;

?>

<?php if ($action_links): ?>
<ul class="action-links">
<?php print render($action_links); ?>
</ul>
<?php endif; ?>

<?php
print render($page['content']);
?>
</div>
</div>
</section>

<?php include 'inc/footer.php'; ?>
</div>
