<?php if ($page['top_banner']): ?>
<div class="top_banner">
<?php print render($page['top_banner']); ?>
</div>
<?php endif; ?>



<?php
$classes = '';
if (isset($wrapclasses)) {
  $classes = implode(' ', $wrapclasses);
}
?>

<div id="wrap" class="<?php print $classes; ?>">
<?php include 'inc/header.php'; ?>
<section>

<div class="container content_top">
  <div class="span_12 col p60 pbottom0 ptop0">

    <h1>Submit an Event</h1>
    <?php if ($messages): ?>
<div id="messages">
<?php print $messages; ?>
</div> <!-- /.section, /#messages -->
<?php endif; ?>
    <?php print render($page['content']); ?>
  </div>
</div>
</section>

<?php include 'inc/footer.php'; ?>