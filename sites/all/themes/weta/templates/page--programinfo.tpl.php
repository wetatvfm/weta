<?php if ($messages): ?>
<div id="messages"><div class="section clearfix">
<?php print $messages; ?>
</div></div> <!-- /.section, /#messages -->
<?php endif; ?>
<?php
print render($page['content']);
?>
<script src="<?php print base_path() . drupal_get_path('theme', $GLOBALS['theme']); ?>/templates/js/touch.detection.js"></script>

