<?php if ($messages): ?>
<div id="messages"><div class="section clearfix">
<?php print $messages; ?>
</div></div> <!-- /.section, /#messages -->
<?php endif; ?>
<?php
print render($page['content']);
?>