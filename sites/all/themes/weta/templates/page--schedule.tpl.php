<?php if ($page['top_banner']): ?>
<div class="top_banner">
<?php print render($page['top_banner']); ?>
</div>
<?php endif; ?>



<?php
$classes = '';
if (isset($wrapclasses)) {
  $classes = implode(' ', $wrapclasses);
}
?>

<div id="wrap" class="<?php print $classes; ?>">
<?php include 'inc/header.php'; ?>
<section>





<div class="container content_top">
  <div class="span_8 col p0-60 width-adjust">
    <h1><?php print drupal_get_title(); ?></h1>
    <?php if ($messages): ?>
      <div class="span_12">
        <div id="messages">
          <?php print $messages; ?>
        </div>
      </div> <!-- /.section, /#messages -->
    <?php endif; ?>

    <?php print render($page['schedule_topleft']); ?>
  </div>

  <div class="span_4 col p0-60-0-0 adCol">
    <?php print render($page['schedule_topright']); ?>
    <div class="hide-mobile">
      <?php print render($page['schedule_ad']); ?>
    </div>
  </div>
</div>

<?php if ($page['schedule_alert']): ?>
<div class ="container">
  <?php print render($page['schedule_alert']); ?>
</div>
<?php endif; ?>

<?php print render($page['content']); ?>

<div class="container">
  <div class="span_6 col half-box p20">
    <h2 class="format classical"><img src="<?php print base_path() . drupal_get_path('theme', $GLOBALS['theme']); ?>/templates/images/notes.jpg" alt="" />&nbsp;&nbsp;Classical WETA</h2>
    <?php print render($page['schedule_midleft']); ?>
  </div>
  <div class="span_6 col half-box p20">
    <h2 class="format classical"><img src="<?php print base_path() . drupal_get_path('theme', $GLOBALS['theme']); ?>/templates/images/notes.jpg" alt="" />&nbsp;&nbsp;VivaLaVoce</h2>
    <?php print render($page['schedule_midright']); ?>
  </div>
</div>

<div class="container">
<div class="span_12 col p60 show-mobile">
<?php print render($page['schedule_ad_mobile']); ?>
</div>
</div>

<div class="container">
<div class="span_12 col p60" id="channelguide">
<?php print render($page['schedule_bottom1']); ?>
</div>
</div>

<div class="container">
<div class="span_12 col p60">
<?php print render($page['schedule_bottom2']); ?>
</div>
</div>

</section>

<?php include 'inc/footer.php'; ?>