<?php

/**
 * implementation of hook_panels_layouts()
 */
// Plugin definition
$plugin = array(
  'title' => t('From Our Blogs'),
  'category' => t('Mini-panels'),
  'theme' => 'blogs_block',
  'icon' => 'blogs_block.png',
  'regions' => array('middle' => t('Middle column')),
);
