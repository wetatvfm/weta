<?php
function weta_calendar_panels_layouts() {
  $items['calendar'] = array(
    'title' => t('Calendar Landing Page layout'),
    'category' => t('Special pages'),
    'theme' => 'calendar',
    'icon' => 'calendar.png',
    'regions' => array(
      'top' => t('Top'),
      'top_features' => t('Top features'),
      'listings' => t('Listings'),
      'submit_event' => t('Submit event'),
      'login' => t('Login box'),
    ),
  );
  return $items;
}