<div class="container content_top">
  <div class="span_12 col p60 pbottom0 ptop0">
    <?php print $content['top']; ?>
  </div>
</div>

<div class="container">
  <div class="feature-row trans">
    <?php print $content['top_features']; ?>
    <div class="clear"></div>
  </div>
</div>

<?php print $content['listings']; ?>

<div class="container">
  <div class="span_8 col p0-60">
    <?php print $content['submit_event']; ?>
  </div>
  <div class="span_4 col p30">
    <?php print $content['login']; ?>
  </div>
</div>