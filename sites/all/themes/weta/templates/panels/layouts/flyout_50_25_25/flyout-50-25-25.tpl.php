<?php
/**
 * @file
 * Template for the Flyout 50/25/25 mini panel layout.
 *
 * This template provides a very simple "three column" panel display layout,
 * with one large column and two small ones.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   $content['middle']: The only panel in the layout.
 */
?>
<div class="subnav" id="donate_nav_sub">
<div class="subnav-home-inner">
<div class="hlf_1">
  <div class="hlf_inner">
	  <?php print $content['left']; ?>
  </div>
</div>
<div class="hlf_2">
  <div class="hlf_inner">
    <?php print $content['right']; ?>
    <div class="list_wrap">
      <?php print $content['right1']; ?>
    </div>
    <div class="list_wrap">
      <?php print $content['right2']; ?>
    </div>
  </div>
</div>
<div class="clear"></div>
<div class="advert">
  <?php print $content['ad']; ?>
</div>
</div>
</div>