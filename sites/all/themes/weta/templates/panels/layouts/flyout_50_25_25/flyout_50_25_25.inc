<?php

/**
 * implementation of hook_panels_layouts()
 */
// Plugin definition
$plugin = array(
  'title' => t('Flyout 50-25-25'),
  'category' => t('Mini-panels'),
  'theme' => 'flyout_50_25_25',
  'icon' => 'flyout_50_25_25.png',
  'regions' => array(
    'left' => t('Left column'),
    'right' => t('Right full width'),
    'right1' => t('First right column'),
    'right2' => t('Second right column'),
    'ad' => t('Ad')
  ),
);
