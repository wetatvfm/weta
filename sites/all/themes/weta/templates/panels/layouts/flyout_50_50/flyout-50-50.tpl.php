<?php
/**
 * @file
 * Template for the Flyout 50/50 mini panel layout.
 *
 * This template provides a very simple "two column" panel display layout.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   $content['middle']: The only panel in the layout.
 */
?>
<div class="subnav">
<div class="subnav-home-inner">
<div class="hlf_1">
  <div class="hlf_inner">
	  <?php print $content['left']; ?>
  </div>
</div>
<div class="hlf_2">
  <div class="hlf_inner">
    <?php print $content['right']; ?>
  </div>
</div>
<div class="clear"></div>
<div class="advert">
  <?php print $content['ad']; ?>
</div>
</div>
</div>