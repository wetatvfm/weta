<?php

/**
 * implementation of hook_panels_layouts()
 */
// Plugin definition
$plugin = array(
  'title' => t('Flyout 50-50'),
  'category' => t('Mini-panels'),
  'theme' => 'flyout_50_50',
  'icon' => 'flyout_50_50.png',
  'regions' => array(
    'left' => t('Left column'),
    'right' => t('Right column'),
    'ad' => t('Ad')
  ),
);
