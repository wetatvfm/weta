<?php
/**
 * @file
 * Template for the Flyout Single mini panel layout.
 *
 * This template provides a very simple "one column" panel display layout.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   $content['middle']: The only panel in the layout.
 */
?>
<div class="subnav">
  <div class="subnav-home-inner">
    <div class="thirds_1">
      <div class="thirds_inner">
        <?php print $content['left']; ?>
      </div>
    </div>
    <div class="thirds_2">
      <div class="thirds_inner">
        <?php print $content['middle']; ?>
      </div>
    </div>
    <div class="thirds_3">
      <div class="thirds_inner">
        <?php print $content['right']; ?>
      </div>
    </div>
    <div class="clear"></div>
  </div>
</div>
