<?php

/**
 * implementation of hook_panels_layouts()
 */
// Plugin definition
$plugin = array(
  'title' => t('Flyout: Three Columns'),
  'category' => t('Mini-panels'),
  'theme' => 'flyout_thirds',
  'icon' => 'flyout_three_column.png',
  'regions' => array(
    'left' => t('Left'),
    'middle' => t('Middle'),
    'right' => t('Right')
  ),
);
