<?php
function weta_fmlanding_panels_layouts() {
  $items['fmlanding'] = array(
    'title' => t('FM Landing Page layout'),
    'category' => t('Special pages'),
    'theme' => 'fmlanding',
    'icon' => 'fmlanding.png',
    'regions' => array(
      'top' => t('Top'),
      'alerts' => t('Alerts'),
      'nowplaying' => t('Now playing'),
      'top_features' => t('Top features'),
      'mid_features' => t('Mid features'),
      'bottom_features' => t('Bottom features'),
    ),
  );
  return $items;
}