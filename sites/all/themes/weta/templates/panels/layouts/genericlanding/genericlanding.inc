<?php
function weta_genericlanding_panels_layouts() {
  $items['genericlanding'] = array(
    'title' => t('Generic Landing Page layout'),
    'category' => t('Special pages'),
    'theme' => 'genericlanding',
    'icon' => 'genericlanding.png',
    'regions' => array(
      'top' => t('Top'),
      'alerts' => t('Alerts'),
      'top_features' => t('Top features'),
      'second_features' => t('Second row features'),
      'third_features' => t('Third row features'),
      'bottom_features' => t('Bottom features'),
    ),
  );
  return $items;
}