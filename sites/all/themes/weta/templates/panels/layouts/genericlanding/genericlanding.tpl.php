<div <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
<div class="container topshadow">
<?php print $content['top']; ?>
</div>

<div class="container">
<?php print $content['alerts']; ?>
</div>

<div class="container">
<div class="span_12 col">
<div class="divider3"></div>
</div>
</div>

<div class="container">
<?php print $content['top_features']; ?>
</div>

<div class="container topshadow graybar">
<?php print $content['second_features']; ?>
</div>

<div class="container whitebar">
<?php print $content['third_features']; ?>
</div>

<div class="container">
<div class="feature-row no-bg">
<?php print $content['bottom_features']; ?>
<div class="clear"></div>
</div>
</div>
</div>