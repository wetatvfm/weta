<?php
function weta_hero_panels_layouts() {
  $items['hero'] = array(
    'title' => t('Homepage layout with hero carousel'),
    'category' => t('Special pages'),
    'theme' => 'hero',
    'icon' => 'hero.png',
    'regions' => array(
      'top' => t('Top slideshow'),
      'alerts' => t('Alerts'),
      'top_features' => t('Top features'),
      'mid_features' => t('Mid features'),
      'callout' => t('Callout'),
      'bottom_features' => t('Bottom features'),
    ),
  );
  return $items;
}
