<div class="container hero">
<?php print $content['top']; ?>
</div>

<div class="container">
<?php print $content['alerts']; ?>
</div>

<div class="container nobg">
<div class="span_12 col">
<div class="divider5"></div>
</div>
</div>


<div class="container whitebar">
<?php print $content['top_features']; ?>
</div>

<div  class="container">
<div class="span_12 col">
<div class="divider1"></div>
</div>
</div>

<div  class="container">
<?php print $content['mid_features']; ?>
</div>


<div class="container">
<?php print $content['callout']; ?>
</div>


<div class="container">
<?php print $content['bottom_features']; ?>
</div>
