<div class="container hero">
<?php print $content['top']; ?>
</div>

<div class="container">
<?php print $content['alerts']; ?>
</div>

<div class="container nobg">
<div class="span_12 col">
<div class="divider5"></div>
</div>
</div>


<div class="container whitebar">
<?php print $content['top_features']; ?>
</div>

<?php if ($content['promo']) :?>
<div  class="container">
<div class="span_12 col">
<div class="divider1"></div>
</div>
</div>

<div class="container whitebar">
<?php print $content['promo']; ?>
</div>
<?php endif; ?>

<div  class="container">
<div class="span_12 col">
<div class="divider1"></div>
</div>
</div>

<div  class="container">
<?php print $content['mid_features']; ?>
</div>


<div class="container whitebar">
<?php print $content['news']; ?>
</div>

<div  class="container">
<div class="span_12 col">
<div class="divider1"></div>
</div>
</div>

<div class="container whitebar">
  <div class="p55">
    <div class="span_8 col p0-60-0-0">
      <?php print $content['tv_features']; ?>
    </div>
    <div class="span_4 col">
      <?php print $content['blogs']; ?>
    </div>
  </div>
</div>

<div  class="container">
<div class="span_12 col">
<div class="divider1"></div>
</div>
</div>

<div class="container whitebar">
<?php print $content['callout']; ?>
</div>

<div  class="container">
<div class="span_12 col">
<div class="divider1"></div>
</div>
</div>

<div class="container whitebar">
<?php print $content['bottom_features']; ?>
</div>

<div class="container">
<?php print $content['additional_bottom_features']; ?>
</div>
