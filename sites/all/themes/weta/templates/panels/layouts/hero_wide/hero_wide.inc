<?php
function weta_hero_wide_panels_layouts() {
  $items['hero_wide'] = array(
    'title' => t('Wide homepage'),
    'category' => t('Special pages'),
    'theme' => 'hero_wide',
    'icon' => 'hero_wide.png',
    'regions' => array(
      'top' => t('Top slideshow'),
      'alerts' => t('Alerts'),
      'top_features' => t('Top features'),
      'promo' => t('Full width promo'),
      'mid_features' => t('Mid features'),
      'news' => t('News'),
      'tv_features' => t('TV Features'),
      'blogs' => t('Blogs'),
      'callout' => t('Callout'),
      'bottom_features' => t('Bottom features'),
      'additional_bottom_features' => t('Additional bottom features'),
    ),
  );
  return $items;
}
