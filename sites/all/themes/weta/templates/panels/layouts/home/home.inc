<?php
function weta_home_panels_layouts() {
  $items['home'] = array(
    'title' => t('Homepage layout'),
    'category' => t('Special pages'),
    'theme' => 'home',
    'icon' => 'home.png',
    'regions' => array(
      'top' => t('Top slideshow'),
      'alerts' => t('Alerts'),
      'top_features' => t('Top features'),
      'mid_features' => t('Mid features'),
      'callout' => t('Callout'),
      'bottom_features' => t('Bottom features'),
    ),
  );
  return $items;
}