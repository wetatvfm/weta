<?php
function weta_kidslanding_panels_layouts() {
  $items['kidslanding'] = array(
    'title' => t('Kids Landing Page layout'),
    'category' => t('Special pages'),
    'theme' => 'kidslanding',
    'icon' => 'kidslanding.png',
    'regions' => array(
      'top' => t('Top'),
      'alerts' => t('Alerts'),
      'mid_features' => t('Mid features'),
      'schedules' => t('Schedules'),
      'events' => t('Events'),
      'bottom_features' => t('Bottom features'),
    ),
  );
  return $items;
}