<div id="kids-intro" class="container topshadow">
<?php print $content['top']; ?>
</div>

<div class="container">
<?php print $content['alerts']; ?>
</div>

<div class="container">
	<div class="kids-banner-top topshadow">&nbsp;</div>
	<div class="kids-banner">
		<div class="span_6 col">
			<div class="p20">
        <?php
        $img = '<img src="' . base_path() . path_to_theme() . '/templates/images/kids-see-all.png" alt="See all programs from A to Z" />';
        $link = 'kids/programsatoz';
        print l($img, $link, array('html' => TRUE));
        ?>
			</div>
		</div>
    <div class="span_6 col">
			<div class="kids-banner-main p20">
        <h2>Your favorite stars are on WETA PBS Kids!</h2>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>

<div class="container">
  <?php print $content['mid_features']; ?>
</div>

<div class="container whitebar kids-bottom">
  <div class="span_6 col kids-on-now p0-60">
    <?php print $content['schedules']; ?>
  </div>
  <div class="span_6 col kids-on-now p0-60-0-0">
    <?php print $content['events']; ?>
  </div>
  <div class="clearfix"></div>
  <?php print $content['bottom_features']; ?>
</div>
