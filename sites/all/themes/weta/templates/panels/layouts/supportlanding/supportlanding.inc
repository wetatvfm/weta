<?php
function weta_supportlanding_panels_layouts() {
  $items['supportlanding'] = array(
    'title' => t('Support Landing Page layout'),
    'category' => t('Special pages'),
    'theme' => 'supportlanding',
    'icon' => 'supportlanding.png',
    'regions' => array(
      'top' => t('Top'),
      'quad_top_left' => t('Quad Box: Top left'),
      'quad_top_right' => t('Quad Box: Top right'),
      'quad_bottom_left' => t('Quad Box: Bottom left'),
      'quad_bottom_right' => t('Quad Box: Bottom right'),
      'left_bottom' => t('Left bottom features'),
      'gift_mobile' => t('Gift Box: Mobile version'),
      'gift_desktop' => t('Gift Box: Desktop version'),
      'bottom_features' => t('Bottom features'),
    ),
  );
  return $items;
}