<div class="container topshadow">
  <?php print $content['top']; ?>
</div>

<div class="container">
  <div class="span_12 col">
    <div class="divider1"></div>
  </div>
</div>

<div class="container show-mobile">
  <?php print $content['gift_mobile']; ?>
</div>

<div class="container">
  <div class="span_6 col p0-0-0-60 br">
	  <div class="quad_row">
	    <div class="quad quad_left">
	      <?php print $content['quad_top_left']; ?>
	    </div>
	    <div class="quad quad_right">
	      <?php print $content['quad_top_right']; ?>
	    </div>
	    <div class="clear"></div>
    </div>
    <div class="quad_row">
      <div class="quad quad_left">
	      <?php print $content['quad_bottom_left']; ?>
	    </div>
	    <div class="quad quad_right">
	      <?php print $content['quad_bottom_right']; ?>
	    </div>
	    <div class="clear"></div>
    </div>

    <div class="ptop10">
      <?php print $content['left_bottom']; ?>
    </div>
  </div>

  <div class="span_6 col p0-60-0-0 gutter">
    <?php print $content['gift_desktop']; ?>
  </div>
</div>


<div class="container">
  <div class="p60">
  <?php if (!empty($content['bottom_features'])): ?>
    <div class="divider2"></div>
  <?php endif; ?>
      <?php print $content['bottom_features']; ?>
    <div class="clear"></div>
  </div>
</div>


