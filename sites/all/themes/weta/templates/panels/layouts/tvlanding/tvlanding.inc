<?php
function weta_tvlanding_panels_layouts() {
  $items['tvlanding'] = array(
    'title' => t('TV Landing Page layout'),
    'category' => t('Special pages'),
    'theme' => 'tvlanding',
    'icon' => 'tvlanding.png',
    'regions' => array(
      'top' => t('Top'),
      'alerts' => t('Alerts'),
      'schedules' => t('Schedules'),
      'mid_features' => t('Mid features'),
      'bottom_features_left' => t('Left bottom features'),
      'bottom_features_right' => t('Right bottom features'),
      'bottom_features_ads' => t('Right bottom house ads'),
    ),
  );
  return $items;
}