<div class="container topshadow tv_bg">
<?php print $content['top']; ?>
</div>

<div class="container">
<?php print $content['alerts']; ?>
</div>

<div class="container">
<div class="span_12 col">
<div class="divider3"></div>
</div>
</div>

<div class="container">
<?php print $content['schedules']; ?>
</div>

<div class="container">
	<div class="feature-row">
    <div class="divider1"></div>
    <?php print $content['mid_features']; ?>
  </div>
</div>

<div class="container">
  <div class="p55">
    <div class="span_4 col">
      <?php print $content['bottom_features_left']; ?>
    </div>
    <div class="span_8 col">
      <?php print $content['bottom_features_right']; ?>
      <?php if ($content['bottom_features_ads']): ?>
      <div class="span_12 col light_gray_bar mtop_20">
        <?php print $content['bottom_features_ads']; ?>
      </div>
      <?php endif; ?>
    </div>
    <div class="clear"></div>
  </div>
</div>