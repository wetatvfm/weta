<?php
function weta_tvseries_panels_layouts() {
  $items['tvseries'] = array(
    'title' => t('TV Series Page layout'),
    'category' => t('Special pages'),
    'theme' => 'tvseries',
    'icon' => 'tvseries.png',
    'regions' => array(
      'banner' => t('Banner'),
      'top' => t('Top'),
      'left' => t('Left column'),
      'right_top' => t('Right column top'),
      'right_bottom' => t('Right column bottom'),
      'bottom' => t('Bottom'),
    ),
  );
  return $items;
}
