<?php

$background_url = '';
$topstyle = 'content_top_plain';
if ($content['banner']) {

$topstyle = 'content_top';
$doc = new DOMDocument();
$doc->loadHTML(trim($content['banner']));
$image_tags = $doc->getElementsByTagName('img');

foreach($image_tags as $tag) {
$background_url = $tag->getAttribute('src');
$background_url = str_replace('styles/cropped_banner/public/', '', $background_url);
}
?>

<div class="container fullwidth-banner" style="background: url('<?php print $background_url; ?>') top center no-repeat;">
<div class="span_6 col cropped_banner">
<?php print $content['banner']; ?>
</div>
<div class="span_6 col p20-60-10 fullwidth_banner">
<?php print $content['top']; ?>
</div>
</div>
<?php
}
else {
?>
<div class="container">
<div class="span_12 col p0-60">
<?php print $content['top']; ?>
</div>
</div>
<?php
}
?>
<div class="container <?php print $topstyle; ?>">
<div class="span_12 col p0-60">
<?php print $content['left']; ?>

<?php print $content['right_bottom']; ?>
</div>

<div class="span_12 col p0-60">
<?php print $content['bottom']; ?>
</div>
</div>
