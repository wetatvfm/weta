<?php
// Add eTruncate plug-in for tv schedules
drupal_add_js('sites/all/libraries/eTruncate/jquery.eTruncate.min.js');
drupal_add_js('sites/all/libraries/eTruncate/jquery.custom.js');
?>
<div class="container">
<div class="span_12 col p0-60">
<?php print $content['top']; ?>
</div>
</div>

<div class="container content_top_plain">
<div class="span_8 col p0-60">
<?php print $content['left']; ?>
</div>
<div class="span_4 col p0-60-0-0">
<p>[Share this block here]</p>
<p>[Comment count here]</p>
<?php print $content['right_top']; ?>
<p>[Connect around the Web here]</p>
<p>[From our Blogs]</p>
<p>[Newsletter signup</p>
<?php print $content['right_bottom']; ?>
</div>
</div>