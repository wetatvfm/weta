<?php
function weta_tvseries_basic_panels_layouts() {
  $items['tvseries_basic'] = array(
    'title' => t('TV Series Basic Page layout'),
    'category' => t('Special pages'),
    'theme' => 'tvseries_basic',
    'icon' => 'tvseries_basic.png',
    'regions' => array(
      'top' => t('Top'),
      'left' => t('Left column'),
      'right_top' => t('Right column top'),
      'right_bottom' => t('Right column bottom'),
    ),
  );
  return $items;
}