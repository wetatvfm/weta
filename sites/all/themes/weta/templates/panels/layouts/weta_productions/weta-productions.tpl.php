<div class="span_12 col">
  <div class="dot-top p15">
    <?php print $content['header']; ?>
  </div>
</div>
<div class="clear"></div>

<div class="border-box">
<div class="small_tri"></div>
  <?php print $content['top']; ?>
<div class="clear"></div>


<div class="span_6 col img_lt gray_bar">
  <?php print $content['left']; ?>
</div>

<div class="span_6 col img_lt gray_bar">
  <?php print $content['right']; ?>
</div>
<div class="clear"></div>
</div>