<?php
/**
 * implementation of hook_panels_layouts()
 */
// Plugin definition
$plugin = array(
  'title' => t('WETA Productions'),
  'category' => t('Mini-panels'),
  'theme' => 'weta_productions',
  'icon' => 'weta_productions.png',
  'regions' => array(
    'header' => t('Header'),
    'top' => t('Top'),
    'left' => t('Left'),
    'right' => t('Right'),
  ),

);