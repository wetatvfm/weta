<?php
/**
 * @file panels-pane--fieldable-panels-pane--media-manager-feature-block.tpl.php
 * Panel pane template for the Media Manager Feature Block fieldable pane
 *
 * Variables available:
 * - $pane->type: the content type inside this pane
 * - $pane->subtype: The subtype, if applicable. If a view it will be the
 *   view name; if a node it will be the nid, etc.
 * - $title: The title of the content
 * - $content: The actual content
 * - $links: Any links associated with the content
 * - $more: An optional 'more' link (destination only)
 * - $admin_links: Administrative links associated with the content
 * - $feeds: Any feed icons or associated with the content
 * - $display: The complete panels display object containing all kinds of
 *   data including the contexts and all of the other panes being displayed.
 *
 * Added variable to control title header
 *
 */
?>



<div class="<?php print $classes; ?> span_3 col p30" <?php print $id; ?>>
  <?php if ($admin_links): ?>
    <?php print $admin_links; ?>
  <?php endif; ?>

<div class="red video">
    <div class="video">
      <?php print render($title_prefix); ?>
        <span class="screenshot"><span class="play-overlay"></span></span>
        <?php if ($content['image']): ?>
          <?php print $content['image']; ?>
        <?php endif; ?>
      <?php print render($title_suffix); ?>
    </div>
    <div id="node-feature-block-basic-square-group-text-wrapper" class="text_wrapper group-text-wrapper field-group-div">
      <div class="field-linked-title"> <?php print render($title_prefix); ?>
      <?php print $content['passport']; ?>
      <?php print $title; ?><?php print render($title_suffix); ?></div>
      <?php print render($content['field_mm_subhead']); ?>
    </div>
  </div>

</div>

