<?php
/**
 * @file
 * Template file for the example display.
 *
 * Variables available:
 *
 * $plugin: The pager plugin object. This contains the view.
 *
 * $plugin->view
 *   The view object for this navigation.
 *
 * $nav_title
 *   The formatted title for this view. In the case of block
 *   views, it will be a link to the full view, otherwise it will
 *   be the formatted name of the year, month, day, or week.
 *
 * $prev_url
 * $next_url
 *   Urls for the previous and next calendar pages. The links are
 *   composed in the template to make it easier to change the text,
 *   add images, etc.
 *
 * $prev_options
 * $next_options
 *   Query strings and other options for the links that need to
 *   be used in the l() function, including rel=nofollow.
 */
?>
<?php if (!empty($pager_prefix)) print $pager_prefix; ?>

<?php
  // add classes
  $prev_options['attributes']['class'][] = 'prev';
  $next_options['attributes']['class'][] = 'next';

  // rewrite prev and next urls to account for conflict
  // between date filter and date args
  $prev = drupal_parse_url($prev_url);

  // calendar
  if (isset($prev['query']['date_filter'])) {
    // figure out what the previous date is based on the filter
    $prev_date = date('Y-m-d', strtotime($prev['query']['date_filter']['value']['date'] . '-1day'));

    // unset the filter to avoid confusion
    unset($prev['query']['date_filter']);

    // set the URL query to the previous date
    $prev['query']['date'] = $prev_date;

    // construct the URL
    $prev_url = url($prev['path'], array('query' => $prev['query'], 'fragment' => $prev['fragment']));
  }

  // playlists
  if (isset($prev['query']['playlist_date'])) {
    $parts = parse_url($prev['path']);
    $path = explode('/', $parts['path']);

    foreach ($path as $i => $p) {
      // check to see if path chunk is in yyyy-mm-dd format
      // if so, replace it with appropriate date
      $dt = DateTime::createFromFormat("Y-m-d", $p);
      if ($dt !== false && !array_sum($dt->getLastErrors())) {
        $prev_date = date('Y-m-d', strtotime($prev['query']['playlist_date']['value']['date'] . ' -1day'));
        $path[$i] = $prev_date;
      }
    }
    $prev['path'] = implode('/', $path);

    unset($prev['query']['playlist_date']);
    $prev_url = url($prev['path'], array('query' => $prev['query'], 'fragment' => $prev['fragment']));
  }

  $next = drupal_parse_url($next_url);

  // calendar
  if (isset($next['query']['date_filter'])) {
    // figure out what the next date is based on the filter
    $next_date = date('Y-m-d', strtotime($next['query']['date_filter']['value']['date'] . '+1day'));

    // unset the filter to avoid confusion
    unset($next['query']['date_filter']);

    // set the URL query to the next date
    $next['query']['date'] = $next_date;

    // construct the URL
    $next_url = url($next['path'], array('query' => $next['query'], 'fragment' => $next['fragment']));
  }

  // playlists
  if (isset($next['query']['playlist_date'])) {
    $parts = parse_url($next['path']);
    $path = explode('/', $parts['path']);

    foreach ($path as $i => $p) {
      // check to see if path chunk is in yyyy-mm-dd format
      // if so, replace it with appropriate date
      $dt = DateTime::createFromFormat("Y-m-d", $p);
      if ($dt !== false && !array_sum($dt->getLastErrors())) {
        $next_date = date('Y-m-d', strtotime($next['query']['playlist_date']['value']['date'] . ' +1day'));
        $path[$i] = $next_date;
      }
    }
    $next['path'] = implode('/', $path);
    unset($next['query']['playlist_date']);
    $next_url = url($next['path'], array('query' => $next['query'], 'fragment' => $next['fragment']));
  }

  // Format the date/title
  if (!empty($plugin->view->exposed_input['date'])) {
    $date = strtotime($plugin->view->exposed_input['date']);
  }
  else {
    $date = strtotime($plugin->view->args[0]);
  }
  $nav_title = date('D', $date) . ' &bull; ' . date('F j, Y', $date);
?>


    <?php print l('', $prev_url, $prev_options); ?><div class="dt"><h1><?php print $nav_title ?></h1></div><?php print l('', $next_url, $next_options); ?>

