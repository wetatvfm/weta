<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>
<a class="show-lb fancybox" href="#roadblock" style="display:none;">Click</a>
<div style="display:none;">
<div id="roadblock">
  <?php if ($rows): ?>
      <?php print $rows; ?>
      <?php
drupal_add_js('jQuery(document).ready(function(e) {
    $("img[usemap]").rwdImageMaps();
});', 'inline');
drupal_add_js(base_path() . path_to_theme() . '/templates/js/jquery.cookie.js', array('group' => 'JS_THEME', 'scope' => 'footer', 'preprocess' => FALSE));
drupal_add_js(base_path() . path_to_theme() . '/templates/js/lightbox-weta.js', array('group' => 'JS_THEME', 'scope' => 'footer', 'preprocess' => FALSE));
?>
  <?php elseif ($empty): ?>
      <?php print $empty; ?>
  <?php endif; ?>
</div>
</div>

<?php /* class view */ ?>