<?php

$name = trim(strip_tags($row->field_field_sponsor_display_name[0]['rendered']));
$preroll_path = trim($row->field_field_preroll[0]['rendered']);
$stream_title = 'Classical WETA, 90.9 FM';
$stream_url = 'mms://stream.weta.org/winstream';

?>
<asx version = "3.0">
	<title><?php print $stream_title; ?></title>
	<entry clientskip="no">
		<title>Thanks to our sponsor, <?php print $name; ?></title>
		<ref href = "<?php print $preroll_path; ?>" />
	</entry>

	<entry>
		<title><?php print $stream_title; ?></title>
		<ref href = "<?php print $stream_url; ?>" />
	</entry>
</asx>