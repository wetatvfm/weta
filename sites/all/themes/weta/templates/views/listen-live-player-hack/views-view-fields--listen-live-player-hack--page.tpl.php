<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
?>

<?php

// set up the script tag

// can't hard-code the base url.  Must also encode the slashes correctly
$swfpath = str_replace('"', '', json_encode($GLOBALS['base_url'] . base_path())) . 'sites\/all\/libraries\/jplayer\/Jplayer.swf';

// pull the preroll path from the view, and encode the slashes.
$prerollpath = str_replace('"', '',json_encode(trim($row->field_field_preroll[0]['rendered'])));

//set up script tag
$preroll_script = <<<EOT
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","wetaPlayerSettings":{"basePath":"\/","playerPage":"90-9FM","swfPath":"$swfpath","streamPath":"http:\/\/stream.weta.org:8000\/;","vlvStreamPath":"http:\/\/stream.weta.org:8006\/;","prerollPath":"$prerollpath","streamPlayerId":1,"vlvPlayerId":2,"ccPlayerId":3,"ccMedia":[{"mp3":"http:\/\/d7.weta.org\/sites\/default\/files\/audio\/cc_focm_singaporeembassy_2013-11-12.mp3","title":"Friends of Classical Music"}]},"ajaxPageState":{"js":{"misc\/jquery.js":1,"misc\/drupal.js":1,"sites\/all\/libraries\/jplayer\/jquery.jplayer.min.js":1,"sites\/all\/libraries\/jplayer\/add-on\/jplayer.playlist.min.js":1,"sites\/all\/modules\/weta_custom\/weta_player2\/js\/load_player.js":1,"sites\/all\/modules\/weta_custom\/weta_player2\/js\/now_playing.js":1,"0":1}}});
//--><!]]>
</script>
EOT;

// set it as a variable so it can be called in the <head> of the HTML template
variable_set('preroll', $preroll_script);

$classical_weta_link = '/fm';
$donate_now_link = 'http://weta.convio.net/donatefm';
$adlink728x90 = strip_tags($row->field_field_ad_link_780x90[0]['rendered']);
$creativelink728x90 = strip_tags($row->field_field_creative_link_780x90[0]['rendered']);
$adlink300x250 = strip_tags($row->field_field_ad_link_300x250[0]['rendered']);
$creativelink300x250 = strip_tags($row->field_field_creative_link_300x250[0]['rendered']);
$name = strip_tags($row->field_field_sponsor_display_name[0]['rendered']);

$banner = <<<EOT
<div><div><style><!--
a:link { color: #000000 }a:visited { color: #000000 }a:hover { color: #000000 }a:active { color: #000000 }  --></style><script><!--
(function(){window.ss=function(){};})();var viewReq = new Array();function vu(u) {var i=new Image();i.src=u.replace("&amp;","&");viewReq.push(i);}(function(){var c=function(a,e,h){var b=document;b.addEventListener?b.addEventListener(a,e,h||!1):b.attachEvent&&b.attachEvent("on"+a,e)};var d,f=!1,g=!1;c("mousedown",function(){f=!0});c("keydown",function(){g=!0});document.addEventListener&&c("click",function(a){d=a},!0);window.accbk=function(){var a=d?d:window.event;return a?f||g?!1:(a.preventDefault?a.preventDefault():a.returnValue=!1,!0):!1};})();function st(id) {var a = document.getElementById(id);if (a) {}}function ha(a,x){  if (accbk()) return;}function ca(a) {window.open(document.getElementById(a).href);}function ia(a,e,x) {if (accbk()) return;}function ga(o,e,x) {if (document.getElementById) {var a=o.id.substring(1),p="",r="",g=e.target,t,f,h;if (g) {t=g.id;f=g.parentNode;if (f) {p=f.id;h=f.parentNode;if (h)r=h.id;}} else {h=e.srcElement;f=h.parentNode;if (f)p=f.id;t=h.id;}if (t==a||p==a||r==a)return true;ia(a,e,x);window.open(document.getElementById(a).href);}}
//-->
</script></div><div leftMargin="0" topMargin="0" marginwidth="0" marginheight="0" style="background:transparent" ><div id="google_image_div" style="overflow:hidden; position:absolute"><script>vu("http://weta.org/adsink?wetaAdId\x3d33066528362")</script><a id="aw0" target="_blank" href="$adlink728x90" onFocus="ss('aw0')" onMouseDown="st('aw0')" onMouseOver="ss('aw0')" onClick="ha('aw0')"><img src="$creativelink728x90" border="0" width="728" height="90" alt="Thanks to our sponsor, $name" class="img_ad"  /></a><script>var onDrtLoad = function() {if (typeof posWidgetIframeList != 'undefined' && posWidgetIframeList) {for (var i = 0; i < posWidgetIframeList.length; i++) {posWidgetIframeList[i].G_handleAdDoritosFlowDone();}}};</script><iframe frameborder=0 height=0 width=0 src="http://googleads.g.doubleclick.net/pagead/drt/s?v=r20120211" style="position:absolute" onload="onDrtLoad()"></iframe></div></div></div>
EOT;

$right_ad = <<<EOT
<div><div><style><!--
a:link { color: #000000 }a:visited { color: #000000 }a:hover { color: #000000 }a:active { color: #000000 }  --></style><script><!--
(function(){window.ss=function(){};})();var viewReq = new Array();function vu(u) {var i=new Image();i.src=u.replace("&amp;","&");viewReq.push(i);}(function(){var c=function(a,e,h){var b=document;b.addEventListener?b.addEventListener(a,e,h||!1):b.attachEvent&&b.attachEvent("on"+a,e)};var d,f=!1,g=!1;c("mousedown",function(){f=!0});c("keydown",function(){g=!0});document.addEventListener&&c("click",function(a){d=a},!0);window.accbk=function(){var a=d?d:window.event;return a?f||g?!1:(a.preventDefault?a.preventDefault():a.returnValue=!1,!0):!1};})();function st(id) {var a = document.getElementById(id);if (a) {}}function ha(a,x){  if (accbk()) return;}function ca(a) {window.open(document.getElementById(a).href);}function ia(a,e,x) {if (accbk()) return;}function ga(o,e,x) {if (document.getElementById) {var a=o.id.substring(1),p="",r="",g=e.target,t,f,h;if (g) {t=g.id;f=g.parentNode;if (f) {p=f.id;h=f.parentNode;if (h)r=h.id;}} else {h=e.srcElement;f=h.parentNode;if (f)p=f.id;t=h.id;}if (t==a||p==a||r==a)return true;ia(a,e,x);window.open(document.getElementById(a).href);}}
//-->
</script></div><div leftMargin="0" topMargin="0" marginwidth="0" marginheight="0" style="background:transparent" ><div id="google_image_div" style="overflow:hidden; position:absolute"><a href="$adlink300x250" target="_blank"><img src="$creativelink300x250" border="0" width="300" height="250" alt="Thanks to our sponsor, $name" class="img_ad"  /></a></div></div></div>
EOT;

$player_id = 1;
$stream_title = 'Classical WETA 90.9 FM Live Stream';
?>

<div id="ll-player">
      <div class="p10 show-mobile mobile-player" id="jp-header">
        <div class="classical-weta"><a target="_blank" href="<?php print $classical_weta_link; ?>">Classical WETA</a></div>
        <div class="donate-now"><a target="_blank" href="<?php print $donate_now_link; ?>">Donate Now</a></div>
        <div class="clearfix"></div>
        <h1>Listen Live</h1>
			</div>
		<div class="span_12 col hide-mobile centered topbanner">
			<?php print $banner; ?>
			<div class="clearfix"></div>
		</div>
		<div class="ll-player-wrap">
		<div class="span_7 col">




    <div id="js-tabs" class="player-wrapper hide-player-mobile">

        <div class="item-list"><ul><li class="first"><a href="#" class="active active">CLASSICAL WETA</a></li>
<li class="last"><a href="/listen-live/viva-la-voce">VIVALAVOCE</a></li>
</ul></div>
        <div id="weta-player">
<div id="jp_container_<?php print $player_id; ?>" class="jp-audio-stream">
  <div class="jp-type-single">
    <div>
        <h3><?php print $stream_title; ?></h3>
      <div id="now-playing"></div>
    </div>
    <div id="jquery_jplayer_<?php print $player_id; ?>" class="jp-jplayer"></div>
    <div class="jp-gui jp-interface">
      <ul class="jp-controls">
        <li><a href="javascript:;" class="jp-play" tabindex="1">play</a></li>
        <li><a href="javascript:;" class="jp-pause" tabindex="1">pause</a></li>
      </ul>
      <div id="volume-control-<?php print $player_id; ?>">
        <ul class="jp-volume-controls">
          <li><a href="javascript:;" class="jp-mute" tabindex="1" title="mute">mute</a></li>
          <li><a href="javascript:;" class="jp-unmute" tabindex="1" title="unmute">unmute</a></li>
        </ul>
        <div class="max-volume"><a href="javascript:;" class="jp-volume-max" tabindex="1" title="max volume">max volume</a></div>
        <div class="jp-volume-bar">
          <div class="jp-volume-bar-value"></div>
        </div>
      </div>
    </div>
    <div class="jp-no-solution">
      <span>Update Required</span>
      <p>To play the media you will need to either update your browser to a recent version or update your Flash Player.</p>
      <p><a href="http://get.adobe.com/flashplayer/" target="_blank">Update Flash Now.</a></p>
    </div>
  </div>
</div>

          <div id="extra_text">
          <a href="/fm/listenlive#other_players" target="_blank">Listen in a different player →</a>
          </div>
        </div>

    </div>
    <div class="clearfix"></div>
    </div>

    <div id="player-ad-square" class="span_5 col">
      <div class="ad300">
          <?php print $right_ad; ?>

      </div>
    </div>

</div>

    <div class="clearfix"></div>
    <div id="jp-footer" class="p10 hide-mobile">
      <div class="classical-weta"><a target="_blank" href="<?php print $classical_weta_link; ?>">Classical WETA</a></div>
      <div class="donate-now"><a target="_blank" href="<?php print $donate_now_link; ?>">Donate Now</a></div>
    </div>
</div>




