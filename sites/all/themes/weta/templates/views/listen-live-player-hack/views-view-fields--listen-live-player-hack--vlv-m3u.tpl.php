<?php

$name = trim(strip_tags($row->field_field_sponsor_display_name[0]['rendered']));
$preroll_path = trim($row->field_field_preroll[0]['rendered']);
$stream_title = 'VivaLaVoce';
$stream_url = 'http://stream.weta.org:8006';

$output = '#EXTM3U';
weta_player2_plaintext_addline($output, '#EXTINF:0, Thanks to our sponsors: ' . $name);
weta_player2_plaintext_addline($output, $preroll_path);
weta_player2_plaintext_addline($output, '#EXTINF:-1, ' . $stream_title);
weta_player2_plaintext_addline($output, $stream_url);
print $output;


?>
