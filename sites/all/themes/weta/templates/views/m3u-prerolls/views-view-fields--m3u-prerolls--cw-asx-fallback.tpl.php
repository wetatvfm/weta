<?php

$stream_title = 'Classical WETA, 90.9 FM';
$stream_url = 'https://stream.weta.org:8010/fmlive';

?>
<asx version = "3.0">
	<title><?php print $stream_title; ?></title>

	<entry>
		<title><?php print $stream_title; ?></title>
		<ref href = "<?php print $stream_url; ?>" />
	</entry>
</asx>
