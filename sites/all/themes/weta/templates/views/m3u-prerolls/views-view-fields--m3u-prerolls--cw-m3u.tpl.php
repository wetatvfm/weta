<?php

$stream_title = 'Classical WETA, 90.9 FM';
$stream_url = 'https://stream.weta.org:8010/fmlive';

$output = '#EXTM3U';
weta_player_plaintext_addline($output, '#EXTINF:-1, ' . $stream_title);
weta_player_plaintext_addline($output, $stream_url);
print $output;


?>
