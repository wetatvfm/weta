<?php

$stream_title = 'VivaLaVoce';
$stream_url = 'https://stream.weta.org:8008/viva';

?>
<asx version = "3.0">
	<title><?php print $stream_title; ?></title>

	<entry>
		<title><?php print $stream_title; ?></title>
		<ref href = "<?php print $stream_url; ?>" />
	</entry>
</asx>
