<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>
<div class="<?php print $classes; ?>">
  <div class="container">
    <div class="span_6 col  center-mobile">
      <div class="current-month">
        <?php print render($title_prefix); ?>
        <?php if ($title): ?>
          <?php print $title; ?>
        <?php endif; ?>
        <?php print render($title_suffix); ?>

    <?php if ($header): ?>
      <div class="view-header">
        <?php print $header; ?>
      </div>
    <?php endif; ?>
      </div>
    </div>
    <div class="span_2 col">
      &nbsp;
		</div>

    <div class="span_2 col mob50">
      <div class="spec_ev"><img src="<?php print base_path() . path_to_theme(); ?>/templates/images/tickets.png" alt="" /><p>WETA Ticket<br /> Giveaways</p></div>
    </div>

    <div class="span_2 col mob50">
      <div class="spec_ev"><img src="<?php print base_path() . path_to_theme(); ?>/templates/images/spec_ev.gif" alt="" /><p>WETA Special<br /> Event</p></div>
    </div>
  </div>
  <div class="container">
    <div class="span_12">
      <div class="cal_wrap p0-60">
        <?php if ($exposed): ?>
          <div class="view-filters" id="cal_form">
            <?php print $exposed; ?>
          </div>
        <?php endif; ?>
      <div class="clear"></div>
  <?php if ($attachment_before): ?>
    <div class="attachment attachment-before">
      <?php print $attachment_before; ?>
    </div>
  <?php endif; ?>

  <?php if ($rows): ?>
    <div class="view-content">
      <?php print $rows; ?>
    </div>
  <?php elseif ($empty): ?>
    <div class="view-empty">
      <?php print $empty; ?>
    </div>
  <?php endif; ?>

  <?php if ($pager): ?>
    <?php print $pager; ?>
  <?php endif; ?>

  <?php if ($attachment_after): ?>
    <div class="attachment attachment-after">
      <?php print $attachment_after; ?>
    </div>
  <?php endif; ?>

  <?php if ($more): ?>
    <?php print $more; ?>
  <?php endif; ?>

  <?php if ($footer): ?>
    <div class="view-footer">
      <?php print $footer; ?>
    </div>
  <?php endif; ?>

  <?php if ($feed_icon): ?>
    <div class="feed-icon">
      <?php print $feed_icon; ?>
    </div>
  <?php endif; ?>
</div>
</div>
</div>
</div>
<?php /* class view */ ?>

