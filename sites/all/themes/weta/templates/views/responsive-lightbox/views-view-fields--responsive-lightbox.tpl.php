<?php

/**
 * @file
 * Simple view template to all the fields as a row.
 *
 * Stripped of everything but the content
 *
*/
?>
<?php foreach ($fields as $id => $field): ?>
    <?php print $field->content; ?>
<?php endforeach; ?>
