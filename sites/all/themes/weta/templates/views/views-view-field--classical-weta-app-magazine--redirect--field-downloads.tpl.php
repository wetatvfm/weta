<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
?>
<?php
/**
 * This needs to redirect the view to the actual magazine PDF.  So, we're
 * check to see if the returned output is a valid URL and a PDF file.
 * If so, redirect to it.
 * If not, redirect to the magazine landing page.
 */

$redirect = '/about/magazine';
if (!filter_var($output, FILTER_VALIDATE_URL) === false) {
  $path_info = pathinfo($output);
  if ($path_info['extension'] == 'pdf') {
    $redirect = $output;
  }
}
drupal_goto($redirect);
?>

