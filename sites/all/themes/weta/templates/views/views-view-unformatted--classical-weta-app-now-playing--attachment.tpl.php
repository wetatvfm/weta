<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 *
 * Removed unneccessary HTML
 */
?>
<?php foreach ($rows as $id => $row): ?>
<?php print trim($row); ?>
<?php endforeach; ?>
