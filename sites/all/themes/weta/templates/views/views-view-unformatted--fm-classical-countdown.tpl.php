<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 *
 * Added count
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>

<?php
//$countdown = 90;
//print $countdown . '. ' .$row;
?>

<?php foreach ($rows as $id => $row): ?>
  <div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?>>
    <p><?php print $row; ?></p>
    <?php //$countdown = $countdown - 1; ?>
  </div>
<?php endforeach; ?>
