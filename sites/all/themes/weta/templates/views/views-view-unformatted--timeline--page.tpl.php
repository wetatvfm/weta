<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>

<?php foreach ($rows as $id => $row): ?>
  <ul id="issues" class="timelinr-issues">
    <?php print $row; ?>
  </ul>
<?php endforeach; ?>
