<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * Added classes to each row since UI transforms underscores to hyphens.
 *
 * @ingroup views_templates
 */

?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
  <div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .' feature_box trans span_6 p10 col"';  } ?>>
    <?php print $row; ?>
  </div>
<?php endforeach; ?>
