<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 * Stripped divs and classes
 */
?>

<?php foreach ($rows as $id => $row): ?>

    <?php print $row; ?>

<?php endforeach; ?>

