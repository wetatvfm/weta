<?php

/**
 * @file
 * Customize the display of a complete webform.
 *
 * This file may be renamed "webform-form-[nid].tpl.php" to target a specific
 * webform on your site. Or you can leave it "webform-form.tpl.php" to affect
 * all webforms on your site.
 *
 * Available variables:
 * - $form: The complete form array.
 * - $nid: The node ID of the Webform.
 *
 * The $form array contains two main pieces:
 * - $form['submitted']: The main content of the user-created form.
 * - $form['details']: Internal information stored by Webform.
 *
 * If a preview is enabled, these keys will be available on the preview page:
 * - $form['preview_message']: The preview message renderable.
 * - $form['preview']: A renderable representing the entire submission preview.
 */
?>
<?php

  drupal_add_js('sites/all/themes/weta/templates/js/toggle-weta.js', array('group' => JS_THEME, 'preprocess' => FALSE));
  // Print out the progress bar at the top of the page
  print drupal_render($form['progressbar']);

  // Print out the preview message if on the preview page.
  if (isset($form['preview_message'])) {
    print '<div class="messages warning">';
    print drupal_render($form['preview_message']);
    print '</div>';
  }

  // Before printing out the form, get the nids from the select options
  // and load and render the nodes in place of the select option titles.
  foreach ($form['submitted']['options'] as $i => $option) {
    // drill down to find just the radio options, not the metadata
    if (is_array($option) && isset($option['#type']) && $option['#type'] == 'radio') {
      $nid = $option['#return_value'];
      // make sure the value is numeric
      if (is_numeric($nid)) {
        $this_node = node_load($nid);
        // make sure this is actually a node
        if (isset($this_node->nid)) {
          // prepare the node for display
          $display_node = node_view($this_node, 'image_top_with_description');
          $themed_node = drupal_render($display_node);
          // set rendered node as new select option title
          $form['submitted']['options'][$i]['#title'] = $themed_node;
          // add flag to options array for future processing in weta_form_element
          $form['submitted']['options'][$i]['#form_type'] = 'bracket';
        }
      }
    }
  }

  // Print out the main part of the form.
  // Feel free to break this up and move the pieces within the array.
  print drupal_render($form['submitted']);

  print '<div class="clearfix"></div>';

  // Always print out the entire $form. This renders the remaining pieces of the
  // form that haven't yet been rendered above (buttons, hidden elements, etc).
  print drupal_render_children($form);
