<?php

/**
 * @file
 * Customize the display of Roosevelts Selfie Submissions
 *
 * Available variables:
 * - $node: The node object for this webform.
 * - $submission: The Webform submission array.
 * - $email: If sending this submission in an e-mail, the e-mail configuration
 *   options.
 * - $format: The format of the submission being printed, either "html" or
 *   "text".
 * - $renderable: The renderable submission array, used to print out individual
 *   parts of the submission, just like a $form array.
 */
?>

<?php
  $variables = array(
    'style_name' => 'merlin',
    'path' => $renderable['photo']['#value']->uri,
    'alt' => 'Image submitted by ' . $renderable['first_name']['#value'],
    'width' => '',
    'height' => '',
  );
  $image = theme_image_style($variables);
?>

<?php print $image; ?>
