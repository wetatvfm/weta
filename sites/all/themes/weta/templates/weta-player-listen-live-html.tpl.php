<?php
/**
 * @file
 * Template to render the weta streaming audio player page.
 */
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
    <title><?php print $title; ?></title>
    <?php print $includes; ?>

<?php
$base_url = $GLOBALS['base_url'] . base_path();
$args = arg();
$icon = 'classical_weta_homescreen_icon.png';
$sicon = 'classical_weta_shortcut_icon.png';
if (isset($args[1]) && $args[1] == 'viva-la-voce') {
  $icon = 'vlv_homescreen_icon.png';
  $sicon = 'vlv_homescreen_icon.png';
}
?>

<link rel="apple-touch-icon" href="<?php print $base_url; ?>sites/all/themes/weta/templates/images/<?php print $icon; ?>" />
<link rel="shortcut icon" sizes="196x196" href="<?php print $base_url; ?>sites/all/themes/weta/templates/images/<?php print $sicon; ?>">
  </head>
  <body class="player">
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-P633L6"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P633L6');</script>
<!-- End Google Tag Manager -->

    <div id="ll-player">
      <div class="p10 show-mobile mobile-player" id="jp-header">
        <div class="classical-weta"><a target="_blank" href="<?php print $classical_weta_link; ?>">Classical WETA</a></div>
        <div class="donate-now"><a target="_blank" href="<?php print $donate_now_link; ?>">Donate Now</a></div>
        <div class="clearfix"></div>
        <h1>Listen Live</h1>
			</div>
		<div class="span_12 col hide-mobile centered topbanner">
			<?php print $banner; ?>
			<div class="clearfix"></div>
		</div>
		<div class="ll-player-wrap">
		<div class="span_7 col">




    <div id="js-tabs" class="player-wrapper hide-player-mobile">

        <?php print $tabs; ?>
        <div id="weta-player"><?php print $content; ?>
          <div id="extra_text"><?php print $extra_text; ?>
          </div>
        </div>

    </div>
    <div class="clearfix"></div>
    </div>

    <div id="player-ad-square" class="span_5 col">
      <div class="ad300">
          <?php print $right_ad; ?>

      </div>
    </div>

</div>

    <div class="clearfix"></div>
    <div id="jp-footer" class="p10 hide-mobile">
      <div class="classical-weta"><a target="_blank" href="<?php print $classical_weta_link; ?>">Classical WETA</a></div>
      <div class="donate-now"><a target="_blank" href="<?php print $donate_now_link; ?>">Donate Now</a></div>
    </div>
</div>


  </body>
</html>
