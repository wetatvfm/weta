﻿<?php
header('Content-type: text/xml');  

include '../sites/all/modules/pbs_tvapi/pbs_tvapi.module';
require_once('ObjectToXML.php');  

$method = 'whatsOn/getAirdatesByFeeds';

$params = array(
  'startTime' => date('Y-m-d', strtotime('today')).'T06:00:00',
  'feedIDs' => '167,9854,11311,12435',
);

$scheduleitems = pbs_tvapi_call($method, $params);

// initialize the series array
$series = array();

foreach ($scheduleitems as $i => $episode) {
  $series[] = $episode->series_id;
}


$series = array_unique($series);

$seriesinfo = '';
$count = 0;
$xml = '';
foreach ($series as $i => $id) {
  if ($id > 0) {
    $method = 'programInfo/getSeriesData';
    $params = array (
      'seriesID' => $id,
    );
    $seriesitem = pbs_tvapi_call($method, $params, TRUE);
    //$seriesitem['series'] = $seriesitem[0];
    //array_shift($seriesitem);
    //$xml .= array2xml($seriesitem);    
    //print '<pre>'.print_r($seriesitem,1).'</pre>';
    if ($count == 0) {
      $seriesinfo = $seriesitem;
    } else {  
      $seriesinfo = array_merge((array) $seriesinfo, (array) $seriesitem);
    }
    $count++;

  }
}

//$newfeed = json_encode($seriesinfo);

//print '<pre>'.print_r($seriesinfo,1).'</pre>';  
//print $newfeed;
//return $newfeed;

try 
{
    $xml = new array2xml('my_node');
    $xml->createNode( $seriesinfo );
    echo $xml;
} 
catch (Exception $e) 
{
    echo $e->getMessage();
}
  

// output the XML  

//echo $xml; 
?>